package com.yetu.gateway.home.core.management.internal.exception;

public class NotFoundException extends Exception{

	public NotFoundException(String message, Throwable throwable){
		super(message,throwable);
	}
	
}
