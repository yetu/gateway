package com.yetu.gateway.home.core.management.internal.things;

import java.util.List;
import java.util.Map;

import org.eclipse.smarthome.core.thing.Thing;

import com.yetu.abstraction.model.ConnectionDetails;
import com.yetu.abstraction.model.ThingModelFactory;

public class ZWaveConnectionDetailsBuilder extends ConnectionDetailsBuilder {

	@Override
	public List<ConnectionDetails> generateConnectionDetails(Thing eshThing) {
		List<ConnectionDetails> conDetailsList = super.getDefaultConnectionDetails(eshThing);
		
		Map<String,String> properties = eshThing.getProperties();
		
		
		ConnectionDetails connDetails = ThingModelFactory.eINSTANCE.createConnectionDetails();
		connDetails.setName("Z-Wave");

		addProperty(connDetails,properties,"zwave_node_id");
		addProperty(connDetails,properties,"zwave_manufacturer_id");
		addProperty(connDetails,properties,"zwave_product_id");
		addProperty(connDetails,properties,"zwave_product_type_id");
	
		conDetailsList.add(connDetails);
		return conDetailsList;
	}
}
