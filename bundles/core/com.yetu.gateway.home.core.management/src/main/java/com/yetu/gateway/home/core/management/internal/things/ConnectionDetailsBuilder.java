package com.yetu.gateway.home.core.management.internal.things;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.smarthome.core.thing.Thing;

import com.yetu.abstraction.model.ConnectionDetails;
import com.yetu.abstraction.model.ThingModelFactory;

public abstract class ConnectionDetailsBuilder {
	
	public List<ConnectionDetails> getDefaultConnectionDetails(Thing eshThing) {
		List<ConnectionDetails> conDetailsList = new ArrayList<ConnectionDetails>();

		ConnectionDetails connDetails = ThingModelFactory.eINSTANCE.createConnectionDetails();
		connDetails.setName("ESH");
		connDetails.getProperties().add(ThingModelFactory.eINSTANCE.createAttribute("ThingUID", eshThing.getUID().getAsString()));
		connDetails.getProperties().add(ThingModelFactory.eINSTANCE.createAttribute("ThingTypeUID", eshThing.getThingTypeUID().getAsString()));
		
		if (eshThing.getBridgeUID() != null) {
			connDetails.getProperties().add(ThingModelFactory.eINSTANCE.createAttribute("BridgeUID", eshThing.getBridgeUID().getAsString()));
		}

		conDetailsList.add(connDetails);
		return conDetailsList;
	}

	abstract public List<ConnectionDetails> generateConnectionDetails(Thing eshThing);

	protected void addProperty(ConnectionDetails connDetails, Map<String,String> properties, String property){
		if (properties.containsKey(property)){
			connDetails.getProperties().add(ThingModelFactory.eINSTANCE.createAttribute(property, properties.get(property)));
		}
	}	
}
