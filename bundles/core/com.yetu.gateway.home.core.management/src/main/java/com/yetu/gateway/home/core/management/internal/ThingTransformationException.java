package com.yetu.gateway.home.core.management.internal;


public class ThingTransformationException extends Exception{
	public ThingTransformationException(String message){
		super(message);
	}
}
