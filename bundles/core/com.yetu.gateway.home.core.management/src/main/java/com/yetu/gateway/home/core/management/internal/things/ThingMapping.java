package com.yetu.gateway.home.core.management.internal.things;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.smarthome.core.items.GenericItem;
import org.eclipse.smarthome.core.items.Item;
import org.eclipse.smarthome.core.items.StateChangeListener;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.library.types.OpenClosedType;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.Thing;
import com.yetu.gateway.home.core.management.internal.exception.CapabilityNotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.ComponentNotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.InvalidPropertyValueException;
import com.yetu.gateway.home.core.management.internal.exception.NotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.PropertyNotFoundException;

public class ThingMapping {

	private static Logger logger = LoggerFactory.getLogger(ThingMapping.class);
	
	private Thing thing;
	private ThingUID eshThingUID;
	private Map<String , Capability> channelMapping;
	private Map<Capability, Channel> capabilityMapping;
	private Map<String, Capability> itemCapabilityMapping;
	private StateChangeListener itemStateChangeListener;		
	private List<ThingMappingListener> propertyValueChangeListeners;

	
	
	public ThingMapping(Thing thing, ThingUID uid){
		this.thing = thing;		
		this.eshThingUID = uid;
		channelMapping = new HashMap<>();			
		capabilityMapping = new HashMap<>();
		itemCapabilityMapping = new HashMap<>();
		propertyValueChangeListeners = new ArrayList<>();
		generateItemStateChangeListener();
	}
		
	
	
	public void addChannelCapabilityPair(final Channel channel, final Capability capability){		
		channelMapping.put(channel.getUID().toString(),capability);		
		capabilityMapping.put(capability, channel);
		for (Item item : channel.getLinkedItems()){
			registerItem(item, capability);
		}
	}
	
	protected void registerItem(Item item, Capability capability){
		itemCapabilityMapping.put(item.getName(),capability);
		if (item instanceof GenericItem){
			GenericItem genericItem = (GenericItem)item;
			genericItem.addStateChangeListener(itemStateChangeListener);
		}
	}
	
	public Channel getChannel(final Capability capability){
		return capabilityMapping.get(capability);
	}
	
	public Capability getCapability(final ChannelUID channelUid){
		return channelMapping.get(channelUid.toString());
	}
	
	public Thing getThing(){
		return thing;
	}
	
	public ThingUID getEshThingUID(){
		return eshThingUID;
	}
	
	
	private void generateItemStateChangeListener(){
		itemStateChangeListener = new StateChangeListener() {
			
			@Override
			public void stateUpdated(Item item, State state) {
				processItemStateUpdated(item, state);
			}
			
			@Override
			public void stateChanged(Item item, State oldState, State newState) {
				processItemStateChange(item, oldState, newState);
			}
		};
	}
	
	private void processItemStateUpdated(Item item, State state){
	//	processItemStateChange(item, null, state);
	}
	
	private void processItemStateChange(Item item, State oldState, State newState) {
		logger.debug("processing item state change for item {} (oldState = {} | newState = {})",item.getName(),oldState,newState);		
		Property property = getProperty(item);		
		if (property != null){		
			Object oldValue = property.getValue();			
			logger.debug("found Property '{}' which is linked to item (unit = {})",property.getName(),property.getUnit());
			Object newValue = generatePropertyValue(property, newState);
			property.setValue(newValue);
			dispatchPropertyChangeEvent(property, oldValue, newValue);
		} else {
			// this is not an exception at all. There might be items that do not belong to any properties.
			logger.warn("found no property that is linked to this item");
		}	
	}
	
	
	
	
	
	private Object generatePropertyValue(Property property, State state){
		
		
		if (state instanceof DecimalType){
			return ((DecimalType) state).toBigDecimal();
		} else if (state instanceof OnOffType){
			return new Boolean(((OnOffType)state) == OnOffType.ON);
		} else if (state instanceof OpenClosedType){
			return new Boolean(((OpenClosedType)state) == OpenClosedType.OPEN);
		} else if (state instanceof StringType){
			return new String(((StringType)state).toString());
		}
		
		return state.toString();
	}
	
	
	
	private Property getProperty(Item item){
		
		Property property = null;
		Capability capability = itemCapabilityMapping.get(item.getName());
		if (capability != null){
			if (capability.getProperties().size() == 1){
				property = capability.getProperties().get(0);
			}
			
		} else {
			// TODO: what now? a Item that is linked to a channel changed but somehow the item is not registered. 
			// Item could have been added after channel was added to this mapping?
			// not sure how to continue with this issue
			logger.error("unable to recieve property for given item '{}' linked with channel '{}'! ",item.getName());
		}
		
		
		
		return property;
	}
	
	public Property getProperty(String componentId, String capabilityId, String propertyName) throws NotFoundException{
		Component component = getThing().getComponent(componentId);
		if (component == null){
			throw new ComponentNotFoundException(getThing(), componentId);
		}
		Capability capability = component.getCapability(capabilityId);
		if (capability == null){
			throw new CapabilityNotFoundException(component, capabilityId);
		}
		for (Property property : capability.getProperties()){
			if (property.getName().equalsIgnoreCase(propertyName)){
				return property;
			}
		}
		throw new PropertyNotFoundException(capability, propertyName);
	}
	
	public void setProperty(String componentId, String capabilityId, String propertyName, String valueAsString) throws NotFoundException, InvalidPropertyValueException{
		Property property = getProperty(componentId, capabilityId, propertyName);		
		property.setValue(PropertyValueTransformer.generatePropertyValue(property,valueAsString));
				
	}
	
	private void dispatchThingStatusChangeEvent(com.yetu.abstraction.model.ThingStatus oldStatus, com.yetu.abstraction.model.ThingStatus newStatus){
		logger.debug("dispatching Thing Status Change Event for {}. Changed from {} to {}", oldStatus, newStatus);
		synchronized (propertyValueChangeListeners) {
			for (ThingMappingListener listener : propertyValueChangeListeners){
				logger.debug("informing {}",listener.getClass().getName());
				listener.onThingStateChange(this, oldStatus, newStatus);
			}
		}
	}
	
	private void dispatchPropertyChangeEvent(Property property, Object oldValue, Object newValue){
		logger.debug("dispatching Property Change Event");
		synchronized (propertyValueChangeListeners) {
			for (ThingMappingListener listener : propertyValueChangeListeners){
				logger.debug("informing {}",listener.getClass().getName());
				listener.onPropertyChange(this, property, oldValue, newValue);
			}
		}
	}
	
	public void addPropertyChangeListener(ThingMappingListener listener){
		synchronized(propertyValueChangeListeners){
			if (!propertyValueChangeListeners.contains(listener)){
				propertyValueChangeListeners.add(listener);
			}
		}
	}
	
	public void removePropertyChangeListener(ThingMappingListener listener){
		synchronized (propertyValueChangeListeners) {
			propertyValueChangeListeners.remove(listener);
		}
	}
	
	
	public void updateThingStatus(ThingStatus eshThingStatus){
		if (eshThingStatus == null) {
			logger.warn("Thing status is null");
			return;
		}
		
		com.yetu.abstraction.model.ThingStatus oldStatus = getThing().getStatus();
		switch (eshThingStatus){
		case ONLINE:
		case INITIALIZING:
		case REMOVING:
			if (getThing().getStatus() != com.yetu.abstraction.model.ThingStatus.ONLINE){				
				getThing().setStatus(com.yetu.abstraction.model.ThingStatus.ONLINE);
				dispatchThingStatusChangeEvent(oldStatus,getThing().getStatus());
			}
			break;
		case REMOVED:
		case OFFLINE:
			if (getThing().getStatus() != com.yetu.abstraction.model.ThingStatus.OFFLINE){				
				getThing().setStatus(com.yetu.abstraction.model.ThingStatus.OFFLINE);
				dispatchThingStatusChangeEvent(oldStatus,getThing().getStatus());
			}
			break;		
		case UNINITIALIZED:
		default :
			if (getThing().getStatus() != com.yetu.abstraction.model.ThingStatus.UNKNOWN){				
				getThing().setStatus(com.yetu.abstraction.model.ThingStatus.UNKNOWN);
				dispatchThingStatusChangeEvent(oldStatus,getThing().getStatus());
			}
		break;
		}
	}
	
}
