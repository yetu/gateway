package com.yetu.gateway.home.core.management.internal.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShellExecutor {
	public static final Logger logger = LoggerFactory.getLogger(ShellExecutor.class);
	private static ShellExecutor instance;
	
	public ShellExecutor() {
		
	}
	
	public static ShellExecutor getInstance() {
		if (instance == null) {
			instance = new ShellExecutor();
		}
		
		return instance;
	}
	
	public void performSync() throws IOException, InterruptedException{	
		logger.debug("Performing sync...");
		Process p = Runtime.getRuntime().exec("sync");	
		p.waitFor();		
	}
}
