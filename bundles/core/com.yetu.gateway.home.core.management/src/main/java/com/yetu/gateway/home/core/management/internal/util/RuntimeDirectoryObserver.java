package com.yetu.gateway.home.core.management.internal.util;

import java.io.File;
import java.io.IOException;

import org.eclipse.smarthome.config.core.ConfigConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for doing a sync whenever files have changed in the runtime directory
 * @author Sebastian Garn, Mathias Runge
 *
 */
public class RuntimeDirectoryObserver extends Thread {
	private static String EXCLUDED_DIRECTORY_NAME = "logs";
	
	private static Logger logger = LoggerFactory.getLogger(RuntimeDirectoryObserver.class);

	public static int DEFAULT_SCAN_DELAY = 5000;

	private String directory = ConfigConstants.DEFAULT_USERDATA_FOLDER;
	
	private boolean stop;
	
	private long previousLastModified;

	public RuntimeDirectoryObserver() {
		String progArg = System.getProperty(ConfigConstants.USERDATA_DIR_PROG_ARGUMENT);

        if (progArg != null) {
        	directory = progArg;
        }
        
        previousLastModified = -1;
        stop = false;
	}
	
	public void doStop(){
		stop = true;
	}
	
	public boolean isStopping(){
		return stop;
	}
	
	@Override
	public void run() {
		File dir = new File(directory);
		while(!isStopping()) {
			try {
				Thread.sleep(DEFAULT_SCAN_DELAY);
				
				
			} catch(InterruptedException e) {
				logger.debug("Interrupted RuntimeDirectoryObserver",e);
			}
			try {

				if (changesOccurred(dir)){
					logger.debug("Files in directory {} got modified",dir);
					executeSyncCmd();
				}
			} catch (Exception exc){
				logger.error("unable to check whether sync is needed or even unable to execute the sync.",exc);
			}
			
			
		}
	}
	
	private void executeSyncCmd() throws IOException, InterruptedException{
		ShellExecutor.getInstance().performSync();
	}
	
	private boolean changesOccurred(File file) {
		// set initial value
		if (previousLastModified == -1) {
			previousLastModified = getLastModified(file);
			return false;
		}
		
		long lastModified = getLastModified(file);
		boolean result =  previousLastModified < lastModified;
		previousLastModified = lastModified;
		return result;
	}
	
	private long getLastModified(File file){
		return getLastModified(file,file.lastModified());
	}
	
	private long getLastModified(File file, long currenModificationValue){
		long lastModified;
		
		if (file.isDirectory() && file.getName().compareTo(EXCLUDED_DIRECTORY_NAME) == 0) {
			return currenModificationValue;
		}
		
		if (file.lastModified() > currenModificationValue){
			lastModified = file.lastModified();
		} else {
			lastModified = currenModificationValue;
		}
		long tmp = lastModified;
		if (file.isDirectory()){
			for (File f : file.listFiles()){
				tmp = getLastModified(f, lastModified);
				if (tmp > lastModified){
					lastModified = tmp;
				}
			}
		}

		return lastModified;
	}
	
}