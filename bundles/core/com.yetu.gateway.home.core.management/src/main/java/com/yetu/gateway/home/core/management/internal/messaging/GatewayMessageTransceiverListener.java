package com.yetu.gateway.home.core.management.internal.messaging;

import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest.Entity;

public interface GatewayMessageTransceiverListener {
	
	public void onHandshakeCompleted();

	public void onStartDiscoverySessionRequest(String sessionId);
	public void onStopDiscoverySessionRequest(String sessionId);
	
	public void onSetPropertyCommand(String thingId, String componentId, String capabilityId, String propertyName, String value);
	
	public void onThingRemoveRequest(String thingId);
	
	public void onConfigurationRequest();
	
	public void onResetRequest(Entity[] entities);

}
