package com.yetu.gateway.home.core.management.internal.things;

import java.math.BigDecimal;
import java.util.StringTokenizer;

import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.HSBType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.library.types.PercentType;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.types.Command;

import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.ValueType;
import com.yetu.gateway.home.core.management.internal.exception.InvalidPropertyValueException;
import com.yetu.gateway.home.core.management.internal.exception.ValueTransformationException;

public class PropertyValueTransformer {

	public static Object generatePropertyValue(Property property, String valueAsString) throws InvalidPropertyValueException{
		ValueType valType = property.getUnit().getValueType();

		
		switch (valType){
		case BOOLEAN:			
			if ("true".equalsIgnoreCase(valueAsString) || 
					("ja").equalsIgnoreCase(valueAsString) ||
					("on").equalsIgnoreCase(valueAsString) ||
					("open").equalsIgnoreCase(valueAsString) || 
					("locked").equalsIgnoreCase(valueAsString) || 
					("1").equalsIgnoreCase(valueAsString) || 
					("yes").equalsIgnoreCase(valueAsString)){
				return new Boolean(true);
			} else {
				if ("false".equalsIgnoreCase(valueAsString) || 
						("nein").equalsIgnoreCase(valueAsString) ||
						("off").equalsIgnoreCase(valueAsString) ||
						("closed").equalsIgnoreCase(valueAsString) || 
						("unlocked").equalsIgnoreCase(valueAsString) || 
						("0").equalsIgnoreCase(valueAsString) || 
						("no").equalsIgnoreCase(valueAsString)){
					return new Boolean(false);
				}
			}			
		case NUMBER:
			try {
				if ( valueAsString.contains(".")){
					Double dval = Double.parseDouble(valueAsString);
					return BigDecimal.valueOf(dval);
				} else {
					return BigDecimal.valueOf(Long.parseLong(valueAsString));	
				}
			} catch (Exception exc){
				throw new InvalidPropertyValueException(property, valueAsString);
			}			
			
		case STRING:
			return new String(valueAsString);
		case UNDEFINED:
			throw new InvalidPropertyValueException(property,valueAsString,"ValueType "+valType.name()+" of property is not known by software.",null);
		default:
			throw new InvalidPropertyValueException(property,valueAsString);
		}	
	}
	
	public static Command generateCommand(Channel channel, Property property, String valueAsString) throws InvalidPropertyValueException, ValueTransformationException{
		Object obj = generatePropertyValue(property, valueAsString);
		
		
		if ("Switch".equalsIgnoreCase(channel.getAcceptedItemType())){
			if (obj instanceof Boolean){
				if ((Boolean)obj){
					return OnOffType.ON;
				} else {
					return OnOffType.OFF;
				}
			}
			if (obj instanceof BigDecimal){
				if (((BigDecimal)obj).intValue() != 0){
					return OnOffType.ON;
				} else {
					return OnOffType.OFF;
				}
			}
		} 
		if ("Rollershutter".equalsIgnoreCase(channel.getAcceptedItemType())){
			
		}
		if ("Contact".equalsIgnoreCase(channel.getAcceptedItemType())){
			if (obj instanceof Boolean){
				return OnOffType.valueOf(Boolean.toString((Boolean)obj));
			}
			if (obj instanceof BigDecimal){
				if (((BigDecimal)obj).intValue() != 0){
					return OnOffType.ON;
				} else {
					return OnOffType.OFF;
				}
			}
		}
		if ("Number".equalsIgnoreCase(channel.getAcceptedItemType())){
			if (obj instanceof BigDecimal){
				return (new DecimalType((BigDecimal)obj));
			}
			if (obj instanceof Boolean){
				if ((Boolean)obj){
					return (new DecimalType(BigDecimal.ONE));
				} else {
					return (new DecimalType(BigDecimal.ZERO));
				}
			}
		}
		if ("Dimmer".equalsIgnoreCase(channel.getAcceptedItemType())){
			if (obj instanceof BigDecimal){
				return (new DecimalType((BigDecimal)obj));
			}
			
		}
		if ("StringType".equalsIgnoreCase(channel.getAcceptedItemType())){
			return new StringType(valueAsString);
		}
		if ("DateTime".equalsIgnoreCase(channel.getAcceptedItemType())){
			
		}
		if ("Color".equalsIgnoreCase(channel.getAcceptedItemType())){
			if (obj instanceof String){
				if ("HSBCOLOR".equalsIgnoreCase(property.getUnit().getId())){
					return PropertyValueTransformer.convertColorToHSBType((String)obj);
				} else {
					return HSBType.valueOf((String)obj);
				}
			}
		}
		if ("Image".equalsIgnoreCase(channel.getAcceptedItemType())){
			
		}
		if ("Player".equalsIgnoreCase(channel.getAcceptedItemType())){
			
		}
		
		throw new InvalidPropertyValueException(property, valueAsString, "Accepted item type for channel "+channel.getUID()+" is "+channel.getAcceptedItemType(),null);
	}
	
	public static HSBType convertColorToHSBType(String color) throws ValueTransformationException{
		
		StringTokenizer tok = new StringTokenizer(color,";");
		if (tok.countTokens() != 3){
			throw new ValueTransformationException("The given string '"+color+"' is not a valid HSB description. Use <hue>;<saturation>;<brightness> to define a color!");
		}
		
		String hueStr = tok.nextToken();		
		String satStr = tok.nextToken();
		String brightStr = tok.nextToken();
		
		if (hueStr.contains(",")){
			hueStr = hueStr.replace(",",".");
		}		
		
		if (brightStr.contains(",")){
			brightStr = brightStr.replace(",",".");
		}
		
		if (satStr.contains(",")){
			satStr = satStr.replace(",",".");
		}
		
		try {
			BigDecimal hue = BigDecimal.valueOf(Double.parseDouble(hueStr));
			BigDecimal brightness = BigDecimal.valueOf(Double.parseDouble(brightStr));		
			BigDecimal saturation = BigDecimal.valueOf(Double.parseDouble(satStr));
			return new HSBType(new DecimalType(hue),new PercentType(saturation),new PercentType(brightness));
		} catch (NumberFormatException exc){
			throw new ValueTransformationException("Unable to transform given color '"+color+"' to HSBType",exc);
		}	
		
		
	}
}
