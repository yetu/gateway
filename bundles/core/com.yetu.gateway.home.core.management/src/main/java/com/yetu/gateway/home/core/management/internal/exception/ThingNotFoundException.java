package com.yetu.gateway.home.core.management.internal.exception;

public class ThingNotFoundException extends NotFoundException{
	
	public String thingId;
	
	public ThingNotFoundException(String thingId, String message){
		this(thingId,message,null);
	}
	
	public ThingNotFoundException(String thingId, String message, Throwable throwable){
		super(message, throwable);
		this.thingId = thingId;
	}
	
	public String getThingId(){
		return thingId;
	}
	
}
