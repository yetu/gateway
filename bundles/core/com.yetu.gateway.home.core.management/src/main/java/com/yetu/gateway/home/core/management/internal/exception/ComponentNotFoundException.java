package com.yetu.gateway.home.core.management.internal.exception;

import com.yetu.abstraction.model.Thing;

public class ComponentNotFoundException extends NotFoundException{

	private Thing thing;
	private String componentId;
	
	public ComponentNotFoundException(Thing thing, String componentId){
		this(thing,componentId,"Thing '"+thing.getId()+"' does not contain a component with id '"+componentId+"'",null);		
	}
	
	public ComponentNotFoundException(Thing thing, String componentId, String message, Throwable throwable){
		super(message, throwable);
		this.thing = thing;
		this.componentId = componentId;
	}
	
	public String getComonentId(){
		return componentId;
	}
	
	public Thing getThing(){
		return thing;
	}
	
}
