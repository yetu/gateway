package com.yetu.gateway.home.core.management.internal.exception;

import com.yetu.abstraction.model.Property;

public class InvalidPropertyValueException extends Exception {

	private Property property;
	private String value;
	
	public InvalidPropertyValueException(Property property, String value){
		this(property,value,"The given value '"+value+"' is not valid for ValueType '"+property.getUnit().getValueType().name()+"'!",null);
	}
	
	public InvalidPropertyValueException(Property property, String value, String message, Throwable throwable){
		super(message,throwable);
		this.property = property;
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
	
	public Property getProperty(){
		return property;
	}
}
