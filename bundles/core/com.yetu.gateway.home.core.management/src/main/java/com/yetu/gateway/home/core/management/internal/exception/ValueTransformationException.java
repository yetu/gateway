package com.yetu.gateway.home.core.management.internal.exception;

public class ValueTransformationException extends Exception{
	public ValueTransformationException(String message){
		this(message,null);
	}

	
	public ValueTransformationException(String message, Throwable throwable){
		super(message,throwable);
	}
}

