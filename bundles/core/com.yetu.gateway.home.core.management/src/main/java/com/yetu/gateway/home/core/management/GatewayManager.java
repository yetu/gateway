package com.yetu.gateway.home.core.management;


/**
 * 
 * In general, ESH and it's bindings will be managed by the user due to typing commands on the console or providing scripts. For our purposes, no user will ever manage
 * the smart home stack directly at the gateway. Thus, setting ESH into discovery mode, reporting about new Things, etc. will be done by a GatewayManager. 
 * 
 * @author Mathias Runge (initial Contribution)
 *
 */
public interface GatewayManager {

}
