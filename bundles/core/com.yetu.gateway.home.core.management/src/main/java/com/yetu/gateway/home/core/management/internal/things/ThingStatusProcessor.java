package com.yetu.gateway.home.core.management.internal.things;

import java.util.List;

import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThingStatusProcessor extends Thread {
	private static Logger logger = LoggerFactory.getLogger(ThingStatusProcessor.class);

	private ThingRegistry thingRegistry;
	private List<ThingMapping> thingMappings;
	private int pollingDelay;
	
	private boolean stop = false;
	
	public ThingStatusProcessor(int pollingDelay, ThingRegistry thingRegistry, List<ThingMapping> thingMappings) {
		this.pollingDelay = pollingDelay;
		this.thingRegistry = thingRegistry;
		this.thingMappings = thingMappings;
	}
	
	@Override
	public void run() {
		while (!stop) {
			synchronized(thingMappings) {
				for (ThingMapping currMapping : thingMappings) {
					// get esh thing to check its state
					Thing thing = thingRegistry.get(currMapping.getEshThingUID());
					
					if (thing == null) {
						//logger.error("Failed to retrieve thing");
						continue;
					}

					currMapping.updateThingStatus(thing.getStatus());
				}
			}

			try {
				Thread.sleep(pollingDelay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	public boolean isStopped() {
		return stop;
	}
	
	public void stopProcessor() {
		logger.debug("Stopping ThingStatus Processor");
		stop = true;
	}
}
