package com.yetu.gateway.home.core.management.internal.exception;

import com.yetu.abstraction.model.Component;

public class CapabilityNotFoundException extends NotFoundException{

	
	private Component component;
	private String capabilityId;
	
	public CapabilityNotFoundException(Component component, String capabilityId){
		this(component, capabilityId, "Component '"+component.getId()+"' of thing '"+component.getThing().getId()+
				"' does not contain capability '"+capabilityId+"'!",null);	
	}
	
	public CapabilityNotFoundException(Component component, String capabilityId, String message, Throwable throwable){
		super(message,throwable);
		this.component = component;
		this.capabilityId = capabilityId;
	}
	
	public Component getComponent(){
		return component;
	}
	
	public String getCapabilityId(){
		return capabilityId;
	}
	
}
