package com.yetu.gateway.home.core.management.internal.discovery;

public class DiscoverySessionException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6334886912388907908L;
	String sessionId;
	
	public DiscoverySessionException(String sessionId) {
		this(sessionId,null,null);
	}
	
	public DiscoverySessionException(String sessionId, String message, Throwable throwable) {
		super(message,throwable);
		this.sessionId = sessionId;
	}	
	
	public String getSessionId(){
		return sessionId;
	}
	
}
