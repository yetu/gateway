package com.yetu.gateway.home.core.management.internal.things;

import java.util.List;

import org.eclipse.smarthome.core.thing.Thing;

import com.yetu.abstraction.model.ConnectionDetails;

public class ConnectionDetailsFactory {
	
	public static List<ConnectionDetails> getConnectionDetails(Thing eshThing){
		String bindingId = eshThing.getThingTypeUID().getBindingId();
		
		ConnectionDetailsBuilder builder = null;
		if ("yetuzwave".equalsIgnoreCase(bindingId)){
			builder = new ZWaveConnectionDetailsBuilder();
		}
		else {
			builder = new DefaultConnectionDetailsBuilder();
		}

		return builder.generateConnectionDetails(eshThing);
	}
}
