package com.yetu.gateway.home.core.management.internal.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.Thing;

import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.ComponentType;
import com.yetu.abstraction.model.Unit;

public class AbstractionTag {
	
	public final static String ABSTRACTION_IDENTIFIER = "#abstraction:";
	
	public final static String COMPONENT_ID 	 = "comp_id";
	public final static String COMPONENT_TYPE 	 = "comp_type";
	//public final static String MAIN_COMPONENT_ID  = "main_comp";
	public final static String CAPABILITY_ID 	 = "cap_id";
	public final static String PROPERTY_NAME 	 = "prop_name";
	public final static String PROPERTY_UNIT 	 = "prop_unit";
	//public final static String THING_NAME		 = "thing_name";
	public final static String COMPONENT_NAME	 = "comp_name";
	
	
	HashMap<String, String> attributeValues;

	
	public AbstractionTag(String tag){
		
		attributeValues = new HashMap<>();
		
		if (tag == null){
			return;
		}
		
		if (!tag.startsWith(ABSTRACTION_IDENTIFIER)){
			return;
		}
		String abstraction = tag.substring(ABSTRACTION_IDENTIFIER.length());
		
		StringTokenizer tok = new StringTokenizer(abstraction,";");
		
		if (tok.countTokens() != 0){	
			while (tok.hasMoreTokens()){
				setAttributeValuePair(tok.nextToken());
			}
		}
	}
	
	protected void setAttributeValuePair(String str){
		if (str == null){
			return;
		}
		
		if (!str.contains("=")){
			return;
		}
		
		
		String key = str.substring(0,str.indexOf("="));
		String value = str.substring(str.indexOf("=")+1,str.length());
		attributeValues.put(key, value);
		
		
	}
	
	public AbstractionTag(String comp_id, ComponentType comp_type, Capability cap_id, Unit prop_unit){
		attributeValues.put(COMPONENT_ID, comp_id);
		if (comp_type != null){
			attributeValues.put(COMPONENT_TYPE, comp_type.getId());
		}
		if (cap_id != null){
			attributeValues.put(CAPABILITY_ID, cap_id.getId());
		}
		if (prop_unit != null){
			attributeValues.put(PROPERTY_UNIT, prop_unit.getId());
		}
	}
	
	public String getAttribute(String attr_id){
		return attributeValues.get(attr_id);
	}
	
	public String getComponentTypeId(){
		return attributeValues.get(COMPONENT_TYPE);
	}

	public String getCapabilityId(){
		return attributeValues.get(CAPABILITY_ID);
	}
	
	public String toString(){
		return "";
	}
	
	public String getComponentId(){
		return attributeValues.get(COMPONENT_ID);
	}
	
	public boolean hasUnitId(){
		return attributeValues.get(PROPERTY_UNIT) != null;
	}
	
	public String getUnitId(){
		return attributeValues.get(PROPERTY_UNIT);
	}
	
	public static boolean isAbstractionTag(String str){
		return (str.startsWith(ABSTRACTION_IDENTIFIER));
	}
	
	public static boolean hasThingAbstractionTags(Thing thing){
		for (Channel channel : thing.getChannels()){
			for (String str : channel.getDefaultTags()){
				if (AbstractionTag.isAbstractionTag(str)){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isValid(){
		if (getComponentId() == null){
			return false;
		}
		if (getComponentTypeId() == null){
			return false;
		}
		if (getCapabilityId() == null){
			return false;
		}
		
		return true;
	}
	
	public List<String> getMissingAttributes(){
		List<String> missingAttributes = new ArrayList<>();
		
		if (getComponentId() == null){
			missingAttributes.add(COMPONENT_ID);
		}
		if (getComponentTypeId() == null){
			missingAttributes.add(COMPONENT_TYPE);
		}
		if (getCapabilityId() == null){
			missingAttributes.add(CAPABILITY_ID);
		}
		
		return missingAttributes;
	}
}
