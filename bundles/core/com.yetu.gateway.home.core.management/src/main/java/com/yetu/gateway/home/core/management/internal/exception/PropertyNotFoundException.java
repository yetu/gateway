package com.yetu.gateway.home.core.management.internal.exception;

import com.yetu.abstraction.model.Capability;

public class PropertyNotFoundException extends NotFoundException {

	private Capability capability;
	private String propertyName;
	
	public PropertyNotFoundException(Capability capability, String propertyName){
		this(capability,propertyName,"capability '"+capability.getId()+"' does not contain a property with name '"+propertyName+"'!",null);
	}
	
	public PropertyNotFoundException(Capability capability, String propertyName, String message, Throwable throwable){
		super(message,throwable);
		this.capability = capability;
		this.propertyName = propertyName;
	}
	
	public Capability getCapability(){
		return capability;
	}
	
	public String getPropertyName(){
		return propertyName;
	}
	
}
