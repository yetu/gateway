package com.yetu.gateway.home.core.management.internal;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import com.yetu.abstraction.spec.ThingSpecification;
import com.yetu.abstraction.spec.impl.ThingSpecPackageImpl;



public class YetuThingSpecificationLoader {
	
	
	public static ThingSpecification load(String filename) throws IOException{
		ThingSpecPackageImpl.init();
		Resource resource;
		
	    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

	    Map<String, Object> m = reg.getExtensionToFactoryMap();

	    m.put("xmi", new XMIResourceFactoryImpl());
	    ResourceSet resSet = new ResourceSetImpl();

	    try {
	    	resource = resSet.getResource(URI.createURI(filename), true);
	    	resource.load(Collections.EMPTY_MAP);
  
	    	EObject root = resource.getContents().get(0);
  			return (ThingSpecification)root;
	    } catch (Exception e) {
	      throw new IOException(e);
	    }
	}
}
