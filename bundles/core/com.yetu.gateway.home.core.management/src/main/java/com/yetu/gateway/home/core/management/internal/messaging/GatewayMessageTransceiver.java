package com.yetu.gateway.home.core.management.internal.messaging;

import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.abstraction.model.Thing;
import com.yetu.abstraction.model.ThingStatus;
import com.yetu.gateway.home.core.messaging.MessageTransport;
import com.yetu.gateway.home.core.messaging.types.Channel;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest.Entity;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ConfigurationReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionFinished;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.PropertyChanged;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ResetReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StopDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAlreadyAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingDiscovered;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoveFailure;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoved;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingStatusChanged;

public class GatewayMessageTransceiver {
	protected static Logger logger = LoggerFactory.getLogger(GatewayMessageTransceiver.class);
	
	protected MessageTransport messageTransport = null;
	protected GatewayMessageTransceiverListener listener;
	
	protected MessageReceiver messageReceiver = null;
	protected boolean active = false;
	
	public GatewayMessageTransceiver(MessageTransport messageTransport, GatewayMessageTransceiverListener listener){
		this.messageTransport = messageTransport;		
		this.listener = listener;
	}
	
	public void activate(){
		if (isActive()) {
			logger.warn("Activate() called, but MessageTransceiver is already active.");
			return;
		}
		logger.debug("Message Transceiver will be activated now");		
		messageReceiver = new MessageReceiver(messageTransport.getIncoming());
		messageReceiver.setName("Message Receiver");
		messageReceiver.start();
		active = true;
	}
	
	public void deactivate(){
		this.messageTransport.getIncoming().setAvailable(false);
		if (messageReceiver != null){		
			messageReceiver.interrupt();
			messageReceiver = null;
		}
		active = false;
	}
	
	protected boolean isActive(){
		return active;
	}
			
	
	protected void onMessageReceived(IncomingMessage inMessage){
	
		if (inMessage instanceof HandshakeReply){
			logger.debug("Handshake completed");
			listener.onHandshakeCompleted();
		} else if (inMessage instanceof StartDiscoverySessionRequest) {
			logger.debug("Start discovery session request received");
			String sessionId = ((StartDiscoverySessionRequest)inMessage).getSessionId();
			String requestId = ((StartDiscoverySessionRequest)inMessage).getRequestId();
			logger.debug("sending DiscoverySessionReply. Session ID: {}. Request ID: {}",sessionId, requestId);
			sendStartDiscoverySessionReply(requestId,sessionId);			
			listener.onStartDiscoverySessionRequest(sessionId);			
		} else if (inMessage instanceof StopDiscoverySessionRequest){
			logger.debug("Stop discovery session request received");
			String sessionId = ((StopDiscoverySessionRequest)inMessage).getSessionId();
			String requestId = ((StopDiscoverySessionRequest)inMessage).getRequestId();
			logger.debug("sending StopDiscoverySessionReply. Session ID: {}. Request ID: {}",sessionId, requestId);
			sendStopDiscoverySessionReply(requestId,sessionId);
			listener.onStopDiscoverySessionRequest(sessionId);
		} else if (inMessage instanceof SetProperty){
			SetProperty setPropCmd = (SetProperty)inMessage;
			logger.debug("setProperty received: thingId={}, comp={}, cap={}, prop={}, value={}",
												setPropCmd.getThingId(),
												           setPropCmd.getComponentId(),
												           			setPropCmd.getCapabilityId(), 
												           					setPropCmd.getPropertyName(), 
												           							setPropCmd.getValue());
			listener.onSetPropertyCommand(setPropCmd.getThingId(), setPropCmd.getComponentId(), 
					setPropCmd.getCapabilityId(),setPropCmd.getPropertyName(),	setPropCmd.getValue());
		} else if (inMessage instanceof RemoveThingRequest){
			RemoveThingRequest request = (RemoveThingRequest)inMessage;
			String requestId = request.getRequestId();
			String thingId = request.getThingId();
			sendThingRemoveReply(requestId);
			listener.onThingRemoveRequest(thingId);
		} else if (inMessage instanceof ConfigurationRequest){
			listener.onConfigurationRequest();
		} else if (inMessage instanceof ResetRequest){
			String requestId = ((ResetRequest)inMessage).getRequestId();
			Entity[] entities = ((ResetRequest)inMessage).getEntities();
			listener.onResetRequest(entities);
			
			sendResetReply(requestId);
		}
		
	}
	
	
	
	public void sendConfigurationReply(Collection<Thing> things, String sessionId){
		ConfigurationReply msg = new ConfigurationReply(things, sessionId);
		sendMessage(msg);
	}
	
	public void sendSetPropertyError(SetPropertyError.ErrorCode errorCode, String cause, String thingId, String componentId,
			String capabilityId, String propertyName, String value){
		SetPropertyError msg = new SetPropertyError(errorCode, cause, thingId, componentId, capabilityId, propertyName, value);
		sendMessage(msg);
	}
	
	public void sendThingStatusChanged(String thingId, ThingStatus oldStatus, ThingStatus newStatus){
		ThingStatusChanged msg = new ThingStatusChanged(thingId, newStatus);
		sendMessage(msg);
	}
	
	public void sendPropertyChanged(String thingId, String componentId, String capabilityId, String propertyName, String propertyValue, Date changeTime){
		
		PropertyChanged msg = new PropertyChanged(thingId, componentId, capabilityId, propertyName, changeTime, propertyValue);
		sendMessage(msg);
	}
	
	public void sendThingRemoveFailure(String thingId, String message, ThingRemoveFailure.ErrorCode errorCode){
		ThingRemoveFailure msg = new ThingRemoveFailure(thingId, message, errorCode);
		sendMessage(msg);
	}
	
	public void sendThingRemoved(String thingId){
		ThingRemoved msg = new ThingRemoved(thingId);
		sendMessage(msg);
	}
	
	public void sendThingAdded(String sessionId, Thing thing){
		ThingAdded msg = new ThingAdded(thing, sessionId);
		sendMessage(msg);
	}
	
	public void sendThingAlreadyAdded(String thingId){
		ThingAlreadyAdded msg = new ThingAlreadyAdded(thingId);
		sendMessage(msg);
	}
	
	public void sendThingDiscovered(String sessionId, String thingId){
		logger.debug("sending ThingDiscovered with sessionid = {} and thingId = {}.",""+sessionId, thingId);
		ThingDiscovered msg = new ThingDiscovered(sessionId, thingId);
		sendMessage(msg);
	}
	
	 public void sendDiscoverySessionError(String sessionId, String message, int errorCode){	
		DiscoverySessionError msg = new DiscoverySessionError(sessionId, message, errorCode);
		sendMessage(msg);
	}

	public void sendDiscoverySessionFinished(String sessionId){
		DiscoverySessionFinished msg = new DiscoverySessionFinished(sessionId);
		sendMessage(msg);
	}
	
	public void sendDiscoverySessionStarted(String sessionId){
		OutgoingMessage msg = new DiscoverySessionStarted(sessionId);
		sendMessage(msg);
	}
	
	public void sendThingRemoveReply(String requestId){
		RemoveThingReply msg = new RemoveThingReply(requestId);
		sendMessage(msg);
	}
	
	public void sendStartDiscoverySessionReply(String requestId, String sessionId){
		OutgoingMessage msg = new StartDiscoverySessionReply(requestId, sessionId);
		sendMessage(msg);
	}
	
	public void sendStopDiscoverySessionReply(String requestId, String sessionId){
		OutgoingMessage msg = new StopDiscoverySessionReply(requestId, sessionId);
		sendMessage(msg);
	}
	
	public void sendResetReply(String requestId){
		OutgoingMessage msg = new ResetReply(requestId);
		sendMessage(msg);
	}
	
	public void sendHandshake(String gatewayId, String gatewayImageVersion){
		logger.debug("Send handshake: "+gatewayId+" with gateway image version "+gatewayImageVersion);
		sendMessage(new HandshakeRequest(gatewayId,gatewayImageVersion));
	}
	
	protected synchronized void sendMessage(OutgoingMessage msg){	
		logger.debug("sending message {}",msg.getClass());
		messageTransport.getOutgoing().sendMessage(msg);
	}
	
	protected class MessageReceiver extends Thread{
		
		Channel<IncomingMessage> inChannel = null;
	
		public MessageReceiver(Channel<IncomingMessage> inChannel){
			this.inChannel = inChannel;
		}

		public void run(){
			IncomingMessage msg;
			inChannel.setAvailable(true);
			while(!isInterrupted()){				
				 try {					 
					msg = inChannel.getNextMessage();
					onMessageReceived(msg);
				} catch (InterruptedException e) {
					logger.debug("{} interrupted",this.getName());					
				}				 
			}		
			inChannel.setAvailable(false);
		}		
	}
	
}
