package com.yetu.gateway.home.core.management;

public class ManagementBundleConstants {
	public static final String CONF_KEY_GATEWAY_ID = "gatewayid";
	public static final String CONF_KEY_THINGSPEC_FILENAME = "thingspec_filename";
	public static final String CONF_KEY_LSBRELEASE_FILENAME = "lsbrelease_filename";

	public static final int DEFAULT_THING_STATUS_POLL_INTERVAL = 2000;
}
