package com.yetu.gateway.home.core.management.internal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.config.discovery.DiscoveryListener;
import org.eclipse.smarthome.config.discovery.DiscoveryResult;
import org.eclipse.smarthome.config.discovery.DiscoveryResultFlag;
import org.eclipse.smarthome.config.discovery.DiscoveryService;
import org.eclipse.smarthome.config.discovery.DiscoveryServiceRegistry;
import org.eclipse.smarthome.config.discovery.inbox.Inbox;
import org.eclipse.smarthome.config.discovery.inbox.InboxListener;
import org.eclipse.smarthome.core.dynamic.thing.DynamicThingItemsProvider;
import org.eclipse.smarthome.core.storage.StorageService;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ManagedThingProvider;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingRegistry;
import org.eclipse.smarthome.core.thing.ThingRegistryChangeListener;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.thing.setup.ThingSetupManager;
import org.eclipse.smarthome.core.thing.type.ThingType;
import org.eclipse.smarthome.core.thing.type.ThingTypeRegistry;
import org.eclipse.smarthome.core.types.Command;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.ThingStatus;
import com.yetu.abstraction.model.Unit;
import com.yetu.abstraction.spec.ThingSpecification;
import com.yetu.gateway.home.core.management.GatewayManager;
import com.yetu.gateway.home.core.management.ManagementBundleConstants;
import com.yetu.gateway.home.core.management.internal.discovery.DiscoveryProcessor;
import com.yetu.gateway.home.core.management.internal.discovery.DiscoveryProcessorListener;
import com.yetu.gateway.home.core.management.internal.discovery.DiscoverySessionException;
import com.yetu.gateway.home.core.management.internal.exception.CapabilityNotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.ComponentNotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.InvalidPropertyValueException;
import com.yetu.gateway.home.core.management.internal.exception.NotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.PropertyNotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.ThingNotFoundException;
import com.yetu.gateway.home.core.management.internal.exception.ValueTransformationException;
import com.yetu.gateway.home.core.management.internal.messaging.GatewayMessageTransceiver;
import com.yetu.gateway.home.core.management.internal.messaging.GatewayMessageTransceiverListener;
import com.yetu.gateway.home.core.management.internal.things.PropertyValueTransformer;
import com.yetu.gateway.home.core.management.internal.things.ThingMapping;
import com.yetu.gateway.home.core.management.internal.things.ThingMappingListener;
import com.yetu.gateway.home.core.management.internal.things.ThingStatusProcessor;
import com.yetu.gateway.home.core.management.internal.util.AbstractionTag;
import com.yetu.gateway.home.core.management.internal.util.RuntimeDirectoryObserver;
import com.yetu.gateway.home.core.messaging.MessageTransport;
import com.yetu.gateway.home.core.messaging.types.ChannelListener;
import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException;
import com.yetu.gateway.home.core.messaging.types.errorhandling.MessageReplyTimeout;
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest.Entity;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoveFailure;
import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.RemoteManagementBundleConstants;
import com.yetu.gateway.home.core.monitoring.model.DeviceProperty;
import com.yetu.gateway.home.core.utils.ConfigParser;

public class GatewayManagerImpl implements GatewayManager, ManagedService, GatewayMessageTransceiverListener {
	public static ThingUID EMPTY_THING_UID = new ThingUID("_:_:_");
	
	private static Logger logger = LoggerFactory.getLogger(GatewayManagerImpl.class);

	// Thing Handling
	protected ThingRegistry thingRegistry = null;
	protected ThingRegistryChangeListener thingRegistryListener = null;
	protected ThingSpecification thingSpecification = null;
	protected YetuThingFactory yetuThingFactory = null;		// do not use directly, make use of getYetuFactory() instead!!!
	protected ThingSetupManager thingSetupManager = null;
	protected DynamicThingItemsProvider thingItemProvider = null;
	protected ManagedThingProvider managedThingProvider = null;
	protected ThingTypeRegistry thingTypeRegistry = null;
	protected Inbox inbox = null;
	protected InboxListener inboxListener = null;
		
	protected List<ThingMapping> thingMappings = null;
	protected ThingMappingListener propertyValueChangeListener = null;
	
	// Discovery Service Handling
	protected DiscoveryServiceRegistry discoveryServiceRegistry = null;
	protected DiscoveryListener	discoveryServiceListener = null;
	protected DiscoveryProcessor discoveryProcessor = null;
	
	// Message Handling
	protected MessageTransport messageTransport = null;
	protected ChannelListener<OutgoingMessage> messageTransportChannelListener = null;
	protected GatewayMessageTransceiver messageTransceiver = null;
	
	// Remote Management
	protected RemoteGatewayManagementAccessor remoteGatewayManagementAccessor = null;
	
	// Persistence
	protected StorageService storageService = null;
	
	// operational
	protected boolean activationWanted = false;
	protected boolean activated = false;
	protected boolean communicationUp = false;
	protected Dictionary<String,?> properties = null;
	protected String currentSession = null;
	
	// Properties
	protected String cfgGatewayId;
	protected String lsbReleaseFile;
	protected String gatewayImageVersion;
	
	protected ThingUID thingAddedDuringDiscoverySession;
	
	private ThingStatusProcessor thingStatusProcessor;
	private RuntimeDirectoryObserver runtimeDirectoryObserver;
	
	// ++++++++++++++ ACTIVATION  /  DEACTIVATION ++++++++++++++++++
	
	public GatewayManagerImpl(){	
		this.thingMappings = new ArrayList<>();
		setUncaughtExceptionHandler();
	}
	
	public void onActivated() {
		logger.debug("onActivated called");
		activationWanted = true;
		activateIfPossible();
	}
	
	public void onDeactivated() {		
		logger.debug("onDeactivated called");
		activationWanted = false;
		deactivate();
	}
			
	protected void activateIfPossible(){
		if (!activationWanted){
			return;
		}
		boolean readyForActivation = true;
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("\nTrying to activate GatwayManager:");
		strBuff.append("\n-----------------------------------");
		
		
		
		if (inbox == null){
			strBuff.append("\n   Inbox.......................NULL");
			readyForActivation = false;			
		} else {
			strBuff.append("\n   Inbox.......................OK");
		}
		
		if (this.thingRegistry == null){
			strBuff.append("\n   ThingRegistry...............NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   ThingRegistry...............OK");
		}
		if (this.thingTypeRegistry == null){
			strBuff.append("\n   ThingTypeRegistry...........NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   ThingTypeRegistry...........OK");
		}
		if (thingSetupManager == null){
			strBuff.append("\n   ThingsSetupManager..........NULL");
			readyForActivation = false;			
		} else {
			strBuff.append("\n   ThingsSetupManager..........OK");
		}
		if (managedThingProvider == null){
			strBuff.append("\n   ManagedThingProvider........NULL");
			readyForActivation = false;			
		} else {
			strBuff.append("\n   ManagedThingProvider........OK");
		}
		if (this.discoveryServiceRegistry == null){
			strBuff.append("\n   DiscoveryServiceRegistry....NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   DiscoveryServiceRegistry....OK");
		}
		
		if (this.messageTransport == null){
			strBuff.append("\n   MessageTransport............NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   MessageTransport............OK");
		}
		
		if (this.properties == null){
			
			strBuff.append("\n   Configuration...............NULL");
			readyForActivation = false;
		} else {
			if (this.cfgGatewayId == null || this.lsbReleaseFile == null){
				strBuff.append("\n   Configuration...............FAIL");
				readyForActivation = false;
			} else {
				strBuff.append("\n   Configuration...............OK");
			}
		}
		if (this.storageService == null){
			strBuff.append("\n   StorageService..............NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   StorageService..............OK");
		}
		if (this.thingSpecification == null){
			strBuff.append("\n   ThingSpecification..........NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   ThingSpecification..........OK");
		}
		
		if (this.thingItemProvider == null){
			strBuff.append("\n   ThingItemProvider...........NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   ThingItemProvider...........OK");
		}
		
		if (this.remoteGatewayManagementAccessor == null){
			strBuff.append("\n   RemoteGatewayManagement...NULL");
			readyForActivation = false;
		} else {
			strBuff.append("\n   RemoteGatewayManagement.....OK");
		}
		
		strBuff.append("\n-----------------------------------"); 
		if (readyForActivation){
			activated = true;
			strBuff.append("\nGatewayManager.................OK");  
		} else {
			activated = false;
			strBuff.append("\nGatewayManager.................FAIL");  
		}
		logger.debug(strBuff.toString());
		
		if (activated){
			logger.debug("Gateway is activated");
			if (messageTransceiver != null){
				messageTransceiver.activate();
			}
			
			generateThingAbstractionMappings();
			
			this.gatewayImageVersion = retrieveGatewayImageVersion(lsbReleaseFile);
			logger.debug("Gateway image version is: "+gatewayImageVersion);

			remoteGatewayManagementAccessor.sendDeviceProperty(
					new DeviceProperty(RemoteManagementBundleConstants.DEVICE_PROPERTY_IMAGE_VERSION, this.gatewayImageVersion)
			);
			
			if (communicationUp){
				messageTransceiver.sendHandshake(this.cfgGatewayId, this.gatewayImageVersion);
			} // handshake will be sended when activated and communication is up, see onCommunicationUp()
			
			// observe thing mappings for status changes
			setupThingStatusProcessor();
			setupRuntimeDirectoryObserver();
		}	
	}
	
	protected void deactivate(){
		logger.debug("Gateway is deactivated");
		if (messageTransceiver != null){
			messageTransceiver.deactivate();
		}
		
		// TODO
		
		activated = false;
	}
	// -------------------- ACTIVATION  /  DEACTIVATION --------------------
	// ++++++++++++++++++++++++ Operational ++++++++++++++++++++++++++++++++
	
	
	private void setUncaughtExceptionHandler(){
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread t, Throwable e) {
				logger.error("Uncaught Exception was thrown for Thread {}",t.getName(),e);
				
			}
		});		
	}
	
	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
		logger.debug("Configuration received");
		
		ConfigParser cfg = new ConfigParser(properties);
		String thingSpecficationPath = null;
		
		try {
			cfgGatewayId = cfg.getStringValue(ManagementBundleConstants.CONF_KEY_GATEWAY_ID);
			thingSpecficationPath = cfg.getStringValue(ManagementBundleConstants.CONF_KEY_THINGSPEC_FILENAME);
			lsbReleaseFile = cfg.getStringValue(ManagementBundleConstants.CONF_KEY_LSBRELEASE_FILENAME);
			
		} catch(Exception e) {
			logger.error("Failed to parse config",e);
		}
		
		logger.debug("Gatewayid is: "+cfgGatewayId);
		logger.debug("ThingSpecification filename is {}",thingSpecficationPath);
		logger.debug("Lsb-release filename is: "+lsbReleaseFile);
		loadSpecification(thingSpecficationPath);
		this.properties = properties;
		activateIfPossible();
	}
	
	protected void loadSpecification(String filename){
		logger.debug("Loading ThingSpecification {}",filename);
		try {
			this.thingSpecification = YetuThingSpecificationLoader.load(filename);
		} catch (IOException ioExc){
			logger.error("unable to load ThingSpecification {}",filename,ioExc);
			thingSpecification = null;
		}		
		
	}
	
	// ++++++++++++++++++++++++++ Item Handling +++++++++++++++++++++++++++++++++++++
	
	
	// -------------------------- Item Handling ------------------------------------
	// ++++++++++++++++++++++++++ INBOX HANDLING +++++++++++++++++++++++++++++++++++
	
	public void setInbox(Inbox inbox){
		logger.debug("Inbox assigned");
		
	
		this.inbox = inbox;
		
		if (inboxListener == null){
			inboxListener = new InboxListener() {
				
				@Override
				public void thingUpdated(Inbox source, DiscoveryResult result) {
					onThingUpdatedInInbox(source, result);					
				}
				
				@Override
				public void thingRemoved(Inbox source, DiscoveryResult result) {
					onThingRemovedFromInbox(source, result);
				}
				
				@Override
				public void thingAdded(Inbox source, DiscoveryResult result) {
					onThingAddedToInbox(source, result);					
				}
			};
		}
		inbox.addInboxListener(inboxListener);
		activateIfPossible();
	}
	
	public void unsetInbox(Inbox inbox){
		logger.debug("Inbox unassigned");
		inbox.removeInboxListener(inboxListener);
		this.inbox = null;
		deactivate();
	}
	
	protected void onThingAddedToInbox(Inbox source, DiscoveryResult result){
		
		logger.debug("new Thing added to inbox {}",result.getThingUID());		
		
		if (!activated){
			logger.debug("GatewayManager is not active now, will do nothing here");
			return;
		}
		
		String thingId = getYetuThingFactory().getYetuThingId(result.getThingUID());
		if (!isThingMappingExisting(result.getThingUID())){
			// discovery session was started manually
			if (thingAddedDuringDiscoverySession == null) {
				thingAddedDuringDiscoverySession = EMPTY_THING_UID;
			}
			
			synchronized(thingAddedDuringDiscoverySession) {
				thingAddedDuringDiscoverySession = result.getThingUID();
			}

			logger.debug("new ESH-Thing with uid {} was discovered and will have yetu thing id {}",result.getThingUID().toString(),thingId);
			messageTransceiver.sendThingDiscovered(discoveryProcessor.getCurrentSessionId(),thingId);
			Map<String, Object> props = new HashMap<>(result.getProperties());
			Configuration conf = new Configuration(props);

			Thing resultingThing = thingSetupManager.addThing(result.getThingUID(), conf, result.getBridgeUID(), result.getLabel(),new ArrayList<String>(),false);
			
			// no binding found which can handle the new thing
			if (resultingThing == null) {
				logger.error("There exists no binding which can handle given thing. Stopping discovery session and sending error.");
				processDiscoverySessionError(discoveryProcessor.getCurrentSessionId(), "No binding was found which can handle the new thing", 0);
			}
		} else {
			logger.error("Invalid inbox entry. Thing was already added.");
			inbox.remove(result.getThingUID());
		}
		
	}	
	
	protected void onThingRemovedFromInbox(Inbox source, DiscoveryResult result){
		
	}
	
	protected void onThingUpdatedInInbox(Inbox source, DiscoveryResult result){
		if (result.getFlag().equals(DiscoveryResultFlag.NEW)) {
			onThingAddedToInbox(inbox, result);
		}
	}
	
	// --------------------------- INBOX HANDLING --------------------------------------
	
	// ++++++++++++++++++++++++++++ THING TYPE HANDLING +++++++++++++++++++++++++++++++++
	
	public void setThingTypeRegistry(ThingTypeRegistry thingTypeRegistry){
		logger.debug("ThingTypeRegistry assigned");
		this.thingTypeRegistry = thingTypeRegistry;
		activateIfPossible();
	}
	
	public void unsetThingTypeRegistry(ThingTypeRegistry thingTypeRegistry){
		this.thingTypeRegistry = null;
		deactivate();
	}
	
	
	// ---------------------------- THING TYPE HANDLING ---------------------------------
	
	// +++++++++++++++++++++++++++ THING HANDLING --------------------------------------
	
	public void setThingRegistry(ThingRegistry thingRegistry){
		
		logger.debug("ThingRegistry assigned");
		this.thingRegistry = thingRegistry;
		
		if (thingRegistryListener == null){
			thingRegistryListener = new ThingRegistryChangeListener() {
				
				@Override
				public void updated(Thing oldElement, Thing element) {
					onThingUpdatedOnRegistry(oldElement,element);				
				}
				
				@Override
				public void removed(Thing element) {
					onThingRemovedFromRegistry(element);				
				}
				
				@Override
				public void added(Thing element) {
					onThingAddedToRegistry(element);				
				}
			};		
		}
		
		thingRegistry.addRegistryChangeListener(thingRegistryListener);
		activateIfPossible();
		
	}
	
	protected void generateThingAbstractionMappings(){
		logger.debug("generating thing abstractions for thing in ThingRegistry");

		for (Thing thing : thingRegistry.getAll()){
			if (AbstractionTag.hasThingAbstractionTags(thing)){
				
				try {
					if (!areItemsAlreadyLinked(thing)){
						thingItemProvider.provideItems(thing);						
					}
					String name = generateThingName(thing);
					addThingMapping(getYetuThingFactory().createYetuThingAbstraction(thing,name));
								
				} catch (Exception exc){
					logger.error("unable to transform eshThing into yetuThing {}",thing.getUID(),exc);
					exc.printStackTrace();
				}
			}
		}

		printeMalAus();
	}
	
	private void setupThingStatusProcessor() {
		logger.debug("Setting up ThingStatusProcessor");
		
		if (thingStatusProcessor == null || thingStatusProcessor.isStopped()) {
			thingStatusProcessor = new ThingStatusProcessor(ManagementBundleConstants.DEFAULT_THING_STATUS_POLL_INTERVAL, thingRegistry, thingMappings);
			thingStatusProcessor.start();
		}
	}
	
	private void setupRuntimeDirectoryObserver() {
		logger.debug("Setting up RuntimeDirectoryObserver");
		
		if (runtimeDirectoryObserver == null || runtimeDirectoryObserver.isStopping()) {
			runtimeDirectoryObserver = new RuntimeDirectoryObserver();
			runtimeDirectoryObserver.start();
		}
	}
	
	protected void addThingMapping(ThingMapping thingMapping){
		logger.debug("registering ThingMapping for thing {}",thingMapping);
		if (thingMapping == null){
			
		}
		if (propertyValueChangeListener == null){
			propertyValueChangeListener = new ThingMappingListener() {
				
				@Override
				public void onPropertyChange(ThingMapping thingMapping, Property property,
						Object oldValue, Object newValue) {
					onThingPropertyChanged(thingMapping, property, oldValue, newValue);
					
				}

				

				@Override
				public void onThingStateChange(ThingMapping thingMapping,
						ThingStatus oldStatus, ThingStatus newStatus) {
					onThingStatusChanged(thingMapping,oldStatus,newStatus);
					
				}
			};
		}
		
		thingMapping.addPropertyChangeListener(propertyValueChangeListener);
		
		synchronized(thingMappings){
			thingMappings.add(thingMapping);
		}
		
	
		processThingAdded(thingMapping);	
		
	}
	
	protected boolean areItemsAlreadyLinked(Thing thing){
		
		for (Channel ch : thing.getChannels()){
			if (ch.getLinkedItems().isEmpty()){
				return false;
			}
		}
		
		return true;
	}
	
	// will be deleted very soon, just for fast testing
	protected void printeMalAus(){
		
		
		logger.debug("===================================================");
		logger.debug("        PRINT MAL AUS DEN THING SCHEISS");
		logger.debug("===================================================");
		
		for (ThingMapping thingMapping : thingMappings){
			logChannelMappings(thingMapping);
			logThingDetails(thingMapping.getThing());
			logger.debug("");
			logger.debug("___________________________________________________");
			logger.debug("");
		}
	}
	
	protected void logChannelMappings(ThingMapping thingMapping){
		Channel channel; // temporary use
		
		for (Component component : thingMapping.getThing().getComponents()){
			for (Capability capability : component.getCapabilities()){
				channel = thingMapping.getChannel(capability);
				
				logger.debug("   - Channel Mapping    {} <--> {}.{}.{}", channel.getUID(),thingMapping.getThing().getId(),component.getId(),capability.getId());
			}
		}
	}
	
	protected void logThingDetails(com.yetu.abstraction.model.Thing thing){
		logger.debug("     - name = {}",thing.getName());
		logger.debug("     -   id = {}",thing.getId());
		
		for (Component component : thing.getComponents()){
			logComponent(component);
		}
	}
	
	protected void logComponent(Component component){
		logger.debug("     Component ID : {}",component.getId());
		logger.debug("           - name : {}",component.getName());
		logger.debug("           - type : {}",component.getType().getId());
		for (Capability capability : component.getCapabilities()){
			logCapability(capability);
		}
	}
	
	
	protected void logCapability(Capability capability){
		logger.debug("           - Capability ID : {}",capability.getId());
		for (Property property : capability.getProperties()){
			logProperty(property);
		}
	}
	
	
	protected void logProperty(Property property){
		if (property == null){
			logger.debug("                  property : not initialized");
			return;
		}
		Unit unit = property.getUnit();
		if (unit == null){
			 logger.debug("                  property : name = '{}' unit = '{}'",property.getName(),"NO_UNIT");
		} else {
		    logger.debug("                  property : name = '{}' unit = '{}'",property.getName(),unit.getId());
		}
	}
	
	public void unsetThingRegistry(ThingRegistry thingRegistry){
		logger.debug("ThingRegistry unassigned");
		thingRegistry.removeRegistryChangeListener(thingRegistryListener);
		this.thingRegistry = null;
		deactivate();
	}
	
	protected void onThingAddedToRegistry(Thing eshThing){
		logger.debug("Thing {} added to registry",eshThing.getUID());
		
		if (!activated) {
			logger.debug("Bundle not activated now. Ignoring thing. Thing will be considered after bundle was properly initialized.");
			return; 
		}
		
		// generate Items for each channel
		if (!areItemsAlreadyLinked(eshThing)){
			try { 
				thingItemProvider.provideItems(eshThing);
			} catch (Exception exc){
				logger.error("unable to generate all items for thing {}",eshThing.getUID(),exc);
			}
		}
		
//		if (AbstractionTag.hasThingAbstractionTags(eshThing)){
			try {
				String thingName = generateThingName(eshThing);
				addThingMapping(getYetuThingFactory().createYetuThingAbstraction(eshThing,thingName));
//			
			} catch (Exception exc){
				logger.error("unable to transform eshThing {} into yetuThing",eshThing.getUID(),exc);
				exc.printStackTrace();
			}
//		}
	}
	
	protected void processThingAdded(ThingMapping thingMapping){				
		messageTransceiver.sendThingAdded(discoveryProcessor.getCurrentSessionId(),thingMapping.getThing());		
		if (discoveryProcessor != null){
			if (discoveryProcessor.getDiscoverySessionState() != DiscoveryProcessor.DPS_IDLE){
				discoveryProcessor.stopDiscoverySession();
			}
		}
		logger.debug("ThingMappingSize {}",thingMappings.size());
	}
	
	protected void onThingUpdatedOnRegistry(Thing oldThing, Thing newThing){
		logger.debug("OOOOOOOOOOOOOOOHHHHHH BOOOOOOOOOOOOOOOY -> thing updated old = {} new = }{}",oldThing,newThing);
	}
	
	protected void onThingRemovedFromRegistry(Thing thing){
		// TODO remove ThingMapping, send message
		logger.debug("Things was removed, deleting thingMapping and sending ThingRemoved message");
		ThingMapping thingMapping = getThingMapping(thing.getUID());
		if (thingMapping == null){
			// TODO: Handle that shit
			logger.warn("Thing was removed, but no ThingMapping was found for UID '{}'",thing.getUID());
			return;
		}
		yetuThingFactory.removeThingIdMapping(thing.getThingTypeUID());
		thingMappings.remove(thingMapping);
		messageTransceiver.sendThingRemoved(thingMapping.getThing().getId());
		// TODO: if thing is bridge, delete bridged things
		// TODO: if thing is involved in rules, deactivate or delete rules
		// TODO: if binding is always adding thing, store ESH-thing-id in removedList
		// TDOO: remove all linked Items when automated generated
	}
	
	
	protected void setThingSetupManager(ThingSetupManager thingSetupManager) {
	      this.thingSetupManager = thingSetupManager;
	      activateIfPossible();
	}

	protected void unsetThingSetupManager(ThingSetupManager thingSetupManager) {
	   this.thingSetupManager = null;
	}
	
	public void setThingItemsProvider(DynamicThingItemsProvider thingItemProvider){
		this.thingItemProvider = thingItemProvider;
		activateIfPossible();
	}
	
	public void unsetThingItemsProvider(DynamicThingItemsProvider thingItemsProvider){
		this.thingItemProvider = null;
	}
	
	public void removeThing(String thingId) throws ThingNotFoundException{
		ThingMapping thingMapping = getThingMapping(thingId);
		if (thingMapping == null){
			throw new ThingNotFoundException(thingId, "Could not remove Thing with ID '"+thingId+"'!");
		}	
		removeThing(thingMapping);
	}
	
	public void removeThing(ThingMapping thingMapping){		
		thingSetupManager.removeThing(thingMapping.getEshThingUID());		
	}
	
	public void setManagedThingProvider(ManagedThingProvider managedThingProvider){
		this.managedThingProvider = managedThingProvider;
		activateIfPossible();
	}
	
	public void unsetManagedThingProvider(ManagedThingProvider managedThingProvider){
		managedThingProvider = null;
		deactivate();
	}
	
	public void onThingPropertyChanged(ThingMapping thingMapping, Property property, Object oldValue, Object newValue){
		logger.debug("Property '{}' of thing '{}' changed from {} to {}",property.getCapability().getComponent().getId()+"."+property.getCapability().getId().toLowerCase()+"."+property.getName(), thingMapping.getThing().getId(), oldValue, newValue);
		
		messageTransceiver.sendPropertyChanged(thingMapping.getThing().getId(),
												property.getCapability().getComponent().getId(),
												property.getCapability().getId(),
												property.getName(), 
												property.getValue().toString(), 
												new Date());
		
	}
	
	public void onThingStatusChanged(ThingMapping thingMapping,ThingStatus oldStatus,ThingStatus newStatus){
		logger.debug("Status of thing '{}' changed from '{}' to '{}'.",thingMapping.getThing().getId(), oldStatus.name(), newStatus.name());
		messageTransceiver.sendThingStatusChanged(thingMapping.getThing().getId(), oldStatus, newStatus);
	}
	
	
	
	private YetuThingFactory getYetuThingFactory(){
		if (yetuThingFactory == null){
				yetuThingFactory = new YetuThingFactory(thingSpecification, storageService);
		}
		return yetuThingFactory;
				
	}
	
	
	private String generateThingName(Thing thing){
		String name = null;
		
		ThingType thingType = thingTypeRegistry.getThingType(thing.getThingTypeUID());
		
		if (thingType != null){
			name = thingType.getLabel();		
		}
		
		if (name == null){
			name = getThingNameFromProperties(thing);
		}
		
		if (name == null){
			name = thing.getUID().getThingTypeId()+" "+thing.getUID().getId();
		}
		
		// go through all yetu-things and make sure the name is unique
		return name;
	}
	
	private String getThingNameFromProperties(Thing thing){
		String name = thing.getProperties().get("abstraction_thing_name");
		if (name != null){
			return name;
		}
	 	name = "new Device";
	 	
		return name;
	}
	
	// --------------------------------- THING HANDLING --------------------------------
	
	// +++++++++++++++++++++++++++ DISCOVERY SERVICE HANDLING ++++++++++++++++++++++++++
	
	public void setDiscoveryServiceRegistry(DiscoveryServiceRegistry discoveryServiceRegistry){
		logger.debug("DiscoveryServiceRegistry assigned");
		
		this.discoveryServiceRegistry = discoveryServiceRegistry;	
		
		if (discoveryServiceListener == null){
			discoveryServiceListener = new DiscoveryListener() {
				
				@Override
				public void thingRemoved(DiscoveryService source, ThingUID thingUID) {
					onThingRemovedFromDiscovery(source, thingUID);
				}
				
				@Override
				public void thingDiscovered(DiscoveryService source, DiscoveryResult result) {
					onThingDiscovered(source, result);
				}

				@Override
				public Collection<ThingUID> removeOlderResults(
						DiscoveryService source, long timestamp,
						Collection<ThingTypeUID> thingTypeUIDs) {
					// TODO Auto-generated method stub
					return null;
				}
			};
		}
		
		this.discoveryProcessor = createDiscoveryProcessor();
		this.discoveryServiceRegistry.addDiscoveryListener(discoveryServiceListener);

		activateIfPossible();
	}
	
	protected DiscoveryProcessor createDiscoveryProcessor(){
		logger.debug("Create discovery processor called");
		
		DiscoveryProcessorListener listener = new DiscoveryProcessorListener() {			
			@Override
			public void onDiscoveryStarted(String sessionId) {
				processDiscoverySessionStarted(sessionId);
			}
			
			@Override
			public void onDiscoveryFinished(String sessionId) {
				processDiscoverySessionFinished(sessionId);
			}
			
			@Override
			public void onDiscoveryError(String sessionId, String message) {
				processDiscoverySessionError(sessionId, message, DiscoverySessionError.ERROR_DISCOVERY_SERVICE_ERROR);
			}
		};
		
		return new DiscoveryProcessor(discoveryServiceRegistry, listener);
	}
	
	protected void processDiscoverySessionStarted(String sessionId){
		messageTransceiver.sendDiscoverySessionStarted(sessionId);
	}
	
	protected void processDiscoverySessionFinished(String sessionId){
		messageTransceiver.sendDiscoverySessionFinished(sessionId);
	}
	
	protected void processDiscoverySessionError(String sessionId, String message, int errorCode){
		messageTransceiver.sendDiscoverySessionError(sessionId, message, errorCode);
		
		discoveryProcessor.stopDiscoverySession();		
		//discoveryProcessor.reset();
	}
	
	public void unsetDiscoveryServiceRegistry(DiscoveryServiceRegistry discoveryServiceRegistry){
		logger.debug("DiscoveryServiceRegistry unassigned");
		discoveryServiceRegistry.removeDiscoveryListener(discoveryServiceListener);
		this.discoveryServiceRegistry = null;
		deactivate();
	}
	
	protected void onThingDiscovered(DiscoveryService source, DiscoveryResult result){
		logger.debug("new Thing was discovered {} {}",result.getBindingId(),result.getThingUID());
		// Attention, everything that handles thing discovery should be placed at onThingAddedToInbox
		// except the case when a thing already exists because then onThingAddedToInbox will never be called
		// please note: this method gets called in parallel with onThingDiscovered in inbox
		
		if (!activated){
			logger.debug("GatewayManager is not active now, will do nothing here");
			return;
		}
		
		if (!discoveryProcessor.isDiscoverySessionRunning()) {
			logger.info("Ignoring ThingDiscovered event since no discovery session is running. Processing of new thing takes place as soon as it gets added to inbox.");
			return;
		}

		boolean thingAlreadyExists = getYetuThingFactory().isYetuThingIdExisting(result.getThingUID());
        
        // thing was never added before
        if (!thingAlreadyExists) {
        	logger.debug("Thing will be handled after inbox event");
        	return;
        }

	    synchronized(thingAddedDuringDiscoverySession) {
    		if (!result.getThingUID().equals(thingAddedDuringDiscoverySession)) {
    			ThingMapping mapping = getThingMapping(result.getThingUID());
    			logger.debug("Thing was already added before but not during this discovery session. ESH thing id: {}, yetu thing id: {}", result.getThingUID(), mapping.getThing().getId());
    			messageTransceiver.sendThingAlreadyAdded(mapping.getThing().getId());
    		}
    	}
	 
	}
	
	protected void onThingRemovedFromDiscovery(DiscoveryService source, ThingUID thingUID){
		
	}
	
	
	// --------------------------- DISCOVERY SERVICE HANDLING --------------------------------------------
	
	
	// ++++++++++++++++++++++++++++++++++ MESSAGE HANDLING +++++++++++++++++++++++++++++++++++++++++++++++
	
	/**
	 * Message transport allows to retrieve and route message from/to core.
	 * @param transport
	 */
	public void setMessageTransport(MessageTransport transport) {
		logger.debug("Message transport assigned");
		messageTransport = transport;
		messageTransceiver = new GatewayMessageTransceiver(messageTransport,this);
		messageTransportChannelListener = new ChannelListener<OutgoingMessage>() {
			
			
			@Override
			public void onChannelUnavailable() {
				onCommunicationDown();
			}
			
			@Override
			public void onChannelAvailable() {
				onCommunicationUp();
			}

			@Override
			public void onChannelError(ChannelException error) {
				logger.warn("Received channel error");
				if (error instanceof MessageReplyTimeout) {
					MessageReplyTimeout timeout = (MessageReplyTimeout)error;
					onSendingMessageFailed(timeout.getRequest());
				}
				else {
					logger.error("Unable to handle error: "+error);
				}
			}

			
		};
		
		messageTransport.getOutgoing().addChannelListener(messageTransportChannelListener);
		if (messageTransport.getOutgoing().isAvailable()){
			onCommunicationUp();
		} else {
			onCommunicationDown();
		}
		activateIfPossible();
	}
	
	public void unsetMessageTransport(MessageTransport transport) {
		messageTransport = null;
		deactivate();
	}
	
	protected void onCommunicationDown(){
		logger.debug("Communication Channel Down");
		communicationUp = false;
	}
	
	
	protected void onCommunicationUp(){
		this.communicationUp = true;
		logger.debug("Communication Channel UP");
		messageTransport.getIncoming().setAvailable(true);
		if (activated){
			messageTransceiver.sendHandshake(this.cfgGatewayId, this.gatewayImageVersion);
		}
	}
	
	protected void onSendingMessageFailed(AbstractMessage message){
		logger.debug("Error occured related to message: "+message);
		
		if (message instanceof HandshakeRequest) {
			logger.debug("HandshakeReply was missing");
			if (this.communicationUp) {
				messageTransceiver.sendHandshake(this.cfgGatewayId, this.gatewayImageVersion);
			}
		}
	}
	
	
	@Override
	public void onHandshakeCompleted() {
		logger.debug("Handshake completed");
	}
	
	@Override
	public void onStopDiscoverySessionRequest(String sessionId){
		logger.debug("Stop discovery session request received. Session id : "+sessionId);
		if (discoveryProcessor == null){
			// may not possible
			return;
		}
		try {
			// check whether discovery session is active
			if (!discoveryProcessor.isReadyForDiscoverySession()){
				discoveryProcessor.stopDiscoverySession();
			} else {				
				messageTransceiver.sendDiscoverySessionFinished(sessionId);
			}
		} catch (Exception exc){
			logger.error("Error when trying to stop Discovery session",exc);
		}
	}
	
	@Override
	public void onStartDiscoverySessionRequest(String sessionId) {
		logger.debug("Start discovery session request received. Session id: "+sessionId);
		if (discoveryProcessor == null) {
			messageTransceiver.sendDiscoverySessionError(sessionId,"Unable to start Discovery Session. Discovery Processor is not initialized",DiscoverySessionError.ERROR_NOT_INITIALIZED);
			return;
		}		
		try {
			if (discoveryProcessor.isReadyForDiscoverySession()){	
				thingAddedDuringDiscoverySession = EMPTY_THING_UID;
				discoveryProcessor.startDiscoverySession(sessionId);
			} else {				
				messageTransceiver.sendDiscoverySessionError(sessionId,"Unable to start Discovery Session '"+sessionId+"'. Another session is in progress ('"
																		+discoveryProcessor.getCurrentSessionId()+"')",DiscoverySessionError.ERROR_DISCOVERY_IN_PROGRESS);
			}
			
		} catch (DiscoverySessionException exc){
			logger.error("Unable to start discovery session.",exc);
			messageTransceiver.sendDiscoverySessionError(sessionId,"Unable to start Discovery Session. "+exc.getMessage(),DiscoverySessionError.ERROR_UNKNOWN);
		}
	}

	
	@Override
	public void onConfigurationRequest(){
		logger.debug("Received configuration request");
		String sessionId = discoveryProcessor.getCurrentSessionId();
		ArrayList<com.yetu.abstraction.model.Thing> things = new ArrayList<>();

		synchronized(thingMappings){
			for (ThingMapping mapping : thingMappings){
				things.add(mapping.getThing());
			}
		}
			
		logger.debug("Sending configuration reply");
		messageTransceiver.sendConfigurationReply(things, sessionId);
	}
	
	@Override
	public void onThingRemoveRequest(String thingId){
		logger.debug("ThingRemoveRequest received. ThingId = {}",thingId);
		try {
			removeThing(thingId);
			// thing removed message will be sent when thingregistry calles onThingRemoved
		} catch (ThingNotFoundException exception){
			messageTransceiver.sendThingRemoveFailure(thingId, exception.getMessage(),ThingRemoveFailure.ErrorCode.THING_NOT_FOUND);
		}
		
	}
	

	
	@Override
	public void onSetPropertyCommand(String thingId, String componentId, String capabilityId, String propertyName, String value){
		logger.debug("set property command received");
		ThingMapping thingMapping = getThingMapping(thingId);
		if (thingMapping == null){
			logger.error("There exist no Thing with id '"+thingId+"'");
			messageTransceiver.sendSetPropertyError(SetPropertyError.ErrorCode.ERROR_THING_NOT_FOUND, "There exist no Thing with id '"+thingId+"'", thingId, componentId, capabilityId, propertyName, value);
			return;
		}		
		try {
			Property property = thingMapping.getProperty(componentId, capabilityId, propertyName);
			Thing thing = thingRegistry.get(thingMapping.getEshThingUID());
			if (thing == null){
				logger.error("found no thing with id {}",thingMapping.getEshThingUID().toString());
				return;
			}
			Channel channel = thingMapping.getChannel(property.getCapability());
			if (channel == null){
				logger.error("found no channel for thing {} that was mapped to {}.{}.{}.{}",thingMapping.getEshThingUID().toString(),
																							thingId,
																							componentId,
																							capabilityId,
																							propertyName);
				return;
			}
			Command command = PropertyValueTransformer.generateCommand( channel, property, value);
			if (command == null){
				logger.error("unable to generate command for value '{}', channel {} and property {}.{}.{}.{}",value, channel.getUID(),
																							thingId,
																							componentId,
																							capabilityId,
																							propertyName);
				messageTransceiver.sendSetPropertyError(SetPropertyError.ErrorCode.ERROR_UNKNOWN, "", thingId, componentId, capabilityId, propertyName, value);
				return;
			}
			ThingHandler handler = thing.getHandler();
			if (handler == null){
				logger.error("found no handler for thing {}! Unable to process command for channel {}",channel.getUID());
				return;
			}
			handler.handleCommand(channel.getUID(), command);
			
			thingMapping.setProperty(componentId, capabilityId, propertyName, value);
		} catch (NotFoundException exception) {
			messageTransceiver.sendSetPropertyError(getErrorCode(exception), exception.getMessage(), thingId, componentId, capabilityId, propertyName, value);
		} catch (InvalidPropertyValueException exception){
			messageTransceiver.sendSetPropertyError(SetPropertyError.ErrorCode.ERROR_VALUE_IS_NOT_VALID, exception.getMessage(), thingId, componentId, capabilityId, propertyName, value);
		} catch (ValueTransformationException exception){
			messageTransceiver.sendSetPropertyError(SetPropertyError.ErrorCode.ERROR_VALUE_IS_NOT_VALID, exception.getMessage(), thingId, componentId, capabilityId, propertyName, value);
		}
	}
	
	
	
	protected SetPropertyError.ErrorCode getErrorCode(NotFoundException exception){
		if (exception instanceof ThingNotFoundException){
			return SetPropertyError.ErrorCode.ERROR_THING_NOT_FOUND;
		} else if (exception instanceof ComponentNotFoundException){
			return SetPropertyError.ErrorCode.ERROR_COMPONENT_NOT_FOUND;
		} else if (exception instanceof CapabilityNotFoundException){
			return SetPropertyError.ErrorCode.ERROR_CAPABILITIY_NOT_FOUND;
		} else if (exception instanceof PropertyNotFoundException){
			return SetPropertyError.ErrorCode.ERROR_PROPERTY_NOT_FOUND;
		}
		return SetPropertyError.ErrorCode.ERROR_NOT_FOUND;
	}
	
	// ---------------------------------- MASSAGE HANDLING -----------------------------------------------

	// +++++++++++++++++++++++++++++++++++ ESH DB Storage Service ++++++++++++++++++++++++++++++++++++++++
	
	public void setStorageService(StorageService storageService){
		if (this.storageService != null){
			logger.warn("storageService already set. Got another service injected and will ignore it");
			return;
		}
				
		this.storageService = storageService;		
		activateIfPossible();
	}
	
	public void unsetStorageService(StorageService storageService){
		if (this.storageService == storageService){
			this.storageService = null;
			deactivate();
		}
	}
	
	// ----------------------------------- ESH DB Storage Service ----------------------------------------
	
	// +++++++++++++++++++++++++++++++++++ Remote Management Service +++++++++++++++++++++++++++++++++++++
	
	public void setRemoteGatewayManagementAccessor(RemoteGatewayManagementAccessor remoteGatewayManagementAccessor) {
		logger.debug("Remote management accessor assigend");
		this.remoteGatewayManagementAccessor = remoteGatewayManagementAccessor;

		activateIfPossible();
	}
	
	public void unsetRemoteGatewayManagementAccessor(RemoteGatewayManagementAccessor remoteGatewayManagementAccessor) {
		this.remoteGatewayManagementAccessor = null;
	}
	
	// ---------------------------------- Remote Management Service --------------------------------------
	
	// ++++++++++++++++++++++++++++++++++++++  Utils +++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	private boolean isThingMappingExisting(ThingUID thingUid){
		return (getThingMapping(thingUid) != null);
	}	
	
	private ThingMapping getThingMapping(ThingUID thingUid){
		synchronized(thingMappings){
			for (ThingMapping mapping : thingMappings){
				if (mapping.getEshThingUID().equals(thingUid)){
					return mapping;
				}
			}
		}
		return null;
	}
	
	private ThingMapping getThingMapping(String thingId){
		synchronized(thingMappings){
			for (ThingMapping mapping : thingMappings){
				logger.debug("Checking thing mapping: "+mapping+" with "+mapping.getThing().getId()+" EQUALS? "+thingId);
				if (mapping.getThing().getId().equalsIgnoreCase(thingId)){
					return mapping;
				}
			}
		}
		return null;
	}

	@Override
	public void onResetRequest(Entity[] entities) {
		logger.debug("On reset request: "+entities);
		for (Entity currEntity : entities) {
			if (currEntity.compareTo(ResetRequest.Entity.SESSIONS)==0){
				resetSessions();
			}
			if (currEntity.compareTo(ResetRequest.Entity.THINGS)==0){
				resetThings();
			}
		}
	}
	
	
	private void resetSessions(){
		logger.debug("RESET OF SESSIONS");
		discoveryProcessor.reset();
	}
	
	private void resetThings(){
		logger.debug("REMOVING ALL THINGS");
		for (ThingMapping thingMapping : thingMappings){
			try {
				removeThing(thingMapping.getThing().getId());
			} catch (ThingNotFoundException exc){
				logger.error("Unable to remove Thing with id {}",thingMapping.getThing().getId(),exc);
			}
		}
	}
	
	/**
	 * Reads /etc/lsb-release file to get image version
	 * @return the version or DEVICE_PROPERTY_IMAGE_VERSION_UNKNOWN if reading file has failed
	 */
	private String retrieveGatewayImageVersion(String lsbReleaseFile) {
		String result = RemoteManagementBundleConstants.DEVICE_PROPERTY_IMAGE_VERSION_UNKNOWN;

		String versionKey = "CHROMEOS_RELEASE_VERSION";
		String version = "";
		
		String trackKey = "CHROMEOS_RELEASE_TRACK";
		String track = "";
		
		File file = new File(lsbReleaseFile);
		
		if (!file.exists() || !file.isFile()) {
			return result;
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
	 
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith(trackKey+"=")) {
					track = line.substring(trackKey.length()+1);
				}
				else if(line.startsWith(versionKey+"=")) {
					version = line.substring(versionKey.length()+1);
				}
			}
			
			reader.close();
		} catch(Exception e) {
			logger.error("Failed to retrieve gateway image version",e);
		}
		
		if (version.length() != 0 && track.length() != 0) {
			result = track+"-"+version;
		}
		
		return result;
	}

	
}
