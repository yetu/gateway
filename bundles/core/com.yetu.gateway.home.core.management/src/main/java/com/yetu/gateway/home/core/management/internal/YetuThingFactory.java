package com.yetu.gateway.home.core.management.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.eclipse.smarthome.core.storage.Storage;
import org.eclipse.smarthome.core.storage.StorageService;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.abstraction.model.Attribute;
import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.ComponentType;
import com.yetu.abstraction.model.ConnectionDetails;
import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.Thing;
import com.yetu.abstraction.model.ThingModelFactory;
import com.yetu.abstraction.model.Unit;
import com.yetu.abstraction.spec.CapabilitySpecification;
import com.yetu.abstraction.spec.ThingSpecification;
import com.yetu.gateway.home.core.management.internal.things.ConnectionDetailsFactory;
import com.yetu.gateway.home.core.management.internal.things.ThingMapping;
import com.yetu.gateway.home.core.management.internal.util.AbstractionTag;


public class YetuThingFactory {
	private final static String STORAGE_NAME_THING_IDS = "yetuThingIdMapping";
	
	private static Logger logger = LoggerFactory.getLogger(YetuThingFactory.class);
	private ThingSpecification thingSpecification;
	private StorageService storageService;
	private Storage<String> storage;
	
	public YetuThingFactory(ThingSpecification thingSpecification, StorageService storageService){
		logger.debug("Created. Spec is: "+thingSpecification);
		if (thingSpecification == null){
			logger.error("ThingSpecification is null, YetuThingFactory won't work properly. Throwing NullPointerException now");
			throw new NullPointerException("Given ThingSpecification is null");
		}
		this.storageService = storageService;
		
		if (storageService == null){
			logger.error("Storage service is null. YetuThingFactory won't work properly. Throwing NullPointerException now");
			throw new NullPointerException("StorageService is null");
		}
		this.thingSpecification = thingSpecification;
		loadThingIdMappings();
	}
	
	private void loadThingIdMappings(){
		
		storage = storageService.getStorage(STORAGE_NAME_THING_IDS);
		if (storage == null){
			storageService.getStorage(STORAGE_NAME_THING_IDS, this.getClass().getClassLoader());
		}
		if (storage == null){
			logger.error("Unable to receive storage from storage service");
		}
	}
	
	
	
	public String getYetuThingId(ThingUID eshThingUID){
		String id = storage.get(eshThingUID.toString());
		if (id == null){
			id =  generateThingId();
			storage.put(eshThingUID.toString(),id);
		}
		return id;
	}
	
	public boolean isYetuThingIdExisting(ThingUID eshThingUID) {
		return storage.get(eshThingUID.toString()) != null;
	}
	
	public static String generateThingId(){		 
		 return UUID.randomUUID().toString();
	}
	
	
	public ThingMapping createYetuThingAbstraction(org.eclipse.smarthome.core.thing.Thing eshThing, String name) throws ThingTransformationException{
		
		logger.debug("Generating thing '{}' out of eshThing {}",name,eshThing.getUID());
		
		Thing yetuThing = ThingModelFactory.eINSTANCE.createThing(getYetuThingId(eshThing.getUID()));	
		
		
		// Each channel that contains a default tag for the yetu thing abstraction will be analyzed 
		// the given details are stored in ChannelComponentMappings
		List<ChannelComponentMapper> channelComponentMappingList = generateChannelComponentMapping(eshThing);
		
		
		// TODO: check whether each component defined in mapping has only one capability for a single type 
		// (e.g. prevent hat component SOCKET has two capabilities SWITCHABLE)
		// throw exception in that case
		
		
		
		// After TODO has finished, thing model is able to get generated
		
		// hashmap for all components that are contained in thing
		Map<String,Component> components = new HashMap<>();
		String mainComponentId = eshThing.getProperties().get("abstraction_main_comp");
		Component component;
		for (ChannelComponentMapper mapper : channelComponentMappingList){
//			if (mapper.abstractionTag.getAttribute(AbstractionTag.MAIN_COMPONENT_ID) != null){
//				mainComponentId = mapper.abstractionTag.getAttribute(AbstractionTag.MAIN_COMPONENT_ID);
//			}
			// To continue, the component addressed in the is needed
			if (components.containsKey(mapper.getId())){
				// take it if component with this id was generated already
				component = components.get(mapper.getId());
			} else {
				// component with given id is not existing yet, so it will be created
				component = thingSpecification.createComponent(mapper.getId(),thingSpecification.getComponentSpecification(mapper.getComponentType()));
				if (component == null){
					// TODO oops, component was not created. That sucks
				}
				// setting the name, but take care that each name is unique within one thing (not necessary, but more usable for the user)
				component.setName(getName(components, mapper));
				// add component to temporal hashmap
				components.put(mapper.getId(),component);
			}	
					
			
			
			Capability capability;
			// during the generation, all default capabilities for each component have also been generated. 
			// when current channel capability is not already added to current component (because it is not a default one) ...
			if (!component.hasCapability(mapper.getCapabilitySpecification().getId())){
				// ... then create capability and add to current component
				capability = thingSpecification.createCapability(mapper.getCapabilitySpecification());
				component.getCapabilities().add(capability);
			} else {
				// ... else get capability
				capability = component.getCapability(mapper.getCapabilitySpecification().getId());
			}
			// the unit is not given at all time (e.q. capability Measurement has a property value without a specified unit
			// this unit is given by the channel-tag. the needed information is temporarily stored in the mapper)
			// add unit information if it is missing in the current capability.
			addUnitIfMissing(mapper,capability);
		
		}
		
		
		// add the generated components to yetuThing now
		yetuThing.getComponents().addAll(components.values());
		
		// setting mainComponent to yetuThing
			if (mainComponentId != null){
			Component mainComp = yetuThing.getComponent(mainComponentId);
			yetuThing.setMainComponent(mainComp);
		} else {
			logger.warn("No main component type was given! Will set the type of first component as main component type.");
			yetuThing.setMainComponent(yetuThing.getComponents().get(0));
		}
		
		// TODO: validate Thing before returning
		
		yetuThing.setName(name);
		
		
		// Setting ConnectionDetails
		logger.debug("Generating connection details");
		List<ConnectionDetails> conDetailsList = ConnectionDetailsFactory.getConnectionDetails(eshThing);
		
		logger.debug(conDetailsList+"");
		if (conDetailsList != null){
			for (ConnectionDetails conDetails : conDetailsList){
				logger.debug("Adding connection detail: "+conDetails);
				yetuThing.getConnectionDetails().add(conDetails);
				for (Attribute a : conDetails.getProperties()) {
					logger.debug(a.getName()+" "+a.getValue());
				}
			}
		}
		
		
		// generate the yetu thing abstraction mapper ...
		ThingMapping thingMapping = new ThingMapping(yetuThing, eshThing.getUID());
		// ... and link between channels and capabilities
		addChannelCapabilityMappings(thingMapping, channelComponentMappingList);
		
		// return the shit
		return thingMapping;
	}
	
	private void addChannelCapabilityMappings(ThingMapping thingMapping, List<ChannelComponentMapper> channelMappings){
		Thing yetuThing = thingMapping.getThing();
		Component component;
		Channel channel;
		Capability capability;
		for (ChannelComponentMapper ccMapping : channelMappings){
			component = yetuThing.getComponent(ccMapping.getId());
			capability = component.getCapability(ccMapping.getCapabilitySpecification().getId());
			channel = ccMapping.getChannel();
			
			thingMapping.addChannelCapabilityPair(channel, capability);
		}
	}
	
	
	private void addUnitIfMissing(ChannelComponentMapper mapper, Capability capability){
		if (mapper.hasUnit()) {
			if (capability.getProperties().size() == 1){
				Property property = capability.getProperties().get(0);
				if (property.getUnit() == null){
					property.setUnit(mapper.getUnit());
				}
			}
		}
	}

	
	private String getName(Map<String, Component> componentMap, ChannelComponentMapper mapper){
		String name = mapper.getComponentType().getId().toLowerCase();
		name = name.substring(0,1).toUpperCase() + name.substring(1,name.length()).toLowerCase();
		
		if (!nameAlreadyExists(name, componentMap)){
			return name;
		}
		
		int counter = 2;
		while (nameAlreadyExists(name+counter, componentMap)){
			counter++;
		}		
		return name+counter;
	}
	
	private boolean nameAlreadyExists(String name, Map<String,Component> componentMap){
		for (Component component : componentMap.values()){
			if (component.getName().equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
		
	}
		
	private List<ChannelComponentMapper> generateChannelComponentMapping(org.eclipse.smarthome.core.thing.Thing eshThing) throws ThingTransformationException{
		 List<ChannelComponentMapper> map = new ArrayList<>();
		 AbstractionTag tag;
		 CapabilitySpecification capSpec;
		 ComponentType cType;
		 Unit unit;
		 ChannelComponentMapper mapper;	
		 
		 for (Channel ch : eshThing.getChannels()){
				if (!ch.getDefaultTags().isEmpty()){
					for (String str : ch.getDefaultTags()){
						if (AbstractionTag.isAbstractionTag(str)){
							tag = new AbstractionTag(str);
							logger.debug("Thingspec is: "+thingSpecification);
							capSpec = thingSpecification.getCapabilitySpecification(tag.getCapabilityId());
							if (capSpec == null){
								throw new ThingTransformationException("No capability with id '"+tag.getCapabilityId()+"' specified."+
									" Unable to transform channel '"+ch.getUID()+"' with given abstractino tag '"+str+"'.");
							}
							cType = thingSpecification.getComponentType(tag.getComponentTypeId());
							if (cType == null){
								throw new ThingTransformationException("No component of type '"+tag.getComponentTypeId()+"' specified."+
									" Unable to transform channel '"+ch.getUID()+"' with given abstractino tag '"+str+"'.");
							}
							if (tag.hasUnitId()){
								unit = thingSpecification.getUnit(tag.getUnitId());
							} else unit = null;			
							mapper = new ChannelComponentMapper(tag, ch, tag.getComponentId(), cType, capSpec, unit);
						
							map.add(mapper);
						}
					}
				}			
			}	 
		 return map;
	}
	
	private class ChannelComponentMapper{
		ComponentType componentType;
		Channel channel;
		CapabilitySpecification capabilitySpec;
		String id;
		Unit unit;
		AbstractionTag abstractionTag;
		
		
		public ChannelComponentMapper(AbstractionTag tag, Channel channel,String id, ComponentType compType, CapabilitySpecification capSpec, Unit unit){
			this.abstractionTag = tag;
			this.componentType = compType;
			this.channel = channel;
			this.capabilitySpec = capSpec;
			this.unit = unit;
			this.id = id;
		}
		

		public ComponentType getComponentType() {
			return componentType;
		}

		public Channel getChannel() {
			return channel;
		}

		public CapabilitySpecification getCapabilitySpecification() {
			return capabilitySpec;
		}
		
		public String getId(){
			return id;
		}
		
		public boolean hasUnit(){
			return unit != null;
		}
		
		public Unit getUnit(){
			return unit;
		}
	}

	public void removeThingIdMapping(ThingTypeUID thingTypeUID) {
		storage.remove(thingTypeUID.toString());		
	}
	
	
	
}
