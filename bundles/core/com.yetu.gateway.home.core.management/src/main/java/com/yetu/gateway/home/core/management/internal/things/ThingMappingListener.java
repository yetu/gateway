package com.yetu.gateway.home.core.management.internal.things;



import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.ThingStatus;

public interface ThingMappingListener {
	public void onPropertyChange(ThingMapping thingMapping, Property property, Object oldValue, Object newValue);
	public void onThingStateChange(ThingMapping thingMapping, ThingStatus oldStatus, ThingStatus newStatus);
}
