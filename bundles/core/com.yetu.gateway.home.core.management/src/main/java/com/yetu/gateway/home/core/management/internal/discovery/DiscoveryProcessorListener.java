package com.yetu.gateway.home.core.management.internal.discovery;

public interface DiscoveryProcessorListener {
	public void onDiscoveryStarted(String sessionId);
	public void onDiscoveryFinished(String sessionId);
	public void onDiscoveryError(String sessionId, String message);
}
