package com.yetu.gateway.home.core.management.internal.things;

import java.util.List;

import org.eclipse.smarthome.core.thing.Thing;

import com.yetu.abstraction.model.ConnectionDetails;

public class DefaultConnectionDetailsBuilder extends ConnectionDetailsBuilder {

	@Override
	public List<ConnectionDetails> generateConnectionDetails(Thing eshThing) {
		List<ConnectionDetails> conDetailsList = super.getDefaultConnectionDetails(eshThing);
		
		// feel free to add default properties which should be added to connection details
		
		return conDetailsList;
	}
}
