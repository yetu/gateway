package com.yetu.gateway.home.core.management.internal.discovery;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CancellationException;

import org.eclipse.smarthome.config.discovery.DiscoveryServiceRegistry;
import org.eclipse.smarthome.config.discovery.ScanListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiscoveryProcessor {
			
	
	// the following states will be used for each discovery service as well as for the discovery processor itself
	// establish separated flags if this is confusing developers (DPS = DiscoveryProcessorState)
	public static final int DPS_IDLE 		= 0;	
	public static final int DPS_STARTING 	= 1;
	public static final int DPS_STARTED		= 2;
	public static final int DPS_STOPPING	= 3;
	public static final int DPS_STOPPED		= 4;
	public static final int DPS_CANCELED	= 5;
	public static final int DPS_CLEANING_UP	= 6;
	public static final int DPS_ERROR		= 7;
	
	public static final long DEFAULT_WAITING_FOR_ERROR_DURATION = 5000;
	private long waiting_for_error_duration = DEFAULT_WAITING_FOR_ERROR_DURATION;
	private DiscoveryServiceErrorObserver discoveryServiceErrorObserver;
	
	protected static Logger logger = LoggerFactory.getLogger(DiscoveryProcessor.class);	
	
	protected DiscoveryServiceRegistry discoveryServiceRegistry = null;	
	protected DiscoveryProcessorListener discoveryProcessorListener = null;
	protected String currentSessionId;
	protected int discoveryProcessorState = DPS_IDLE;
	
	// mappings for discovery service communication
	private Map<ScanListener,String> scanListeners = null; 
	private Map<String,Integer> scanningBindingStates = null;
	
	
	public DiscoveryProcessor(DiscoveryServiceRegistry discoveryServiceRegistry, DiscoveryProcessorListener listener){
		this.discoveryServiceRegistry = discoveryServiceRegistry;
		this.discoveryProcessorListener = listener;
		
		cleanUp();
	}
	
	public void setWaitingForDiscoveryServiceErrorDuration(long duration){
		this.waiting_for_error_duration = duration;
	}
	
	public long getWaitingForDiscoveryServiceErrorDuration(){
		return waiting_for_error_duration;
	}
	
	public boolean isReadyForDiscoverySession(){
		return getDiscoverySessionState() == DPS_IDLE;
	}		

	
	public String getCurrentSessionId(){
		return currentSessionId;
	}
	
	/**
	 * ESH contains several discovery services which all will be started in this method. For each binding, a scan state will be mapped and set to DPS_STARTING. 
	 * An observer thread is listening for reported errors. After a given duration , the observer is checking whether an error occurred or not. If no error occurred
	 * the mappings will be set to STARTED and the discovery processor reports the session started event.If an error occurred, the service reports it error itself and the 
	 * mapping will be set to DPS_ERROR.  
	 * @param sessionId Id of the session that should be started
	 * @throws DiscoverySessionException will be thrown, if it is not possible for the processor to start the discovery session. E.g. when a 
	 * session was already started. 
	 */
	public void startDiscoverySession(String sessionId) throws DiscoverySessionException{
		
		
		logger.debug("startDiscoverySession was called with sessionId {}",sessionId);
		
		if (!isReadyForDiscoverySession()){
			throw  generateNotReadyForDiscoverySessionException(sessionId);					
		}
		
		currentSessionId = sessionId;
		setState(DPS_STARTING);  // setting the state of the DiscoveryProcessor itself to DPS_STARTING, 
								 // will be set to started, when all services are started and no error occurred.		
		// discoveryServiceRegistry returns a list that could contain the same bindingId several times, but we are only interested in a set of unique bindingIds
		List<String> bindingIds = getUniqueBindingIds(discoveryServiceRegistry.getSupportedBindings());	
		
	
		for (String bindingId : bindingIds){				
			setScanningBindingState(bindingId,DPS_STARTING);
			if (!discoveryServiceRegistry.startScan(bindingId, createScanListener(bindingId))){				
				scanningBindingStates.put(bindingId,DPS_ERROR);
				discoveryProcessorListener.onDiscoveryError(sessionId,"Unable to start discovery for binding "+bindingId+"!");							
			}
		}
					
		discoveryServiceErrorObserver = new DiscoveryServiceErrorObserver(waiting_for_error_duration);			
		discoveryServiceErrorObserver.start();
		
	}
	
	
	
	/**
	 * All discovery services that where registered (e.g. during start) will be stopped.
	 */
	public void stopDiscoverySession(){
		// wait until error observer is finished. This call blocks!
		waitForDiscoveryServiceErrorObserverFinished();
		
		logger.debug("Stopping discovery services. DiscoveryProcessor is in state {}",getDiscoverySessionState());
		
		if (isErrorOccured()){
			// TODO: check if something more is needed than just stopping the service
		}
		
		for (String bindingId : scanListeners.values()){				
			discoveryServiceRegistry.abortScan(bindingId);				
		}
		
	}
	
	public int getDiscoverySessionState(){
		return discoveryProcessorState;
	}
	
	public boolean isDiscoverySessionRunning() {
		return (getDiscoverySessionState() != DPS_IDLE);
	}
	
	// +++++++++++++++++++++++++++++++++++++ Operational Helpers ++++++++++++++++++++++++++++++++++++++++
	
	

	
	private void setScanningBindingState(String bindingId, int state){
		synchronized (scanningBindingStates) {
			scanningBindingStates.put(bindingId, state);
		}
	}
	
	/**
	 * discoveryServiceRegistry.getSupportedBindingIDs() returns a list that could contain the same bindingId several times. This method reduces this list
	 * to unique bindingIds
	 * @param ids list will all bindingIds
	 * @return list that includes a bindingID only one time
	 */
	private List<String> getUniqueBindingIds(List<String> ids){
		List<String> result = new ArrayList<>();
		for (String id : ids){		
			if (!result.contains(id)){
				result.add(id);
			}
		}		
		return result;
	}
	
	/**
	 * 
	 * @param bindingId
	 * @return
	 */
	private ScanListener createScanListener(String bindingId){
		ScanListener scanListener = new ScanListener() {
			
			@Override
			public void onFinished() {
				logger.debug("Scan listener "+this+" received finished event");
				processDiscoveryServiceFinished(getBindingId(this));
			}
			
			@Override
			public void onErrorOccurred(Exception exception) {
				logger.debug("Scan listener "+this+" received error event");
				processDiscoverySessionError(getBindingId(this), exception);
				
			}
		};
		
		scanListeners.put(scanListener,bindingId);		
		return scanListener;
	}
	
	private String getBindingId(ScanListener listener){
		String bindingId = scanListeners.get(listener);
		if (bindingId == null){
			logger.warn("getBindingId(ScanListener {}) returns null for bindingId. This could happen, when a service was sending events that is not registered");
		}
		return bindingId;
	}
	
	/**
	 * The DiscoveryServiceErrorObserver calls this method when no error occurred for a certain time after starting the services. 
	 * The method sets the discovery mapping to DPS_STARTED.
	 */
	protected void  processNoDiscoveryServiceErrorOccured(){	
		logger.debug("no error occured during observation");
		if (areAllDiscoveryServicesHaveState(DPS_STARTING)){
			logger.debug("all discovery services are in state DPS_STARTING and will be set to DPS_STARTED");		
			List<String> bindingIds = getUniqueBindingIds(discoveryServiceRegistry.getSupportedBindings());				
			for (String bindingId : bindingIds){
				processDiscoveryServiceStarted(bindingId);
			}
		} 
	}
	
	/**
	 * The DiscoveryServiceErrorObserver calls this method when an error occurred for a certain time after starting the services. 
	 * The method sets the discovery processor state to DPS_ERROR and reports the DiscoveryError to the discoveryProcessorListener. 
	 */
	protected void  processDiscoveryServiceErrorOccurred(){
		logger.debug("Error occured during observation");
		setState(DPS_ERROR);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				discoveryProcessorListener.onDiscoveryError(getCurrentSessionId(), "Error occured during start of discovery services");		
				
			}
		}).start();
		
		// TODO: discoveryServices reporting errors individually. This information is gone over here. Remember all errors and send this information too.
	}
	
	
	private void processDiscoveryServiceStarted(String bindingId){
		setScanningBindingState(bindingId,DPS_STARTED);		
		if (areAllDiscoveryServicesStarted()){
			logger.debug("all discovery services are in state DPS_STARTED. Setting state for processor to state DPS_STARTED");
			setState(DPS_STARTED);
		}
	}
//	
//	private void processDiscoveryServiceCanceled(String bindingId){
//		setScanningBindingState(bindingId,DPS_CANCELED);		
//		if (areAllDiscoveryServicesStopped()){
//			setState(DPS_STOPPED);
//		}
//	}
	
	private void processDiscoveryServiceFinished(String bindingId){
		setScanningBindingState(bindingId,DPS_STOPPED);	
		if (areAllDiscoveryServicesStopped()){
			setState(DPS_STOPPED);
		}
	}	

	private void processDiscoverySessionError(String bindingId, Exception exception){
		// an CancellationException will be thrown when a discovery scan was aborted
		// Of course it's no error, thus check if this was the case and set the related mapping to DPS_STOPPED
		if (exception != null){
			if (exception instanceof CancellationException){
				setScanningBindingState(bindingId,DPS_STOPPED);			
				if (areAllDiscoveryServicesStopped()){
					setState(DPS_STOPPED);
				}
				return;
			}
		}
		
		setScanningBindingState(bindingId,DPS_ERROR);			
		setState(DPS_ERROR);
				
		String msg = "Error ("+exception.getClass().getName()+") occured during discovery session. Binding "+bindingId+". "+exception.getMessage();		
		logger.error(msg,exception);
		// TODO: remember this error over here that it could be reported to discoveryProcessorListener in method processDiscoveryServiceErrorOccurred
	}
	
	
	private boolean areAllDiscoveryServicesHaveState(int state){
		synchronized(scanningBindingStates){
		
			for (int s : scanningBindingStates.values()){
				if (s != state){
					return false;
				}
			}
		}
		return true;
	}
	
	
	
	private boolean isErrorOccured(){		
		synchronized(scanningBindingStates){
			for (Entry<String, Integer> currEntry : scanningBindingStates.entrySet()){				
				if (currEntry.getValue() == DPS_ERROR){
					return true;
				}
			}
		}
		return false;
	}
	
	protected boolean areAllDiscoveryServicesStopped(){
		synchronized(scanningBindingStates){
			for (int state : scanningBindingStates.values()){
				if ((state != DPS_STOPPED) && (state != DPS_CANCELED)){
					return false;
				}
			}
		}
		return true;
	}
	
	protected boolean areAllDiscoveryServicesStarted(){
		logger.debug("requesting if all are started");
		return areAllDiscoveryServicesHaveState(DPS_STARTED);
	}
	
	protected void setState(int state){
		logger.debug("Setting new state of DiscoveryProcessor: "+state);
		
		if (this.discoveryProcessorState == state){
			// state has not changed, return
			return;
		}
		this.discoveryProcessorState = state;
		switch (state){
			case DPS_STARTED:
				
				discoveryProcessorListener.onDiscoveryStarted(getCurrentSessionId());				
				break;
			case DPS_STOPPED:
			case DPS_CANCELED:
				discoveryProcessorListener.onDiscoveryFinished(getCurrentSessionId());
				cleanUp();
				break;
			case DPS_ERROR:
				// discoveryProcessorListener will be informed when service reports error
				break;
			
			default:
				// nothing to do
		}
	}
	

		

	
	private DiscoverySessionException generateNotReadyForDiscoverySessionException(String sessionId){
		if (getDiscoverySessionState() != DPS_IDLE){
			return new DiscoverySessionException(sessionId, "DiscoveryProcessor is not ready for new discovery session!"+
												"Other session needs to be stopped first (sessionId = "+currentSessionId+")",null);
		}
		return new DiscoverySessionException(sessionId, "DiscoveryProcessor is not ready for new discovery session right now!",null);
	}

	
	
	protected boolean isDiscoveryServiceAvailable(String bindingId){
		List<String> supportedBindings = discoveryServiceRegistry.getSupportedBindings();		
		for (String id : supportedBindings){
			if (id.equalsIgnoreCase(bindingId)){
				return true;
			}
		}
		return false;
	}
	
	private void cleanUp(){		
		if ((getDiscoverySessionState() != DPS_IDLE) && (getDiscoverySessionState() != DPS_STOPPING) && (getDiscoverySessionState() != DPS_STOPPED)){
			stopDiscoverySession();
			waitForServicesToBeStopped(5000);
		}
		setState(DPS_CLEANING_UP);
	
		scanListeners = new HashMap<>();
		scanningBindingStates = new HashMap<>();
		currentSessionId = null;
		setState(DPS_IDLE);
	}
		
	private void waitForServicesToBeStopped(long milliseconds){
		if (areAllDiscoveryServicesStopped()){
			setState(DPS_STOPPED);
			return;
		}
		long start = new Date().getTime();
		while((getDiscoverySessionState() != DPS_STOPPED) && ((start + milliseconds)>new Date().getTime())){
			try {
				Thread.sleep(100);
			} catch (Exception exc){
				
			}
		}
		if (getDiscoverySessionState() != DPS_STOPPED) {
			// hmm... that's shit
			logger.error("Unable to stop DiscoveryProcessor");
		}
	}
	
	// +++++++++++++++++++++++++++++++++++++ DiscoveryServiceErrorObserver +++++++++++++++++++++++++++++++++++++++++++++++++
	
	private class DiscoveryServiceErrorObserver extends Thread{
		
		long duration;
		boolean running;
		boolean stop;
		
		public DiscoveryServiceErrorObserver(long duration){
			this.duration = duration;
			this.running = true;
			this.stop = false;
		}
		
		
		public void run(){
			logger.debug("error observer started for {} seconds",""+duration/1000.0);
			long startMillies = new Date().getTime();
			
			while(!stop && !isErrorOccured() && (((new Date().getTime()) -startMillies)<duration)){
				try {
					Thread.sleep(100);
				} catch (InterruptedException exc){
					logger.error("DiscoveryServiceErrorObserver was interrupted and will stop now",exc);
				}
			}
			logger.debug("finished loop. stop = {} and isErrorOccured() = {}",stop,isErrorOccured());
			// the observer was forced to stop
			if (stop) {
				logger.debug("Forcing the discovery session observer to shut down");
				running = false;
				return;
			}
			
			
			if (!isInterrupted()) {
				if (isErrorOccured()){
					processDiscoveryServiceErrorOccurred();
				} else {
					// only emit this event if we still want to start a discovery session
					if (getDiscoverySessionState() == DPS_STARTING) {
						processNoDiscoveryServiceErrorOccured();
					}
				}	
			}
			
			logger.debug("oberserver completed");
			running = false;
		}
		
		public boolean isRunning() {
			return running;
		}
		
		public void stopObservation() {
			stop = true;
		}
	}
	
	public void waitForDiscoveryServiceErrorObserverFinished() {
		if (discoveryServiceErrorObserver == null || !discoveryServiceErrorObserver.isRunning()) {
			logger.debug("DiscoveryServiceErrorObserver already finished");
			return;
		}
		
		discoveryServiceErrorObserver.stopObservation();
		logger.debug("Waiting for discoveryServiceErrorObserver until it is finished");

		try {
			while (discoveryServiceErrorObserver.isRunning()) {
				Thread.sleep(50);
			}
		} catch(Exception e) {
			logger.error("Error while waiting for discoveryServiceErrorObserver",e);
		}
	}

	public void reset() {
		logger.debug("Reset was called");
		currentSessionId = null;

		cleanUp();
	}

	
}
