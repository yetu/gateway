package com.yetu.gateway.home.core.monitoring.model;

public class ManagementItem {
	private String source;
	
	public ManagementItem(String source) {
		this.source = source;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
}
