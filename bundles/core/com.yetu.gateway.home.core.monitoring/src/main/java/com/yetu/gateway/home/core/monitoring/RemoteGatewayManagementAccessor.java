package com.yetu.gateway.home.core.monitoring;

import java.io.File;

import com.yetu.gateway.home.core.monitoring.model.Alarm;
import com.yetu.gateway.home.core.monitoring.model.DeviceProperty;
import com.yetu.gateway.home.core.monitoring.model.Event;
import com.yetu.gateway.home.core.monitoring.model.FileDescriptor;
import com.yetu.gateway.home.core.monitoring.model.Measurement;
import com.yetu.gateway.home.core.monitoring.model.OperationListener;

/**
 * Implementing RemoteGatewayManagementAccessor classes take care of sending collected information like
 * alarm, events and so on to the appropriate management server.
 * 
 * @author Sebastian Garn, Mario Camou
 */
public interface RemoteGatewayManagementAccessor {
	/**
	 * Sends an alarm to remote management server
	 * @param Alarm - alarm
	 */
	public void sendAlarm(Alarm alarm);
	
	/**
	 * Sends an event to remote management server
	 * @param Event - event
	 */
	public void sendEvent(Event event);
	
	/**
	 * Sends a measurement to remote management server
	 * @param Event - event
	 */
	public void sendMeasurement(Measurement event);
	
	/**
	 * Sends multiple measurements to remote management server
	 * @param Event - event
	 */
	public void sendMeasurements(Measurement[] event);
	
	/**
	 * Sends a string to remote management that will be stored as a text file
	 * @param contnet - String: value to store
	 * @param storageFilename - String: name of the stored file
	 * @return FileDescriptor or null
	 */
	public FileDescriptor storeAsTextFile(String content, String storageFilename);
	
	/**
	 * Sends a file. Tries to zip it if zipBeforeSending is true.
	 * @param fileToSend - File: which file has to be uploaded
	 * @param storageFilename - String: under which name the file has to be stored
	 * @param zipBeforeSending - boolean: zips the file if it is not zipped yet
	 * @return FileDescriptor pointing to the uploaded file or null
	 */
	public FileDescriptor sendFile(File fileToSend, String storageFilename, boolean zipBeforeSending);
	
	/**
	 * Retrieves part of a file and sends it
	 * @param lineCount - int: how many lines you want to transmit
	 * @param startFromBottom - boolean: start reading the lines from bottom or top
	 * @param storageFilename - String: name of the stored file
	 * @return
	 */
	public FileDescriptor sendPartOfFile(File fileToSend, int lineCount, boolean startFromBottom, String storageFilename);

	/**
	 * Registers a new operation listener. An operation listener takes care of
	 * evaluating incoming options which are sent by the management server (e.g. for maintenance puposes)
	 * @param OperationListener - handler
	 */
	public void registerOperationListener(OperationListener handler);
	
	/**
	 * Removes an operation listener
	 * @param OperationListener - handler
	 */
	public void unregisterOperationListener(OperationListener handler);
	
	/**
	 * Creates or updates a property of the device
	 * @param key - String: id of the property
	 * @param value - Object: value of the property
	 */
	public void sendDeviceProperty(DeviceProperty property);
}