package com.yetu.gateway.home.core.monitoring.model;

import java.util.Date;

public class Measurement extends ManagementItem {
	private Date time;
	private Property property;
	private double value;
	private String name;

	public Measurement(String source, String name, double value, Property property, Date time) {
		super(source);
		this.name = name;
		this.value = value;
		this.property = property;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
