package com.yetu.gateway.home.core.monitoring.model;


public class OperationResult {
	public enum Status {
		SUCCESS,
		FAILURE;	
	}
	
	Status status;
	String description;
	
	public OperationResult(Status status, String description) {
		this.status = status;
		this.description = description;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public String getDescription() {
		return description;
	}
}