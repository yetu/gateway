package com.yetu.gateway.home.core.monitoring.internal.operations;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.model.FileDescriptor;
import com.yetu.gateway.home.core.monitoring.model.OperationDescriptor;
import com.yetu.gateway.home.core.monitoring.model.OperationListener;
import com.yetu.gateway.home.core.monitoring.model.OperationResult;
import com.yetu.gateway.home.core.monitoring.model.OperationResult.Status;

/**
 * Operation that gives back a list of all running processes and theri consumption 
 * 
 * @author Sebastian Garn
 */
public class GetProcessesInformationOperation implements OperationListener {
	private static Logger logger = LoggerFactory.getLogger(GetProcessesInformationOperation.class);
	
	private static String OPERATION_NAME = "ps";
	private static String PARAMETER_STORE_IN_FILE = "storeinfile";

	static ArrayList<String> parameters;
	
	OperationDescriptor descriptor;
	
	public GetProcessesInformationOperation() {
		parameters = new ArrayList<String>();
		parameters.add(PARAMETER_STORE_IN_FILE);
		
		descriptor = new OperationDescriptor(OPERATION_NAME,parameters);
	}
	
	@Override
	public OperationDescriptor handledOperation() {
		return descriptor;
	}

	@Override
	public OperationResult execute(Map<String, String> parameters, RemoteGatewayManagementAccessor accessor) {
		boolean storeInFile = false;
		try {
			storeInFile = Boolean.parseBoolean(parameters.get(PARAMETER_STORE_IN_FILE));
		} catch(Exception e) {
			logger.error("Failed to fetch parameter. Using default value.",e);
		}
		
		String result = "";

		if (!storeInFile) {
			result = getProcessInformation("ps -Ao pid,pcpu,pmem,comm");
			return new OperationResult(Status.SUCCESS, result);
		}
		else {
			result = getProcessInformation("ps aux");
			FileDescriptor fileDescriptor = accessor.storeAsTextFile(result, "ps_aux.log");
			
			if (fileDescriptor == null) {
				return new OperationResult(Status.FAILURE, "Device was unable to upload file. Please check device user permissions.");
			}
			
			return new OperationResult(Status.SUCCESS, fileDescriptor.getPath());
		}
	}
	
	private String getProcessInformation(String command) {
		String result = "";
		
		try {
			Process process = Runtime.getRuntime().exec(command);

		    process.waitFor();
		 
		    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		 
		    String line = "";	
		    while ((line = reader.readLine())!= null) {
		    	result += line + "\n";
		    }

		} catch(Exception e) {
			logger.error("Failed to retrieve process list",e);
		}
		
		return result;
	}
}
