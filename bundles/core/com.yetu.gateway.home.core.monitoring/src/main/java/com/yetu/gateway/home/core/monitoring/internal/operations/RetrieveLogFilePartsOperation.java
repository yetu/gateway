package com.yetu.gateway.home.core.monitoring.internal.operations;

import java.io.File;
import java.util.Map;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.model.FileDescriptor;
import com.yetu.gateway.home.core.monitoring.model.OperationDescriptor;
import com.yetu.gateway.home.core.monitoring.model.OperationResult;
import com.yetu.gateway.home.core.monitoring.model.OperationResult.Status;

/**
 * This operation tries to locate a file on local hard drive and when found uploading parts of it
 * to the remote management platform 
 * 
 * @author Sebastian Garn
 */
public class RetrieveLogFilePartsOperation extends RetrieveLogFileOperation {
	static String OPERATION_NAME = "getlogpart";
	private static String PARAMETER_LINECOUNT = "linecount";
	private static String PARAMETER_BOTTOM_START = "startfrombottom";
	
	public RetrieveLogFilePartsOperation() {
		parameters.add(PARAMETER_LINECOUNT);
		parameters.add(PARAMETER_BOTTOM_START);
		
		descriptor = new OperationDescriptor(RetrieveLogFilePartsOperation.OPERATION_NAME, parameters);
	}

	@Override
	public OperationResult execute(Map<String, String> parameters, RemoteGatewayManagementAccessor accessor) {
		String filename = parameters.get(PARAMETER_FILENAME);
		int lines = Integer.parseInt(parameters.get(PARAMETER_LINECOUNT));
		boolean startFromBottom = Boolean.parseBoolean(parameters.get(PARAMETER_BOTTOM_START));

		File file = new File(filename);
		if (!file.exists()) {
			return new OperationResult(Status.FAILURE, "Unable to open file "+filename);
		}

		if (!RETRIEVABLE_FILES.contains(file.getAbsolutePath().toString())) {
			if (!RETRIEVABLE_DIRECTORIES.contains(file.getParentFile().toString())) {
				return new OperationResult(Status.FAILURE, "Unable to upload file due to security restrictions. You can request the following files: "+RETRIEVABLE_FILES+ " or files within: "+RETRIEVABLE_DIRECTORIES);
			}
		}
			
		FileDescriptor fileDescriptor = accessor.sendPartOfFile(file, lines, startFromBottom, file.getName());
		if (fileDescriptor == null) {
			return new OperationResult(Status.FAILURE, "Device was unable to upload file. Please check device user permissions.");
		}
		
		return new OperationResult(Status.SUCCESS, fileDescriptor.getPath());
	}
}
