package com.yetu.gateway.home.core.monitoring.model;

/**
 * Defines a device property which is attached to the remote representation
 * 
 * @author Sebastian Garn
 *
 */
public class DeviceProperty extends ManagementItem {
	private String key;
	private Object value;

	public DeviceProperty(String key, Object value) {
		super(null);
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public Object getValue() {
		return value;
	}
}
