package com.yetu.gateway.home.core.monitoring.internal.measurements;

import java.lang.management.ManagementFactory;
import java.util.Date;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.internal.types.PeriodicTask;
import com.yetu.gateway.home.core.monitoring.model.Measurement;
import com.yetu.gateway.home.core.monitoring.model.Property;

public class SystemMeasurementTask extends PeriodicTask {
	private static Logger logger = LoggerFactory.getLogger(SystemMeasurementTask.class);
	
	private RemoteGatewayManagementAccessor remoteManagement;
	private MBeanServer mBeanServer;
	private ObjectName name; 
	
	
	private static Property PROCESS_CPU_PROPERTY = Property.create("Process CPU load","%");
	private static Property SYSTEM_CPU_PROPERTY = Property.create("System CPU load","%");
	private static Property FREE_PHYS_MEM_PROPERTY = Property.create("Free memory","kb");
	private static Property USED_PHYS_MEM_PROPERTY = Property.create("Used memory","kb");
	
	public SystemMeasurementTask(long interval, RemoteGatewayManagementAccessor remoteManagement) {
		super(interval);
		
		this.remoteManagement = remoteManagement;
		
		mBeanServer = ManagementFactory.getPlatformMBeanServer();
		
		try {
			name = ObjectName.getInstance("java.lang:type=OperatingSystem");
		} catch(Exception e) {
			logger.error("Failed to start SystemMeasurementTask.",e);
		}
	}

	@Override
	public void action() {
		double processCpu = 0.0;
		double systemCpu = 0.0;
		double freeMemory = 0.0;
		double usedMemory = 0.0;
		
		AttributeList list = null;
		
		try {
			list = mBeanServer.getAttributes(name, new String[]{ "ProcessCpuLoad", "SystemCpuLoad", "FreePhysicalMemorySize", "TotalPhysicalMemorySize" });	
		} catch(Exception e) {
			logger.error("Failed to start SystemMeasurementTask.",e);
		}
		
    	if (list.isEmpty() || list == null || list.size() != 4) {
    		logger.warn("Could not measure system state");
    		return;
    	}

	    Attribute processCpuAtt = (Attribute)list.get(0);
	    Attribute systemCpuAtt = (Attribute)list.get(1);
	    Attribute freeMemAtt = (Attribute)list.get(2);
	    Attribute totalMemAtt = (Attribute)list.get(3);
	    
	    double valueProcessCpu  = (Double)processCpuAtt.getValue();
	    double valueSystemCpu  = (Double)systemCpuAtt.getValue();
	    double valueFreeMem  = ((Long)freeMemAtt.getValue()).doubleValue();
	    double valueTotalMem  = ((Long)totalMemAtt.getValue()).doubleValue();

	    if (valueProcessCpu == -1.0 || valueSystemCpu == -1.0 || valueFreeMem == -1.0 || valueTotalMem == -1.0) return;

	    processCpu = valueProcessCpu*100;
	    systemCpu = valueSystemCpu*100;
	    freeMemory = valueFreeMem/1024.0;
	    usedMemory = (valueTotalMem-valueFreeMem)/1024.0;

		Measurement[] measurements = {
			new Measurement("SystemMeasurementTask", "CPU Usage", processCpu, PROCESS_CPU_PROPERTY, new Date()),
			new Measurement("SystemMeasurementTask", "CPU Usage", systemCpu, SYSTEM_CPU_PROPERTY, new Date()),
			new Measurement("SystemMeasurementTask", "Physical memory", freeMemory, FREE_PHYS_MEM_PROPERTY, new Date()),
			new Measurement("SystemMeasurementTask", "Physical memory", usedMemory, USED_PHYS_MEM_PROPERTY, new Date())};
		
		remoteManagement.sendMeasurements(measurements);
	}
}
