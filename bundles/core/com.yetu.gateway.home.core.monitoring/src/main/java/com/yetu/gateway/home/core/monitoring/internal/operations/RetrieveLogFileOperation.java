package com.yetu.gateway.home.core.monitoring.internal.operations;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.model.FileDescriptor;
import com.yetu.gateway.home.core.monitoring.model.OperationDescriptor;
import com.yetu.gateway.home.core.monitoring.model.OperationListener;
import com.yetu.gateway.home.core.monitoring.model.OperationResult;
import com.yetu.gateway.home.core.monitoring.model.OperationResult.Status;

/**
 * This operation tries to locate a file on local harddrive and to upload it
 * to the remote management platform 
 * 
 * @author Sebastian Garn
 */
public class RetrieveLogFileOperation implements OperationListener {
	static String OPERATION_NAME = "getlog";
	static String PARAMETER_FILENAME = "filename";
	
	// this list contains a list of absolute files which can be requested
	static List<String> RETRIEVABLE_FILES = Arrays.asList("/var/log/messages", "/etc/lsb-release");
	
	// all files contained in the following listed directories may be request
	static List<String> RETRIEVABLE_DIRECTORIES = Arrays.asList("/opt/yetu/smarthome/runtime/logs",
																"/var/log/ui",
																"/var/log",
																"/var/log/update_engine");
	
	static ArrayList<String> parameters;
	
	OperationDescriptor descriptor;
	
	public RetrieveLogFileOperation() {
		parameters = new ArrayList<String>(); 
		parameters.add(PARAMETER_FILENAME);
		
		descriptor = new OperationDescriptor(OPERATION_NAME, parameters);
	}
	
	@Override
	public OperationDescriptor handledOperation() {
		return descriptor;
	}

	@Override
	public OperationResult execute(Map<String, String> parameters, RemoteGatewayManagementAccessor accessor) {
		String filename = parameters.get(PARAMETER_FILENAME);

		File file = new File(filename);
		if (!file.exists()) {
			return new OperationResult(Status.FAILURE, "Unable to open file "+filename);
		}

		if (!RETRIEVABLE_FILES.contains(file.getAbsolutePath().toString())) {
			if (!RETRIEVABLE_DIRECTORIES.contains(file.getParentFile().toString())) {
				return new OperationResult(Status.FAILURE, "Unable to upload file due to security restrictions. You can request the following files: "+RETRIEVABLE_FILES+ " or files within: "+RETRIEVABLE_DIRECTORIES);
			}
		}
			
		FileDescriptor fileDescriptor = accessor.sendFile(file, file.getName(), true);
		if (fileDescriptor == null) {
			return new OperationResult(Status.FAILURE, "Device was unable to upload file. Please check device user permissions.");
		}
		
		return new OperationResult(Status.SUCCESS, fileDescriptor.getPath());
	}
}
