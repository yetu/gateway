package com.yetu.gateway.home.core.monitoring.internal.measurements;

import java.util.Date;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.internal.types.PeriodicTask;
import com.yetu.gateway.home.core.monitoring.model.Measurement;
import com.yetu.gateway.home.core.monitoring.model.Property;

public class JVMMeasurementTask extends PeriodicTask {
	private RemoteGatewayManagementAccessor remoteManagement;
	
	Runtime runtime;
	
	private static Property FREE_MEMORY_PROPERTY = Property.create("Free memory","kb");
	private static Property ALLOCATED_MEMORY_PROPERTY = Property.create("Allocated memory","kb");
	
	public JVMMeasurementTask(long interval, RemoteGatewayManagementAccessor remoteManagement) {
		super(interval);
		
		this.remoteManagement = remoteManagement;
		runtime = Runtime.getRuntime();
	}

	@Override
	public void action() {
		Measurement[] measurements = {
				new Measurement("JVMMeasurementTask", "JVM memory", runtime.freeMemory()/1024.0, FREE_MEMORY_PROPERTY, new Date()),
				new Measurement("JVMMeasurementTask", "JVM memory", runtime.totalMemory()/1024.0, ALLOCATED_MEMORY_PROPERTY, new Date())
		};
		remoteManagement.sendMeasurements(measurements);
	}
}
