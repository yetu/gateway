package com.yetu.gateway.home.core.monitoring.model;

import java.util.Map;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;

/**
 * Implementing classes that get registered at remote management
 * can perform specific operations depending on the returned OperationDescriptor. 
 * 
 * @author Sebastian Garn
 *
 */
public interface OperationListener {
	/**
	 * Determines for which operation this listener is responsible.
	 * 
	 * @return OperationDescriptor
	 */
	public OperationDescriptor handledOperation();
	
	/**
	 * This method gets asynchronously and periodically called. Implement
	 * the behavior of the operation listener over here.
	 *
	 * @param Map<String, String> parameters
	 * @param RemoteGatewayManagementAccessor accessor
	 * @return OperationResult
	 */
	public OperationResult execute(Map<String, String> parameters, RemoteGatewayManagementAccessor accessor);
}