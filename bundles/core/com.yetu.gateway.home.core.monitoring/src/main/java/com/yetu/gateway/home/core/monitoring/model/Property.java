package com.yetu.gateway.home.core.monitoring.model;


public class Property {
	private String name;
	private String unit;
	
	public static Property create(String name, String unit) {
		return new Property(name, unit);
	}
	
	private Property(String name, String unit) {
		this.name = name;
		this.unit = unit;
	}
	
	public String getName() {
		return name;
	}
	
	public String getUnit() {
		return unit;
	}
}
