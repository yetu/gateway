package com.yetu.gateway.home.core.monitoring.model;

import java.util.List;

/**
 * Describes the name of an operation and a list of all necessary parameters
 * @author Sebastian Garn, Mario Camou
 */
public class OperationDescriptor {
	private String name;
	private List<String> parameters;
	
	public OperationDescriptor(String name, List<String> parameters) {
		this.name = name;
		this.parameters = parameters;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getParameters() {
		return parameters;
	}
	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}
}