package com.yetu.gateway.home.core.monitoring.internal.types;

import java.util.Calendar;

public abstract class PeriodicTask extends Thread {
	private long interval;
	private boolean stopped;

	public PeriodicTask(long interval) {
		this.interval = interval;
	}
	
	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public void terminate() {
		stopped = true;
	}
	
	public abstract void action();
	
	@Override
	public void run() {
		while (true) {
			if (stopped) return;
			
			long startTime = Calendar.getInstance().getTimeInMillis();
			action();
			long consumedTime = Calendar.getInstance().getTimeInMillis()-startTime;

			if (stopped) return;
			
			try {
				if (consumedTime < interval) {
					Thread.sleep(interval-consumedTime);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
