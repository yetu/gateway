package com.yetu.gateway.home.core.monitoring.model;

import java.util.Date;

public class Event extends ManagementItem {
	private String text;
	private String type;
	private Date time;
	
	public Event(String source, String text, String type, Date time) {
		super(source);
		this.text = text;
		this.type = type;
		this.time = time;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
}