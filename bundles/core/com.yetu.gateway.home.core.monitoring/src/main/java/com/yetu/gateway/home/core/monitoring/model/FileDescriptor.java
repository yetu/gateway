package com.yetu.gateway.home.core.monitoring.model;

/**
 * Describes the place of a file on remote management system 
 * 
 * @author Sebastian Garn
 */
public class FileDescriptor {
	private String path;

	public FileDescriptor(String path) {
		this.path = path;
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
