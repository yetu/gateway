package com.yetu.gateway.home.core.monitoring.model;

import java.util.Date;

public class Alarm extends ManagementItem {
	public enum Severity {
		WARNING,
		MINOR,
		MAJOR,
		CRITICAL
	}
	
	public enum Status {
		ACTIVE,
		INACTIVE
	}
	
	private String source;
	private String text;
	private String type;
	private Date time;
	private Severity severity;
	private Status status;
	
	public Alarm(String source, String text, String type, Date time, Severity severity, Status status) {
		super(source);
		this.text = text;
		this.type = type;
		this.time = time;
		this.severity = severity;
		this.status = status;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Severity getSeverity() {
		return severity;
	}
	public void setSeverity(Severity severity) {
		this.severity = severity;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
