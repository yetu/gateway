package com.yetu.gateway.home.core.monitoring.internal.operations;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.model.OperationDescriptor;
import com.yetu.gateway.home.core.monitoring.model.OperationListener;
import com.yetu.gateway.home.core.monitoring.model.OperationResult;
import com.yetu.gateway.home.core.monitoring.model.OperationResult.Status;

/**
 * Operation that lists the content of given directory 
 * 
 * @author Sebastian Garn
 */
public class ListDirectoryContentsOperation implements OperationListener {
	private static String OPERATION_NAME = "ls";
	private static String PARAMETER_DIRECTORY = "directory";
	
	// directories for which you can apply ls command
	private static List<String> ALLOWED_DIRECTORIES = Arrays.asList("/opt/yetu/smarthome/runtime/logs",
																	"/var/log/ui",
																	"/var/log",
																	"/var/log/update_engine");
	
	static ArrayList<String> parameters;
	
	OperationDescriptor descriptor;
	
	public ListDirectoryContentsOperation() {
		parameters = new ArrayList<String>(); 
		parameters.add(PARAMETER_DIRECTORY);
		
		descriptor = new OperationDescriptor(OPERATION_NAME,parameters);
	}
	
	@Override
	public OperationDescriptor handledOperation() {
		return descriptor;
	}

	@Override
	public OperationResult execute(Map<String, String> parameters, RemoteGatewayManagementAccessor accessor) {
		String directory = parameters.get(PARAMETER_DIRECTORY);

		File dir = new File(directory);

		if (!ALLOWED_DIRECTORIES.contains(dir.getAbsolutePath().toString())) {
			return new OperationResult(Status.FAILURE, "Unable to list content of directory due to security restrictions. You are allowed to list contents of the following directories: "+ALLOWED_DIRECTORIES);
		}
		
		if (!dir.exists() || !dir.isDirectory()) {
			return new OperationResult(Status.FAILURE, "Directory does not exist "+dir);
		}
		
		String directoryContent = "";
		
		
		for (File currFile : dir.listFiles()) {
			directoryContent+=currFile.getName()+" ["+currFile.length()+"b] ("+new Date(currFile.lastModified())+"), ";
		}
		
		if (directoryContent.endsWith(", ")) directoryContent = directoryContent.substring(0, directoryContent.length()-2);
		
		return new OperationResult(Status.SUCCESS, directoryContent);
	}
}
