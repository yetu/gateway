package com.yetu.gateway.home.core.monitoring.internal.measurements;

import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.util.Date;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.internal.types.PeriodicTask;
import com.yetu.gateway.home.core.monitoring.model.Measurement;
import com.yetu.gateway.home.core.monitoring.model.Property;

public class DiskUsageMeasurementTask extends PeriodicTask {
	private static Logger logger = LoggerFactory.getLogger(DiskUsageMeasurementTask.class);
	private RemoteGatewayManagementAccessor remoteManagement;
	
	public DiskUsageMeasurementTask(long interval, RemoteGatewayManagementAccessor remoteManagement) {
		super(interval);
		
		this.remoteManagement = remoteManagement;
		
		logger.debug("Periodically measuring the disk utilization");
	}

	@Override
	public void action() {
		LinkedList<Measurement> measurements = new LinkedList<Measurement>(); 

		int elemCount = 0;
		for (FileStore store : FileSystems.getDefault().getFileStores()) {
			try {
				if (store.name().startsWith("/dev")) {
			        double totalDisk = store.getTotalSpace();
			        double usedDisk = totalDisk - store.getUsableSpace();
			        double value;
	
			        if (totalDisk == 0.0) {
			        	value = 0.0;
			        } else {
			        	value = Math.round(10000.0*usedDisk/totalDisk)/100.0;
			        }
			        
			        // replace spaces within device name since cumulocity shows weird behaviour
			        measurements.add(
			        		new Measurement(
			        				"DiskUtilizationMeasurementTask",
			        				"Disk space usage",
			        				value,
			        				Property.create(store.name().replace("/", "."),"%"),
			        				new Date()
			        		)
			        );
			        
			        elemCount++;
				}
		    }
		    catch (Exception e) {
		        logger.error("Failed to measure disk utilization.",e);
		    }
		}

		logger.debug("Elem count: "+elemCount);
		logger.debug("Measurements count: "+measurements.size());
		logger.debug(measurements+"");
		remoteManagement.sendMeasurements(measurements.toArray(new Measurement[elemCount]));
	}
}
