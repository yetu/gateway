package com.yetu.gateway.home.core.monitoring.internal;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.internal.measurements.DiskUsageMeasurementTask;
import com.yetu.gateway.home.core.monitoring.internal.measurements.JVMMeasurementTask;
import com.yetu.gateway.home.core.monitoring.internal.measurements.SystemMeasurementTask;
import com.yetu.gateway.home.core.monitoring.internal.operations.GetProcessesInformationOperation;
import com.yetu.gateway.home.core.monitoring.internal.operations.ListDirectoryContentsOperation;
import com.yetu.gateway.home.core.monitoring.internal.operations.RetrieveLogFileOperation;
import com.yetu.gateway.home.core.monitoring.internal.operations.RetrieveLogFilePartsOperation;
import com.yetu.gateway.home.core.monitoring.model.Event;

/**
 * This class uses a remote gateway management accessor to track the state of the gateway.
 * This includes e.g. measurement of cpu load, memory consumption and state of loaded bundles 
 * 
 * @author Sebastian Garn
 */
public class GatewayStateTracker {
	private static Logger logger = LoggerFactory.getLogger(GatewayStateTracker.class);
	private RemoteGatewayManagementAccessor remoteManagement;
	
	private static long DEFAULT_MEASURMENT_INTERVAL = 20000;

	private SystemMeasurementTask systemMeasurementTask;
	private JVMMeasurementTask jVMMeasurementTask;
	private DiskUsageMeasurementTask diskUsageMeasurementTask;
	
	private RetrieveLogFileOperation retrieveLogFileOperation;
	private RetrieveLogFilePartsOperation retrieveLogFilePartsOperation;
	private ListDirectoryContentsOperation listDirectoryContentsOperation;
	private GetProcessesInformationOperation processesInformationOperation;


	public void setup() {
		remoteManagement.sendEvent(new Event(GatewayStateTracker.class.getName(), "Remote management accessor bound.", "Info", new Date()));

		retrieveLogFileOperation = new RetrieveLogFileOperation();
		remoteManagement.registerOperationListener(retrieveLogFileOperation);
		
		retrieveLogFilePartsOperation = new RetrieveLogFilePartsOperation();
		remoteManagement.registerOperationListener(retrieveLogFilePartsOperation);
		
		listDirectoryContentsOperation = new ListDirectoryContentsOperation();
		remoteManagement.registerOperationListener(listDirectoryContentsOperation);
		
		processesInformationOperation = new GetProcessesInformationOperation();
		remoteManagement.registerOperationListener(processesInformationOperation);
		
		systemMeasurementTask = new SystemMeasurementTask(DEFAULT_MEASURMENT_INTERVAL, remoteManagement);
		systemMeasurementTask.start();

		jVMMeasurementTask = new JVMMeasurementTask(DEFAULT_MEASURMENT_INTERVAL, remoteManagement);
		jVMMeasurementTask.start();
		
		diskUsageMeasurementTask = new DiskUsageMeasurementTask(DEFAULT_MEASURMENT_INTERVAL*6, remoteManagement);
		diskUsageMeasurementTask.start();
	}
	
	public void addRemoteGatewayManagementAccessor(RemoteGatewayManagementAccessor accessor) {
		logger.debug("RemoteManagementAccessor added. Setting up GatewayStateTracker.");
		
		remoteManagement = accessor;
		setup();
	}
	
	public void removeRemoteGatewayManagementAccessor(RemoteGatewayManagementAccessor accessor) {
		remoteManagement.unregisterOperationListener(retrieveLogFileOperation);
		remoteManagement.unregisterOperationListener(retrieveLogFilePartsOperation);
		remoteManagement.unregisterOperationListener(listDirectoryContentsOperation);
		remoteManagement.unregisterOperationListener(processesInformationOperation);

		systemMeasurementTask.terminate();
		jVMMeasurementTask.terminate();
		diskUsageMeasurementTask.terminate();
		
		remoteManagement = null;
	}
	
	public void onActivate() {
		logger.debug("GatewayStateTracker activated");
	}
	
	public void onDeactivate() {
		logger.debug("GatewayStateTracker activated");
	}
}
