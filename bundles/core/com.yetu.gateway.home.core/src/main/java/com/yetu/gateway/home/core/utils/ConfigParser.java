package com.yetu.gateway.home.core.utils;

import java.util.Dictionary;
import java.util.NoSuchElementException;

/**
 * Simple class to retrieve configuration values from a configuration dictionary.
 * @author Sebastian Garn
 *
 */
public class ConfigParser {
	private Dictionary<String, ?> properties;
	
	public ConfigParser(Dictionary<String, ?> properties) {
		this.properties = properties;
	}
	
	private Object getElemValue(String key) throws NoSuchElementException {
		Object result = properties.get(key);
		
		if (result == null) {
			throw new NoSuchElementException("Element '"+key+"' not found");
		}
		
		return result;
	}
	
	/**
	 * Retrieves a boolean value. If not found an exception will be thrown.
	 * @param String key
	 * @return value in list
	 */
	public boolean getBooleanValue(String key) throws NoSuchElementException, ClassCastException {
		Object result = getElemValue(key);
		
		try {
			return Boolean.parseBoolean((String)result);
		} catch(ClassCastException e) {
			throw new NoSuchElementException("Expected: boolean, Found: "+result.getClass());
		}
	}
	
	/**
	 * Retrieves a boolean value. If not found the method will return the default value.
	 * @param String key
	 * @param boolean defaultValue
	 * @return value in list or default value
	 */
	public boolean getValue(String key, boolean defaultValue) throws ClassCastException {
		if (properties.get(key) != null) {
			return Boolean.parseBoolean((String)properties.get(key));
		}
		
		return defaultValue;
	}
	
	/**
	 * String
	 */
	
	/**
	 * Retrieves a string value. If not found an exception will be thrown.
	 * @param String key
	 * @return value in list
	 */
	public String getStringValue(String key) throws NoSuchElementException, ClassCastException {
		Object result = getElemValue(key);
		
		try {
			return (String)result;
		} catch(ClassCastException e) {
			throw new NoSuchElementException("Expected: String, Found: "+result.getClass());
		}
	}
	
	/**
	 * Retrieves a String value. If not found the method will return the default value.
	 * @param String key
	 * @param String defaultValue
	 * @return value in list or default value
	 */
	public String getValue(String key, String defaultValue) throws ClassCastException {
		if (properties.get(key) != null) {
			return (String)properties.get(key);
		}
		
		return defaultValue;
	}
	
	/**
	 * int
	 */
	
	/**
	 * Retrieves an integer value. If not found an exception will be thrown.
	 * @param String key
	 * @return value in list
	 */
	public int getIntegerValue(String key) throws NoSuchElementException, ClassCastException {
		Object result = getElemValue(key);
		
		try {
			return Integer.parseInt((String)result);
		} catch(ClassCastException e) {
			throw new NoSuchElementException("Expected: int, Found: "+result.getClass());
		}
	}
	
	/**
	 * Retrieves an integer value. If not found the method will return the default value.
	 * @param String key
	 * @param int defaultValue
	 * @return value in list or default value
	 */
	public int getValue(String key, int defaultValue) throws ClassCastException {
		if (properties.get(key) != null) {
			return Integer.parseInt((String)properties.get(key));
		}
		
		return defaultValue;
	}
	
	/**
	 * double
	 */
	
	/**
	 * Retrieves a double value. If not found an exception will be thrown.
	 * @param String key
	 * @return value in list
	 */
	public double getDoubleValue(String key) throws NoSuchElementException, ClassCastException {
		Object result = getElemValue(key);
		
		try {
			return Double.parseDouble((String)result);
		} catch(ClassCastException e) {
			throw new NoSuchElementException("Expected: int, Found: "+result.getClass());
		}
	}
	
	/**
	 * Retrieves a double value. If not found the method will return the default value.
	 * @param String key
	 * @param double defaultValue
	 * @return value in list or default value
	 */
	public double getValue(String key, double defaultValue) throws ClassCastException {
		if (properties.get(key) != null) {
			return Double.parseDouble((String)properties.get(key));
		}
		
		return defaultValue;
	}
}
