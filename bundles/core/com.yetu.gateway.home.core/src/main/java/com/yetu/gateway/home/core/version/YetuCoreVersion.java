package com.yetu.gateway.home.core.version;

public class YetuCoreVersion {

	public static String getImplementationVersion(){
		return YetuCoreVersion.class.getPackage().getImplementationVersion();
	}
	
	public static String getImplementationVendor(){
		return YetuCoreVersion.class.getPackage().getImplementationVendor();
	}
	
	public static String getImplementationTitle(){
		return YetuCoreVersion.class.getPackage().getImplementationTitle();
	}
	
	
	public static void main(String[] args){		
		String version = getImplementationVersion();
		if (version != null){			
			System.out.println("version "+version);
		} else {
			System.out.println("No version information available.");
		}
							
	}
}
