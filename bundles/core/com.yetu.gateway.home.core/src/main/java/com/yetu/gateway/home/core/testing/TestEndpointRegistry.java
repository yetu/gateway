package com.yetu.gateway.home.core.testing;

import java.util.ArrayList;

public interface TestEndpointRegistry {
	public void addTestEndpoint(TestEndpoint endpoint);
	public void removeTestEndpoint(TestEndpoint endpoint);
	public ArrayList<TestEndpoint> getTestEndpoints();
}
