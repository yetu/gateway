package com.yetu.gateway.home.core.testing;

import java.util.Map;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This service allows to access gateway core functionalities via test api
 * 
 * @author Sebastian Garn
 */
public class GatewayCoreTestAccess {
	private static final Logger logger = LoggerFactory.getLogger(GatewayCoreTestAccess.class);

	private static String BASE_NAME = "com.yetu.gateway.home.core";
	
	private static String PARAM_KEY_BUNDLE_SYMBOLIC_NAME = "name";
	
	public void onActivate() {
		logger.debug("Activated");
	}
	
	TestEndpoint restartBundleEndpoint = new TestEndpoint(BASE_NAME, "restartBundle", TestEndpoint.Method.UPDATE, new String[]{PARAM_KEY_BUNDLE_SYMBOLIC_NAME}, "Restarts bundle with given symbolic name") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			logger.debug("Restart bundle "+parameters.get(PARAM_KEY_BUNDLE_SYMBOLIC_NAME)[0]);
			try {
				restartBundle(parameters.get(PARAM_KEY_BUNDLE_SYMBOLIC_NAME)[0], FrameworkUtil.getBundle(this.getClass()).getBundleContext());
			} catch(Exception e) {
				throw new Exception("Failed to restart bundle.", e);
			}

			return "";
		}
	};
	
	public void addTestEndpointRegistry(TestEndpointRegistry registry) {
		registry.addTestEndpoint(restartBundleEndpoint);
	}
	
	public void removeTestEndpointRegistry(TestEndpointRegistry registry) {
		registry.removeTestEndpoint(restartBundleEndpoint);
	}
	
	private void restartBundle(String symbolicName, BundleContext currContext) throws Exception {
		for (Bundle currBundle : currContext.getBundles()) {
			if (currBundle.getSymbolicName().compareTo(symbolicName) == 0) {
					currBundle.stop();
					currBundle.start();
					return;
			}
		}
		
		throw new Exception("Could not find bundle with symbolic name "+symbolicName);
	}
}
