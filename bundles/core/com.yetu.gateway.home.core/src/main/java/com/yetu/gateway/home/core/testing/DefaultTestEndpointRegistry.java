package com.yetu.gateway.home.core.testing;

import java.util.ArrayList;

public class DefaultTestEndpointRegistry implements TestEndpointRegistry {
	ArrayList<TestEndpoint> endpoints = new ArrayList<TestEndpoint>(); 
	
	@Override
	public void addTestEndpoint(TestEndpoint endpoint) {
		synchronized (endpoints) {
			if (!endpoints.contains(endpoint)) {
				endpoints.add(endpoint);
			}
		}
	}

	@Override
	public void removeTestEndpoint(TestEndpoint endpoint) {
		synchronized (endpoints) {
			endpoints.remove(endpoint);	
		}
	}

	@Override
	public ArrayList<TestEndpoint> getTestEndpoints() {
		return endpoints;
	}

}
