package com.yetu.gateway.home.core.testing;

import java.util.Map;

/**
 * Describes a test endpoint. Each endpoint has usually one specific task like creating a dummy thing or
 * setting a property of a thing
 * 
 * @author Sebastian Garn
 *
 */
public abstract class TestEndpoint {
	private String endpointId;
	private String operation;
	private Method method;
	private String description;
	private String[] expectedParameters;
	
	public enum Method {
		CREATE,
		GET,
		UPDATE,
		REMOVE
	}
	
	
	public TestEndpoint(String endpointId, String operation, Method method, String[] expectedParameters, String description) {
		this.endpointId = endpointId;
		this.operation = operation;
		this.method = method;
		this.description = description;
		this.expectedParameters = expectedParameters;
	}
	
	public String getEndpointId() {
		return endpointId;
	}
	
	public String getOperation() {
		return operation;
	}
	
	public Method getMethod() {
		return method;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String[] getExpectedParameters() {
		return expectedParameters;
	}
	
	/**
	 * Checks whether request contains all necessary keys
	 * @param parameters Map<String, String[]> - key/values of request
	 * @return
	 */
	public boolean parametersFulfilled(Map<String, String[]> parameters) {
		for (String currParam : expectedParameters) {
			if (!parameters.containsKey(currParam)) {
				return false;
			}
		}

		return true;
	}
	
	public abstract String handle(Map<String, String[]> parameters) throws Exception;
}
