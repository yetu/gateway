/**
 */
package com.yetu.abstraction.spec;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Capability Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.yetu.abstraction.spec.CapabilitySpecification#getId <em>Id</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.CapabilitySpecification#getProperties <em>Properties</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.CapabilitySpecification#getActions <em>Actions</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.CapabilitySpecification#getEvents <em>Events</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.yetu.abstraction.spec.ThingSpecPackage#getCapabilitySpecification()
 * @model
 * @generated
 */
public interface CapabilitySpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getCapabilitySpecification_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.spec.CapabilitySpecification#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.spec.PropertySpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getCapabilitySpecification_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<PropertySpecification> getProperties();

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.spec.ActionSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getCapabilitySpecification_Actions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActionSpecification> getActions();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.spec.EventSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getCapabilitySpecification_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventSpecification> getEvents();

} // CapabilitySpecification
