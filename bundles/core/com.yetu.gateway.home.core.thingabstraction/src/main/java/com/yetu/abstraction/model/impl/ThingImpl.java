/**
 */
package com.yetu.abstraction.model.impl;

import com.yetu.abstraction.model.Attribute;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.ComponentType;
import com.yetu.abstraction.model.ConnectionDetails;
import com.yetu.abstraction.model.Thing;
import com.yetu.abstraction.model.ThingModelPackage;
import com.yetu.abstraction.model.ThingStatus;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Thing</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getConnectionDetails <em>Connection Details</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getComponents <em>Components</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getMainComponent <em>Main Component</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getDisplayType <em>Display Type</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.impl.ThingImpl#getStatus <em>Status</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ThingImpl extends MinimalEObjectImpl.Container implements Thing {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getManufacturer() <em>Manufacturer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturer()
	 * @generated
	 * @ordered
	 */
	protected static final String MANUFACTURER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getManufacturer() <em>Manufacturer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturer()
	 * @generated
	 * @ordered
	 */
	protected String manufacturer = MANUFACTURER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> properties;

	/**
	 * The cached value of the '{@link #getConnectionDetails() <em>Connection Details</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectionDetails()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectionDetails> connectionDetails;

	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<Component> components;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMainComponent() <em>Main Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainComponent()
	 * @generated
	 * @ordered
	 */
	protected Component mainComponent;

	/**
	 * The cached value of the '{@link #getDisplayType() <em>Display Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayType()
	 * @generated
	 * @ordered
	 */
	protected ComponentType displayType;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final ThingStatus STATUS_EDEFAULT = ThingStatus.UNKNOWN;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected ThingStatus status = STATUS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ThingModelPackage.Literals.THING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingModelPackage.THING__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManufacturer(String newManufacturer) {
		String oldManufacturer = manufacturer;
		manufacturer = newManufacturer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingModelPackage.THING__MANUFACTURER, oldManufacturer, manufacturer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<Attribute>(Attribute.class, this, ThingModelPackage.THING__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectionDetails> getConnectionDetails() {
		if (connectionDetails == null) {
			connectionDetails = new EObjectContainmentEList<ConnectionDetails>(ConnectionDetails.class, this, ThingModelPackage.THING__CONNECTION_DETAILS);
		}
		return connectionDetails;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Component> getComponents() {
		if (components == null) {
			components = new EObjectContainmentWithInverseEList<Component>(Component.class, this, ThingModelPackage.THING__COMPONENTS, ThingModelPackage.COMPONENT__THING);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingModelPackage.THING__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getMainComponent() {
		if (mainComponent != null && mainComponent.eIsProxy()) {
			InternalEObject oldMainComponent = (InternalEObject)mainComponent;
			mainComponent = (Component)eResolveProxy(oldMainComponent);
			if (mainComponent != oldMainComponent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ThingModelPackage.THING__MAIN_COMPONENT, oldMainComponent, mainComponent));
			}
		}
		return mainComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component basicGetMainComponent() {
		return mainComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setMainComponent(Component newMainComponent) {
		Component oldMainComponent = mainComponent;
		mainComponent = newMainComponent;
		if (displayType == null){
			setDisplayType(mainComponent.getType());
		}
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingModelPackage.THING__MAIN_COMPONENT, oldMainComponent, mainComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType getDisplayType() {
		if (displayType != null && displayType.eIsProxy()) {
			InternalEObject oldDisplayType = (InternalEObject)displayType;
			displayType = (ComponentType)eResolveProxy(oldDisplayType);
			if (displayType != oldDisplayType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ThingModelPackage.THING__DISPLAY_TYPE, oldDisplayType, displayType));
			}
		}
		return displayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType basicGetDisplayType() {
		return displayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayType(ComponentType newDisplayType) {
		ComponentType oldDisplayType = displayType;
		displayType = newDisplayType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingModelPackage.THING__DISPLAY_TYPE, oldDisplayType, displayType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThingStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(ThingStatus newStatus) {
		ThingStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingModelPackage.THING__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Component getComponent(String componentId) {
		for (Component component : this.getComponents()){
			if (component.getId().equalsIgnoreCase(componentId)){
				return component;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ThingModelPackage.THING__COMPONENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getComponents()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ThingModelPackage.THING__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case ThingModelPackage.THING__CONNECTION_DETAILS:
				return ((InternalEList<?>)getConnectionDetails()).basicRemove(otherEnd, msgs);
			case ThingModelPackage.THING__COMPONENTS:
				return ((InternalEList<?>)getComponents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ThingModelPackage.THING__NAME:
				return getName();
			case ThingModelPackage.THING__MANUFACTURER:
				return getManufacturer();
			case ThingModelPackage.THING__PROPERTIES:
				return getProperties();
			case ThingModelPackage.THING__CONNECTION_DETAILS:
				return getConnectionDetails();
			case ThingModelPackage.THING__COMPONENTS:
				return getComponents();
			case ThingModelPackage.THING__ID:
				return getId();
			case ThingModelPackage.THING__MAIN_COMPONENT:
				if (resolve) return getMainComponent();
				return basicGetMainComponent();
			case ThingModelPackage.THING__DISPLAY_TYPE:
				if (resolve) return getDisplayType();
				return basicGetDisplayType();
			case ThingModelPackage.THING__STATUS:
				return getStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ThingModelPackage.THING__NAME:
				setName((String)newValue);
				return;
			case ThingModelPackage.THING__MANUFACTURER:
				setManufacturer((String)newValue);
				return;
			case ThingModelPackage.THING__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends Attribute>)newValue);
				return;
			case ThingModelPackage.THING__CONNECTION_DETAILS:
				getConnectionDetails().clear();
				getConnectionDetails().addAll((Collection<? extends ConnectionDetails>)newValue);
				return;
			case ThingModelPackage.THING__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends Component>)newValue);
				return;
			case ThingModelPackage.THING__ID:
				setId((String)newValue);
				return;
			case ThingModelPackage.THING__MAIN_COMPONENT:
				setMainComponent((Component)newValue);
				return;
			case ThingModelPackage.THING__DISPLAY_TYPE:
				setDisplayType((ComponentType)newValue);
				return;
			case ThingModelPackage.THING__STATUS:
				setStatus((ThingStatus)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ThingModelPackage.THING__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ThingModelPackage.THING__MANUFACTURER:
				setManufacturer(MANUFACTURER_EDEFAULT);
				return;
			case ThingModelPackage.THING__PROPERTIES:
				getProperties().clear();
				return;
			case ThingModelPackage.THING__CONNECTION_DETAILS:
				getConnectionDetails().clear();
				return;
			case ThingModelPackage.THING__COMPONENTS:
				getComponents().clear();
				return;
			case ThingModelPackage.THING__ID:
				setId(ID_EDEFAULT);
				return;
			case ThingModelPackage.THING__MAIN_COMPONENT:
				setMainComponent((Component)null);
				return;
			case ThingModelPackage.THING__DISPLAY_TYPE:
				setDisplayType((ComponentType)null);
				return;
			case ThingModelPackage.THING__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ThingModelPackage.THING__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ThingModelPackage.THING__MANUFACTURER:
				return MANUFACTURER_EDEFAULT == null ? manufacturer != null : !MANUFACTURER_EDEFAULT.equals(manufacturer);
			case ThingModelPackage.THING__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case ThingModelPackage.THING__CONNECTION_DETAILS:
				return connectionDetails != null && !connectionDetails.isEmpty();
			case ThingModelPackage.THING__COMPONENTS:
				return components != null && !components.isEmpty();
			case ThingModelPackage.THING__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ThingModelPackage.THING__MAIN_COMPONENT:
				return mainComponent != null;
			case ThingModelPackage.THING__DISPLAY_TYPE:
				return displayType != null;
			case ThingModelPackage.THING__STATUS:
				return status != STATUS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ThingModelPackage.THING___GET_COMPONENT__STRING:
				return getComponent((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", manufacturer: ");
		result.append(manufacturer);
		result.append(", id: ");
		result.append(id);
		result.append(", status: ");
		result.append(status);
		result.append(')');
		return result.toString();
	}

} //ThingImpl
