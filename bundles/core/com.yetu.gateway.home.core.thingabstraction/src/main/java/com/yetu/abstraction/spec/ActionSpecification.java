/**
 */
package com.yetu.abstraction.spec;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.yetu.abstraction.spec.ActionSpecification#getName <em>Name</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.ActionSpecification#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.yetu.abstraction.spec.ThingSpecPackage#getActionSpecification()
 * @model
 * @generated
 */
public interface ActionSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getActionSpecification_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.spec.ActionSpecification#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.spec.ActionParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getActionSpecification_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActionParameterSpecification> getParameters();

} // ActionSpecification
