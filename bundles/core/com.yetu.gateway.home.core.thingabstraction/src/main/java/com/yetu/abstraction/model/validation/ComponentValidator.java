/**
 *
 * $Id$
 */
package com.yetu.abstraction.model.validation;

import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.ComponentType;

import com.yetu.abstraction.model.Thing;
import org.eclipse.emf.common.util.EList;

/**
 * A sample validator interface for {@link com.yetu.abstraction.model.Component}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ComponentValidator {
	boolean validate();

	boolean validateName(String value);
	boolean validateType(ComponentType value);
	boolean validateId(String value);
	boolean validateCapabilities(EList<Capability> value);

	boolean validateThing(Thing value);
}
