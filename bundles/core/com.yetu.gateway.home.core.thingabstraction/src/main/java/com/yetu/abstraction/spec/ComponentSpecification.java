/**
 */
package com.yetu.abstraction.spec;

import com.yetu.abstraction.model.ComponentType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.yetu.abstraction.spec.ComponentSpecification#getType <em>Type</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.ComponentSpecification#getCapabilities <em>Capabilities</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.yetu.abstraction.spec.ThingSpecPackage#getComponentSpecification()
 * @model
 * @generated
 */
public interface ComponentSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ComponentType)
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getComponentSpecification_Type()
	 * @model
	 * @generated
	 */
	ComponentType getType();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.spec.ComponentSpecification#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ComponentType value);

	/**
	 * Returns the value of the '<em><b>Capabilities</b></em>' reference list.
	 * The list contents are of type {@link com.yetu.abstraction.spec.CapabilitySpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capabilities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capabilities</em>' reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getComponentSpecification_Capabilities()
	 * @model
	 * @generated
	 */
	EList<CapabilitySpecification> getCapabilities();

} // ComponentSpecification
