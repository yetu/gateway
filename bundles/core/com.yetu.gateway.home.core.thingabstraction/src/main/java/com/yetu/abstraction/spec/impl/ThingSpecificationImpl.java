/**
 */
package com.yetu.abstraction.spec.impl;

import com.yetu.abstraction.model.Action;
import com.yetu.abstraction.model.ActionParameter;
import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.ComponentType;
import com.yetu.abstraction.model.Event;
import com.yetu.abstraction.model.ThingModelFactory;
import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.Unit;
import com.yetu.abstraction.spec.ActionParameterSpecification;
import com.yetu.abstraction.spec.ActionSpecification;
import com.yetu.abstraction.spec.CapabilitySpecification;
import com.yetu.abstraction.spec.ComponentSpecification;
import com.yetu.abstraction.spec.EventSpecification;
import com.yetu.abstraction.spec.PropertySpecification;
import com.yetu.abstraction.spec.ThingSpecPackage;
import com.yetu.abstraction.spec.ThingSpecification;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Thing Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.yetu.abstraction.spec.impl.ThingSpecificationImpl#getCapabilitySpecifications <em>Capability Specifications</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.impl.ThingSpecificationImpl#getUnits <em>Units</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.impl.ThingSpecificationImpl#getComponentSpecifications <em>Component Specifications</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.impl.ThingSpecificationImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.impl.ThingSpecificationImpl#getComponentTypes <em>Component Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ThingSpecificationImpl extends MinimalEObjectImpl.Container implements ThingSpecification {
	/**
	 * The cached value of the '{@link #getCapabilitySpecifications() <em>Capability Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilitySpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<CapabilitySpecification> capabilitySpecifications;

	/**
	 * The cached value of the '{@link #getUnits() <em>Units</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnits()
	 * @generated
	 * @ordered
	 */
	protected EList<Unit> units;

	/**
	 * The cached value of the '{@link #getComponentSpecifications() <em>Component Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentSpecification> componentSpecifications;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = "0.1";

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComponentTypes() <em>Component Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentType> componentTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThingSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ThingSpecPackage.Literals.THING_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CapabilitySpecification> getCapabilitySpecifications() {
		if (capabilitySpecifications == null) {
			capabilitySpecifications = new EObjectContainmentEList<CapabilitySpecification>(CapabilitySpecification.class, this, ThingSpecPackage.THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS);
		}
		return capabilitySpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Unit> getUnits() {
		if (units == null) {
			units = new EObjectContainmentEList<Unit>(Unit.class, this, ThingSpecPackage.THING_SPECIFICATION__UNITS);
		}
		return units;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentSpecification> getComponentSpecifications() {
		if (componentSpecifications == null) {
			componentSpecifications = new EObjectContainmentEList<ComponentSpecification>(ComponentSpecification.class, this, ThingSpecPackage.THING_SPECIFICATION__COMPONENT_SPECIFICATIONS);
		}
		return componentSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingSpecPackage.THING_SPECIFICATION__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentType> getComponentTypes() {
		if (componentTypes == null) {
			componentTypes = new EObjectContainmentEList<ComponentType>(ComponentType.class, this, ThingSpecPackage.THING_SPECIFICATION__COMPONENT_TYPES);
		}
		return componentTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Capability createCapability(String capabilityId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Component createComponent(String id, ComponentType componentType) {
		ComponentSpecification cSpec = getComponentSpecification(componentType);
		return createComponent(id, cSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ComponentSpecification getComponentSpecification(ComponentType componentType) {		
		for (ComponentSpecification compSpec : getComponentSpecifications()){
			if (compSpec.getType().getId().equalsIgnoreCase(componentType.getId())){
				return compSpec;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ComponentSpecification getComponentSpecification(String componentTypeId) {
		ComponentType compType = getComponentType(componentTypeId);
		if (compType != null){
			return getComponentSpecification(compType);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Component createComponent(String id, ComponentSpecification componentSpecification) {
		Component component = ThingModelFactory.eINSTANCE.createComponent(id, componentSpecification.getType());				
		for (CapabilitySpecification capSpec : componentSpecification.getCapabilities()){				
			component.getCapabilities().add(createCapability(capSpec));
		}		
		return component;
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Unit getUnit(String unitId) {
		for (Unit unit : getUnits()){
			if (unit.getId().equalsIgnoreCase(unitId)){
				return unit;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ComponentType getComponentType(String componentTypeId) {
		for (ComponentType ctype : getComponentTypes()){
			if (ctype.getId().equalsIgnoreCase(componentTypeId)){
				return ctype;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CapabilitySpecification getCapabilitySpecification(String capabilityId) {
		for (CapabilitySpecification cSpec : getCapabilitySpecifications()){
			if (cSpec.getId().equalsIgnoreCase(capabilityId)){
				return cSpec;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Capability createCapability(CapabilitySpecification capabilitySpecification) {
		Capability capability = ThingModelFactory.eINSTANCE.createCapability(capabilitySpecification.getId());
		
		for (PropertySpecification propSpec : capabilitySpecification.getProperties()){
			capability.getProperties().add(createProperty(propSpec));
		}
		
		for (EventSpecification evntSpec : capabilitySpecification.getEvents()){
			capability.getEvents().add(createEvent(evntSpec));
		}
		
		for (ActionSpecification actionSpec : capabilitySpecification.getActions()){
			capability.getActions().add(createAction(actionSpec));
		}
		
		return capability;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String componentTypeId() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	protected Action createAction(ActionSpecification specification){
		Action action = ThingModelFactory.eINSTANCE.createAction();
		action.setName(specification.getName());
		
		for (ActionParameterSpecification actionParamSpec : specification.getParameters()){
			action.getParameter().add(createActionParameter(actionParamSpec));
		}
		
		return action;
	}
	
	
	protected ActionParameter createActionParameter(ActionParameterSpecification specification){
		ActionParameter parameter = ThingModelFactory.eINSTANCE.createActionParameter();
		
		parameter.setName(specification.getName());
		parameter.setUnit(specification.getUnit());
		
		return parameter;
	}
	
	protected Event createEvent(EventSpecification specification){
		Event event = ThingModelFactory.eINSTANCE.createEvent();
			
		return event;
	}
	
	
	protected Property createProperty(PropertySpecification specification){
		Property property = ThingModelFactory.eINSTANCE.createProperty();
		property.setName(specification.getName());
		property.setWritable(specification.isWritable());
		property.setUnit(specification.getUnit());
		
		return property;
	}
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ThingSpecPackage.THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS:
				return ((InternalEList<?>)getCapabilitySpecifications()).basicRemove(otherEnd, msgs);
			case ThingSpecPackage.THING_SPECIFICATION__UNITS:
				return ((InternalEList<?>)getUnits()).basicRemove(otherEnd, msgs);
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_SPECIFICATIONS:
				return ((InternalEList<?>)getComponentSpecifications()).basicRemove(otherEnd, msgs);
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_TYPES:
				return ((InternalEList<?>)getComponentTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ThingSpecPackage.THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS:
				return getCapabilitySpecifications();
			case ThingSpecPackage.THING_SPECIFICATION__UNITS:
				return getUnits();
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_SPECIFICATIONS:
				return getComponentSpecifications();
			case ThingSpecPackage.THING_SPECIFICATION__VERSION:
				return getVersion();
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_TYPES:
				return getComponentTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ThingSpecPackage.THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS:
				getCapabilitySpecifications().clear();
				getCapabilitySpecifications().addAll((Collection<? extends CapabilitySpecification>)newValue);
				return;
			case ThingSpecPackage.THING_SPECIFICATION__UNITS:
				getUnits().clear();
				getUnits().addAll((Collection<? extends Unit>)newValue);
				return;
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_SPECIFICATIONS:
				getComponentSpecifications().clear();
				getComponentSpecifications().addAll((Collection<? extends ComponentSpecification>)newValue);
				return;
			case ThingSpecPackage.THING_SPECIFICATION__VERSION:
				setVersion((String)newValue);
				return;
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_TYPES:
				getComponentTypes().clear();
				getComponentTypes().addAll((Collection<? extends ComponentType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ThingSpecPackage.THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS:
				getCapabilitySpecifications().clear();
				return;
			case ThingSpecPackage.THING_SPECIFICATION__UNITS:
				getUnits().clear();
				return;
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_SPECIFICATIONS:
				getComponentSpecifications().clear();
				return;
			case ThingSpecPackage.THING_SPECIFICATION__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_TYPES:
				getComponentTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ThingSpecPackage.THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS:
				return capabilitySpecifications != null && !capabilitySpecifications.isEmpty();
			case ThingSpecPackage.THING_SPECIFICATION__UNITS:
				return units != null && !units.isEmpty();
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_SPECIFICATIONS:
				return componentSpecifications != null && !componentSpecifications.isEmpty();
			case ThingSpecPackage.THING_SPECIFICATION__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ThingSpecPackage.THING_SPECIFICATION__COMPONENT_TYPES:
				return componentTypes != null && !componentTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ThingSpecPackage.THING_SPECIFICATION___CREATE_CAPABILITY__STRING:
				return createCapability((String)arguments.get(0));
			case ThingSpecPackage.THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTTYPE:
				return createComponent((String)arguments.get(0), (ComponentType)arguments.get(1));
			case ThingSpecPackage.THING_SPECIFICATION___GET_COMPONENT_SPECIFICATION__COMPONENTTYPE:
				return getComponentSpecification((ComponentType)arguments.get(0));
			case ThingSpecPackage.THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTSPECIFICATION:
				return createComponent((String)arguments.get(0), (ComponentSpecification)arguments.get(1));
			case ThingSpecPackage.THING_SPECIFICATION___CREATE_CAPABILITY__CAPABILITYSPECIFICATION:
				return createCapability((CapabilitySpecification)arguments.get(0));
			case ThingSpecPackage.THING_SPECIFICATION___GET_UNIT__STRING:
				return getUnit((String)arguments.get(0));
			case ThingSpecPackage.THING_SPECIFICATION___GET_COMPONENT_TYPE__STRING:
				return getComponentType((String)arguments.get(0));
			case ThingSpecPackage.THING_SPECIFICATION___COMPONENT_TYPE_ID:
				return componentTypeId();
			case ThingSpecPackage.THING_SPECIFICATION___GET_CAPABILITY_SPECIFICATION__STRING:
				return getCapabilitySpecification((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (version: ");
		result.append(version);
		result.append(')');
		return result.toString();
	}

} //ThingSpecificationImpl
