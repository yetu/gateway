/**
 */
package com.yetu.abstraction.spec.impl;

import com.yetu.abstraction.model.ThingModelPackage;
import com.yetu.abstraction.model.impl.ThingModelPackageImpl;
import com.yetu.abstraction.spec.ActionParameterSpecification;
import com.yetu.abstraction.spec.ActionSpecification;
import com.yetu.abstraction.spec.CapabilitySpecification;
import com.yetu.abstraction.spec.ComponentSpecification;
import com.yetu.abstraction.spec.EventSpecification;
import com.yetu.abstraction.spec.PropertySpecification;
import com.yetu.abstraction.spec.ThingSpecFactory;
import com.yetu.abstraction.spec.ThingSpecPackage;
import com.yetu.abstraction.spec.ThingSpecification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ThingSpecPackageImpl extends EPackageImpl implements ThingSpecPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass thingSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass capabilitySpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertySpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionParameterSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentSpecificationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ThingSpecPackageImpl() {
		super(eNS_URI, ThingSpecFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ThingSpecPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ThingSpecPackage init() {
		if (isInited) return (ThingSpecPackage)EPackage.Registry.INSTANCE.getEPackage(ThingSpecPackage.eNS_URI);

		// Obtain or create and register package
		ThingSpecPackageImpl theThingSpecPackage = (ThingSpecPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ThingSpecPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ThingSpecPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		ThingModelPackageImpl theThingModelPackage = (ThingModelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ThingModelPackage.eNS_URI) instanceof ThingModelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ThingModelPackage.eNS_URI) : ThingModelPackage.eINSTANCE);

		// Create package meta-data objects
		theThingSpecPackage.createPackageContents();
		theThingModelPackage.createPackageContents();

		// Initialize created meta-data
		theThingSpecPackage.initializePackageContents();
		theThingModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theThingSpecPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ThingSpecPackage.eNS_URI, theThingSpecPackage);
		return theThingSpecPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getThingSpecification() {
		return thingSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThingSpecification_CapabilitySpecifications() {
		return (EReference)thingSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThingSpecification_Units() {
		return (EReference)thingSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThingSpecification_ComponentSpecifications() {
		return (EReference)thingSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThingSpecification_Version() {
		return (EAttribute)thingSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThingSpecification_ComponentTypes() {
		return (EReference)thingSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__CreateCapability__String() {
		return thingSpecificationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__CreateComponent__String_ComponentType() {
		return thingSpecificationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__GetComponentSpecification__ComponentType() {
		return thingSpecificationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__CreateComponent__String_ComponentSpecification() {
		return thingSpecificationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__CreateCapability__CapabilitySpecification() {
		return thingSpecificationEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__GetUnit__String() {
		return thingSpecificationEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__GetComponentType__String() {
		return thingSpecificationEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__ComponentTypeId() {
		return thingSpecificationEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThingSpecification__GetCapabilitySpecification__String() {
		return thingSpecificationEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCapabilitySpecification() {
		return capabilitySpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCapabilitySpecification_Id() {
		return (EAttribute)capabilitySpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCapabilitySpecification_Properties() {
		return (EReference)capabilitySpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCapabilitySpecification_Actions() {
		return (EReference)capabilitySpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCapabilitySpecification_Events() {
		return (EReference)capabilitySpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertySpecification() {
		return propertySpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySpecification_Name() {
		return (EAttribute)propertySpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySpecification_Writable() {
		return (EAttribute)propertySpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertySpecification_Unit() {
		return (EReference)propertySpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActionSpecification() {
		return actionSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActionSpecification_Name() {
		return (EAttribute)actionSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActionSpecification_Parameters() {
		return (EReference)actionSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActionParameterSpecification() {
		return actionParameterSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActionParameterSpecification_Name() {
		return (EAttribute)actionParameterSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActionParameterSpecification_Unit() {
		return (EReference)actionParameterSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventSpecification() {
		return eventSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventSpecification_Name() {
		return (EAttribute)eventSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentSpecification() {
		return componentSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentSpecification_Type() {
		return (EReference)componentSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentSpecification_Capabilities() {
		return (EReference)componentSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThingSpecFactory getThingSpecFactory() {
		return (ThingSpecFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		thingSpecificationEClass = createEClass(THING_SPECIFICATION);
		createEReference(thingSpecificationEClass, THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS);
		createEReference(thingSpecificationEClass, THING_SPECIFICATION__UNITS);
		createEReference(thingSpecificationEClass, THING_SPECIFICATION__COMPONENT_SPECIFICATIONS);
		createEAttribute(thingSpecificationEClass, THING_SPECIFICATION__VERSION);
		createEReference(thingSpecificationEClass, THING_SPECIFICATION__COMPONENT_TYPES);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___CREATE_CAPABILITY__STRING);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTTYPE);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___GET_COMPONENT_SPECIFICATION__COMPONENTTYPE);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTSPECIFICATION);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___CREATE_CAPABILITY__CAPABILITYSPECIFICATION);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___GET_UNIT__STRING);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___GET_COMPONENT_TYPE__STRING);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___COMPONENT_TYPE_ID);
		createEOperation(thingSpecificationEClass, THING_SPECIFICATION___GET_CAPABILITY_SPECIFICATION__STRING);

		capabilitySpecificationEClass = createEClass(CAPABILITY_SPECIFICATION);
		createEAttribute(capabilitySpecificationEClass, CAPABILITY_SPECIFICATION__ID);
		createEReference(capabilitySpecificationEClass, CAPABILITY_SPECIFICATION__PROPERTIES);
		createEReference(capabilitySpecificationEClass, CAPABILITY_SPECIFICATION__ACTIONS);
		createEReference(capabilitySpecificationEClass, CAPABILITY_SPECIFICATION__EVENTS);

		propertySpecificationEClass = createEClass(PROPERTY_SPECIFICATION);
		createEAttribute(propertySpecificationEClass, PROPERTY_SPECIFICATION__NAME);
		createEAttribute(propertySpecificationEClass, PROPERTY_SPECIFICATION__WRITABLE);
		createEReference(propertySpecificationEClass, PROPERTY_SPECIFICATION__UNIT);

		actionSpecificationEClass = createEClass(ACTION_SPECIFICATION);
		createEAttribute(actionSpecificationEClass, ACTION_SPECIFICATION__NAME);
		createEReference(actionSpecificationEClass, ACTION_SPECIFICATION__PARAMETERS);

		actionParameterSpecificationEClass = createEClass(ACTION_PARAMETER_SPECIFICATION);
		createEAttribute(actionParameterSpecificationEClass, ACTION_PARAMETER_SPECIFICATION__NAME);
		createEReference(actionParameterSpecificationEClass, ACTION_PARAMETER_SPECIFICATION__UNIT);

		eventSpecificationEClass = createEClass(EVENT_SPECIFICATION);
		createEAttribute(eventSpecificationEClass, EVENT_SPECIFICATION__NAME);

		componentSpecificationEClass = createEClass(COMPONENT_SPECIFICATION);
		createEReference(componentSpecificationEClass, COMPONENT_SPECIFICATION__TYPE);
		createEReference(componentSpecificationEClass, COMPONENT_SPECIFICATION__CAPABILITIES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ThingModelPackage theThingModelPackage = (ThingModelPackage)EPackage.Registry.INSTANCE.getEPackage(ThingModelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(thingSpecificationEClass, ThingSpecification.class, "ThingSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getThingSpecification_CapabilitySpecifications(), this.getCapabilitySpecification(), null, "capabilitySpecifications", null, 0, -1, ThingSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThingSpecification_Units(), theThingModelPackage.getUnit(), null, "units", null, 0, -1, ThingSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThingSpecification_ComponentSpecifications(), this.getComponentSpecification(), null, "componentSpecifications", null, 0, -1, ThingSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThingSpecification_Version(), ecorePackage.getEString(), "version", "0.1", 1, 1, ThingSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThingSpecification_ComponentTypes(), theThingModelPackage.getComponentType(), null, "componentTypes", null, 0, -1, ThingSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getThingSpecification__CreateCapability__String(), theThingModelPackage.getCapability(), "createCapability", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "capabilityId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getThingSpecification__CreateComponent__String_ComponentType(), theThingModelPackage.getComponent(), "createComponent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "id", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theThingModelPackage.getComponentType(), "componentType", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getThingSpecification__GetComponentSpecification__ComponentType(), this.getComponentSpecification(), "getComponentSpecification", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theThingModelPackage.getComponentType(), "componentType", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getThingSpecification__CreateComponent__String_ComponentSpecification(), theThingModelPackage.getComponent(), "createComponent", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "id", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getComponentSpecification(), "componentSpecification", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getThingSpecification__CreateCapability__CapabilitySpecification(), theThingModelPackage.getCapability(), "createCapability", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCapabilitySpecification(), "capabilitySpecification", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getThingSpecification__GetUnit__String(), theThingModelPackage.getUnit(), "getUnit", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "unitId", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getThingSpecification__GetComponentType__String(), theThingModelPackage.getComponentType(), "getComponentType", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "componentTypeId", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getThingSpecification__ComponentTypeId(), ecorePackage.getEString(), "componentTypeId", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getThingSpecification__GetCapabilitySpecification__String(), this.getCapabilitySpecification(), "getCapabilitySpecification", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "capabilityId", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(capabilitySpecificationEClass, CapabilitySpecification.class, "CapabilitySpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCapabilitySpecification_Id(), ecorePackage.getEString(), "id", null, 1, 1, CapabilitySpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCapabilitySpecification_Properties(), this.getPropertySpecification(), null, "properties", null, 0, -1, CapabilitySpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCapabilitySpecification_Actions(), this.getActionSpecification(), null, "actions", null, 0, -1, CapabilitySpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCapabilitySpecification_Events(), this.getEventSpecification(), null, "events", null, 0, -1, CapabilitySpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertySpecificationEClass, PropertySpecification.class, "PropertySpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPropertySpecification_Name(), ecorePackage.getEString(), "name", null, 1, 1, PropertySpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySpecification_Writable(), ecorePackage.getEBoolean(), "writable", null, 1, 1, PropertySpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertySpecification_Unit(), theThingModelPackage.getUnit(), null, "unit", null, 0, 1, PropertySpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionSpecificationEClass, ActionSpecification.class, "ActionSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActionSpecification_Name(), ecorePackage.getEString(), "name", null, 1, 1, ActionSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActionSpecification_Parameters(), this.getActionParameterSpecification(), null, "parameters", null, 0, -1, ActionSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionParameterSpecificationEClass, ActionParameterSpecification.class, "ActionParameterSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActionParameterSpecification_Name(), ecorePackage.getEString(), "name", null, 1, 1, ActionParameterSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActionParameterSpecification_Unit(), theThingModelPackage.getUnit(), null, "unit", null, 0, 1, ActionParameterSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventSpecificationEClass, EventSpecification.class, "EventSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEventSpecification_Name(), ecorePackage.getEString(), "name", null, 1, 1, EventSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentSpecificationEClass, ComponentSpecification.class, "ComponentSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentSpecification_Type(), theThingModelPackage.getComponentType(), null, "type", null, 0, 1, ComponentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentSpecification_Capabilities(), this.getCapabilitySpecification(), null, "capabilities", null, 0, -1, ComponentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //SpecPackageImpl
