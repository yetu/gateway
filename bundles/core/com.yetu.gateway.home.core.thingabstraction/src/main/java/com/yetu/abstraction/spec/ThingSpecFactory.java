/**
 */
package com.yetu.abstraction.spec;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.yetu.abstraction.spec.ThingSpecPackage
 * @generated
 */
public interface ThingSpecFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ThingSpecFactory eINSTANCE = com.yetu.abstraction.spec.impl.ThingSpecFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Thing Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Thing Specification</em>'.
	 * @generated
	 */
	ThingSpecification createThingSpecification();

	/**
	 * Returns a new object of class '<em>Capability Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Capability Specification</em>'.
	 * @generated
	 */
	CapabilitySpecification createCapabilitySpecification();

	/**
	 * Returns a new object of class '<em>Property Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property Specification</em>'.
	 * @generated
	 */
	PropertySpecification createPropertySpecification();

	/**
	 * Returns a new object of class '<em>Action Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Specification</em>'.
	 * @generated
	 */
	ActionSpecification createActionSpecification();

	/**
	 * Returns a new object of class '<em>Action Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Parameter Specification</em>'.
	 * @generated
	 */
	ActionParameterSpecification createActionParameterSpecification();

	/**
	 * Returns a new object of class '<em>Event Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Specification</em>'.
	 * @generated
	 */
	EventSpecification createEventSpecification();

	/**
	 * Returns a new object of class '<em>Component Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Specification</em>'.
	 * @generated
	 */
	ComponentSpecification createComponentSpecification();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ThingSpecPackage getThingSpecPackage();

} //SpecFactory
