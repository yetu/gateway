/**
 */
package com.yetu.abstraction.spec;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.yetu.abstraction.spec.ThingSpecFactory
 * @model kind="package"
 * @generated
 */
public interface ThingSpecPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "spec";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://yetu.com/spec/abstraction";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ThingSpecPackage eINSTANCE = com.yetu.abstraction.spec.impl.ThingSpecPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.yetu.abstraction.spec.impl.ThingSpecificationImpl <em>Thing Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.yetu.abstraction.spec.impl.ThingSpecificationImpl
	 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getThingSpecification()
	 * @generated
	 */
	int THING_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Capability Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS = 0;

	/**
	 * The feature id for the '<em><b>Units</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION__UNITS = 1;

	/**
	 * The feature id for the '<em><b>Component Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION__COMPONENT_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION__VERSION = 3;

	/**
	 * The feature id for the '<em><b>Component Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION__COMPONENT_TYPES = 4;

	/**
	 * The number of structural features of the '<em>Thing Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Create Capability</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___CREATE_CAPABILITY__STRING = 0;

	/**
	 * The operation id for the '<em>Create Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTTYPE = 1;

	/**
	 * The operation id for the '<em>Get Component Specification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___GET_COMPONENT_SPECIFICATION__COMPONENTTYPE = 2;

	/**
	 * The operation id for the '<em>Create Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTSPECIFICATION = 3;

	/**
	 * The operation id for the '<em>Create Capability</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___CREATE_CAPABILITY__CAPABILITYSPECIFICATION = 4;

	/**
	 * The operation id for the '<em>Get Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___GET_UNIT__STRING = 5;

	/**
	 * The operation id for the '<em>Get Component Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___GET_COMPONENT_TYPE__STRING = 6;

	/**
	 * The operation id for the '<em>Component Type Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___COMPONENT_TYPE_ID = 7;

	/**
	 * The operation id for the '<em>Get Capability Specification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION___GET_CAPABILITY_SPECIFICATION__STRING = 8;

	/**
	 * The number of operations of the '<em>Thing Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THING_SPECIFICATION_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl <em>Capability Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl
	 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getCapabilitySpecification()
	 * @generated
	 */
	int CAPABILITY_SPECIFICATION = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_SPECIFICATION__ID = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_SPECIFICATION__PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_SPECIFICATION__ACTIONS = 2;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_SPECIFICATION__EVENTS = 3;

	/**
	 * The number of structural features of the '<em>Capability Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_SPECIFICATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Capability Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.yetu.abstraction.spec.impl.PropertySpecificationImpl <em>Property Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.yetu.abstraction.spec.impl.PropertySpecificationImpl
	 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getPropertySpecification()
	 * @generated
	 */
	int PROPERTY_SPECIFICATION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SPECIFICATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Writable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SPECIFICATION__WRITABLE = 1;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SPECIFICATION__UNIT = 2;

	/**
	 * The number of structural features of the '<em>Property Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SPECIFICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Property Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.yetu.abstraction.spec.impl.ActionSpecificationImpl <em>Action Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.yetu.abstraction.spec.impl.ActionSpecificationImpl
	 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getActionSpecification()
	 * @generated
	 */
	int ACTION_SPECIFICATION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SPECIFICATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SPECIFICATION__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Action Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SPECIFICATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Action Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.yetu.abstraction.spec.impl.ActionParameterSpecificationImpl <em>Action Parameter Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.yetu.abstraction.spec.impl.ActionParameterSpecificationImpl
	 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getActionParameterSpecification()
	 * @generated
	 */
	int ACTION_PARAMETER_SPECIFICATION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_PARAMETER_SPECIFICATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_PARAMETER_SPECIFICATION__UNIT = 1;

	/**
	 * The number of structural features of the '<em>Action Parameter Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_PARAMETER_SPECIFICATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Action Parameter Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_PARAMETER_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.yetu.abstraction.spec.impl.EventSpecificationImpl <em>Event Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.yetu.abstraction.spec.impl.EventSpecificationImpl
	 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getEventSpecification()
	 * @generated
	 */
	int EVENT_SPECIFICATION = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_SPECIFICATION__NAME = 0;

	/**
	 * The number of structural features of the '<em>Event Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_SPECIFICATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Event Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.yetu.abstraction.spec.impl.ComponentSpecificationImpl <em>Component Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.yetu.abstraction.spec.impl.ComponentSpecificationImpl
	 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getComponentSpecification()
	 * @generated
	 */
	int COMPONENT_SPECIFICATION = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_SPECIFICATION__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Capabilities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_SPECIFICATION__CAPABILITIES = 1;

	/**
	 * The number of structural features of the '<em>Component Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_SPECIFICATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Component Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_SPECIFICATION_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link com.yetu.abstraction.spec.ThingSpecification <em>Thing Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Thing Specification</em>'.
	 * @see com.yetu.abstraction.spec.ThingSpecification
	 * @generated
	 */
	EClass getThingSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.ThingSpecification#getCapabilitySpecifications <em>Capability Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Capability Specifications</em>'.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getCapabilitySpecifications()
	 * @see #getThingSpecification()
	 * @generated
	 */
	EReference getThingSpecification_CapabilitySpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.ThingSpecification#getUnits <em>Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Units</em>'.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getUnits()
	 * @see #getThingSpecification()
	 * @generated
	 */
	EReference getThingSpecification_Units();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.ThingSpecification#getComponentSpecifications <em>Component Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component Specifications</em>'.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getComponentSpecifications()
	 * @see #getThingSpecification()
	 * @generated
	 */
	EReference getThingSpecification_ComponentSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link com.yetu.abstraction.spec.ThingSpecification#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getVersion()
	 * @see #getThingSpecification()
	 * @generated
	 */
	EAttribute getThingSpecification_Version();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.ThingSpecification#getComponentTypes <em>Component Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component Types</em>'.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getComponentTypes()
	 * @see #getThingSpecification()
	 * @generated
	 */
	EReference getThingSpecification_ComponentTypes();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#createCapability(java.lang.String) <em>Create Capability</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Capability</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#createCapability(java.lang.String)
	 * @generated
	 */
	EOperation getThingSpecification__CreateCapability__String();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#createComponent(java.lang.String, com.yetu.abstraction.model.ComponentType) <em>Create Component</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Component</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#createComponent(java.lang.String, com.yetu.abstraction.model.ComponentType)
	 * @generated
	 */
	EOperation getThingSpecification__CreateComponent__String_ComponentType();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#getComponentSpecification(com.yetu.abstraction.model.ComponentType) <em>Get Component Specification</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Component Specification</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getComponentSpecification(com.yetu.abstraction.model.ComponentType)
	 * @generated
	 */
	EOperation getThingSpecification__GetComponentSpecification__ComponentType();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#createComponent(java.lang.String, com.yetu.abstraction.spec.ComponentSpecification) <em>Create Component</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Component</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#createComponent(java.lang.String, com.yetu.abstraction.spec.ComponentSpecification)
	 * @generated
	 */
	EOperation getThingSpecification__CreateComponent__String_ComponentSpecification();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#createCapability(com.yetu.abstraction.spec.CapabilitySpecification) <em>Create Capability</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Capability</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#createCapability(com.yetu.abstraction.spec.CapabilitySpecification)
	 * @generated
	 */
	EOperation getThingSpecification__CreateCapability__CapabilitySpecification();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#getUnit(java.lang.String) <em>Get Unit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Unit</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getUnit(java.lang.String)
	 * @generated
	 */
	EOperation getThingSpecification__GetUnit__String();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#getComponentType(java.lang.String) <em>Get Component Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Component Type</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getComponentType(java.lang.String)
	 * @generated
	 */
	EOperation getThingSpecification__GetComponentType__String();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#componentTypeId() <em>Component Type Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Component Type Id</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#componentTypeId()
	 * @generated
	 */
	EOperation getThingSpecification__ComponentTypeId();

	/**
	 * Returns the meta object for the '{@link com.yetu.abstraction.spec.ThingSpecification#getCapabilitySpecification(java.lang.String) <em>Get Capability Specification</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Capability Specification</em>' operation.
	 * @see com.yetu.abstraction.spec.ThingSpecification#getCapabilitySpecification(java.lang.String)
	 * @generated
	 */
	EOperation getThingSpecification__GetCapabilitySpecification__String();

	/**
	 * Returns the meta object for class '{@link com.yetu.abstraction.spec.CapabilitySpecification <em>Capability Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Capability Specification</em>'.
	 * @see com.yetu.abstraction.spec.CapabilitySpecification
	 * @generated
	 */
	EClass getCapabilitySpecification();

	/**
	 * Returns the meta object for the attribute '{@link com.yetu.abstraction.spec.CapabilitySpecification#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.yetu.abstraction.spec.CapabilitySpecification#getId()
	 * @see #getCapabilitySpecification()
	 * @generated
	 */
	EAttribute getCapabilitySpecification_Id();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.CapabilitySpecification#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see com.yetu.abstraction.spec.CapabilitySpecification#getProperties()
	 * @see #getCapabilitySpecification()
	 * @generated
	 */
	EReference getCapabilitySpecification_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.CapabilitySpecification#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see com.yetu.abstraction.spec.CapabilitySpecification#getActions()
	 * @see #getCapabilitySpecification()
	 * @generated
	 */
	EReference getCapabilitySpecification_Actions();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.CapabilitySpecification#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see com.yetu.abstraction.spec.CapabilitySpecification#getEvents()
	 * @see #getCapabilitySpecification()
	 * @generated
	 */
	EReference getCapabilitySpecification_Events();

	/**
	 * Returns the meta object for class '{@link com.yetu.abstraction.spec.PropertySpecification <em>Property Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Specification</em>'.
	 * @see com.yetu.abstraction.spec.PropertySpecification
	 * @generated
	 */
	EClass getPropertySpecification();

	/**
	 * Returns the meta object for the attribute '{@link com.yetu.abstraction.spec.PropertySpecification#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.yetu.abstraction.spec.PropertySpecification#getName()
	 * @see #getPropertySpecification()
	 * @generated
	 */
	EAttribute getPropertySpecification_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.yetu.abstraction.spec.PropertySpecification#isWritable <em>Writable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Writable</em>'.
	 * @see com.yetu.abstraction.spec.PropertySpecification#isWritable()
	 * @see #getPropertySpecification()
	 * @generated
	 */
	EAttribute getPropertySpecification_Writable();

	/**
	 * Returns the meta object for the reference '{@link com.yetu.abstraction.spec.PropertySpecification#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see com.yetu.abstraction.spec.PropertySpecification#getUnit()
	 * @see #getPropertySpecification()
	 * @generated
	 */
	EReference getPropertySpecification_Unit();

	/**
	 * Returns the meta object for class '{@link com.yetu.abstraction.spec.ActionSpecification <em>Action Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Specification</em>'.
	 * @see com.yetu.abstraction.spec.ActionSpecification
	 * @generated
	 */
	EClass getActionSpecification();

	/**
	 * Returns the meta object for the attribute '{@link com.yetu.abstraction.spec.ActionSpecification#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.yetu.abstraction.spec.ActionSpecification#getName()
	 * @see #getActionSpecification()
	 * @generated
	 */
	EAttribute getActionSpecification_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link com.yetu.abstraction.spec.ActionSpecification#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see com.yetu.abstraction.spec.ActionSpecification#getParameters()
	 * @see #getActionSpecification()
	 * @generated
	 */
	EReference getActionSpecification_Parameters();

	/**
	 * Returns the meta object for class '{@link com.yetu.abstraction.spec.ActionParameterSpecification <em>Action Parameter Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Parameter Specification</em>'.
	 * @see com.yetu.abstraction.spec.ActionParameterSpecification
	 * @generated
	 */
	EClass getActionParameterSpecification();

	/**
	 * Returns the meta object for the attribute '{@link com.yetu.abstraction.spec.ActionParameterSpecification#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.yetu.abstraction.spec.ActionParameterSpecification#getName()
	 * @see #getActionParameterSpecification()
	 * @generated
	 */
	EAttribute getActionParameterSpecification_Name();

	/**
	 * Returns the meta object for the reference '{@link com.yetu.abstraction.spec.ActionParameterSpecification#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see com.yetu.abstraction.spec.ActionParameterSpecification#getUnit()
	 * @see #getActionParameterSpecification()
	 * @generated
	 */
	EReference getActionParameterSpecification_Unit();

	/**
	 * Returns the meta object for class '{@link com.yetu.abstraction.spec.EventSpecification <em>Event Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Specification</em>'.
	 * @see com.yetu.abstraction.spec.EventSpecification
	 * @generated
	 */
	EClass getEventSpecification();

	/**
	 * Returns the meta object for the attribute '{@link com.yetu.abstraction.spec.EventSpecification#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.yetu.abstraction.spec.EventSpecification#getName()
	 * @see #getEventSpecification()
	 * @generated
	 */
	EAttribute getEventSpecification_Name();

	/**
	 * Returns the meta object for class '{@link com.yetu.abstraction.spec.ComponentSpecification <em>Component Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Specification</em>'.
	 * @see com.yetu.abstraction.spec.ComponentSpecification
	 * @generated
	 */
	EClass getComponentSpecification();

	/**
	 * Returns the meta object for the reference '{@link com.yetu.abstraction.spec.ComponentSpecification#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see com.yetu.abstraction.spec.ComponentSpecification#getType()
	 * @see #getComponentSpecification()
	 * @generated
	 */
	EReference getComponentSpecification_Type();

	/**
	 * Returns the meta object for the reference list '{@link com.yetu.abstraction.spec.ComponentSpecification#getCapabilities <em>Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Capabilities</em>'.
	 * @see com.yetu.abstraction.spec.ComponentSpecification#getCapabilities()
	 * @see #getComponentSpecification()
	 * @generated
	 */
	EReference getComponentSpecification_Capabilities();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ThingSpecFactory getThingSpecFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.yetu.abstraction.spec.impl.ThingSpecificationImpl <em>Thing Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.yetu.abstraction.spec.impl.ThingSpecificationImpl
		 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getThingSpecification()
		 * @generated
		 */
		EClass THING_SPECIFICATION = eINSTANCE.getThingSpecification();

		/**
		 * The meta object literal for the '<em><b>Capability Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THING_SPECIFICATION__CAPABILITY_SPECIFICATIONS = eINSTANCE.getThingSpecification_CapabilitySpecifications();

		/**
		 * The meta object literal for the '<em><b>Units</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THING_SPECIFICATION__UNITS = eINSTANCE.getThingSpecification_Units();

		/**
		 * The meta object literal for the '<em><b>Component Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THING_SPECIFICATION__COMPONENT_SPECIFICATIONS = eINSTANCE.getThingSpecification_ComponentSpecifications();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THING_SPECIFICATION__VERSION = eINSTANCE.getThingSpecification_Version();

		/**
		 * The meta object literal for the '<em><b>Component Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THING_SPECIFICATION__COMPONENT_TYPES = eINSTANCE.getThingSpecification_ComponentTypes();

		/**
		 * The meta object literal for the '<em><b>Create Capability</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___CREATE_CAPABILITY__STRING = eINSTANCE.getThingSpecification__CreateCapability__String();

		/**
		 * The meta object literal for the '<em><b>Create Component</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTTYPE = eINSTANCE.getThingSpecification__CreateComponent__String_ComponentType();

		/**
		 * The meta object literal for the '<em><b>Get Component Specification</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___GET_COMPONENT_SPECIFICATION__COMPONENTTYPE = eINSTANCE.getThingSpecification__GetComponentSpecification__ComponentType();

		/**
		 * The meta object literal for the '<em><b>Create Component</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___CREATE_COMPONENT__STRING_COMPONENTSPECIFICATION = eINSTANCE.getThingSpecification__CreateComponent__String_ComponentSpecification();

		/**
		 * The meta object literal for the '<em><b>Create Capability</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___CREATE_CAPABILITY__CAPABILITYSPECIFICATION = eINSTANCE.getThingSpecification__CreateCapability__CapabilitySpecification();

		/**
		 * The meta object literal for the '<em><b>Get Unit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___GET_UNIT__STRING = eINSTANCE.getThingSpecification__GetUnit__String();

		/**
		 * The meta object literal for the '<em><b>Get Component Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___GET_COMPONENT_TYPE__STRING = eINSTANCE.getThingSpecification__GetComponentType__String();

		/**
		 * The meta object literal for the '<em><b>Component Type Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___COMPONENT_TYPE_ID = eINSTANCE.getThingSpecification__ComponentTypeId();

		/**
		 * The meta object literal for the '<em><b>Get Capability Specification</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THING_SPECIFICATION___GET_CAPABILITY_SPECIFICATION__STRING = eINSTANCE.getThingSpecification__GetCapabilitySpecification__String();

		/**
		 * The meta object literal for the '{@link com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl <em>Capability Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl
		 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getCapabilitySpecification()
		 * @generated
		 */
		EClass CAPABILITY_SPECIFICATION = eINSTANCE.getCapabilitySpecification();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CAPABILITY_SPECIFICATION__ID = eINSTANCE.getCapabilitySpecification_Id();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAPABILITY_SPECIFICATION__PROPERTIES = eINSTANCE.getCapabilitySpecification_Properties();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAPABILITY_SPECIFICATION__ACTIONS = eINSTANCE.getCapabilitySpecification_Actions();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAPABILITY_SPECIFICATION__EVENTS = eINSTANCE.getCapabilitySpecification_Events();

		/**
		 * The meta object literal for the '{@link com.yetu.abstraction.spec.impl.PropertySpecificationImpl <em>Property Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.yetu.abstraction.spec.impl.PropertySpecificationImpl
		 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getPropertySpecification()
		 * @generated
		 */
		EClass PROPERTY_SPECIFICATION = eINSTANCE.getPropertySpecification();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SPECIFICATION__NAME = eINSTANCE.getPropertySpecification_Name();

		/**
		 * The meta object literal for the '<em><b>Writable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SPECIFICATION__WRITABLE = eINSTANCE.getPropertySpecification_Writable();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_SPECIFICATION__UNIT = eINSTANCE.getPropertySpecification_Unit();

		/**
		 * The meta object literal for the '{@link com.yetu.abstraction.spec.impl.ActionSpecificationImpl <em>Action Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.yetu.abstraction.spec.impl.ActionSpecificationImpl
		 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getActionSpecification()
		 * @generated
		 */
		EClass ACTION_SPECIFICATION = eINSTANCE.getActionSpecification();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_SPECIFICATION__NAME = eINSTANCE.getActionSpecification_Name();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_SPECIFICATION__PARAMETERS = eINSTANCE.getActionSpecification_Parameters();

		/**
		 * The meta object literal for the '{@link com.yetu.abstraction.spec.impl.ActionParameterSpecificationImpl <em>Action Parameter Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.yetu.abstraction.spec.impl.ActionParameterSpecificationImpl
		 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getActionParameterSpecification()
		 * @generated
		 */
		EClass ACTION_PARAMETER_SPECIFICATION = eINSTANCE.getActionParameterSpecification();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_PARAMETER_SPECIFICATION__NAME = eINSTANCE.getActionParameterSpecification_Name();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_PARAMETER_SPECIFICATION__UNIT = eINSTANCE.getActionParameterSpecification_Unit();

		/**
		 * The meta object literal for the '{@link com.yetu.abstraction.spec.impl.EventSpecificationImpl <em>Event Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.yetu.abstraction.spec.impl.EventSpecificationImpl
		 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getEventSpecification()
		 * @generated
		 */
		EClass EVENT_SPECIFICATION = eINSTANCE.getEventSpecification();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_SPECIFICATION__NAME = eINSTANCE.getEventSpecification_Name();

		/**
		 * The meta object literal for the '{@link com.yetu.abstraction.spec.impl.ComponentSpecificationImpl <em>Component Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.yetu.abstraction.spec.impl.ComponentSpecificationImpl
		 * @see com.yetu.abstraction.spec.impl.ThingSpecPackageImpl#getComponentSpecification()
		 * @generated
		 */
		EClass COMPONENT_SPECIFICATION = eINSTANCE.getComponentSpecification();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_SPECIFICATION__TYPE = eINSTANCE.getComponentSpecification_Type();

		/**
		 * The meta object literal for the '<em><b>Capabilities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_SPECIFICATION__CAPABILITIES = eINSTANCE.getComponentSpecification_Capabilities();

	}

} //SpecPackage
