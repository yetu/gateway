/**
 */
package com.yetu.abstraction.spec;

import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.ComponentType;
import com.yetu.abstraction.model.Unit;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Thing Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.yetu.abstraction.spec.ThingSpecification#getCapabilitySpecifications <em>Capability Specifications</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.ThingSpecification#getUnits <em>Units</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.ThingSpecification#getComponentSpecifications <em>Component Specifications</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.ThingSpecification#getVersion <em>Version</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.ThingSpecification#getComponentTypes <em>Component Types</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.yetu.abstraction.spec.ThingSpecPackage#getThingSpecification()
 * @model
 * @generated
 */
public interface ThingSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Capability Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.spec.CapabilitySpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capability Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability Specifications</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getThingSpecification_CapabilitySpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<CapabilitySpecification> getCapabilitySpecifications();

	/**
	 * Returns the value of the '<em><b>Units</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.model.Unit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Units</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Units</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getThingSpecification_Units()
	 * @model containment="true"
	 * @generated
	 */
	EList<Unit> getUnits();

	/**
	 * Returns the value of the '<em><b>Component Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.spec.ComponentSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Specifications</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getThingSpecification_ComponentSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentSpecification> getComponentSpecifications();

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * The default value is <code>"0.1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getThingSpecification_Version()
	 * @model default="0.1" required="true"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.spec.ThingSpecification#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Component Types</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.model.ComponentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Types</em>' containment reference list.
	 * @see com.yetu.abstraction.spec.ThingSpecPackage#getThingSpecification_ComponentTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentType> getComponentTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Capability createCapability(String capabilityId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Component createComponent(String id, ComponentType componentType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ComponentSpecification getComponentSpecification(ComponentType componentType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" idRequired="true" componentSpecificationRequired="true"
	 * @generated
	 */
	Component createComponent(String id, ComponentSpecification componentSpecification);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" capabilitySpecificationRequired="true"
	 * @generated
	 */
	Capability createCapability(CapabilitySpecification capabilitySpecification);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" unitIdRequired="true"
	 * @generated
	 */
	Unit getUnit(String unitId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	ComponentType getComponentType(String componentTypeId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	String componentTypeId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" capabilityIdRequired="true"
	 * @generated
	 */
	CapabilitySpecification getCapabilitySpecification(String capabilityId);

} // ThingSpecification
