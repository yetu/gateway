/**
 */
package com.yetu.abstraction.spec.util;

import com.yetu.abstraction.spec.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.yetu.abstraction.spec.ThingSpecPackage
 * @generated
 */
public class ThingSpecAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ThingSpecPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThingSpecAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ThingSpecPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThingSpecSwitch<Adapter> modelSwitch =
		new ThingSpecSwitch<Adapter>() {
			@Override
			public Adapter caseThingSpecification(ThingSpecification object) {
				return createThingSpecificationAdapter();
			}
			@Override
			public Adapter caseCapabilitySpecification(CapabilitySpecification object) {
				return createCapabilitySpecificationAdapter();
			}
			@Override
			public Adapter casePropertySpecification(PropertySpecification object) {
				return createPropertySpecificationAdapter();
			}
			@Override
			public Adapter caseActionSpecification(ActionSpecification object) {
				return createActionSpecificationAdapter();
			}
			@Override
			public Adapter caseActionParameterSpecification(ActionParameterSpecification object) {
				return createActionParameterSpecificationAdapter();
			}
			@Override
			public Adapter caseEventSpecification(EventSpecification object) {
				return createEventSpecificationAdapter();
			}
			@Override
			public Adapter caseComponentSpecification(ComponentSpecification object) {
				return createComponentSpecificationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link com.yetu.abstraction.spec.ThingSpecification <em>Thing Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.yetu.abstraction.spec.ThingSpecification
	 * @generated
	 */
	public Adapter createThingSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.yetu.abstraction.spec.CapabilitySpecification <em>Capability Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.yetu.abstraction.spec.CapabilitySpecification
	 * @generated
	 */
	public Adapter createCapabilitySpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.yetu.abstraction.spec.PropertySpecification <em>Property Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.yetu.abstraction.spec.PropertySpecification
	 * @generated
	 */
	public Adapter createPropertySpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.yetu.abstraction.spec.ActionSpecification <em>Action Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.yetu.abstraction.spec.ActionSpecification
	 * @generated
	 */
	public Adapter createActionSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.yetu.abstraction.spec.ActionParameterSpecification <em>Action Parameter Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.yetu.abstraction.spec.ActionParameterSpecification
	 * @generated
	 */
	public Adapter createActionParameterSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.yetu.abstraction.spec.EventSpecification <em>Event Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.yetu.abstraction.spec.EventSpecification
	 * @generated
	 */
	public Adapter createEventSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.yetu.abstraction.spec.ComponentSpecification <em>Component Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.yetu.abstraction.spec.ComponentSpecification
	 * @generated
	 */
	public Adapter createComponentSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ThingSpecAdapterFactory
