/**
 */
package com.yetu.abstraction.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.yetu.abstraction.model.Component#getName <em>Name</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Component#getType <em>Type</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Component#getId <em>Id</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Component#getCapabilities <em>Capabilities</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Component#getThing <em>Thing</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.yetu.abstraction.model.ThingModelPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getComponent_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Component#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ComponentType)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getComponent_Type()
	 * @model required="true"
	 * @generated
	 */
	ComponentType getType();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Component#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ComponentType value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see com.yetu.abstraction.model.ThingModelPackage#getComponent_Id()
	 * @model required="true" changeable="false"
	 * @generated
	 */
	String getId();

	/**
	 * Returns the value of the '<em><b>Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.model.Capability}.
	 * It is bidirectional and its opposite is '{@link com.yetu.abstraction.model.Capability#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capabilities</em>' containment reference list.
	 * @see com.yetu.abstraction.model.ThingModelPackage#getComponent_Capabilities()
	 * @see com.yetu.abstraction.model.Capability#getComponent
	 * @model opposite="component" containment="true"
	 * @generated
	 */
	EList<Capability> getCapabilities();

	/**
	 * Returns the value of the '<em><b>Thing</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.yetu.abstraction.model.Thing#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Thing</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thing</em>' container reference.
	 * @see #setThing(Thing)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getComponent_Thing()
	 * @see com.yetu.abstraction.model.Thing#getComponents
	 * @model opposite="components" required="true" transient="false"
	 * @generated
	 */
	Thing getThing();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Component#getThing <em>Thing</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Thing</em>' container reference.
	 * @see #getThing()
	 * @generated
	 */
	void setThing(Thing value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" capabilityIdRequired="true"
	 * @generated
	 */
	boolean hasCapability(String capabilityId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model capabilityIdRequired="true"
	 * @generated
	 */
	Capability getCapability(String capabilityId);

} // Component
