/**
 */
package com.yetu.abstraction.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Thing</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.yetu.abstraction.model.Thing#getName <em>Name</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getProperties <em>Properties</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getConnectionDetails <em>Connection Details</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getComponents <em>Components</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getId <em>Id</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getMainComponent <em>Main Component</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getDisplayType <em>Display Type</em>}</li>
 *   <li>{@link com.yetu.abstraction.model.Thing#getStatus <em>Status</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.yetu.abstraction.model.ThingModelPackage#getThing()
 * @model
 * @generated
 */
public interface Thing extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Thing#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Manufacturer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manufacturer</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manufacturer</em>' attribute.
	 * @see #setManufacturer(String)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_Manufacturer()
	 * @model
	 * @generated
	 */
	String getManufacturer();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Thing#getManufacturer <em>Manufacturer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manufacturer</em>' attribute.
	 * @see #getManufacturer()
	 * @generated
	 */
	void setManufacturer(String value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.model.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getProperties();

	/**
	 * Returns the value of the '<em><b>Connection Details</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.model.ConnectionDetails}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection Details</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection Details</em>' containment reference list.
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_ConnectionDetails()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConnectionDetails> getConnectionDetails();

	/**
	 * Returns the value of the '<em><b>Components</b></em>' containment reference list.
	 * The list contents are of type {@link com.yetu.abstraction.model.Component}.
	 * It is bidirectional and its opposite is '{@link com.yetu.abstraction.model.Component#getThing <em>Thing</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' containment reference list.
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_Components()
	 * @see com.yetu.abstraction.model.Component#getThing
	 * @model opposite="thing" containment="true"
	 * @generated
	 */
	EList<Component> getComponents();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Thing#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Main Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Component</em>' reference.
	 * @see #setMainComponent(Component)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_MainComponent()
	 * @model
	 * @generated
	 */
	Component getMainComponent();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Thing#getMainComponent <em>Main Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Component</em>' reference.
	 * @see #getMainComponent()
	 * @generated
	 */
	void setMainComponent(Component value);

	/**
	 * Returns the value of the '<em><b>Display Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Type</em>' reference.
	 * @see #setDisplayType(ComponentType)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_DisplayType()
	 * @model required="true"
	 * @generated
	 */
	ComponentType getDisplayType();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Thing#getDisplayType <em>Display Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Type</em>' reference.
	 * @see #getDisplayType()
	 * @generated
	 */
	void setDisplayType(ComponentType value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link com.yetu.abstraction.model.ThingStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see com.yetu.abstraction.model.ThingStatus
	 * @see #setStatus(ThingStatus)
	 * @see com.yetu.abstraction.model.ThingModelPackage#getThing_Status()
	 * @model required="true"
	 * @generated
	 */
	ThingStatus getStatus();

	/**
	 * Sets the value of the '{@link com.yetu.abstraction.model.Thing#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see com.yetu.abstraction.model.ThingStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(ThingStatus value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model componentIdRequired="true"
	 * @generated
	 */
	Component getComponent(String componentId);

} // Thing
