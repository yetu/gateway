/**
 */
package com.yetu.abstraction.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.yetu.abstraction.model.ThingModelPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends EObject {
} // Event
