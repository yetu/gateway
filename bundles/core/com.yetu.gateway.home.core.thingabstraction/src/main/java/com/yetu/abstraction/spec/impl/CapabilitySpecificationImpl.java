/**
 */
package com.yetu.abstraction.spec.impl;

import com.yetu.abstraction.spec.ActionSpecification;
import com.yetu.abstraction.spec.CapabilitySpecification;
import com.yetu.abstraction.spec.EventSpecification;
import com.yetu.abstraction.spec.PropertySpecification;
import com.yetu.abstraction.spec.ThingSpecPackage;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Capability Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link com.yetu.abstraction.spec.impl.CapabilitySpecificationImpl#getEvents <em>Events</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CapabilitySpecificationImpl extends MinimalEObjectImpl.Container implements CapabilitySpecification {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertySpecification> properties;

	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<ActionSpecification> actions;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<EventSpecification> events;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CapabilitySpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ThingSpecPackage.Literals.CAPABILITY_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThingSpecPackage.CAPABILITY_SPECIFICATION__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertySpecification> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<PropertySpecification>(PropertySpecification.class, this, ThingSpecPackage.CAPABILITY_SPECIFICATION__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActionSpecification> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentEList<ActionSpecification>(ActionSpecification.class, this, ThingSpecPackage.CAPABILITY_SPECIFICATION__ACTIONS);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventSpecification> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList<EventSpecification>(EventSpecification.class, this, ThingSpecPackage.CAPABILITY_SPECIFICATION__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ACTIONS:
				return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ID:
				return getId();
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__PROPERTIES:
				return getProperties();
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ACTIONS:
				return getActions();
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__EVENTS:
				return getEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ID:
				setId((String)newValue);
				return;
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends PropertySpecification>)newValue);
				return;
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ACTIONS:
				getActions().clear();
				getActions().addAll((Collection<? extends ActionSpecification>)newValue);
				return;
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends EventSpecification>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ID:
				setId(ID_EDEFAULT);
				return;
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__PROPERTIES:
				getProperties().clear();
				return;
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ACTIONS:
				getActions().clear();
				return;
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__EVENTS:
				getEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__ACTIONS:
				return actions != null && !actions.isEmpty();
			case ThingSpecPackage.CAPABILITY_SPECIFICATION__EVENTS:
				return events != null && !events.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //CapabilitySpecificationImpl
