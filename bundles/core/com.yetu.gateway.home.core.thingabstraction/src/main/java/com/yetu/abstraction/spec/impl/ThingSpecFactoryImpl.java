/**
 */
package com.yetu.abstraction.spec.impl;

import com.yetu.abstraction.spec.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ThingSpecFactoryImpl extends EFactoryImpl implements ThingSpecFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ThingSpecFactory init() {
		try {
			ThingSpecFactory theThingSpecFactory = (ThingSpecFactory)EPackage.Registry.INSTANCE.getEFactory(ThingSpecPackage.eNS_URI);
			if (theThingSpecFactory != null) {
				return theThingSpecFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ThingSpecFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThingSpecFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ThingSpecPackage.THING_SPECIFICATION: return createThingSpecification();
			case ThingSpecPackage.CAPABILITY_SPECIFICATION: return createCapabilitySpecification();
			case ThingSpecPackage.PROPERTY_SPECIFICATION: return createPropertySpecification();
			case ThingSpecPackage.ACTION_SPECIFICATION: return createActionSpecification();
			case ThingSpecPackage.ACTION_PARAMETER_SPECIFICATION: return createActionParameterSpecification();
			case ThingSpecPackage.EVENT_SPECIFICATION: return createEventSpecification();
			case ThingSpecPackage.COMPONENT_SPECIFICATION: return createComponentSpecification();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThingSpecification createThingSpecification() {
		ThingSpecificationImpl thingSpecification = new ThingSpecificationImpl();
		return thingSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilitySpecification createCapabilitySpecification() {
		CapabilitySpecificationImpl capabilitySpecification = new CapabilitySpecificationImpl();
		return capabilitySpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertySpecification createPropertySpecification() {
		PropertySpecificationImpl propertySpecification = new PropertySpecificationImpl();
		return propertySpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionSpecification createActionSpecification() {
		ActionSpecificationImpl actionSpecification = new ActionSpecificationImpl();
		return actionSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionParameterSpecification createActionParameterSpecification() {
		ActionParameterSpecificationImpl actionParameterSpecification = new ActionParameterSpecificationImpl();
		return actionParameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventSpecification createEventSpecification() {
		EventSpecificationImpl eventSpecification = new EventSpecificationImpl();
		return eventSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentSpecification createComponentSpecification() {
		ComponentSpecificationImpl componentSpecification = new ComponentSpecificationImpl();
		return componentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThingSpecPackage getThingSpecPackage() {
		return (ThingSpecPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ThingSpecPackage getPackage() {
		return ThingSpecPackage.eINSTANCE;
	}

} //SpecFactoryImpl
