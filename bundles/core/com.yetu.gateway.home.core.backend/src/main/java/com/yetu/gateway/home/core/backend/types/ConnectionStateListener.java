package com.yetu.gateway.home.core.backend.types;

/**
 * Used to get status information about a remote connection.
 * 
 * @author Sebastian Garn
 *
 */
public interface ConnectionStateListener {
	/**
	 * Triggered whenever the connection to the remote entity is lost
	 */
	public void onConnectionLost();

	/**
	 * Triggered as soon as the remote connection was established
	 */
	public void onConnectionEstablished();
}
