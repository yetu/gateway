package com.yetu.gateway.home.core.backend.internal.routing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.internal.communications.CommunicationsHandler;
import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor;
import com.yetu.gateway.home.core.backend.types.ConnectionStateListener;
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler;
import com.yetu.gateway.home.core.messaging.MessageTransport;

/**
 * Takes care of processing incoming and outgoing abstract messages. 
 * 
 * @author Sebastian Garn
 *
 */
public class RouteHandler implements ConnectionStateListener {
	public static final Logger logger = LoggerFactory.getLogger(RouteHandler.class);
	
	private int retryInterval;
	
	private RemoteCommunicationHandler remoteCommunicationHandler;
	private MessageTransport messageTransport;
	private CommunicationsHandler communicationsHandler;
	
	private IncomingMessageHandler incomingMessageHandler;
	private OutgoingMessageHandler outgoingMessageHandler;
	private Thread incomingMessageHandlerThread;
	private Thread outgoingMessageHandlerThread;
	
	public void start(ConnectionDescriptor connectionDescriptor, int retryInterval, MessageTransport messageTransport, RemoteCommunicationHandler remoteCommunicationHandler) {
		logger.debug("Starting message router with following settings: "+connectionDescriptor);

		this.retryInterval = retryInterval;

		this.messageTransport = messageTransport;
		this.remoteCommunicationHandler = remoteCommunicationHandler;
		
		communicationsHandler = new CommunicationsHandler(remoteCommunicationHandler, messageTransport);
		
		remoteCommunicationHandler.addConnectionStateListener(this);
		remoteCommunicationHandler.setup(connectionDescriptor);

		remoteCommunicationHandler.start();
	}
	
	public void stop() {
		logger.debug("Stopping message router");

		remoteCommunicationHandler.removeConnectionStateListener(this);
		remoteCommunicationHandler.stop();
		
		communicationsHandler.stop();

		stopQueueTransceivers();
	}

	@Override
	public synchronized void onConnectionLost() {
		logger.debug("Connection lost");

		communicationsHandler.stop();
		stopQueueTransceivers();

		Thread reconnect = new Thread() {
			public void run() {
				try {
					logger.debug("Reconnecting in "+retryInterval+" ms");
					Thread.sleep(retryInterval);
					
					logger.debug("Reconnecting now!");
					
					remoteCommunicationHandler.start();

				} catch (InterruptedException e) {
					logger.debug("Failed to sleep: "+e.toString());
				}
			}
		};
		reconnect.start();
	}
	
	/**
	 * Stops threads that are waiting for incoming and outgoing messages
	 */
	private void stopQueueTransceivers() {
		logger.debug("Stopping the transceivers");

		if (messageTransport != null) {
			messageTransport.getOutgoing().setAvailable(false);
		}
		
		if (outgoingMessageHandler != null) {
			outgoingMessageHandler.stop();
			outgoingMessageHandlerThread.interrupt();
		}
		
		if (incomingMessageHandler != null) {
			incomingMessageHandler.stop();
			incomingMessageHandlerThread.interrupt();
		}
	}

	@Override
	public void onConnectionEstablished() {
		logger.debug("Connection established");
		
		messageTransport.getOutgoing().setAvailable(true);

		incomingMessageHandler = new IncomingMessageHandler(remoteCommunicationHandler, communicationsHandler);
		incomingMessageHandlerThread = new Thread(incomingMessageHandler);
		incomingMessageHandlerThread.start();

		outgoingMessageHandler = new OutgoingMessageHandler(messageTransport, communicationsHandler);
		outgoingMessageHandlerThread = new Thread(outgoingMessageHandler);
		outgoingMessageHandlerThread.start();
	}
}
