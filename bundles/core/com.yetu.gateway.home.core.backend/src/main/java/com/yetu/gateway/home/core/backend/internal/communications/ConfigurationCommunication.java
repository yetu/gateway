package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ConfigurationReply;

public class ConfigurationCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(ConfigurationCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// configuration request
		addCommunicationState(new CommunicationState() {
			@Override
			public void onTimeout() { }
			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling send configuration request");

				return communication.messageTransport.getIncoming().sendMessage((IncomingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof ConfigurationRequest) {
					
						return true;
					
				}

				return false;
			}
		});
		
		// configuration reply
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling configuration reply");
				
				//return communciation.messageTransport.getOutgoing().sendMessage((OutgoingMessage)message);
				return communication.remoteCommunicationHandler.send((OutgoingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof ConfigurationReply) {
					return true;
				}

				return false;
			}
			
			@Override
			public void onTimeout() {
				
			}
		});
	}
	
	public String toString() {
		return "ConfigurationCommunication";
	}
}
