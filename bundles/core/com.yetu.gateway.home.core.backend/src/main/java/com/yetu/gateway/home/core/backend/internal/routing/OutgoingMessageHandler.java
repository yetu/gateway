package com.yetu.gateway.home.core.backend.internal.routing;

import java.nio.channels.NotYetConnectedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.internal.communications.CommunicationsHandler;
import com.yetu.gateway.home.core.messaging.MessageTransport;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

/**
 * Waits for messages coming from the gw internals that should be sent to backend
 * @author Sebastian Garn
 */
public class OutgoingMessageHandler implements Runnable {
	public static final Logger logger = LoggerFactory.getLogger(OutgoingMessageHandler.class);
	
	private MessageTransport messageTransport;
	private boolean stopped = false;
	private CommunicationsHandler communicationsHandler;
	public static int READ_RETRY_TIMEOUT = 1000;
	
	public OutgoingMessageHandler(MessageTransport messageTransport, CommunicationsHandler communicationsHandler) {
		this.messageTransport = messageTransport;
		this.communicationsHandler = communicationsHandler;
	}
	
	@Override
	public void run() {
		logger.debug("Waiting for outgoing messages");
		boolean handled = false;
		
		while(!stopped) {
			try {
				OutgoingMessage message = messageTransport.getOutgoing().getNextMessage();

				logger.debug("Got message for backend");
				handled = communicationsHandler.handle(message);

				if (!handled) {
					logger.warn("Ignoring message: "+message);
				}
			} catch (NotYetConnectedException nce) {
				logger.debug("Failed to poll outgoing message queue",nce);
				try {
					Thread.sleep(READ_RETRY_TIMEOUT);
				} catch (InterruptedException e1) {
					
				}
			} catch (InterruptedException ie) {
				logger.debug("Polling interrupted");
				stopped = true;
			} catch(Exception e) {
				logger.error("Could not read from incoming queue",e);
				stopped = true;
			}
		}
		
		logger.info("Stopped polling outgoing message queue");
	}
	
	public boolean isStopped() {
		return stopped;
	}
	
	public void stop() {
		logger.debug("Stopping outgoing message poll thread");
		stopped = true;
	}
}
