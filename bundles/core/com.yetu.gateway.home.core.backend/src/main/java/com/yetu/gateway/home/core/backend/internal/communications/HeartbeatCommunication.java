package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HeartbeatRequest;

public class HeartbeatCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(HeartbeatCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Handshake request
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling heartbeat request");
				
				// manipulate the current stage index: as soon as this communication gets started it always
				// remains in the state which means it always expects a heartbeat
				communication.currCommunicationStage -= 1; 
				
				return true;
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof HeartbeatRequest) {
					return true;	
				}

				return false;
			}

			@Override
			public void onTimeout() {
				logger.warn("Heartbeat timeout");

				communication.stop();
				communication.remoteCommunicationHandler.signalizeFaultyConnection();
			}
		}, new Long(20000));
	}

	public String toString() {
		return "HeartbeatCommunication";
	}
}
