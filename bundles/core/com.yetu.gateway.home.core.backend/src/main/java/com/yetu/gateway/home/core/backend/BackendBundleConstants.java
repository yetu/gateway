package com.yetu.gateway.home.core.backend;

public class BackendBundleConstants {
	/** configuration parameters **/
	public static final String CONFIGURATION_IP = "ip";
	public static final String CONFIGURATION_PORT = "port";
	public static final String CONFIGURATION_GATEWAY_ID = "gatewayid";
	public static final String CONFIGURATION_ENABLESSL = "enablessl";
	public static final String CONFIGURATION_RETRY_INTERVAL = "retryinterval";
	public static final String CONFIGURATION_TRUSTSTORE_FILENAME = "truststorefilename";
	public static final String CONFIGURATION_TRUSTSTORE_PASSWORD = "truststorepassword";
	public static final String CONFIGURATION_KEYSTORE_FILENAME = "keystorefilename";
	public static final String CONFIGURATION_KEYSTORE_PASSWORD = "keystorepassword";
	
	/** default values **/
	public static final int CONFIGURATION_PORT_DEFAULT = 9090;
	public static final int CONFIGURATION_RETRY_INTERVAL_DEFAULT = 5000;
	public static final boolean CONFIGURATION_ENABLESSL_DEFAULT = true;
}
