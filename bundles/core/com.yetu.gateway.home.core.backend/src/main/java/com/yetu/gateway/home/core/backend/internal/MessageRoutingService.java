package com.yetu.gateway.home.core.backend.internal;

import java.util.Dictionary;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.BackendBundleConstants;
import com.yetu.gateway.home.core.backend.internal.routing.RouteHandler;
import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor;
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler;
import com.yetu.gateway.home.core.messaging.MessageTransport;
import com.yetu.gateway.home.core.utils.ConfigParser;

/**
 * Observes prerequisites for the protocol handler. Responsible for starting or stopping the
 * message router in case changes in prerequisites occur.
 * 
 * @author Sebastian Garn
 *
 */
public class MessageRoutingService implements ManagedService {
	public static final Logger logger = LoggerFactory.getLogger(MessageRoutingService.class);
	
	private RemoteCommunicationHandler remoteCommunicationHandler;
	private MessageTransport messageTransport;
	private boolean configured = false; 
	private boolean protocolStarted = false;
	private RouteHandler routeHandler;
	private ConnectionDescriptor connectionDescriptor;

	private int cfgRetryInterval;
	
	/**
	 * Called whenever the binding configuration has changed or services were removed.
	 * Checks the prerequisites and starts or stops the protocol if necessary. 
	 */
	private synchronized void onReconfigure() {
		logger.debug("Binding prerequisites have changed. Checking start/stop conditions.");
		if (remoteCommunicationHandler != null && messageTransport != null && configured) {
			if (!protocolStarted) {
				logger.info("All prerequisites fulfuilled. Starting route handler now");
				
				routeHandler = new RouteHandler();
				routeHandler.start(connectionDescriptor, cfgRetryInterval, messageTransport, remoteCommunicationHandler);
				
				protocolStarted = true;
			}
			else {
				logger.info("All prerequisites fulfuilled but protocol already running");
			}
		}
		else {
			//logger.debug("Given prerequisites\nCommHandler: "+remoteCommunicationHandler+", MessageTransport: "+messageTransport+", Configuration: "+configuration);
			if (protocolStarted) {
				logger.info("Stopping protocol due to missing prerequisites");
				
				routeHandler.stop();
				protocolStarted = false;
			}
			else {
				logger.debug("Unable to start route handler due to missing prerequisites");
			}
		}
	}
	
	/**
	 * Service for communicating with backend. Currently we are using a handler based on protobuf.
	 * @param RemoteCommunicationHandler handler
	 */
	public void setRemoteCommunicationHandler(RemoteCommunicationHandler handler) {
		remoteCommunicationHandler = handler;
		onReconfigure();
	}
	
	public void unsetRemoteCommunicationHandler(RemoteCommunicationHandler handler) {
		remoteCommunicationHandler = null;
		onReconfigure();
	}
	
	/**
	 * Message transport allows to retrieve and route message from/to core.
	 * @param transport
	 */
	public void setMessageTransport(MessageTransport transport) {
		messageTransport = transport;
		onReconfigure();
	}
	
	public void unsetMessageTransport(MessageTransport transport) {
		messageTransport = null;
		onReconfigure();
	}

	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
		logger.debug("Configuration provided: "+properties);
		
		if (properties != null) {
			ConfigParser cfg = new ConfigParser(properties);

			try {
				boolean cfgEnableSsl = cfg.getValue(BackendBundleConstants.CONFIGURATION_ENABLESSL, BackendBundleConstants.CONFIGURATION_ENABLESSL_DEFAULT);
				String ip = cfg.getStringValue(BackendBundleConstants.CONFIGURATION_IP);
				int port = cfg.getValue(BackendBundleConstants.CONFIGURATION_PORT, BackendBundleConstants.CONFIGURATION_PORT_DEFAULT);

				cfgRetryInterval = cfg.getValue(BackendBundleConstants.CONFIGURATION_RETRY_INTERVAL, BackendBundleConstants.CONFIGURATION_RETRY_INTERVAL_DEFAULT);
				
				if (cfgEnableSsl) {
					String truststoreFilename = cfg.getStringValue(BackendBundleConstants.CONFIGURATION_TRUSTSTORE_FILENAME);
					String truststorePassword = cfg.getStringValue(BackendBundleConstants.CONFIGURATION_TRUSTSTORE_PASSWORD);
					String keystoreFilename = cfg.getStringValue(BackendBundleConstants.CONFIGURATION_KEYSTORE_FILENAME);
					String keystorePassword = cfg.getStringValue(BackendBundleConstants.CONFIGURATION_KEYSTORE_PASSWORD);
					connectionDescriptor = new ConnectionDescriptor(ip, port, truststoreFilename, truststorePassword, keystoreFilename, keystorePassword);
				}
				else {
					connectionDescriptor = new ConnectionDescriptor(ip, port);
				}

				configured = true;
			} catch(Exception e) {
				logger.error("Invalid configuration. "+e.toString(),e);
				configured = false;
			}
		}
		else {
			configured = false;
		}

		onReconfigure();
	}
}
