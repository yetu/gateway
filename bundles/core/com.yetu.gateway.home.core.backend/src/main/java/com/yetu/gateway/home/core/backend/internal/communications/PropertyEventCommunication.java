package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.PropertyChanged;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;

public class PropertyEventCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(PropertyEventCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// property event
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling property event request");
				
				OutgoingMessage event;
				
				if (message instanceof SetPropertyError) {
					logger.debug("Handling property error");
					event = (SetPropertyError)message;
					
				}
				else if (message instanceof PropertyChanged) {
					logger.debug("Handling property changed event");
					event = (PropertyChanged)message;
				}
				else {
					logger.error("Unable to handle unknown property change event");
					return false;
				}

				return communication.remoteCommunicationHandler.send(event);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof SetPropertyError || message instanceof PropertyChanged) {
					return true;	
				}

				return false;
			}

			@Override
			public void onTimeout() { }
		});
	}

	public String toString() {
		return "PropertyEventCommunication";
	}
}
