package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StopDiscoverySessionReply;

public class StopDiscoverySessionCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(StopDiscoverySessionCommunication.class);
	
	private String sessionId, requestId;
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Stop session request
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling stop discovery session request");
				
				StopDiscoverySessionRequest request = (StopDiscoverySessionRequest)message;
				sessionId = request.getSessionId();
				requestId = request.getRequestId();

				return communication.messageTransport.getIncoming().sendMessage(request);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof StopDiscoverySessionRequest) {
					return true;	
				}

				return false;
			}
			
			@Override
			public void onTimeout() { }
			
		});
		
		// Stop session reply
		addCommunicationState(new CommunicationState() {			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling stop discovery session reply");
				
				StopDiscoverySessionReply reply = (StopDiscoverySessionReply)message;
				communication.remoteCommunicationHandler.send(reply);
				
				return true;
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof StopDiscoverySessionReply) {
					StopDiscoverySessionReply msg = (StopDiscoverySessionReply)message;
					if (msg.getSessionId().compareTo(sessionId) == 0 && msg.getCorrelationId().compareTo(requestId) == 0) {
						return true;
					}
				}

				return false;
			}
			
			@Override
			public void onTimeout() { }
		});
	}
	
	public String toString() {
		return "StopDiscoverySessionCommunication";
	}
}
