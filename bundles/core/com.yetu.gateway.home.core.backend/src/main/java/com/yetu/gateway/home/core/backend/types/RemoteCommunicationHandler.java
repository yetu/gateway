package com.yetu.gateway.home.core.backend.types;

import java.nio.channels.NotYetConnectedException;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

/**
 * Services implementing this interface allow bindings to communicate with a remote entity in an abstract way.
 * Implementing services should be kept as simple as possible: they are NOT responsible for the connection management (e.g.
 * reconnect on connection errors). This should be the task of the bundle that is using the service. 
 * 
 * @author Sebastian Garn
 *
 */
public interface RemoteCommunicationHandler {
	
	/**
	 * Setup the connection. You only need to call this once.
	 * 
	 * @param String ip - ip of remote server
	 * @param int port - port server is listening on
	 * @param long connectRetryDelay - how fast a reconnect should be done
	 * @param String gatewayID - id of the gateway
	 * @param SslParameters sslParameters - can be null to disable ssl
	 * @return true if setup was done successfully
	 */
	public boolean setup(ConnectionDescriptor descriptor);
	
	/**
	 * Initiates the connection.
	 * @return true if communication is starting
	 */
	public boolean start();
	
	/**
	 * Stops the connection in case bundle has to be stopped
	 */
	public void shutdown();
	
	/**
	 * Interrupts the communication
	 */
	public void stop();

	/**
	 * Blocking request to get the next incoming message. Throws an exception if an error
	 * occured (e.g. not connected)
	 * @return An AbstractMessage or null if an error occurred
	 */
	public IncomingMessage receive() throws NotYetConnectedException, InterruptedException;
	
	/**
	 * Sends a message.
	 * @param AbstractMessage message: message to send
	 * @return true if message was sent successfully, false if not
	 */
	public boolean send(OutgoingMessage message) throws NotYetConnectedException;
	
	/**
	 * Adds a listener to observe connection state
	 * @param ConnectionStateListener listener
	 */
	public void addConnectionStateListener(ConnectionStateListener listener);
	
	/**
	 * Removes a connection state listener
	 * @param ConnectionStateListener listener
	 */
	public void removeConnectionStateListener(ConnectionStateListener listener);
	
	/**
	 * In some cases underlying communication frameworks or technologies are not able to detect 
	 * faulty connections by their own or they might need quite a lot of time to detect bad connections.
	 * This method allows bundles, which are using the abstract communication handler, to signalize 
	 * a faulty connection (maybe due to a missing message like a heartbeat). However, it is up
	 * to the implementing service how it is handling a faulty connection. It might do nothing or
	 * it could also close the connection.
	 */
	public void signalizeFaultyConnection();
}
