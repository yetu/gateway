package com.yetu.gateway.home.core.backend.internal.routing;

import java.nio.channels.NotYetConnectedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.internal.communications.CommunicationsHandler;
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;

/**
 * Waits for messages coming from the backend
 * @author Sebastian Garn
 */
public class IncomingMessageHandler implements Runnable {
	public static final Logger logger = LoggerFactory.getLogger(IncomingMessageHandler.class);
	public static int READ_RETRY_TIMEOUT = 1000;
	
	private RemoteCommunicationHandler remoteCommunicationHandler;
	private CommunicationsHandler communicationsHandler;
	private boolean stopped = false;
	
	public IncomingMessageHandler(RemoteCommunicationHandler remoteCommunicationHandler, CommunicationsHandler communicationsHandler) {
		this.remoteCommunicationHandler = remoteCommunicationHandler;
		this.communicationsHandler = communicationsHandler;
	}
	
	@Override
	public void run() {
		logger.debug("Waiting for incoming messages");
		boolean handled = false;

		while(!stopped) {
			IncomingMessage message;
			try {
				message = remoteCommunicationHandler.receive();
				if (message == null) {
					logger.debug("Message is null. Ignoring.");
				}
				else {
					logger.debug("Received message from backend");
					handled = communicationsHandler.handle(message);
					
					if (!handled) {
						logger.warn("Ignoring message: "+message);
					}
				}
			} catch (NotYetConnectedException nce) {
				logger.debug("Failed to poll incoming message queue",nce);
				try {
					Thread.sleep(READ_RETRY_TIMEOUT);
				} catch (InterruptedException e1) {
					
				}
			} catch (InterruptedException ie) {
				logger.debug("Polling interrupted");
				stopped = true;
			} catch(Exception e) {
				logger.error("Could not read from incoming queue",e);
				stopped = true;
			}
		}
		
		logger.info("Stopped polling incoming message queue");
	}
	
	public boolean isStopped() {
		return stopped;
	}
	
	public void stop() {
		logger.debug("Stopping incoming message poll thread");
		stopped = true;
	}
}