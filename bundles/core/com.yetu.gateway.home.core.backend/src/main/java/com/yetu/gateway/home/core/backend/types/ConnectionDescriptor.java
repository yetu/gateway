package com.yetu.gateway.home.core.backend.types;

public class ConnectionDescriptor {
	private String ip;
	private int port;
	
	private boolean ssl;
	private String truststoreFilename;
	private String truststorePassword;
	private String keystoreFilename;
	private String KeystorePassword;

	public ConnectionDescriptor(String ip, int port) {
		this.ssl = false;
		this.ip = ip;
		this.port = port;
	}
	
	public ConnectionDescriptor(String ip, int port, String truststoreFilename, String truststorePassword, String keystoreFilename, String keystorePassword) {
		this.ssl = true;
		this.ip = ip;
		this.port = port;
		this.truststoreFilename = truststoreFilename;
		this.truststorePassword = truststorePassword;
		this.keystoreFilename = keystoreFilename;
		this.KeystorePassword = keystorePassword;
	}
	
	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	public boolean isSsl() {
		return ssl;
	}

	public String getTruststoreFilename() {
		return truststoreFilename;
	}

	public String getTruststorePassword() {
		return truststorePassword;
	}

	public String getKeystoreFilename() {
		return keystoreFilename;
	}

	public String getKeystorePassword() {
		return KeystorePassword;
	}
	
	public String toString() {
		return "(host: "+ip+", port: "+port+", ssl: "+ssl+")";
	}
}
