package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionExpired;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionFinished;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted;

/**
 * Simple communication which forwards discovery session events coming from gw to backend.
 * 
 * @author Sebastian Garn
 */
public class DiscoverySessionEventCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(StartDiscoverySessionCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Start session request
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling discovery session event");

				return communication.remoteCommunicationHandler.send((OutgoingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof DiscoverySessionError) {
					logger.debug("DiscoverySessionError as DiscoverySessionEvent recognized");
				}
				else if (message instanceof DiscoverySessionFinished) {
					logger.debug("DiscoverySessionFinished as DiscoverySessionEvent recognized");
				}
				else if (message instanceof DiscoverySessionStarted) {
					logger.debug("DiscoverySessionStarted as DiscoverySessionEvent recognized");
				}
				else if (message instanceof DiscoverySessionExpired) {
					logger.debug("DiscoverySessionStarted as DiscoverySessionExpired recognized");
				}
				else {
					return false;
				}

				return true;
			}
			
			@Override
			public void onTimeout() { }
			
		});
	}
	
	public String toString() {
		return "DiscoverySessionEventCommunication";
	}
}

