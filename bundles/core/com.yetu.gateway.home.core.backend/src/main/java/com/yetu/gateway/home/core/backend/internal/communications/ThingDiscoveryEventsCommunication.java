package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAlreadyAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingDiscovered;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingNotAdded;

public class ThingDiscoveryEventsCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(ThingDiscoveryEventsCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Handshake request
		addCommunicationState(new CommunicationState() {
			@Override
			public void onTimeout() { }
			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling thing event request");

				return communication.remoteCommunicationHandler.send((OutgoingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof ThingAlreadyAdded || message instanceof ThingAdded || message instanceof ThingNotAdded || message instanceof ThingDiscovered) {
					return true;	
				}

				return false;
			}
		});
	}

	public String toString() {
		return "ThingDiscoveryEventsCommunication";
	}
}
