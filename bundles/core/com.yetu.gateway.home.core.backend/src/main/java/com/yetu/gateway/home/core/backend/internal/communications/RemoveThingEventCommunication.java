package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoveFailure;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoved;

public class RemoveThingEventCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(RemoveThingEventCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Remove thing request
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling remove thing event request");
				
				OutgoingMessage event;
				if (message instanceof ThingRemoved) {
					event = (ThingRemoved)message;
					logger.debug("Handling thing removed event");
				}
				else if (message instanceof ThingRemoveFailure) {
					event = (ThingRemoveFailure)message;
					logger.debug("Handling thing remove event failure");
				}
				else {
					logger.error("Unknown Thing remove event");
					return false;
				}

				return communication.remoteCommunicationHandler.send(event);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof ThingRemoved || message instanceof ThingRemoveFailure) {
					return true;	
				}

				return false;
			}
			
			@Override
			public void onTimeout() { }
			
		});
	}
	
	public String toString() {
		return "RemoveThingEventCommunication";
	}
}
