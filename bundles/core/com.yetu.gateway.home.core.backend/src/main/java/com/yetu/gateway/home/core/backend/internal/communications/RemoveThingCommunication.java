package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply;

public class RemoveThingCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(RemoveThingCommunication.class);
	
	private String requestId;
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Remove thing request
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling start Remove thing request");
				
				RemoveThingRequest request = (RemoveThingRequest)message;
				requestId = request.getRequestId();

				return communication.messageTransport.getIncoming().sendMessage(request);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof RemoveThingRequest) {
					return true;	
				}

				return false;
			}
			
			@Override
			public void onTimeout() { }
			
		});
		
		// Start session reply
		addCommunicationState(new CommunicationState() {			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling start Remove thing reply");
				
				RemoveThingReply reply = (RemoveThingReply)message;
				
				return communication.remoteCommunicationHandler.send(reply);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof RemoveThingReply) {
					RemoveThingReply reply = (RemoveThingReply)message;
					if (reply.getCorrelationId().compareTo(requestId) == 0) {
						return true;
					}
				}

				return false;
			}
			
			@Override
			public void onTimeout() { }
		});
	}
	
	public String toString() {
		return "RemoveThingCommunication";
	}
}
