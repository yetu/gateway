package com.yetu.gateway.home.core.backend.internal.communications;

import java.util.ArrayList;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler;
import com.yetu.gateway.home.core.messaging.MessageTransport;
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;

/**
 * A communication describes which messages of which type belong together 
 * @author Sebastian Garn
 *
 */
public abstract class Communication {
	public static final Logger logger = LoggerFactory.getLogger(Communication.class);
	
	private ArrayList<CommunicationStageEntry> communicationStates;
	
	protected int currCommunicationStage;
	
	protected boolean started;
	protected boolean finished;
	
	protected long expirationTime = 0;
	
	MessageTransport messageTransport;
	RemoteCommunicationHandler remoteCommunicationHandler;
	
	public Communication() {
		communicationStates = new ArrayList<CommunicationStageEntry>();
		
		currCommunicationStage = 0;
	
		started = false;
		finished = false;
		
		setup();
	}
	
	/**
	 * Gets called whenever the communication gets instantiated. Allows extending classes to setup their behaviour (how 
	 * to react on which message) by adding communication states to the communication
	 */
	public abstract void setup();
	
	public void init() {
		if (communicationStates.size() == 0) {
			logger.error("Unable to initialize communication since it has not stages");
			return;
		}

		started = true;
	}
	
	public boolean canHandle(AbstractMessage message) {
		if (!started) return false;

		return communicationStates.get(currCommunicationStage).getStage().canHandle(message);
	}
	
	public boolean handle(AbstractMessage message) {
		boolean handled = communicationStates.get(currCommunicationStage).getStage().handle(message);
		
		// proceed with next stage?
		if (handled) {
			if (currCommunicationStage+1 < communicationStates.size()) {
				currCommunicationStage += 1;
				
				Long timeout = communicationStates.get(currCommunicationStage).getTimeout();
				
				// is there a timeout
				if (timeout != null) {
					expirationTime = Calendar.getInstance().getTimeInMillis()+timeout;
				}
				else {
					expirationTime = 0;
				}
			}
			else {
				logger.debug("No CommunicationStages left. Marking communication as finished.");
				finished = true;
				started = false;
				expirationTime = 0;
			}
		}
		else {
			logger.error("Communication was not able to handle message. Remaining in current stage");
		}
		
		return handled;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
	public boolean isStarted() {
		return started;
	}	

	public boolean checkForExpiration() {
		long currTime = Calendar.getInstance().getTimeInMillis();
		
		if (expirationTime != 0 && currTime > expirationTime) {
			logger.debug("Communication expired");

			expirationTime = 0;
			communicationStates.get(currCommunicationStage).getStage().onTimeout();
			
			return true;
		}
		
		return false;
	}
	
	public void addCommunicationState(CommunicationState communicationState) {
		addCommunicationState(communicationState, null);
	}
	
	public void addCommunicationState(CommunicationState communicationState, Long timeout) {
		synchronized (communicationStates) {
			communicationStates.add(new CommunicationStageEntry(communicationState, timeout));
		}
	}
	
	public void setMessageTransport(MessageTransport messageTransport) {
		this.messageTransport = messageTransport;
	}

	public void setRemoteCommunicationHandler(
			RemoteCommunicationHandler remoteCommunicationHandler) {
		this.remoteCommunicationHandler = remoteCommunicationHandler;
	}
	
	/**
	 * Stops all runting timers and sets this communication as finished 
	 */
	public void stop() {
		finished = true;
	}
	
	public String toString() {
		String currStage = "";
		
		if (finished) currStage = "Finished";
		else if (!started) currStage = "Not started";
		else {
			currStage = communicationStates.get(currCommunicationStage).toString();
		}
		
		return "Communication Stage: "+currStage+" ExpirationTime: "+expirationTime;
	}
	
	private class CommunicationStageEntry {
		private CommunicationState stage;
		private Long timeout;
			
		public CommunicationStageEntry(CommunicationState stage, Long timeout) {
			this.stage = stage;
			this.timeout = timeout;
		}
		
		public CommunicationState getStage() {
			return stage;
		}

		public Long getTimeout() {
			return timeout;
		}
	}
}
