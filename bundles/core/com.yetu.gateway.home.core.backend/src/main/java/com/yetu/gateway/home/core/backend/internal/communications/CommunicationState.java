package com.yetu.gateway.home.core.backend.internal.communications;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;

/**
 * A communication stage reflects a state of a communication. 
 * @author Sebastian Garn
 *
 */
public interface CommunicationState {
	public boolean canHandle(AbstractMessage message);

	public boolean handle(AbstractMessage message);
	
	/**
	 * Optional. Whenever this stage gets activated (gw is waiting for a message that fits this stage) a timer gets started.
	 * If it expires (response not received in time) the onTimeout method gets called.
	 * @param long timeout
	 */
	public void onTimeout();
}
