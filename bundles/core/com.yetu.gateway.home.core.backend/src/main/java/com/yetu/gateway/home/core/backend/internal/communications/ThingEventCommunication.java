package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingStatusChanged;

/**
 * Handles primitive thing events like the status of a thing has changed
 *  
 * @author Sebastian Garn
 *
 */
public class ThingEventCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(ThingEventCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;

		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				OutgoingMessage event;
				
				if (message instanceof ThingStatusChanged) {
					logger.debug("Handling thing status changed");
					event = (ThingStatusChanged)message;
				}
				// add more events here
				else {
					logger.error("Unable to handle unknown thing event");
					return false;
				}

				return communication.remoteCommunicationHandler.send(event);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof ThingStatusChanged) {
					return true;	
				}

				return false;
			}

			@Override
			public void onTimeout() { }
		});
	}

	public String toString() {
		return "ThingEventCommunication";
	}
}
