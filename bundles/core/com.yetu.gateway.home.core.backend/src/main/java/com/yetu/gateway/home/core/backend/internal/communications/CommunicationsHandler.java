package com.yetu.gateway.home.core.backend.internal.communications;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler;
import com.yetu.gateway.home.core.messaging.MessageTransport;
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HeartbeatRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionExpired;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionFinished;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.PropertyChanged;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAlreadyAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingDiscovered;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingNotAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoveFailure;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoved;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingStatusChanged;

/**
 * Takes care of creating and stopping communications. Additionally it routes abstract messages to active communications.
 * 
 * @author Sebastian Garn
 *
 */
public class CommunicationsHandler {
	public static final Logger logger = LoggerFactory.getLogger(CommunicationsHandler.class);
	
	private ArrayList<Communication> activeCommunications;
	private MessageTransport messageTransport;
	private RemoteCommunicationHandler remoteCommunicationHandler;
	private CommunicationsObserver communicationsObserver;
	
	public CommunicationsHandler(RemoteCommunicationHandler remoteCommunicationHandler, MessageTransport messageTransport) {
		activeCommunications = new ArrayList<Communication>();
		this.remoteCommunicationHandler = remoteCommunicationHandler;
		this.messageTransport = messageTransport;
		
		communicationsObserver = new CommunicationsObserver();
	}
	
	/**
	 * Handles a message by calling the appropriate communication. Synchronized to ensure that each running configuration state gets finished
	 * before a new incoming message (by remote connection or internal queue) gets handled. This ensures that everything happens in order
	 * @param AbstractMessage message
	 * @return true if communication was found and able to handle message
	 */
	public synchronized boolean handle(AbstractMessage message) {
		Communication handlingCommunication = null;
		
		// search for active communication
		handlingCommunication = getActiveCommunication(message);
		
		// create new communication
		if (handlingCommunication == null) {
			logger.debug("No running communication found which handles "+message+". Trying to create a new one");
			handlingCommunication = createCommunication(message);
			
			if (handlingCommunication == null) {
				logger.error("Ignoring message: no active communication found and no communication created for message");
				return false;	
			}
		}
			
		boolean handled = handlingCommunication.handle(message);
			
		if (!handled) {
			logger.error("An error occured while communication was handling the message.");
			return false;
		}
		
		cleanUpFinishedCommunications();
			
		return handled;
	}
	
	/**
	 * Searches for an existing communication which is in a stage to handle the given message
	 * @param AbstractMessage message
	 * @return null or the handling communication
	 */
	private Communication getActiveCommunication(AbstractMessage message) {
		synchronized (activeCommunications) {
			for (Communication currCommunication : activeCommunications) {
				if (currCommunication.canHandle(message)) {
					logger.debug("Found active communication which can handle message: "+currCommunication);
					return currCommunication;
				}
				else {
					logger.debug("This doesnt fit: "+currCommunication);
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Searches for communications which are finished and removes them from active communications list
	 * @param AbstractMessage message
	 */
	private void cleanUpFinishedCommunications() {
		logger.debug("Cleaning up finished communications");
		ArrayList<Communication> toRemove = new ArrayList<Communication>();
		
		synchronized (activeCommunications) {
			for (Communication currCommunication : activeCommunications) {
				if (currCommunication.isFinished()) {
					logger.debug("Communication "+currCommunication+" is finished. Removing it.");
					toRemove.add(currCommunication);
				}
			}
			
			activeCommunications.removeAll(toRemove);
		}
	}

	/**
	 * This gets called whenever there is a message which doesn't belong to a running communication
	 * @param Abstract message
	 * @return Communication or null if message does not create a new communication
	 */
	private Communication createCommunication(AbstractMessage message) {
		Communication handlingCommunication = null;
		
		if (message instanceof HandshakeRequest) {
			handlingCommunication = new HandshakeCommunication();
		}
		else if (message instanceof StartDiscoverySessionRequest) {
			handlingCommunication = new StartDiscoverySessionCommunication();
		}
		else if (message instanceof ConfigurationRequest) {
			handlingCommunication = new ConfigurationCommunication();
		}
		else if (message instanceof DiscoverySessionError || message instanceof DiscoverySessionFinished || message instanceof DiscoverySessionStarted || message instanceof DiscoverySessionExpired) {
			handlingCommunication = new DiscoverySessionEventCommunication();
		}
		else if (message instanceof StopDiscoverySessionRequest) {
			handlingCommunication = new StopDiscoverySessionCommunication();
		}
		else if (message instanceof HeartbeatRequest) {
			handlingCommunication = new HeartbeatCommunication();
		}
		else if (message instanceof RemoveThingRequest) {
			handlingCommunication = new RemoveThingCommunication();
		}
		else if (message instanceof ThingRemoved || message instanceof ThingRemoveFailure) {
			handlingCommunication = new RemoveThingEventCommunication();
		}
		else if (message instanceof SetProperty) {
			handlingCommunication = new PropertySetCommunication();
		}
		else if (message instanceof SetPropertyError || message instanceof PropertyChanged) {
			handlingCommunication = new PropertyEventCommunication();
		}
		else if (message instanceof ThingAdded || message instanceof ThingNotAdded || message instanceof ThingDiscovered || message instanceof ThingAlreadyAdded) {
			handlingCommunication = new ThingDiscoveryEventsCommunication();
		}
		else if (message instanceof ResetRequest) {
			handlingCommunication = new ResetCommunication();
		}
		else if (message instanceof ThingStatusChanged) {
			handlingCommunication = new ThingEventCommunication();
		}
		
		if (handlingCommunication == null) {
			return null;
		}
		
		logger.debug("New "+handlingCommunication+" created");
		
		// start observer if necessary
		if (communicationsObserver.stopped) {
			communicationsObserver.reset();
			Thread observerThread = new Thread(communicationsObserver);
			observerThread.setName("CommunicationsObserverThread");
			observerThread.start();
		}

		// init new communication
		synchronized (activeCommunications) {
			handlingCommunication.setMessageTransport(messageTransport);
			handlingCommunication.setRemoteCommunicationHandler(remoteCommunicationHandler);
			handlingCommunication.init();
			
			if (!handlingCommunication.canHandle(message)) {
				logger.error("The previously created communication is not able to handle the message");
				return null;
			}
			
			activeCommunications.add(handlingCommunication);	
		}
		
		return handlingCommunication;
	}
	
	public void stop() {
		
		// Stops all active communications
		logger.debug("Stopping all active communications");
		
		synchronized (activeCommunications) {
			for (Communication currCommunication : activeCommunications) {
				logger.debug("Stopping communication: "+currCommunication);
				currCommunication.stop();
			}
			
			activeCommunications.clear();
		}
		
		communicationsObserver.stop();
	}
	
	private class CommunicationsObserver implements Runnable {
		private boolean stopped = true;
		
		@Override
		public void run() {
			while(!stopped) {
				try {
					Thread.sleep(500);

					synchronized(activeCommunications) {
						for (Communication currCommunication : activeCommunications) {
							if (currCommunication.checkForExpiration()) {
								break;
							}
						}
					}
				} catch (InterruptedException e) {
					logger.debug("Failed to observe active communications: "+e.toString());
				}
			}
		}
		
		public void stop() {
			stopped = true;
		}
		
		public void reset() {
			stopped = false;
		}
	}
}
