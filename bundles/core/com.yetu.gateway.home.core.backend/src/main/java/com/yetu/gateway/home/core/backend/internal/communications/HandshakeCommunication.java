package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.errorhandling.MessageReplyTimeout;
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;

public class HandshakeCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(HandshakeCommunication.class);

	private HandshakeRequest handshakeRequest = null;
	private String requestId = "";
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Handshake request
		addCommunicationState(new CommunicationState() {
			@Override
			public void onTimeout() { }
			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling handshake request");
				
				HandshakeRequest request = (HandshakeRequest)message;
				requestId = request.getRequestId();

				// save an instance of request in case a timeout occurs
				handshakeRequest = request;

				return communication.remoteCommunicationHandler.send((OutgoingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof HandshakeRequest) {
					return true;	
				}

				return false;
			}
		});
		
		// Handshake reply
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling handshake reply");
				
				return communication.messageTransport.getIncoming().sendMessage((IncomingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof HandshakeReply) {
					HandshakeReply reply = (HandshakeReply)(message);
					if (reply.getCorrelationId().equals(requestId)) {
						return true;
					}
				}

				return false;
			}
			
			@Override
			public void onTimeout() {
				logger.warn("HandshakeReply not received within 5 seconds. Throwing exception");
				
				// inform waiting channel threads about missing message
				communication.messageTransport.getOutgoing().signalChannelError(new MessageReplyTimeout(handshakeRequest));

				communication.stop();
			}
		}, new Long(5000));
	}

	public String toString() {
		return "HandshakeCommunication";
	}
}
