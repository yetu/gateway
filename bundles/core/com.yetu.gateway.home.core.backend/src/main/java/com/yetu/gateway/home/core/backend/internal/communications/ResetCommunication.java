package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ResetReply;

public class ResetCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(ResetCommunication.class);

	private String requestId = "";
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Reset request
		addCommunicationState(new CommunicationState() {
			@Override
			public void onTimeout() { }
			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling reset request");
				
				ResetRequest req = (ResetRequest)message;
				requestId = req.getRequestId();

				return communication.messageTransport.getIncoming().sendMessage((IncomingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof ResetRequest) {
					return true;	
				}

				return false;
			}
		});
		
		// Reset reply
		addCommunicationState(new CommunicationState() {
			@Override
			public void onTimeout() { }
			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling reset reply");

				return communication.remoteCommunicationHandler.send((OutgoingMessage)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof ResetReply) {
					ResetReply reply = (ResetReply)(message);
					if (reply.getCorrelationId().equals(requestId)) {
						return true;
					}
				}

				return false;
			}
			
		});
	}

	public String toString() {
		return "ResetCommunication";
	}
}
