package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply;

public class StartDiscoverySessionCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(StartDiscoverySessionCommunication.class);
	
	private String sessionId, requestId;
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Start session request
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling start discovery session request");
				
				StartDiscoverySessionRequest request = (StartDiscoverySessionRequest)message;
				sessionId = request.getSessionId();
				requestId = request.getRequestId();

				return communication.messageTransport.getIncoming().sendMessage(request);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof StartDiscoverySessionRequest) {
					return true;	
				}

				return false;
			}
			
			@Override
			public void onTimeout() { }
			
		});
		
		// Start session reply
		addCommunicationState(new CommunicationState() {			
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling start discovery session reply");
				
				StartDiscoverySessionReply reply = (StartDiscoverySessionReply)message;
				communication.remoteCommunicationHandler.send(reply);
				
				return true;
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof StartDiscoverySessionReply) {
					StartDiscoverySessionReply msg = (StartDiscoverySessionReply)message;
					if (msg.getSessionId().compareTo(sessionId) == 0 && msg.getCorrelationId().compareTo(requestId) == 0) {
						return true;
					}
				}

				return false;
			}
			
			@Override
			public void onTimeout() { }
		});
	}
	
	public String toString() {
		return "StartDiscoverySessionCommunication";
	}
}
