package com.yetu.gateway.home.core.backend.internal.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;

public class PropertySetCommunication extends Communication {
	public static final Logger logger = LoggerFactory.getLogger(PropertySetCommunication.class);
	
	@Override
	public void setup() {
		final Communication communication = this;
		
		// Handshake request
		addCommunicationState(new CommunicationState() {
			@Override
			public boolean handle(AbstractMessage message) {
				logger.debug("Handling set property message");

				return communication.messageTransport.getIncoming().sendMessage((SetProperty)message);
			}
			
			@Override
			public boolean canHandle(AbstractMessage message) {
				if (message instanceof SetProperty) {
					return true;	
				}

				return false;
			}

			@Override
			public void onTimeout() { }
		});
	}

	public String toString() {
		return "PropertySetCommunication";
	}
}
