package com.yetu.gateway.home.core.management.test.discovery;


import org.eclipse.smarthome.core.thing.ThingTypeUID;

public class ManagementTestConstants {
	public static final int DEFAULT_DISCOVERY_TIME_OUT = 300;
	
	public static final String BINDING_ID = "testgatewaymanager";
	
	public final static ThingTypeUID THING_TYPE_TEST = new ThingTypeUID(BINDING_ID, "test");
	
}
