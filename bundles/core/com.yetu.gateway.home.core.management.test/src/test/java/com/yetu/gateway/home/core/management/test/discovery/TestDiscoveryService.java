package com.yetu.gateway.home.core.management.test.discovery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.config.discovery.AbstractDiscoveryService;
import org.eclipse.smarthome.config.discovery.DiscoveryResult;
import org.eclipse.smarthome.config.discovery.DiscoveryResultBuilder;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestDiscoveryService extends AbstractDiscoveryService{

	public static final int SCAN_STATE_UNKNOWN  = 0;
	public static final int SCAN_STATE_STARTED  = 1;
	public static final int SCAN_STATE_ERROR    = 2;
	public static final int SCAN_STATE_FINISHED = 3;
	public static final int SCAN_STATE_ABORTED	= 4;
	
	protected static Logger logger = LoggerFactory.getLogger(TestDiscoveryService.class);
	
	public static List<TestDiscoveryService> instances = null;
	private int id;
	private static int currentThingCount = 33;
	public int scanState = SCAN_STATE_UNKNOWN;
	
	private boolean startWithError = false;
	
	public TestDiscoveryService() throws IllegalArgumentException {
		super(ManagementTestConstants.DEFAULT_DISCOVERY_TIME_OUT);
		
		TestDiscoveryService.getInstances().add(this);
		id = TestDiscoveryService.getInstances().size();
		logger.debug("TestDiscoveryService {} generated",id);
	}
		
	public int getId(){
		return id;
	}
	
	public static List<TestDiscoveryService> getInstances(){
		if (instances == null){
			instances = new ArrayList<>();
		}
		return instances;
	}
	
	@Override
	public boolean isBackgroundDiscoveryEnabled() {
		return false;
	}
	
	
	@Override
	protected void startScan() {
		logger.debug("starting discovery {}",id);
		if (startWithError){
			this.scanState = SCAN_STATE_ERROR;
			if (this.scanListener != null){
				//this.scanListener.onErrorOccurred(new Exception("Error was thrown during start procedure"));
				sendError(new Exception("Error was thrown during start procedure"));
			}
			
		} else {
			this.scanState = SCAN_STATE_STARTED;
		}
	}
		
	@Override
	public Set<ThingTypeUID> getSupportedThingTypes() {
		  HashSet<ThingTypeUID> set = new HashSet<>();
		  set.add(ManagementTestConstants.THING_TYPE_TEST);
		  return set;
	}
	
	public int getScanState(){
		return scanState;
	}
	
	public static ThingUID generateThingUid(ThingTypeUID thingTypeUID){
		currentThingCount++;
		return new ThingUID(thingTypeUID,""+currentThingCount);
	}
	
	public boolean getSendErrorWhenScanIsStarting(){
		return startWithError;
	}
	
	public void setSendErrorWhenScanIsStarting(boolean sendError){
		startWithError = sendError;
	}
	
	public String findThing(ThingTypeUID thingTypeUid){
	
		ThingUID uid =  TestDiscoveryService.generateThingUid(thingTypeUid);
		
			
		Map<String, Object> properties = new HashMap<>();
		DiscoveryResult result = DiscoveryResultBuilder.create(uid)
				.withProperties(properties)
				.withLabel("new TestThing")
				.build();
		
		thingDiscovered(result);	
		return uid.getAsString();
	}
	
	public void finishDiscovery(){	
		this.scanState = SCAN_STATE_FINISHED;
		logger.debug("TestDiscoveryService {} has finihed",id);
		this.stopScan();
		//if (scanListener != null){
			//scanListener.onFinished();
		//}
	}
	
	public void sendError(Exception exception){
		this.scanListener.onErrorOccurred(exception);
	}
}