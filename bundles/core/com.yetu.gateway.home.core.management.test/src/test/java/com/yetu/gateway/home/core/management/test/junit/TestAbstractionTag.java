package com.yetu.gateway.home.core.management.test.junit;

import org.junit.Test;

import com.yetu.gateway.home.core.management.internal.util.AbstractionTag;
import static org.junit.Assert.assertEquals;
public class TestAbstractionTag {

	@Test
	public void testAbstractionTag_valid(){
		AbstractionTag tag = new AbstractionTag("#abstraction:comp_id=component_id;comp_type=COMPONENT_TYPE;cap_id=CAPABILITY_ID;prop_unit=PROPERTY_UNIT");
		assertEquals("component_id",tag.getAttribute(AbstractionTag.COMPONENT_ID));
		assertEquals("component_id",tag.getComponentId());
		assertEquals("COMPONENT_TYPE",tag.getAttribute(AbstractionTag.COMPONENT_TYPE));
		assertEquals("COMPONENT_TYPE",tag.getComponentTypeId());
		assertEquals("CAPABILITY_ID",tag.getAttribute(AbstractionTag.CAPABILITY_ID));
		assertEquals("CAPABILITY_ID",tag.getCapabilityId());		
		assertEquals("PROPERTY_UNIT",tag.getAttribute(AbstractionTag.PROPERTY_UNIT));	
		assert tag.isValid();
		
		
		tag = new AbstractionTag("#abstraction:comp_name=COMPONENT NAME WITH SPACES;comp_id=component_id;comp_type=COMPONENT_TYPE;cap_id=CAPABILITY_ID;prop_unit=PROPERTY_UNIT");
		assertEquals("component_id",tag.getAttribute(AbstractionTag.COMPONENT_ID));
		assertEquals("component_id",tag.getComponentId());
		assertEquals("COMPONENT_TYPE",tag.getAttribute(AbstractionTag.COMPONENT_TYPE));
		assertEquals("COMPONENT_TYPE",tag.getComponentTypeId());
		assertEquals("CAPABILITY_ID",tag.getAttribute(AbstractionTag.CAPABILITY_ID));
		assertEquals("CAPABILITY_ID",tag.getCapabilityId());		
		assertEquals("PROPERTY_UNIT",tag.getAttribute(AbstractionTag.PROPERTY_UNIT));
		assertEquals("COMPONENT NAME WITH SPACES", tag.getAttribute(AbstractionTag.COMPONENT_NAME));
		assert tag.isValid();
	}
	
	@Test
	public void testAbstractionTag_missingAttributes(){
		AbstractionTag tag = new AbstractionTag("#abstraction:comp_type=COMPONENT_TYPE;cap_id=CAPABILITY_ID;prop_unit=PROPERTY_UNIT");

		assertEquals("COMPONENT_TYPE",tag.getAttribute(AbstractionTag.COMPONENT_TYPE));
		assertEquals("COMPONENT_TYPE",tag.getComponentTypeId());
		assertEquals("CAPABILITY_ID",tag.getAttribute(AbstractionTag.CAPABILITY_ID));
		assertEquals("CAPABILITY_ID",tag.getCapabilityId());		
		assertEquals("PROPERTY_UNIT",tag.getAttribute(AbstractionTag.PROPERTY_UNIT));	
		assert !tag.isValid();
		assert tag.getMissingAttributes().contains(AbstractionTag.COMPONENT_ID);
		
		tag = new AbstractionTag("#abstraction:comp_id=component_id;cap_id=CAPABILITY_ID;prop_unit=PROPERTY_UNIT");

		assertEquals("component_id",tag.getAttribute(AbstractionTag.COMPONENT_ID));
		assertEquals("component_id",tag.getComponentId());
		assertEquals("CAPABILITY_ID",tag.getAttribute(AbstractionTag.CAPABILITY_ID));
		assertEquals("CAPABILITY_ID",tag.getCapabilityId());		
		assertEquals("PROPERTY_UNIT",tag.getAttribute(AbstractionTag.PROPERTY_UNIT));	
		assert !tag.isValid();
		assert tag.getMissingAttributes().contains(AbstractionTag.COMPONENT_TYPE);
		
		tag = new AbstractionTag("#abstraction:comp_id=component_id;comp_type=COMPONENT_TYPE;prop_unit=PROPERTY_UNIT");
		assertEquals("component_id",tag.getAttribute(AbstractionTag.COMPONENT_ID));
		assertEquals("component_id",tag.getComponentId());
		assertEquals("COMPONENT_TYPE",tag.getAttribute(AbstractionTag.COMPONENT_TYPE));
		assertEquals("COMPONENT_TYPE",tag.getComponentTypeId());		
		assertEquals("PROPERTY_UNIT",tag.getAttribute(AbstractionTag.PROPERTY_UNIT));	
		assert !tag.isValid();
		assert tag.getMissingAttributes().contains(AbstractionTag.CAPABILITY_ID);
		
		tag = new AbstractionTag("#abstraction:prop_unit=PROPERTY_UNIT");		
		assertEquals("PROPERTY_UNIT",tag.getAttribute(AbstractionTag.PROPERTY_UNIT));	
		assert !tag.isValid();
		assert tag.getMissingAttributes().contains(AbstractionTag.CAPABILITY_ID);
		assert tag.getMissingAttributes().contains(AbstractionTag.COMPONENT_TYPE);
		assert tag.getMissingAttributes().contains(AbstractionTag.COMPONENT_ID);
	}
}
