package com.yetu.gateway.home.core.management.test.discovery;

import org.codehaus.groovy.runtime.metaclass.NewStaticMetaMethod;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestThingHandler extends BaseThingHandler{

	private static Logger logger = LoggerFactory.getLogger(TestThingHandler.class);
	
	
	public TestThingHandler(Thing thing) {
		super(thing);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleCommand(ChannelUID channelUID, Command command) {
		logger.debug("received command {} for channel {}",command,channelUID);
		
		if (command instanceof OnOffType){
			OnOffType on = (OnOffType)command;
			updateState(channelUID, on);
		}
		
		
		
		
	//	throw new UnsupportedOperationException();
		
	}

}
