package com.yetu.gateway.home.core.management.test.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.smarthome.core.library.types.HSBType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.types.Command;
import org.junit.Test;

import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.ThingModelFactory;
import com.yetu.abstraction.model.Unit;
import com.yetu.abstraction.model.ValueType;
import com.yetu.gateway.home.core.management.internal.things.PropertyValueTransformer;

public class TestPropertyValueTransformer {
	
	
	
	

	@Test
	public void testHSBTypeConvertion_normal() {
		
		HSBType hsbType;
		try {
			hsbType = PropertyValueTransformer.convertColorToHSBType("11.11;22.22;33.33");
			assertEquals(hsbType.getHue().doubleValue(),11.11,0.0);
			assertEquals(hsbType.getSaturation().doubleValue(),22.22,0.0);
			assertEquals(hsbType.getBrightness().doubleValue(),33.33,0.0);
			
			hsbType = PropertyValueTransformer.convertColorToHSBType("11;22;33");
			assertEquals(hsbType.getHue().doubleValue(),11.0,0.0);
			assertEquals(hsbType.getSaturation().doubleValue(),22.0,0.0);
			assertEquals(hsbType.getBrightness().doubleValue(),33.0,0.0);
			
			
			hsbType = PropertyValueTransformer.convertColorToHSBType("11,11;22,22;33,33");
			assertEquals(hsbType.getHue().doubleValue(),11.11,0.0);
			assertEquals(hsbType.getSaturation().doubleValue(),22.22,0.0);
			assertEquals(hsbType.getBrightness().doubleValue(),33.33,0.0);
		} catch (Exception exc){
			exc.printStackTrace();
		}
	}

	@Test
	public void test_CommandGeneration_Switch_on(){
		
		Channel channel = new Channel(new ChannelUID("testbinding:testtype:testthing:testchannel"), "Switch");
		Property property = ThingModelFactory.eINSTANCE.createProperty();
		
		property.setUnit(generateUnit("BOOLEAN",ValueType.BOOLEAN,null));
		
		Command cmd;
		
		try {
			cmd = PropertyValueTransformer.generateCommand(channel, property, "true");
			assertNotNull(cmd);
			assertTrue(cmd instanceof OnOffType);
			assertTrue(((OnOffType)cmd).compareTo(OnOffType.ON)== 0);
		} catch (Exception exc){
			exc.printStackTrace();
		}
	}
	
	@Test
	public void test_CommandGeneration_Switch_off(){
		
		Channel channel = new Channel(new ChannelUID("testbinding:testtype:testthing:testchannel"), "Switch");
		Property property = ThingModelFactory.eINSTANCE.createProperty();
		
		property.setUnit(generateUnit("BOOLEAN",ValueType.BOOLEAN,null));
		
		Command cmd;
		
		try {
			cmd = PropertyValueTransformer.generateCommand(channel, property, "false");
			assertNotNull(cmd);
			assertTrue(cmd instanceof OnOffType);
			assertTrue(((OnOffType)cmd).compareTo(OnOffType.OFF)== 0);
		} catch (Exception exc){
			exc.printStackTrace();
		}
	}
	
	
	
	
	
	
	private Unit generateUnit(String id, ValueType valueType, String symbol){
		Unit unit = ThingModelFactory.eINSTANCE.createUnit();
		unit.setValueType(valueType);
		unit.setId(id);
		unit.setSymbol(symbol);
		return unit;
	}
	
}
