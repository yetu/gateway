package com.yetu.gateway.home.core.management.test.discovery;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupType;
import org.eclipse.smarthome.core.thing.type.ChannelGroupTypeUID;
import org.eclipse.smarthome.core.thing.type.ChannelType;
import org.eclipse.smarthome.core.thing.type.ChannelTypeUID;
import org.eclipse.smarthome.core.thing.type.ThingType;
import org.eclipse.smarthome.core.types.StateDescription;
import org.eclipse.smarthome.core.types.StateOption;



public class TestThingHandlerFactory extends BaseThingHandlerFactory{

	@Override
	public boolean supportsThingType(ThingTypeUID thingTypeUID) {
		return (ManagementTestConstants.THING_TYPE_TEST.equals(thingTypeUID));		
	}

	@Override
	protected ThingHandler createHandler(Thing thing) {
		return new TestThingHandler(thing);
	}


	public ThingType generateThingType(ThingUID uid) {
		List<ChannelDefinition> channelDefinitions = new ArrayList<>();
		List<ChannelGroupDefinition> channelGroupDefinitions = new ArrayList<>();
		
		this.addSensorChannels(uid, channelDefinitions, channelGroupDefinitions);
				
		List<String> bridges = null;
		Map<String,String> properties = getProperties();
		
		String label = "TestThing";
		String description = "Thing for testing the Gateway Manager";
		
		return new ThingType(uid.getThingTypeUID(), bridges, label, description, channelDefinitions, channelGroupDefinitions, properties, null);
	}

	protected Map<String,String> getProperties(){
		Map<String,String> properties = new HashMap<String, String>();

		properties.put("protocol","TEST");
	
		
		properties.put("abstraction_thing_name","Simulated Test Device");
		properties.put("abstraction_main_comp","socket");
		
		
		return properties;
	}
	
	private void addSensorChannels(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		String componentId = "socket";
		
		// generate Switchabel for Socket
		StateDescription stateDescription = new StateDescription(new BigDecimal(0),new BigDecimal(1),new BigDecimal(1),"", false, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":switchable");
		
		Set<String> tags = new HashSet<>();	
	
		tags.add("#abstraction:comp_id="+componentId+";comp_type=SOCKET;cap_id=SWITCHABLE");
	
		ChannelType ct = new ChannelType(ctuid,false,"Switch","switchable","descr","Switch", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition("switchable",ct);
	
		

		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+":socket");
		
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Socket","Socket", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition("socket",cgt);
	
		
		//channelDefs.add(def);
		groupDefs.add(gd);
	}
	/**
     * Creates a thing based on given thing type uid.
     * 
     * @param thingTypeUID
     *            thing type uid (should not be null)
     * @param thingUID
     *            thingUID (should not be null)
     * @param configuration
     *            (should not be null)
     * @param bridgeUID
     *            (can be null)
     * @return thing (can be null, if thing type is unknown)
     */
	@Override
    public Thing createThing(ThingTypeUID thingTypeUID, Configuration configuration, ThingUID thingUID, ThingUID bridgeUID) {		
	//	return new TestThing(thingUID);
		ThingType thingType = generateThingType(thingUID);
		
        if (thingType != null) {
            Thing thing = ThingFactory.createThing(thingType, thingUID, configuration, bridgeUID,
                    getConfigDescriptionRegistry()); 
           
            return thing;
        } else {
            return null;
        }
		
    }
	
	
	
}
