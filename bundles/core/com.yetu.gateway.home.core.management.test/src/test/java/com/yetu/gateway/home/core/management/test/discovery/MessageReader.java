package com.yetu.gateway.home.core.management.test.discovery;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ConfigurationReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionFinished;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.PropertyChanged;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StopDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingDiscovered;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoved;
import com.yetu.gateway.home.core.messaging.types.queue.QueueChannel;

public class MessageReader extends Thread{
	
	private static Logger logger = LoggerFactory.getLogger(MessageReader.class);
	
	QueueChannel<OutgoingMessage> outQueue;
	
	ArrayList<OutgoingMessage> receivedMessages = null;
	
	
	public MessageReader(QueueChannel<OutgoingMessage> outQueue){
		this.outQueue = outQueue;
	//	if (receivedMessages == null){
			receivedMessages = new ArrayList<>();
			
		//}
	}
	
	public void cleanUp(){
		synchronized(receivedMessages){
			receivedMessages.clear();
		}
	}
	
	public ThingAdded getThingAddedMessage(String thingId, String sessionId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof ThingAdded){
				ThingAdded message = (ThingAdded)msg;
				if (message.getSessionId() != null){
					if (message.getSessionId().equalsIgnoreCase(sessionId)){
				//		logger.debug("message thing id = {} vs. {}.",message.getThingId(),thingId);
						if (message.getThing() != null){
							if (message.getThing().getId().equalsIgnoreCase(thingId)){
								return message;
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	
	public ThingDiscovered getThingDiscoveredMessage(String thingId, String sessionId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof ThingDiscovered){
				ThingDiscovered message = (ThingDiscovered)msg;
				if (message.getSessionId() != null){
					if (message.getSessionId().equalsIgnoreCase(sessionId)){
				//		logger.debug("message thing id = {} vs. {}.",message.getThingId(),thingId);
						if (message.getThingId().equalsIgnoreCase(thingId)){
							
							return message;
						}
					}
				}
			}
		}
		return null;
	}
	
	public ConfigurationReply getConfigurationReply(){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof ConfigurationReply){
				ConfigurationReply message = (ConfigurationReply)msg;
				
					return message;
				
			}
		}
		return null;
	}
	
	public ThingRemoved getThingRemovedMessage(String thingId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof ThingRemoved){
				ThingRemoved message = (ThingRemoved)msg;
				if (message.getThingId() != null){
					if (message.getThingId().equalsIgnoreCase(thingId)) {				
						return message;
					}
					
				};
			}
		}
		return null;
	}
	
	public RemoveThingReply getRemoveThingReply(String requestId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof RemoveThingReply){
				RemoveThingReply message = (RemoveThingReply)msg;
				if (message.getCorrelationId() == requestId){
					return message;
				}
			}
		}
		return null;
	}
	
	public SetPropertyError getSetPropertyError(SetPropertyError.ErrorCode errorCode){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof SetPropertyError){
				SetPropertyError message = (SetPropertyError)msg;
				if (message.getErrorCode() == errorCode){
					return message;
				}
			}
		}
		return null;
	}
	
	public PropertyChanged getPropertyChanged(String thingId, String componentid, String capability, String property, String value){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof PropertyChanged){
				PropertyChanged message = (PropertyChanged)msg;
				if (message.getThingId().equalsIgnoreCase(thingId)){
					if (message.getComponentId().equalsIgnoreCase(componentid)){
						if (message.getCapabilityId().equalsIgnoreCase(capability)){
							if (message.getPropertyName().equalsIgnoreCase(property)){
								if (message.getValue().equalsIgnoreCase(value)){
									return message;
								}
							}
						}
					}
				
				}
			}
		}
		return null;
	}
	
	public DiscoverySessionError getDiscoverySessionError(String sessionId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof DiscoverySessionError){
				DiscoverySessionError message = (DiscoverySessionError)msg;
				if (message.getSessionId() != null){
					if (message.getSessionId().equalsIgnoreCase(sessionId)){
						return message;
					}
				}
			}
		}
		return null;
	}
	
	public DiscoverySessionFinished getDiscoverySessionFinished(String sessionId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof DiscoverySessionFinished){
				DiscoverySessionFinished message = (DiscoverySessionFinished)msg;
				if (message.getSessionId() != null){
					if (message.getSessionId().equalsIgnoreCase(sessionId)){
						return message;
					}
				}
			}
		}
		return null;
	}
	
	public DiscoverySessionStarted getDiscoverySessionStarted(String sessionId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			if (msg instanceof DiscoverySessionStarted){
				DiscoverySessionStarted message = (DiscoverySessionStarted)msg;
				if (message.getSessionId() != null){
					if (message.getSessionId().equalsIgnoreCase(sessionId)){
						return message;
					}
				}
			}
		}
		return null;
	}
	
	public StopDiscoverySessionReply getStopDsicoverySessionReply(String sessionId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			logger.debug("Message {}",msg.getClass());
			if (msg instanceof StopDiscoverySessionReply){
				StopDiscoverySessionReply reply = (StopDiscoverySessionReply)msg;
				if (reply.getSessionId() != null){
					if (reply.getSessionId().equalsIgnoreCase(sessionId)){
						return reply;
					}
				}
			}
		}
		return null;
	}
	
	public StartDiscoverySessionReply getStartDiscoverySessionReply(String sessionId){
		List<OutgoingMessage> messages = getReceivedMessages();
		for (OutgoingMessage msg : messages){
			logger.debug("Message {}",msg.getClass());
			if (msg instanceof StartDiscoverySessionReply){
				StartDiscoverySessionReply reply = (StartDiscoverySessionReply)msg;
				if (reply.getSessionId() != null){
					if (reply.getSessionId().equalsIgnoreCase(sessionId)){
						return reply;
					}
				}
			}
		}
		return null;
	}
	
	public List<OutgoingMessage> getReceivedMessages(){
		List<OutgoingMessage> result = new ArrayList<>();
		synchronized (receivedMessages) {
			result.addAll(receivedMessages);
		}		
		return result;
	}
	
	protected void addMessage(OutgoingMessage msg){
		logger.debug("received Message {}",msg.getClass());
		synchronized(receivedMessages){
			logger.debug("added to list {}",msg.getClass());
			receivedMessages.add(msg);
		}
		logger.debug("msg added");
	}
	
	public void run(){
		OutgoingMessage newMessage;
		while (!isInterrupted()){
			try {
				newMessage = outQueue.getNextMessage();
				if (newMessage != null){
					addMessage(newMessage);
				}
			} catch (InterruptedException exc){
				
			}
		}
		logger.debug("fertisch");
		
	}
	
	
}
