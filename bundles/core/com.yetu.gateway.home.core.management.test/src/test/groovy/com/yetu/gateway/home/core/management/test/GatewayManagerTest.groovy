package com.yetu.gateway.home.core.management.test

import static org.junit.Assert.*

import org.eclipse.smarthome.core.thing.ThingUID
import org.junit.After;
import org.junit.Before
import org.junit.BeforeClass;
import org.junit.Test
import org.osgi.service.cm.ManagedService

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.yetu.abstraction.model.Capability
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.ComponentType;
import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.Thing
import com.yetu.abstraction.model.ThingModelFactory;
import com.yetu.abstraction.model.impl.ThingModelFactoryImpl;
import com.yetu.abstraction.spec.ThingSpecification;
import com.yetu.gateway.home.core.management.internal.GatewayManagerImpl;
import com.yetu.gateway.home.core.management.internal.YetuThingFactory;
import com.yetu.gateway.home.core.management.internal.YetuThingSpecificationLoader;
import com.yetu.gateway.home.core.management.internal.discovery.DiscoveryProcessor;
import com.yetu.gateway.home.core.management.internal.things.ThingMapping;
import com.yetu.gateway.home.core.management.test.discovery.ManagementTestConstants;
import com.yetu.gateway.home.core.management.test.discovery.MessageReader;
import com.yetu.gateway.home.core.management.test.discovery.TestDiscoveryService;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ConfigurationReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.queue.QueueChannel;

/**
 *
 */
class GatewayManagerTest extends AbstractTest {
	private static GatewayManagerImpl gatewayManager;
	private static List<TestDiscoveryService> testDiscoveryServices;
	private static TestDiscoveryService discoveryService;
	protected static QueueChannel<OutgoingMessage> outQueue = null;
	protected static QueueChannel<IncomingMessage> inQueue = null;
	protected static MessageReader messageReader = null;
	protected static ThingSpecification thingSpecification = null;
	private static final int DURATION_WAITING_FOR_DISCOVERY_SERVICES = 50;
	
	@Before
	void setup() {
		gatewayManager = waitForService(ManagedService.class, GatewayManagerImpl.class, 5000)
		testDiscoveryServices = TestDiscoveryService.getInstances();
		assert gatewayManager != null
		waitFor({testDiscoveryServices.isEmpty() == false},2000);
		assert testDiscoveryServices.isEmpty() != true;
		discoveryService = testDiscoveryServices.get(0);
		assert discoveryService != null;
		removeAllRegisteredThings();
		loadThingSpecification()
		prepareGatewayManager();
	}
	
		
	protected void removeAllRegisteredThings(){		
		for (org.eclipse.smarthome.core.thing.Thing thing : gatewayManager.thingRegistry.getAll()){
			gatewayManager.thingSetupManager.removeThing(thing.getUID());
		}			
	}
	
	protected void prepareGatewayManager(){		
		
		if (outQueue == null) {	
			assert gatewayManager.messageTransport.getOutgoing() instanceof QueueChannel<OutgoingMessage>;
			outQueue = (QueueChannel<OutgoingMessage>) gatewayManager.messageTransport.getOutgoing();
			outQueue.available = true;
		}
		
		if (inQueue == null){
			assert gatewayManager.messageTransport.getIncoming() instanceof QueueChannel<IncomingMessage>;
			inQueue = (QueueChannel<OutgoingMessage>) gatewayManager.messageTransport.getIncoming();
		}	
		while (!inQueue.queue.isEmpty()){
			inQueue.getNextMessage();
		}
		
		if (messageReader == null){
			messageReader = new MessageReader(outQueue);
			messageReader.start();
		}
		
		if (gatewayManager.discoveryProcessor != null){
			gatewayManager.discoveryProcessor.cleanUp();
			gatewayManager.discoveryProcessor.setWaitingForDiscoveryServiceErrorDuration(DURATION_WAITING_FOR_DISCOVERY_SERVICES);
		}

		waitFor({outQueue.queue.isEmpty()},1000);		
		
		assert inQueue.queue.isEmpty();
		assert outQueue.queue.isEmpty();
		
		messageReader.cleanUp();
		
		waitFor({gatewayManager.activated == true},3000);
		assert(gatewayManager.activated == true);
	} 
	
	
	@Test
	void 'assert GetewayManager starts discovery session when StartDiscoverySessionRequest was received'() {				
		startDiscoverySession("123")		
	}
	
	@Test
	void 'assert GetewayManager do not start a second discovery session when StartDiscoverySessionRequest was received'() {
		startDiscoverySession("session1")
		
		discoveryService.setSendErrorWhenScanIsStarting(false);
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest("requestId123","session2");
		
		assert gatewayManager.discoveryProcessor != null;
		assert !gatewayManager.discoveryProcessor.isReadyForDiscoverySession();
		
		gatewayManager.messageTransport.getIncoming().sendMessage(request);
		
		waitFor({messageReader.getStartDiscoverySessionReply(request.getSessionId()) != null},1000);
		assert messageReader.getStartDiscoverySessionReply(request.getSessionId()) != null;
	
		waitFor({messageReader.getDiscoverySessionError("session2") != null}, 1000);	
		DiscoverySessionError errorMsg = messageReader.getDiscoverySessionError("session2");
		assert errorMsg != null
		
		assert errorMsg.getErrorCode() == DiscoverySessionError.ERROR_DISCOVERY_IN_PROGRESS
		
	}
	
	@Test
	void 'assert GatewayManager stops discovery session when StopDiscoverySessionRequest was received'(){
		
		String sessionId = "session123";
		startDiscoverySession(sessionId);
		stopDiscoverySession(sessionId);
	}
	
	@Test
	void 'assert GetewayManager sends StartDiscoverySessionError when any DiscoveryService was throwing an Error while start'() {
		discoveryService.setSendErrorWhenScanIsStarting(true);
		String sessionId = "session123";
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest("requestId123",sessionId);		
		
		assert gatewayManager.discoveryProcessor != null;
		assert gatewayManager.discoveryProcessor.isReadyForDiscoverySession();		
		
		gatewayManager.messageTransport.getIncoming().sendMessage(request);
		
		waitFor({messageReader.getStartDiscoverySessionReply(request.getSessionId()) != null},1000);
		assert messageReader.getStartDiscoverySessionReply(request.getSessionId()) != null;
		
		waitFor({messageReader.getDiscoverySessionError(sessionId) != null}, 1000);
		DiscoverySessionError errorMsg = messageReader.getDiscoverySessionError(sessionId);
		assert errorMsg != null
		
		assert errorMsg.getErrorCode() == DiscoverySessionError.ERROR_DISCOVERY_SERVICE_ERROR
		
	//	assert !gatewayManager.discoveryProcessor.areAllStarted()
		
		waitFor({messageReader.getDiscoverySessionStarted(request.getSessionId()) != null},5000)
		assert messageReader.getDiscoverySessionStarted(request.getSessionId()) == null
	
		waitFor({messageReader.getDiscoverySessionFinished(request.getSessionId()) != null},5000)
		assert messageReader.getDiscoverySessionFinished(request.getSessionId()) != null
	}
	
	
	
	
	@Test
	void 'assert GatewayManager sends DiscoveryFinished even if no session was running when receiving StopDiscoverySessionRequest'(){		
		String sessionId = "session123";		
		stopDiscoverySession(sessionId);
	}
	
	@Test
	void 'assert GetewayManager sends ThingDiscovered and ThingAdded messages when thing was discovered and added'() {
		addTestThing();		
	}
	
	@Test
	void 'assert GatewayManager handles SetProperty message'(){
		println "YEEEEHAWWWW"
		
		
		ThingMapping thingMapping = addTestThing();
		Property property = thingMapping.getThing().getComponent("socket").getCapability("SWITCHABLE").getProperty("on");
		Boolean valueBefore = (Boolean)property.getValue();
		println "valueBefore "+valueBefore;
		SetProperty setPropCmd = new SetProperty(thingMapping.getThing().getId(), "socket", "SWITCHABLE", "on", ""+!valueBefore);
		
		gatewayManager.messageTransport.getIncoming().sendMessage(setPropCmd);
						
		waitFor({messageReader.getPropertyChanged(thingMapping.getThing().getId(), "socket", "SWITCHABLE", "on", ""+!valueBefore)!= null},5000);
		assert messageReader.getPropertyChanged(thingMapping.getThing().getId(), "socket", "SWITCHABLE", "on", ""+!valueBefore)!= null
		
	}
	
	@Test
	void 'assert GatewayManager sends correct SetPropertyError messages'(){
		
		ThingMapping thingMapping = addTestThing();
		
				
		// thing not found error expected
		SetProperty setPropCmd = new SetProperty("thingid_not_existent", "socket", "SWITCHABLE", "on", "true");
		gatewayManager.messageTransport.getIncoming().sendMessage(setPropCmd);
		waitFor({messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_THING_NOT_FOUND) != null},1000)
		assert messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_THING_NOT_FOUND) != null
		messageReader.cleanUp();
	
		// expects component not found error
		setPropCmd = new SetProperty(thingMapping.getThing().getId(), "socket_not_existing", "SWITCHABLE", "on", "true");
		gatewayManager.messageTransport.getIncoming().sendMessage(setPropCmd);
		waitFor({messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_COMPONENT_NOT_FOUND) != null},1000)
		assert messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_COMPONENT_NOT_FOUND) != null
		messageReader.cleanUp();
		
		// expects capability not found error
		setPropCmd = new SetProperty(thingMapping.getThing().getId(), "socket", "capability_not_existing", "on", "true");
		gatewayManager.messageTransport.getIncoming().sendMessage(setPropCmd);
		waitFor({messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_CAPABILITIY_NOT_FOUND) != null},1000)
		assert messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_CAPABILITIY_NOT_FOUND) != null
		messageReader.cleanUp();
		
		// expects property not found error
		setPropCmd = new SetProperty(thingMapping.getThing().getId(), "socket", "SWITCHABLE", "property_not_existing", "true");
		gatewayManager.messageTransport.getIncoming().sendMessage(setPropCmd);
		waitFor({messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_PROPERTY_NOT_FOUND) != null},1000)
		assert messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_PROPERTY_NOT_FOUND) != null
		messageReader.cleanUp();
		
		// expects invalid property value
		setPropCmd = new SetProperty(thingMapping.getThing().getId(), "socket", "SWITCHABLE", "on", "value_is_invalid");
		gatewayManager.messageTransport.getIncoming().sendMessage(setPropCmd);
		waitFor({messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_VALUE_IS_NOT_VALID) != null},1000)
		assert messageReader.getSetPropertyError(SetPropertyError.ErrorCode.ERROR_VALUE_IS_NOT_VALID) != null
		messageReader.cleanUp();
	}
	
//	@Test
//	void 'assert GatewayManager sends PropertyChange message'(){
//		ThingMapping thingMapping = addTestThing();
//		
//		assert !gatewayManager.thingMappings.isEmpty()
//				
//		thingMapping.processItemStateChange(null, null, null)
//	}
	
	@Test
	void 'assert GatewayManager handles RemoveThing messages'(){

		ThingMapping thingMapping = addTestThing();
		assert gatewayManager.thingMappings.contains(thingMapping);
		
		
		String requestId = "request123";
		RemoveThingRequest request = new RemoveThingRequest(requestId,thingMapping.getThing().getId());		
		gatewayManager.messageTransport.getIncoming().sendMessage(request);
		
		waitFor({messageReader.getRemoveThingReply(requestId) != null},1000);
		assert messageReader.getRemoveThingReply(requestId) != null;
		
		waitFor({messageReader.getThingRemovedMessage(thingMapping.getThing().getId())!= null},1000);
		assert messageReader.getThingRemovedMessage(thingMapping.getThing().getId())!= null;
		
	//	assert !gatewayManager.thingMappings.contains(thingMapping);
		
		
	}
	
	@Test
	void 'assert GatewayManager handles GatewayConfigurationRequest messages'(){
		ConfigurationRequest request = new ConfigurationRequest();		
		
		ThingMapping thingMapping = addTestThing()
		
		gatewayManager.messageTransport.getIncoming().sendMessage(request);
		
		waitFor({messageReader.getConfigurationReply() != null},1000)
		ConfigurationReply reply =  messageReader.getConfigurationReply();
		assert reply != null
		
		Collection<Thing> things = reply.getThings();
		
		for (ThingMapping mapping : gatewayManager.thingMappings){
			assert things.contains(mapping.thing)
		}
				
	}
	
		
	protected ThingMapping addTestThing(){
		String sessionId ="abc"
		
		startDiscoverySession(sessionId)
		
		assert TestDiscoveryService.getInstances().size() > 0;
		
		TestDiscoveryService discoveryService =  TestDiscoveryService.getInstances().get(0);
		
		String eshThingId = discoveryService.findThing(ManagementTestConstants.THING_TYPE_TEST);
		assert eshThingId != null;
		String yetuThingId = gatewayManager.getYetuThingFactory().getYetuThingId(new ThingUID(eshThingId));
		println "yetu thing id = "+yetuThingId
		waitFor({messageReader.getThingDiscoveredMessage(yetuThingId,sessionId) != null},2000);
		assert messageReader.getThingDiscoveredMessage(yetuThingId,sessionId);
		
		ThingMapping thingMapping = generateThingMapping(eshThingId);
		assert thingMapping != null;
	//	gatewayManager.thingMappings.add(thingMapping);
		assert gatewayManager.thingMappings.contains(thingMapping);
		gatewayManager.processThingAdded(thingMapping);
		
		assert !gatewayManager.thingMappings.isEmpty()
		
		// assert Thing Added Message
		waitFor({messageReader.getThingAddedMessage(thingMapping.getThing().getId(), sessionId) != null}, 1000);
		assert messageReader.getThingAddedMessage(thingMapping.getThing().getId(), sessionId) != null
		

		waitFor({messageReader.getDiscoverySessionFinished(sessionId) != null}, 5000);
		assert messageReader.getDiscoverySessionFinished(sessionId) != null
		
		// assert discovery finished message
		assert gatewayManager.discoveryProcessor.getDiscoverySessionState() == DiscoveryProcessor.DPS_IDLE
		// test whether discoveryProcessor is in idle mode
		//
		assert !gatewayManager.thingMappings.isEmpty()
		 
		return thingMapping
	}
	
	
	protected void stopDiscoverySession(String sessionId){
		StopDiscoverySessionRequest request = new StopDiscoverySessionRequest("requestId123",sessionId);
		gatewayManager.messageTransport.getIncoming().sendMessage(request);
		
		waitFor({messageReader.getStopDsicoverySessionReply(sessionId) != null},1000);
		assert messageReader.getStopDsicoverySessionReply(sessionId) != null;
		
		waitFor({messageReader.getDiscoverySessionFinished(sessionId) != null}, 5000);
		assert messageReader.getDiscoverySessionFinished(sessionId) != null
		
		// assert discovery finished message
		assert gatewayManager.discoveryProcessor.getDiscoverySessionState() == DiscoveryProcessor.DPS_IDLE
	}
	
	protected void startDiscoverySession(String sessionId){
		println "Starting removething test"
		
		discoveryService.setSendErrorWhenScanIsStarting(false);
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest("requestId123",sessionId);		
		
		assert gatewayManager.discoveryProcessor != null;
		assert gatewayManager.discoveryProcessor.isReadyForDiscoverySession();		
		
		gatewayManager.messageTransport.getIncoming().sendMessage(request);
		
		waitFor({messageReader.getStartDiscoverySessionReply(request.getSessionId()) != null},1000);
		assert messageReader.getStartDiscoverySessionReply(request.getSessionId()) != null;
		waitFor ({gatewayManager.discoveryProcessor.getDiscoverySessionState() == DiscoveryProcessor.DPS_STARTED},1000+DURATION_WAITING_FOR_DISCOVERY_SERVICES);
		
		assert gatewayManager.discoveryProcessor.getDiscoverySessionState() == DiscoveryProcessor.DPS_STARTED;
		assert messageReader.getDiscoverySessionStarted(request.getSessionId()) != null;
		
	}
	
	
	protected ThingMapping generateThingMapping(String thingId){
		Thing thing = generateTestThing(gatewayManager.getYetuThingFactory().getYetuThingId(new ThingUID(thingId)));
		ThingMapping thingMapping = new ThingMapping(thing, new ThingUID(thingId));	
	
		gatewayManager.thingMappings.add(thingMapping);
		
		return thingMapping;
	}
	
	protected Thing generateTestThing(String thingId){
	
		Thing thing = ThingModelFactory.eINSTANCE.createThing(thingId);
	
		ComponentType socket = thingSpecification.getComponentType("SOCKET");
	
		Component component = thingSpecification.createComponent("socket", socket);
		Capability cap = component.getCapability("SWITCHABLE");
		Property prop = cap.getProperty("on");
		prop.setValue(new Boolean(false));
		
		thing.getComponents().add(component);
		thing.setMainComponent(component);
		
		return thing;
	}
	
	
	protected void loadThingSpecification(){
		String filepath = "conf/spec/ThingSpecification.xmi"
		thingSpecification = YetuThingSpecificationLoader.load(filepath);
		assert thingSpecification != null;
	}
}