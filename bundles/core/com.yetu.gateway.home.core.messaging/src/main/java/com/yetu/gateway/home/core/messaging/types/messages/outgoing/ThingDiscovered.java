package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ThingDiscovered extends OutgoingMessage{

	protected String sessionId;
	protected String thingId;
	
	public ThingDiscovered(String thingId){
		this(null,thingId);
	}
	
	public ThingDiscovered(String sessionId, String thingId){
		this.sessionId = sessionId;
		this.thingId = thingId;
	}
	
	public String getSessionId(){
		return sessionId;
	}
	
	public boolean hasSessionId(){
		return (sessionId != null);
	}
	
	public String getThingId(){
		return thingId;
	}
	
}
