package com.yetu.gateway.home.core.messaging.types.errorhandling;

public class ChannelException extends Exception {
	private static final long serialVersionUID = -131509926742995958L;

	public ChannelException() {
		super();
	}
}
