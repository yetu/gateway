package com.yetu.gateway.home.core.messaging.types.messages.incoming;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;

public class StartDiscoverySessionRequest extends IncomingMessage {
	private String sessionId;
	private String requestId;
	
	public StartDiscoverySessionRequest(String requestId, String sessionId){
		this.sessionId = sessionId;
		this.requestId = requestId;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	
	public String getRequestId() {
		return requestId;
	}
}
