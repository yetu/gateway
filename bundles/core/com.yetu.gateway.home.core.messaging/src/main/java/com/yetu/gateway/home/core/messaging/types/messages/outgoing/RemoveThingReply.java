package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class RemoveThingReply extends OutgoingMessage {
	private String correlationId;

	public RemoveThingReply(String correlationId){
		this.correlationId = correlationId;
	}
	
	public String getCorrelationId(){
		return correlationId;
	}
}
