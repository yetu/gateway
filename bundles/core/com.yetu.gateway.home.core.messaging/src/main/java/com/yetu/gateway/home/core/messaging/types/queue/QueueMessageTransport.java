package com.yetu.gateway.home.core.messaging.types.queue;

import java.util.Dictionary;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.MessageTransport;
import com.yetu.gateway.home.core.messaging.types.Channel;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class QueueMessageTransport implements MessageTransport, ManagedService {
	private static final Logger logger = LoggerFactory.getLogger(QueueMessageTransport.class);
	
	private Channel<IncomingMessage> incomingChannel;
	private Channel<OutgoingMessage> outgoingChannel;
	
	public QueueMessageTransport() {
		incomingChannel = new QueueChannel<>();
		outgoingChannel = new QueueChannel<>();
	}
	
	public void onActivate() {
		logger.debug("Starting QueueMessageTransport");
	}
	
	public void onDeactivate() {
		logger.debug("Stopping QueueMessageTransport");
	}
	
	@Override
	public Channel<IncomingMessage> getIncoming() {
		return incomingChannel;
	}

	@Override
	public Channel<OutgoingMessage> getOutgoing() {
		return outgoingChannel;
	}

	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
		logger.debug("Ignoring QueueMessageTransport configuration");
	}

}
