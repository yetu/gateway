package com.yetu.gateway.home.core.messaging.types;

import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException;

/**
 * Represents either a channel for incoming or outgoing messages
 * 
 * @author Sebastian Garn, Mario Camou
 *
 * @param <T>
 */
public interface Channel<T> {
	/**
	 * Put a message on the channel.
	 * @param message
	 * @return true if message was sent on channel successfully. False if an error occured (e.g. channel not available)
	 */
	public boolean sendMessage(T message);
	
	/**
	 * Gets the next message from channel. A call will block until a message is available.
	 * @return next element of type T from queue
	 * @throws InterruptedException
	 */
	public T getNextMessage() throws InterruptedException;
	
	/**
	 * Indicates whether the channel can be used
	 * @return true if channel is available
	 */
	public boolean isAvailable();

	/**
	 * Set the new available state
	 * @param boolean available
	 */
	public void setAvailable(boolean available);
	
	/**
	 * Allows listeners to track state of channel
	 * @param ChannelListener listener
	 */
	public void addChannelListener(ChannelListener<T> listener);
	
	/**
	 * Remove a channel listener
	 * @param ChannelListener listener
	 */
	public void removeChannelListener(ChannelListener<T> listener);
	
	/**
	 * Signalizes a channel error
	 * ChannelException error
	 */
	public void signalChannelError(ChannelException error);
}