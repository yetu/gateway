package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ThingNotAdded extends OutgoingMessage{
	private String code;
	private String cause;
	
	public ThingNotAdded(String code, String cause) {
		this.cause = cause;
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getCause() {
		return cause;
	}
}
