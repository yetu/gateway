package com.yetu.gateway.home.core.messaging.types.messages.incoming;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;

public class RemoveThingRequest extends IncomingMessage {
	private String requestId;
	private String thingId;
	
	public RemoveThingRequest(String requestId, String thingId){
		this.thingId = thingId;
		this.requestId = requestId;
	}
	
	public String getThingId() {
		return thingId;
	}
	
	public String getRequestId() {
		return requestId;
	}
}
