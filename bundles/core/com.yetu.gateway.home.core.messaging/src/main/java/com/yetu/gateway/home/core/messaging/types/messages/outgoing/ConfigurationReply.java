package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.yetu.abstraction.model.Thing;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ConfigurationReply extends OutgoingMessage{
	protected String runningSessionId;
	protected Collection<Thing> things;
	
	public ConfigurationReply(Collection<Thing> things, String runningSessionId){
		this.things = things;
		this.runningSessionId = runningSessionId;
	}
	
	public Collection<Thing> getThings() {
		return things;
	}
	
	public String getRunningSessionId() {
		return runningSessionId;
	}
}
