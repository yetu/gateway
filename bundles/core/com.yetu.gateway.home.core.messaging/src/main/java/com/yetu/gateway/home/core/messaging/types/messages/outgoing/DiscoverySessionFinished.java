package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class DiscoverySessionFinished extends OutgoingMessage{
	protected String sessionId;
	
	public DiscoverySessionFinished(String sessionId){
		this.sessionId = sessionId;
	}
	
	public String getSessionId(){
		return sessionId;
	}
}
