package com.yetu.gateway.home.core.messaging.types.messages.incoming;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;


public class HandshakeReply extends IncomingMessage {
	private String correlationId;
	
	public HandshakeReply(String correlationId) {
		this.correlationId = correlationId;
	}
	
	public String getCorrelationId() {
		return correlationId;
	}
	
	@Override
	public String toString() {
		return "HandshakeReply";
	}
}
