package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ResetReply extends OutgoingMessage {	
	private String correlationId;

	public ResetReply(String correlationId){
		this.correlationId = correlationId;
	}

	public String getCorrelationId() {
		return correlationId;
	}
}
