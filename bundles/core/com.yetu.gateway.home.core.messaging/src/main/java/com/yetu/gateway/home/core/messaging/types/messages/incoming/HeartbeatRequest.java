package com.yetu.gateway.home.core.messaging.types.messages.incoming;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;

public class HeartbeatRequest extends IncomingMessage {
	@Override
	public String toString() {
		return "HeartbeatRequest";
	}
}
