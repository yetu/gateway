package com.yetu.gateway.home.core.messaging.types;

import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException;


public interface ChannelListener<T> {
	public void onChannelAvailable();

	public void onChannelUnavailable();
	
	public void onChannelError(ChannelException error);
}
