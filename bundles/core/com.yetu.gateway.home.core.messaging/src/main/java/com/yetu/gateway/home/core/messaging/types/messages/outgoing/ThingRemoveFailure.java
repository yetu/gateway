package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ThingRemoveFailure extends OutgoingMessage {
	private String thingId;
	private ErrorCode errorCode;
	private String cause;

	public ThingRemoveFailure(String thingId, String cause, ErrorCode errorCode){
		this.thingId = thingId;
		this.errorCode = errorCode;
		this.cause = cause;
	}
	
	public enum ErrorCode{
		THING_NOT_FOUND
	}
	
	public String getThingId() {
		return thingId;
	}
	
	public ErrorCode getErrorCode() {
		return errorCode;
	}
	
	public String getCause() {
		return cause;
	}
}
