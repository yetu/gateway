package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class StopDiscoverySessionReply extends OutgoingMessage {

	protected String sessionId;
	protected String correlationId;
	
	public StopDiscoverySessionReply(String correlationId, String sessionId){
		this.sessionId = sessionId;
		this.correlationId = correlationId;
	}
	
	public String getSessionId(){
		return sessionId;
	}
	
	public String getCorrelationId(){
		return correlationId;
	}
	
}
