package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ThingAlreadyAdded extends OutgoingMessage {
	private String thingId;
	private String sessionId;
	
	public ThingAlreadyAdded(String thingId) {
		this(thingId,null);
	}
	
	public ThingAlreadyAdded(String thingId, String sessionId) {
		this.thingId = thingId;
		this.sessionId = sessionId;
	}
	
	public String getThingId() {
		return thingId;
	}
	
	public String getSessionId() {
		return sessionId;
	}
}
