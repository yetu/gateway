package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;


public class HandshakeRequest extends OutgoingMessage {
	private String gatewayId;
	private String requestId;
	private String gatewayImageVersion;

	public HandshakeRequest(String gatewayId, String gatewayImageVersion) {
		this.gatewayId = gatewayId;
		this.requestId = AbstractMessage.generateRequestId();
		this.gatewayImageVersion = gatewayImageVersion;
	}
	
	public String getGatewayId() {
		return gatewayId;
	}

	public String getRequestId() {
		return requestId;
	}
	
	public String getGatewayImageVersion() {
		return gatewayImageVersion;
	}
	
	@Override
	public String toString() {
		return "HandshakeRequest - GatewayID: "+gatewayId+" GatewayImageVersion: "+gatewayImageVersion+" RequestId: "+requestId;
	}
}
