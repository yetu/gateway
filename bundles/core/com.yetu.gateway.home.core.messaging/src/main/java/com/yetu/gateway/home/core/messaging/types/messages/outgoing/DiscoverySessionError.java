package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class DiscoverySessionError extends OutgoingMessage{

	public final static int ERROR_UNKNOWN = 0;					// unknown error that can't be further specified
	public final static int ERROR_DISCOVERY_IN_PROGRESS = 1;	// another discovery session is in progress
	public final static int ERROR_DISCOVERY_SERVICE_ERROR = 2;	// one ore more discovery services reported an error
	public final static int ERROR_NOT_INITIALIZED = 3;			// discovery processor is null
	public final static int ERROR_UNKNOWN_DEVICE = 4;			// an unknown device was paired and the gw doesnt know how to handle this
	
	
	protected String sessionId;
	protected String errorMsg;
	protected int errorCode;
	
	public DiscoverySessionError(String sessionId, String message, int errorCode){
		this.sessionId = sessionId;
		this.errorMsg = message;
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage(){
		return errorMsg;
	}
	
	public String getSessionId(){
		return sessionId;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	
}
