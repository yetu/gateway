package com.yetu.gateway.home.core.messaging.types;

public enum SessionType {
	THING_DISCOVERY,
	THING_REMOVAL
}
