package com.yetu.gateway.home.core.messaging.types.errorhandling;

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage;

/**
 * Triggered whenever a response for a message was expected but never was received in time
 * @author Sebastian Garn
 *
 */
public class MessageReplyTimeout extends ChannelException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 853519126606393329L;
	private AbstractMessage request;
	
	/**
	 * Constructor
	 * @param AbstractMessage request: the request to which the response is missing
	 */
	public MessageReplyTimeout(AbstractMessage request) {
		super();
		this.request = request;
	}
	
	public AbstractMessage getRequest() {
		return request;
	}
}
