package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import java.util.Date;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class PropertyChanged extends OutgoingMessage{

	private String value;
	private String propertyName;
	private String capabilityId;
	private String componentId;
	private String thingId;
	private Date timeWhenChanged;
	
	public PropertyChanged(String thingId, String componentId, String capabilityId, String propertyName, Date timeWhenChanged, String value){
		this.thingId = thingId;
		this.propertyName = propertyName;
		this.capabilityId = capabilityId;
		this.componentId = componentId;
		this.value = value;
		this.timeWhenChanged = timeWhenChanged;
	}

	public String getValue() {
		return value;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public String getCapabilityId() {
		return capabilityId;
	}

	public String getComponentId() {
		return componentId;
	}

	public String getThingId() {
		return thingId;
	}
	
	public Date getTimeWhenChanged() {
		return timeWhenChanged;
	}
}
