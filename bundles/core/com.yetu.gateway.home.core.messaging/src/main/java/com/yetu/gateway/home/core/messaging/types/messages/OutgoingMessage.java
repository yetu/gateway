package com.yetu.gateway.home.core.messaging.types.messages;

/**
 * Describes a message which usually comes from a bundle and is addressed to a remote system (e.g. server)
 * 
 * @author Sebastian Garn
 *
 */
public abstract class OutgoingMessage extends AbstractMessage {

}
