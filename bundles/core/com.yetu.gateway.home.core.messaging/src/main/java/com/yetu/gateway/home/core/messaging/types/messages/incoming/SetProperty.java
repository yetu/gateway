package com.yetu.gateway.home.core.messaging.types.messages.incoming;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;

public class SetProperty extends IncomingMessage{

	private String value;
	private String propertyName;
	private String capabilityId;
	private String componentId;
	private String thingId;
	
	public SetProperty(String thingId, String componentId, String capabilityId, String propertyName, String value){
		this.value 		  = value;
		this.propertyName = propertyName;
		this.capabilityId = capabilityId;
		this.componentId  = componentId;
		this.thingId 	  = thingId;
	}

	public String getValue() {
		return value;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public String getCapabilityId() {
		return capabilityId;
	}

	public String getComponentId() {
		return componentId;
	}

	public String getThingId() {
		return thingId;
	}
	
	
}
