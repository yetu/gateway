package com.yetu.gateway.home.core.messaging.types.queue;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.Channel;
import com.yetu.gateway.home.core.messaging.types.ChannelListener;
import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException;

public class QueueChannel<T> implements Channel<T> {
	private static final Logger logger = LoggerFactory.getLogger(QueueChannel.class);
	
	private ArrayList<ChannelListener<T>> listeners = new ArrayList<ChannelListener<T>>();
	private LinkedBlockingQueue<T> queue = new LinkedBlockingQueue<>();
	private boolean available = false;
	
	@Override
	public boolean sendMessage(T message) {
		if (!available) {
			logger.error("Channel not ready yet");
			return false;
		}
		
		logger.debug("Queue sending message");
		return queue.offer(message);
	}

	@Override
	public T getNextMessage() throws InterruptedException {
		if (!available) return null;
		
		logger.debug("Queue waiting for message");
		return queue.take();
	}

	@Override
	public boolean isAvailable() {
		return available;
	}
	
	@Override
	public void setAvailable(boolean available) {
		this.available = available;
		
		// inform listeners
		synchronized(listeners) {
			for (ChannelListener<T> currListener : listeners) {
				if (available) {
					currListener.onChannelAvailable();
				}
				else {
					currListener.onChannelUnavailable();
				}
			}
		}
	}

	@Override
	public void addChannelListener(ChannelListener<T> listener) {
		synchronized(listeners) {
			if (listeners.contains(listener)) {
				return;
			}

			listeners.add(listener);
		}
	}
	
	@Override
	public void removeChannelListener(ChannelListener<T> listener) {
		synchronized(listeners) {
			if (!listeners.contains(listener)) {
				return;
			}

			listeners.remove(listener);
		}
	}

	@Override
	public void signalChannelError(ChannelException error) {
		logger.warn("Channel error ocurred",error);
		synchronized(listeners) {
			for (ChannelListener<T> currListener : listeners) {
				logger.debug("Informing listener about channel error");
				currListener.onChannelError(error);
			}
		}
	}
}
