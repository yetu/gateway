package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ThingRemoved extends OutgoingMessage {
	private String thingId;

	public ThingRemoved(String thingId){
		this.thingId = thingId;
	}
	
	public String getThingId() {
		return thingId;
	}
}
