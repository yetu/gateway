package com.yetu.gateway.home.core.messaging.types.messages.incoming;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;


public class StopDiscoverySessionRequest extends IncomingMessage{

	protected String sessionId;
	protected String requestId;
	
	public StopDiscoverySessionRequest(String requestId, String sessionId){
		this.sessionId = sessionId;
		this.requestId = requestId;
	}
	
	public String getSessionId(){
		return sessionId;
	}
	
	public String getRequestId(){
		return requestId;
	}
}
