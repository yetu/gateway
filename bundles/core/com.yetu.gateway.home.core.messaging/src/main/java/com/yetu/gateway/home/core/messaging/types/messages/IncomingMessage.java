package com.yetu.gateway.home.core.messaging.types.messages;



/**
 * Message sent by a remote system (e.g. backend) and destined for a bundle for internal processing
 * 
 * @author Sebastian Garn
 */
public abstract class IncomingMessage extends AbstractMessage {

}