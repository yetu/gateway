package com.yetu.gateway.home.core.messaging.types.messages.incoming;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;

public class ResetRequest extends IncomingMessage{
	public enum Entity {
		THINGS,
		SESSIONS
	}
	
	private String requestId;
	private Entity[] entities;

	public ResetRequest(String requestId, Entity[] entities){
		this.requestId = requestId;
		this.entities = entities;
	}

	public Entity[] getEntities() {
		return entities;
	}

	public String getRequestId() {
		return requestId;
	}

	public boolean containsEntity(Entity entity) {
		for (Entity currEntity : entities) {
			if (currEntity.compareTo(entity) == 0) {
				return true;
			}
		}
		
		return false;
	}
}
