package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class StartDiscoverySessionReply extends OutgoingMessage {
	private String sessionId;
	private String correlationId;

	public StartDiscoverySessionReply(String correlationId, String sessionId){
		this.sessionId = sessionId;
		this.correlationId = correlationId;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	
	public String getCorrelationId(){
		return correlationId;
	}
}
