package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class DiscoverySessionExpired extends OutgoingMessage{

	protected String sessionId;
	
	public DiscoverySessionExpired(String sessionId){
		this.sessionId = sessionId;
	}
	
	public String getSessionId(){
		return sessionId;
	}	
}
