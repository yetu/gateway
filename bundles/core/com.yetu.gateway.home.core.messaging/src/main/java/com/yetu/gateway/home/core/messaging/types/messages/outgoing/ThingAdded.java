package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.abstraction.model.Thing;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ThingAdded extends OutgoingMessage{

	private String sessionId;
	private Thing thing;
	
	public ThingAdded(Thing thing){
		this(thing,null);
	}
	
	public ThingAdded(Thing thing, String sessionId){
		this.thing = thing;
		this.sessionId = sessionId;
	}
	
	public Thing getThing(){
		return thing;
	}
	
	public String getSessionId(){
		return sessionId;
	}
	
	public boolean hasSessionId(){
		return sessionId != null;
	}
	
}
