package com.yetu.gateway.home.core.messaging;

import com.yetu.gateway.home.core.messaging.types.Channel;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public interface MessageTransport {
	public Channel<IncomingMessage> getIncoming();
	public Channel<OutgoingMessage> getOutgoing();
}