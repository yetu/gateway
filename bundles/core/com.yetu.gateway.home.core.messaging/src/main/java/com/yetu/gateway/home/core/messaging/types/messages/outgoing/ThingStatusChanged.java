package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.abstraction.model.ThingStatus;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class ThingStatusChanged extends OutgoingMessage {
	protected String thingId;
	protected ThingStatus status;
	
	public ThingStatusChanged(String thingId, ThingStatus status) {
		this.thingId = thingId;
		this.status = status;
	}
	
	public String getThingId(){
		return thingId;
	}
	
	public ThingStatus getStatus(){
		return status;
	}
	
}
