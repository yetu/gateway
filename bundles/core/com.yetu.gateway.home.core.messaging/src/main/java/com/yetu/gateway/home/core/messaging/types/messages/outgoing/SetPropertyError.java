package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class SetPropertyError extends OutgoingMessage {

	
	private ErrorCode errorCode;
	private String thingId;
	private String componentid;
	private String capabilityId;
	private String propertyName;
	private String value;
	private String cause;
	
	public SetPropertyError(ErrorCode errorCode, String cause, String thingId, String componentId, String capabilityId, String propertyName, String value){
		this.value = value;
		this.propertyName = propertyName;
		this.capabilityId = capabilityId;
		this.componentid = componentId;
		this.thingId = thingId;
		this.errorCode = errorCode;
		this.cause = cause;
	}
	
	
	public enum ErrorCode{
		ERROR_UNKNOWN,
		ERROR_NOT_FOUND,
		ERROR_THING_NOT_FOUND,
		ERROR_COMPONENT_NOT_FOUND,
		ERROR_CAPABILITIY_NOT_FOUND,
		ERROR_PROPERTY_NOT_FOUND,
		ERROR_VALUE_IS_NOT_VALID		
	}


	public ErrorCode getErrorCode() {
		return errorCode;
	}


	public String getThingId() {
		return thingId;
	}


	public String getComponentId() {
		return componentid;
	}


	public String getCapabilityId() {
		return capabilityId;
	}


	public String getPropertyName() {
		return propertyName;
	}
	
	public String getValue(){
		return value;
	}


	public String getCause() {
		return cause;
	}
	
	
}
