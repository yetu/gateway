package com.yetu.gateway.home.core.messaging.types.messages;

import java.util.UUID;


/**
 * Abstract messages can be either incoming oder outgoing messages. They are used for internal communication between bundles. 
 * 
 * @author Sebastian Garn
 *
 */
public abstract class AbstractMessage {
    public static String generateRequestId() { return UUID.randomUUID().toString(); }
}
