package com.yetu.gateway.home.core.messaging.types.messages.outgoing;

import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;

public class DiscoverySessionStarted extends OutgoingMessage {

	protected String sessionId;
	
	public DiscoverySessionStarted(String sessionId){
		this.sessionId = sessionId;
	}
	
	public String getSessionId(){
		return sessionId;
	}
	
}
