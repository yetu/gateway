package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.abstraction.model.ThingStatus
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingStatusChanged

/**
 * @author Sebastian Garn
 */
class ThingEventCommunicationTest extends AbstractCommunicationTest {
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates property event communication on property changed or set property error message'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		ThingStatusChanged thingStatusChanged = new ThingStatusChanged("abc", ThingStatus.ONLINE);

		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(thingStatusChanged);
		assert messageSentToBackend instanceof ThingStatusChanged
	}
	
	@After
	void stop() {
		super.stop();
	}
}