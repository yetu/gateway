package com.yetu.gateway.home.core.backend

import static org.junit.Assert.*
import static org.junit.matchers.JUnitMatchers.*

import org.eclipse.smarthome.core.events.EventPublisher
import org.eclipse.smarthome.core.items.Item
import org.eclipse.smarthome.core.items.ItemProvider
import org.eclipse.smarthome.core.items.ItemRegistry
import org.eclipse.smarthome.core.items.ManagedItemProvider
import org.eclipse.smarthome.core.library.items.NumberItem
import org.eclipse.smarthome.core.library.items.StringItem
import org.eclipse.smarthome.core.types.Command
import org.eclipse.smarthome.core.types.State
import org.eclipse.smarthome.test.OSGiTest
import org.osgi.framework.ServiceReference
import org.osgi.util.tracker.ServiceTracker

abstract class AbstractBindingTest extends OSGiTest {
	protected EventPublisher eventPublisher;
	protected boolean eventReceived = false;
	
	/**
	 * Waits for a specific service implementation
	 * @param serviceInterfaceClass - Class: Interface of which defines the service
	 * @param implementingServiceClass - Class: The implementing class that implements the service and you are looking for
	 * @param timeout - int: timeout in ms
	 * @return null if timeout occurs
	 */
	public Object waitForService(Class serviceInterfaceClass, Class implementingServiceClass, int timeout) {
		boolean found = false
		long endsAt = Calendar.getInstance().getTimeInMillis() + timeout

		while (Calendar.getInstance().getTimeInMillis() < endsAt) {
			ServiceTracker tracker = new ServiceTracker(bundleContext, serviceInterfaceClass.getName(), null)
			tracker.open()
			tracker.waitForService(100)
			
			for (ServiceReference s : tracker.getServiceReferences()) {
				if (bundleContext.getService(s).getClass() == implementingServiceClass) {
					return bundleContext.getService(s)
				}
			}
		}
		
		return null
	}
}