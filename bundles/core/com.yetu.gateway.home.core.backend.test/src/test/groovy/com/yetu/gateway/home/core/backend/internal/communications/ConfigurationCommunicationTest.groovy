package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ConfigurationReply

/**
 * @author Sebastian Garn
 */
class ConfigurationCommunicationTest extends AbstractCommunicationTest {
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates thing synch configuration communication on appropriate configuration request'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		ConfigurationRequest request = new ConfigurationRequest();
		assert communicationsHandler.handle(request);
		
		assert communicationsHandler.activeCommunications.size() == 1;
	}
	
	@Test
	void 'assert Gateway synch consists of two messages'() {
		ConfigurationRequest request = new ConfigurationRequest();
		ConfigurationReply reply = new ConfigurationReply(null,"");

		// conf request
		assert messageSentToInQueue == null
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		assert messageSentToInQueue instanceof ConfigurationRequest
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()

		// reply
		messageSentToBackend = null
		assert communicationsHandler.handle(reply);
		
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend instanceof ConfigurationReply
		assert com.isFinished()
	}
	
	@Test
	void 'assert GatewayConfigurationReplies are ignored if there is no communication'() {
		ConfigurationReply reply = new ConfigurationReply(null,"");

		assert communicationsHandler.activeCommunications.size() == 0;
		assert !communicationsHandler.handle(reply);
		assert communicationsHandler.activeCommunications.size() == 0;
	}
	
	@Test
	void 'assert multiple configuration requests produce multiple communications'() {
		ConfigurationRequest requestOne = new ConfigurationRequest();
		ConfigurationRequest requestTwo = new ConfigurationRequest();

		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(requestOne);
		assert communicationsHandler.handle(requestTwo);
		assert communicationsHandler.activeCommunications.size() == 2;
	}
	
	@After
	void stop() {
		super.stop();
	}
}