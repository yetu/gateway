package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.PropertyChanged
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError

/**
 * @author Sebastian Garn
 */
class PropertyEventCommunicationTest extends AbstractCommunicationTest {
	public static String DUMMY_REQUEST_ID = "123";
	public static String DUMMY_THING_ID = "abc";
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates property event communication on property changed or set property error message'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		SetPropertyError requestError = new SetPropertyError(null,"","","","","","");
		PropertyChanged requestChanged = new PropertyChanged("","","","",null,"");

		assert communicationsHandler.handle(requestError);
		assert messageSentToBackend instanceof SetPropertyError
		assert communicationsHandler.activeCommunications.size() == 0;
		
		assert communicationsHandler.handle(requestChanged);
		assert messageSentToBackend instanceof PropertyChanged
		assert communicationsHandler.activeCommunications.size() == 0;
	}
	
	@After
	void stop() {
		super.stop();
	}
}