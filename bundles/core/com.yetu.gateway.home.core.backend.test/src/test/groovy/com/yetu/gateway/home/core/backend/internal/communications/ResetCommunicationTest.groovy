package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException
import com.yetu.gateway.home.core.messaging.types.errorhandling.MessageReplyTimeout
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ResetReply

/**
 * @author Sebastian Garn
 */
class ResetCommunicationTest extends AbstractCommunicationTest {
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates reset communication on reset request'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		assert communicationsHandler.handle(new ResetRequest("abc",null));
		
		assert communicationsHandler.activeCommunications.size() == 1;
	}

	@Test
	void 'assert reset request and response finish communication'() {
		ResetRequest request = new ResetRequest("abc", null);
		
		ResetReply reply = new ResetReply(request.getRequestId());
		
		assert messageSentToBackend == null
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		assert messageSentToInQueue instanceof ResetRequest
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		assert messageSentToBackend == null
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
		
		assert communicationsHandler.activeCommunications.size() == 0;
		
		assert messageSentToBackend instanceof ResetReply
	}
	
	@Test
	void 'assert communications get only created on reset requests'() {
		ResetRequest request = new ResetRequest("abc");
		
		ResetReply reply = new ResetReply(request.getRequestId());

		assert communicationsHandler.activeCommunications.size() == 0;
		assert !communicationsHandler.handle(reply);
		assert communicationsHandler.activeCommunications.size() == 0;
		
		assert communicationsHandler.handle(request);
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 2;
	}
	
	@Test
	void 'assert Correlation id of reset reply has to match the requestid of the reset request'() {
		ResetRequest request = new ResetRequest("abc");
		
		ResetReply reply = new ResetReply("xyz");
		
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		assert !communicationsHandler.handle(reply);
		assert !com.isFinished()
		
		assert communicationsHandler.activeCommunications.size() == 1;
	}
	
	@After
	void stop() {
		super.stop();
	}
}