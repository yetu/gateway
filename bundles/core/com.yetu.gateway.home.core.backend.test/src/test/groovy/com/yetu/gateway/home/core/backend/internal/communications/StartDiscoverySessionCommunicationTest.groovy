package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.SessionType
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply;

/**
 * @author Sebastian Garn
 */
class StartDiscoverySessionCommunicationTest extends AbstractCommunicationTest {
	public static String DUMMY_REQUEST_ID = "123";
	public static String DUMMY_SESSION_ID = "abc";
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates discovery session communication on session request'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);

		assert communicationsHandler.handle(request);
		
		assert communicationsHandler.activeCommunications.size() == 1;
		assert communicationsHandler.activeCommunications.get(0) instanceof StartDiscoverySessionCommunication
	}
	
	@Test
	void 'assert start discovery session request and session reply finish communication and create queue messages'() {
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);
		
		StartDiscoverySessionReply reply = new StartDiscoverySessionReply(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);

		assert messageSentToInQueue == null
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		assert messageSentToInQueue instanceof StartDiscoverySessionRequest
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		assert messageSentToBackend == null
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
		
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend instanceof StartDiscoverySessionReply
	}
	
	@Test
	void 'assert start discovery session replies are ignored if there is no communication'() {
		StartDiscoverySessionReply reply = new StartDiscoverySessionReply("useless", DUMMY_SESSION_ID);

		assert communicationsHandler.activeCommunications.size() == 0;
		assert !communicationsHandler.handle(reply);
		assert communicationsHandler.activeCommunications.size() == 0;
	}
	
	@Test
	void 'assert start reply does not belong to communication if correlation id differs'() {
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);
	
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		StartDiscoverySessionReply reply = new StartDiscoverySessionReply(DUMMY_REQUEST_ID+"x", DUMMY_SESSION_ID);
		assert !communicationsHandler.handle(reply);
		assert !com.isFinished()
		
		reply = new StartDiscoverySessionReply(DUMMY_REQUEST_ID, DUMMY_SESSION_ID+"x");
		assert !communicationsHandler.handle(reply);
		assert !com.isFinished()
		
		reply = new StartDiscoverySessionReply(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
	}
	
	@Test
	void 'assert multiple session requests produce multiple communications'() {
		StartDiscoverySessionRequest requestOne = new StartDiscoverySessionRequest(DUMMY_REQUEST_ID+"one", DUMMY_SESSION_ID+"one");
		
		StartDiscoverySessionRequest requestTwo = new StartDiscoverySessionRequest(DUMMY_REQUEST_ID+"two", DUMMY_SESSION_ID+"two");

		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(requestOne);
		assert communicationsHandler.handle(requestTwo);
		assert communicationsHandler.activeCommunications.size() == 2;
	}
	
	@After
	void stop() {
		super.stop();
	}
}