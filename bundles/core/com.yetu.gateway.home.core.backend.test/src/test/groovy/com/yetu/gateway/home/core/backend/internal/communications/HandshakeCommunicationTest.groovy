package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException
import com.yetu.gateway.home.core.messaging.types.errorhandling.MessageReplyTimeout
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest

/**
 * @author Sebastian Garn
 */
class HandshakeCommunicationTest extends AbstractCommunicationTest {
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates handshake communication on handshake request'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		assert communicationsHandler.handle(new HandshakeRequest("abc",""));
		
		assert communicationsHandler.activeCommunications.size() == 1;
	}
	
	@Test
	void 'assert handshake request and response finishes communication'() {
		HandshakeRequest request = new HandshakeRequest("abc","");
		
		HandshakeReply reply = new HandshakeReply(request.getRequestId());
		
		assert messageSentToBackend == null
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		assert messageSentToBackend instanceof HandshakeRequest
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		assert messageSentToInQueue == null
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
		
		assert communicationsHandler.activeCommunications.size() == 0;
		
		// there should be the reply in the incoming queue
		assert messageSentToInQueue instanceof HandshakeReply
	}
	
	@Test
	void 'assert communications get only created on handshake requests'() {
		HandshakeRequest request = new HandshakeRequest("abc","");
		
		HandshakeReply reply = new HandshakeReply(request.getRequestId());

		assert communicationsHandler.activeCommunications.size() == 0;
		assert !communicationsHandler.handle(reply);
		assert communicationsHandler.activeCommunications.size() == 0;
		
		assert communicationsHandler.handle(request);
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 2;
	}
	
	@Test
	void 'assert handshake communication waits for handshake response otherwises exception gets thrown'() {
		HandshakeRequest request = new HandshakeRequest("abc","");
		
		assert outQueueError == null;
		
		// creates communication
		assert communicationsHandler.handle(request);
		
		// manipulate the communication to decrease time
		communicationsHandler.activeCommunications.get(0).expirationTime = 1;

		waitFor({outQueueError != null},1000);
		assert outQueueError instanceof MessageReplyTimeout;
	}
	
	@Test
	void 'assert handshake communication gets removed from active communications as soon as timeout occurs'() {
		HandshakeRequest request = new HandshakeRequest("abc","");

		// creates communication
		assert communicationsHandler.handle(request);
		
		// manipulate the communication to decrease time
		assert communicationsHandler.activeCommunications.get(0) instanceof HandshakeCommunication;
		HandshakeCommunication com = communicationsHandler.activeCommunications.get(0);
		com.expirationTime = 1;
		
		// wait until the communication 
		waitFor({com.isFinished()},2000);
		assert com.isFinished();
		
		assert communicationsHandler.activeCommunications.size == 1;
		
		// add a new communication by simulating incoming message to trigger clean up
		communicationsHandler.handle(new HandshakeRequest("def",""));
		
		// we should only have one communication since first one was cleand up
		waitFor({communicationsHandler.activeCommunications.size == 1},2000);
		assert communicationsHandler.activeCommunications.size == 1;
	}
	
	@Test
	void 'assert Correlation id of handshake reply has to match the requestid of the handshake request'() {
		HandshakeRequest request = new HandshakeRequest("abc","");
		
		HandshakeReply reply = new HandshakeReply("xyz");
		
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		assert !communicationsHandler.handle(reply);
		assert !com.isFinished()
		
		assert communicationsHandler.activeCommunications.size() == 1;
	}
	
	@After
	void stop() {
		super.stop();
	}
}