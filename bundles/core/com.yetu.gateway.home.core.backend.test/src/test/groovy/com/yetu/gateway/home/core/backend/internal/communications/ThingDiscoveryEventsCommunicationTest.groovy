package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException
import com.yetu.gateway.home.core.messaging.types.errorhandling.MessageReplyTimeout
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAdded
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAlreadyAdded
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingDiscovered
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingNotAdded

/**
 * @author Sebastian Garn
 */
class ThingDiscoveryEventsCommunicationTest extends AbstractCommunicationTest {
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates thing events communication on various events'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		ThingAdded msgAdded = new ThingAdded(null);
		ThingNotAdded msgNotAdded = new ThingNotAdded("","");
		ThingDiscovered msgDiscovered = new ThingDiscovered("");
		ThingAlreadyAdded msgAlreadyDiscovered = new ThingAlreadyAdded("");
		
		assert communicationsHandler.handle(msgAdded);
		assert messageSentToBackend instanceof ThingAdded
		
		assert communicationsHandler.handle(msgNotAdded);
		assert messageSentToBackend instanceof ThingNotAdded
		
		assert communicationsHandler.handle(msgDiscovered);
		assert messageSentToBackend instanceof ThingDiscovered
		
		assert communicationsHandler.handle(msgAlreadyDiscovered);
		assert messageSentToBackend instanceof ThingAlreadyAdded
		
		assert communicationsHandler.activeCommunications.size() == 0;
	}

	@After
	void stop() {
		super.stop();
	}
}