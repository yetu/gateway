package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.eclipse.smarthome.test.OSGiTest

import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler
import com.yetu.gateway.home.core.messaging.MessageTransport
import com.yetu.gateway.home.core.messaging.types.Channel
import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage

/**
 * @author Sebastian Garn
 */
abstract class AbstractCommunicationTest extends OSGiTest {
	protected RemoteCommunicationHandler mockedRemoteComHandler
	protected MessageTransport mockedMessageTransport
	
	// information gathered from mocks
	protected IncomingMessage messageSentToInQueue
	protected OutgoingMessage messageSentToOutQueue
	protected AbstractMessage messageSentToBackend
	protected ChannelException inQueueError;
	protected ChannelException outQueueError;
	protected boolean faultyConnectionDetected;
	
	CommunicationsHandler communicationsHandler
	
	void setup() {
		messageSentToInQueue = null;
		messageSentToOutQueue = null;
		messageSentToBackend = null;
		inQueueError = null;
		outQueueError = null;
		faultyConnectionDetected = false;
		
		mockedRemoteComHandler =	[send:
										{ OutgoingMessage msg ->
											messageSentToBackend = msg
											true
										},
									 signalizeFaultyConnection:
									 	{
											faultyConnectionDetected = true
										}
									] as RemoteCommunicationHandler

		def mockedMessageTransportOut =	[sendMessage:
											{ OutgoingMessage msg ->
												messageSentToOutQueue = msg
												true
											},
										signalChannelError:
											{ ChannelException err -> outQueueError = err }
										] as Channel<OutgoingMessage>
									
		def mockedMessageTransportIn =	[sendMessage:
											{ IncomingMessage msg ->
												messageSentToInQueue = msg
												true
											},
										signalChannelError:
											{ ChannelException err -> inQueueError = err }
										] as Channel<IncomingMessage>
	
		mockedMessageTransport = [	getOutgoing: { mockedMessageTransportOut },
									getIncoming: { mockedMessageTransportIn }] as MessageTransport
		
		communicationsHandler = new CommunicationsHandler(null, null);
		communicationsHandler.messageTransport = mockedMessageTransport;
		communicationsHandler.remoteCommunicationHandler = mockedRemoteComHandler;
	}

	void stop() {
		communicationsHandler.stop();
	}
}