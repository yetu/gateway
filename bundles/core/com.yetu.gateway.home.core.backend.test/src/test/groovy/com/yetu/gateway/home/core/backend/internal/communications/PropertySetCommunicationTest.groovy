package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply

/**
 * @author Sebastian Garn
 */
class PropertySetCommunicationTest extends AbstractCommunicationTest {
	public static String DUMMY_REQUEST_ID = "123";
	public static String DUMMY_THING_ID = "abc";
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates set property communication on set property message'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		SetProperty request = new SetProperty("","","","","");

		assert communicationsHandler.handle(request);
		
		assert communicationsHandler.activeCommunications.size() == 0;
		assert messageSentToInQueue instanceof SetProperty
	}
	
	@Test
	void 'assert you can multiple set property communications'() {
		SetProperty requestOne = new SetProperty("","","","","");
		SetProperty requestTwo = new SetProperty("","","","","");

		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(requestOne);
		assert communicationsHandler.handle(requestTwo);
		
		// they will be immediately finished
		assert communicationsHandler.activeCommunications.size() == 0;
	}
	
	@After
	void stop() {
		super.stop();
	}
}