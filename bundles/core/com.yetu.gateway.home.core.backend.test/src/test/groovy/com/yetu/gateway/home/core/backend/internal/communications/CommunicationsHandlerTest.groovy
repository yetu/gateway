package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest

/**
 * @author Sebastian Garn
 */
class CommunicationsHandlerTest extends AbstractCommunicationTest {
	Communication mockCommunication;
	
	@Before
	void setup() {
		super.setup()
		
		mockCommunication = [setup: {true}] as Communication
	}
	
	@Test
	void 'assert Stopping the handler stops the communication observer'() {
		assert communicationsHandler.communicationsObserver.stopped
		
		// create communication for dummy message so the observer should get started
		communicationsHandler.createCommunication(new HandshakeRequest("",""));
		
		assert !communicationsHandler.communicationsObserver.stopped
		communicationsHandler.stop()
		assert communicationsHandler.communicationsObserver.stopped
	}
	
	@Test
	void 'assert Communications automatically switches to next communication state once state was reached'() {
		CommunicationState mockCommunicationStateOne = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true } ] as CommunicationState
		CommunicationState mockCommunicationStateTwo = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true } ] as CommunicationState
		CommunicationState mockCommunicationStateThree = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true } ] as CommunicationState
														
		mockCommunication.addCommunicationState(mockCommunicationStateOne)
		mockCommunication.addCommunicationState(mockCommunicationStateTwo)
		mockCommunication.addCommunicationState(mockCommunicationStateThree)
		
		communicationsHandler.activeCommunications.add(mockCommunication)
		mockCommunication.started = true;
		
		assert mockCommunication.currCommunicationStage == 0
		
		AbstractMessage abstrMsg = new DummyMessageOne()
		assert communicationsHandler.handle(abstrMsg)
		
		assert mockCommunication.currCommunicationStage == 1
		
		assert communicationsHandler.handle(abstrMsg)
		
		assert mockCommunication.currCommunicationStage == 2
	}
	
	@Test
	void 'assert Communications do not switch to next state if previous was not successful'() {
		CommunicationState mockCommunicationStateOne = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> false } ] as CommunicationState
		CommunicationState mockCommunicationStateTwo = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true } ] as CommunicationState
														
		mockCommunication.addCommunicationState(mockCommunicationStateOne)
		mockCommunication.addCommunicationState(mockCommunicationStateTwo)

		communicationsHandler.activeCommunications.add(mockCommunication)
		mockCommunication.started = true;
		
		assert mockCommunication.currCommunicationStage == 0
		
		AbstractMessage abstrMsg = new DummyMessageOne()
		assert !communicationsHandler.handle(abstrMsg)
		
		assert mockCommunication.currCommunicationStage == 0
	}
	
	@Test
	void 'assert Communications get finished after last stage occured'() {
		CommunicationState mockCommunicationStateOne = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true } ] as CommunicationState
		CommunicationState mockCommunicationStateTwo = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true } ] as CommunicationState
														
		mockCommunication.addCommunicationState(mockCommunicationStateOne)
		mockCommunication.addCommunicationState(mockCommunicationStateTwo)
		
		communicationsHandler.activeCommunications.add(mockCommunication)
		mockCommunication.started = true;
		
		assert mockCommunication.currCommunicationStage == 0
		
		AbstractMessage abstrMsg = new DummyMessageOne()
		assert communicationsHandler.handle(abstrMsg)
		
		assert mockCommunication.currCommunicationStage == 1
		assert mockCommunication.started
		assert !mockCommunication.finished
		
		assert communicationsHandler.handle(abstrMsg)

		assert !mockCommunication.started
		assert mockCommunication.finished
	}
	
	@Test
	void 'assert After timeout communication still expects message'() {
		// start observer by pushing something to queue
		communicationsHandler.createCommunication(new HandshakeRequest("",""));
		
		boolean timeoutOccured = false
		CommunicationState mockCommunicationStateOne = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true } ] as CommunicationState
		CommunicationState mockCommunicationStateTwo = [ canHandle: { AbstractMessage msg -> true }, handle: { AbstractMessage msg -> true }, onTimeout: { timeoutOccured = true } ] as CommunicationState
														
		mockCommunication.addCommunicationState(mockCommunicationStateOne)
		mockCommunication.addCommunicationState(mockCommunicationStateTwo,500)
		
		communicationsHandler.activeCommunications.add(mockCommunication)
		mockCommunication.started = true;
		
		assert mockCommunication.currCommunicationStage == 0
		
		AbstractMessage abstrMsg = new DummyMessageOne()
		assert communicationsHandler.handle(abstrMsg)
		
		assert mockCommunication.currCommunicationStage == 1

		waitFor({timeoutOccured}, 1000)
		assert timeoutOccured;
		assert !mockCommunication.finished
		
		assert communicationsHandler.handle(abstrMsg)
		assert mockCommunication.finished
	}
	
	@Test
	void 'assert CommunicationStages have to be fully finished before another message can be handled to keep the order'() {
		boolean stateOneReached = false;
		boolean stateTwoReached = false;
		CommunicationState mockCommunicationStateOne = [	canHandle: { AbstractMessage msg -> return (msg instanceof DummyMessageOne) }, 
															handle: {
																AbstractMessage msg ->
																stateOneReached = true
																waitFor({false},1000)
																true
															} ] as CommunicationState
		CommunicationState mockCommunicationStateTwo = [ 	canHandle: { AbstractMessage msg -> return (msg instanceof DummyMessageTwo) },
															handle: {
																AbstractMessage msg ->
																	stateTwoReached = true
																	true
															} ] as CommunicationState
		
		mockCommunication.addCommunicationState(mockCommunicationStateOne)
		mockCommunication.addCommunicationState(mockCommunicationStateTwo)
		
		AbstractMessage abstrMsgOne = new DummyMessageOne()
		AbstractMessage abstrMsgTwo = new DummyMessageTwo()
		
		final CommunicationsHandler comHandler = communicationsHandler;
		Thread t = new Thread() {
			public void run() {
				// add communication
				comHandler.activeCommunications.add(mockCommunication)
				mockCommunication.started = true;

				assert comHandler.handle(abstrMsgOne)
			}	
		}
		t.start();

		waitFor({stateOneReached},5000)
		assert stateOneReached
		
		// now push the next message so we should have in theory two parallel communications
		assert communicationsHandler.handle(abstrMsgTwo)
		assert stateTwoReached
	}
	
	private class DummyMessageOne extends AbstractMessage { }
	
	private class DummyMessageTwo extends AbstractMessage { }
	
	@After
	void stop() {
		super.stop()
	}
}