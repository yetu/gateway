package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StopDiscoverySessionReply

/**
 * @author Sebastian Garn
 */
class StopDiscoverySessionCommunicationTest extends AbstractCommunicationTest {
	public static String DUMMY_SESSION_ID = "abc";
	public static String DUMMY_REQUEST_ID = "123";
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates stop discovery session communication on stop discovery session request'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		StopDiscoverySessionRequest request = new StopDiscoverySessionRequest(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);

		assert communicationsHandler.handle(request);
		
		assert communicationsHandler.activeCommunications.size() == 1;
		assert communicationsHandler.activeCommunications.get(0) instanceof StopDiscoverySessionCommunication
	}
	
	@Test
	void 'assert stop discovery session request and stop session reply finish communication and create queue messages'() {
		StopDiscoverySessionRequest request = new StopDiscoverySessionRequest(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);
		
		StopDiscoverySessionReply reply = new StopDiscoverySessionReply(request.getRequestId(), DUMMY_SESSION_ID);
		
		assert messageSentToInQueue == null
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		assert messageSentToInQueue instanceof StopDiscoverySessionRequest
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		assert messageSentToBackend == null
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
		
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend instanceof StopDiscoverySessionReply
	}
	
	@Test
	void 'assert stop discovery session replies are ignored if there is no communication'() {
		StopDiscoverySessionReply reply = new StopDiscoverySessionReply("none", DUMMY_SESSION_ID);

		assert communicationsHandler.activeCommunications.size() == 0;
		assert !communicationsHandler.handle(reply);
		assert communicationsHandler.activeCommunications.size() == 0;
	}
	
	@Test
	void 'assert stop reply does not belong to communication if correlation id or session id differs'() {
		StopDiscoverySessionRequest request = new StopDiscoverySessionRequest(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);
	
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		StopDiscoverySessionReply reply = new StopDiscoverySessionReply(DUMMY_REQUEST_ID+"x", DUMMY_SESSION_ID);
		assert !communicationsHandler.handle(reply);
		assert !com.isFinished()
		
		reply = new StopDiscoverySessionReply(DUMMY_REQUEST_ID, DUMMY_SESSION_ID+"x");
		assert !communicationsHandler.handle(reply);
		assert !com.isFinished()
		
		reply = new StopDiscoverySessionReply(DUMMY_REQUEST_ID, DUMMY_SESSION_ID);
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
	}
	
	@Test
	void 'assert multiple stop session requests produce multiple communications'() {
		StopDiscoverySessionRequest requestOne = new StopDiscoverySessionRequest(DUMMY_REQUEST_ID+"one", DUMMY_SESSION_ID+"one");
		
		StopDiscoverySessionRequest requestTwo = new StopDiscoverySessionRequest(DUMMY_REQUEST_ID+"two", DUMMY_SESSION_ID+"two");

		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(requestOne);
		assert communicationsHandler.handle(requestTwo);
		assert communicationsHandler.activeCommunications.size() == 2;
	}
	
	@After
	void stop() {
		super.stop();
	}
}