package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.errorhandling.ChannelException
import com.yetu.gateway.home.core.messaging.types.errorhandling.MessageReplyTimeout
import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HeartbeatRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest

/**
 * @author Sebastian Garn
 */
class HeartbeatCommunicationTest extends AbstractCommunicationTest {
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates heartbeat communication on heartbeat request'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		assert communicationsHandler.handle(new HeartbeatRequest());
		
		assert communicationsHandler.activeCommunications.size() == 1;
	}
	
	@Test
	void 'assert heartbeat communication never stops'() {
		HeartbeatRequest request = new HeartbeatRequest();
		
		assert messageSentToBackend == null
		assert communicationsHandler.activeCommunications.size() == 0;
		
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert com instanceof HeartbeatCommunication
		assert !com.isFinished()
		
		assert communicationsHandler.handle(request);
		assert !com.isFinished()
		assert communicationsHandler.activeCommunications.size() == 1;
	}
	
	@Test
	void 'assert heartbeat communication has a timeout and reports a faulty connection'() {
		HeartbeatRequest request = new HeartbeatRequest();
		
		assert messageSentToBackend == null
		assert communicationsHandler.activeCommunications.size() == 0;
		
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		com.expirationTime = 1;
		
		waitFor({faultyConnectionDetected},1000);
		assert faultyConnectionDetected

		assert com.isFinished()
	}
	
	@After
	void stop() {
		super.stop();
	}
}