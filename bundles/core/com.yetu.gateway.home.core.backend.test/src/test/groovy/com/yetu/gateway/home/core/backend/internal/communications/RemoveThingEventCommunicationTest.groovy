package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoveFailure;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoved;

/**
 * @author Sebastian Garn
 */
class RemoveThingEventCommunicationTest extends AbstractCommunicationTest {
	public static String DUMMY_REQUEST_ID = "123";
	public static String DUMMY_THING_ID = "abc";
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates thing remove event communication on thing removed event or failure and immediately closes communication'() {
		assert communicationsHandler.activeCommunications.size() == 0;
		ThingRemoved thingRemovedMsg = new ThingRemoved(DUMMY_THING_ID);
		
		messageSentToBackend == null
		assert communicationsHandler.handle(thingRemovedMsg);
		assert messageSentToBackend instanceof ThingRemoved
		
		ThingRemoveFailure thingRemoveFailureMsg = new ThingRemoveFailure(DUMMY_THING_ID,"",ThingRemoveFailure.ErrorCode.THING_NOT_FOUND);
		
		messageSentToBackend == null
		assert communicationsHandler.handle(thingRemoveFailureMsg);
		assert messageSentToBackend instanceof ThingRemoveFailure
		
		assert communicationsHandler.activeCommunications.size() == 0;
	}

	@After
	void stop() {
		super.stop();
	}
}