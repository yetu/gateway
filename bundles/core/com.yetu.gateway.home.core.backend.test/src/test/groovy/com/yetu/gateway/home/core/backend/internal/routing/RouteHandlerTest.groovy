package com.yetu.gateway.home.core.backend.internal.routing


import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.backend.AbstractBindingTest
import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor
import com.yetu.gateway.home.core.backend.types.ConnectionStateListener
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler
import com.yetu.gateway.home.core.messaging.MessageTransport
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage
import com.yetu.gateway.home.core.messaging.types.queue.QueueMessageTransport

/**
 *
 * @author Sebastian Garn
 */
class RouteHandlerTest extends AbstractBindingTest {
	private RemoteCommunicationHandler remoteCommunicationHandler
	private RouteHandler routeHandler;
	private MessageTransport messageTransport;
	private Object readLock;

	@Before
	void setUp() {
		routeHandler = new RouteHandler();

		messageTransport = waitForService(MessageTransport.class, QueueMessageTransport.class, 5000);
		assert messageTransport != null
		
		readLock = new Object();
	}

	@Test
	void 'assert route handler reconnects after connection lost'() {
		DummyRemoteCommunicationHandler remoteConn = new DummyRemoteCommunicationHandler();
		
		// connect with dummy remote handler
		assertFalse(remoteConn.started);
		ConnectionDescriptor dummyConnectionDescriptor = new ConnectionDescriptor("127.0.0.1", 12345);
		routeHandler.start(dummyConnectionDescriptor, 500, messageTransport, remoteConn);

		assertTrue(remoteConn.started);

		// simulate connection interruption
		remoteConn.started = false;
		assert !remoteConn.started

		remoteConn.listener.onConnectionLost();
		
		// protocol should take care of reconnecting
		waitFor({remoteConn.started},5000);
		assert remoteConn.started
	}

	@Test
	void 'assert reading thread gets stopped on connection lost event'() {
		DummyRemoteCommunicationHandler remoteConn = new DummyRemoteCommunicationHandler();
		ConnectionDescriptor dummyConnectionDescriptor = new ConnectionDescriptor("127.0.0.1", 12345);
		
		routeHandler.start(dummyConnectionDescriptor, 500, messageTransport, remoteConn);
		
		// handler is reading queue as soon as connected
		assert !remoteConn.receiveCalled;
		
		// simulate connection established
		remoteConn.listener.onConnectionEstablished();
		
		// queue transceiver should be started
		waitFor({remoteConn.receiveCalled},500)
		assert remoteConn.receiveCalled;
		assert !routeHandler.incomingMessageHandler.isStopped()
		
		// queue listener is alive
		assert routeHandler.incomingMessageHandlerThread.isAlive()
		
		// simulate connection loss
		remoteConn.listener.onConnectionLost();
		
		// thread should get stopped
		waitFor({routeHandler.incomingMessageHandler.isStopped()},500)
		assert routeHandler.incomingMessageHandler.isStopped()
		
		// queue listener shouldn't be alive anymore
		waitFor({!routeHandler.incomingMessageHandlerThread.isAlive()},500)
		assert !routeHandler.incomingMessageHandlerThread.isAlive()
	}
	
	@Test
	void 'assert reading and sending threads get restarted after reconnect'() {
		DummyRemoteCommunicationHandler remoteConn = new DummyRemoteCommunicationHandler();
		ConnectionDescriptor dummyConnectionDescriptor = new ConnectionDescriptor("127.0.0.1", 12345);
		
		routeHandler.start(dummyConnectionDescriptor, 500, messageTransport, remoteConn);
		remoteConn.listener.onConnectionEstablished();
		
		waitFor({remoteConn.receiveCalled},500)
		assert remoteConn.receiveCalled;
		assert routeHandler.incomingMessageHandlerThread.isAlive()
		assert routeHandler.outgoingMessageHandlerThread.isAlive()
		
		// simulate connection loss
		remoteConn.started = false;
		remoteConn.listener.onConnectionLost();

		// queue listener shouldn't be alive anymore
		waitFor({!routeHandler.incomingMessageHandlerThread.isAlive()},500)
		assert !routeHandler.incomingMessageHandlerThread.isAlive()
		waitFor({!routeHandler.outgoingMessageHandlerThread.isAlive()},500)
		assert !routeHandler.outgoingMessageHandlerThread.isAlive()

		// wait for reconnect
		waitFor({remoteConn.started},1000)
		assert remoteConn.started;
		remoteConn.listener.onConnectionEstablished();
		
		// reading thread should be alive again
		waitFor({routeHandler.incomingMessageHandlerThread.isAlive()},1000)
		assert routeHandler.incomingMessageHandlerThread.isAlive()
		
		waitFor({routeHandler.outgoingMessageHandlerThread.isAlive()},1000)
		assert routeHandler.outgoingMessageHandlerThread.isAlive()
	}
	
	@Test
	void 'assert outgoing message queue is unavailable after conntection down'() {
		DummyRemoteCommunicationHandler remoteConn = new DummyRemoteCommunicationHandler();
		ConnectionDescriptor dummyConnectionDescriptor = new ConnectionDescriptor("127.0.0.1", 12345);
		
		routeHandler.start(dummyConnectionDescriptor, 500, messageTransport, remoteConn);
		assert !routeHandler.messageTransport.getOutgoing().isAvailable()
		
		remoteConn.listener.onConnectionEstablished();
		
		// channel should be available
		waitFor({routeHandler.messageTransport.getOutgoing().isAvailable()},500)
		assert routeHandler.messageTransport.getOutgoing().isAvailable();
	
		// simulate connection loss
		remoteConn.started = false;
		remoteConn.listener.onConnectionLost();

		// channel should not be available anymore
		waitFor({!routeHandler.messageTransport.getOutgoing().isAvailable()},500)
		assert !routeHandler.messageTransport.getOutgoing().isAvailable();
		
		// wait for reconnect
		waitFor({remoteConn.started},1000)
		assert remoteConn.started;
		remoteConn.listener.onConnectionEstablished();
		
		// channel should be available again
		waitFor({routeHandler.messageTransport.getOutgoing().isAvailable()},500)
		assert routeHandler.messageTransport.getOutgoing().isAvailable();
	}

	class DummyRemoteCommunicationHandler implements RemoteCommunicationHandler {
		public ConnectionStateListener listener;
		public boolean started = false;
		private boolean receiveCalled = false;
		
		@Override
		public boolean setup(ConnectionDescriptor descriptor) {
			return true;
		}

		@Override
		public boolean start() {
			started = true;
			return true;
		}

		@Override
		public void stop() {

		}

		@Override
		public IncomingMessage receive() {
			receiveCalled = true;

			synchronized(readLock) {
				readLock.wait();
			}
			return null;
		}

		@Override
		public boolean send(OutgoingMessage message) {
			return false;
		}

		@Override
		public void addConnectionStateListener(ConnectionStateListener listener) {
			this.listener = listener;
		}

		@Override
		public void removeConnectionStateListener(ConnectionStateListener listener) {
			
		}

		@Override
		public void signalizeFaultyConnection() {
				
		}

		@Override
		public void shutdown() {

		}
	}
	@After
	void stop() {
		
	}
}