package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply

/**
 * @author Sebastian Garn
 */
class RemoveThingCommunicationTest extends AbstractCommunicationTest {
	public static String DUMMY_REQUEST_ID = "123";
	public static String DUMMY_THING_ID = "abc";
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates thing remove communication on remove request'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		RemoveThingRequest request = new RemoveThingRequest(DUMMY_REQUEST_ID, DUMMY_THING_ID);

		assert communicationsHandler.handle(request);
		
		assert communicationsHandler.activeCommunications.size() == 1;
		assert communicationsHandler.activeCommunications.get(0) instanceof RemoveThingCommunication
	}
	
	@Test
	void 'assert Remove thing request and corresponding reply close the remove communication'() {
		RemoveThingRequest request = new RemoveThingRequest(DUMMY_REQUEST_ID, DUMMY_THING_ID);
		RemoveThingReply reply = new RemoveThingReply(DUMMY_REQUEST_ID);

		assert messageSentToInQueue == null
		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		assert messageSentToInQueue instanceof RemoveThingRequest
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		assert messageSentToBackend == null
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
		
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend instanceof RemoveThingReply
	}
	
	@Test
	void 'assert Remove thing replies are ignored if there is no communication'() {
		RemoveThingReply reply = new RemoveThingReply(DUMMY_REQUEST_ID);

		assert communicationsHandler.activeCommunications.size() == 0;
		assert !communicationsHandler.handle(reply);
		assert communicationsHandler.activeCommunications.size() == 0;
	}
	
	@Test
	void 'assert remove thing reply does not belong to communication if correlation id differs'() {
		RemoveThingRequest request = new RemoveThingRequest(DUMMY_REQUEST_ID, DUMMY_THING_ID);
	
		assert communicationsHandler.handle(request);
		assert communicationsHandler.activeCommunications.size() == 1;
		
		Communication com = communicationsHandler.activeCommunications.get(0);
		assert !com.isFinished()
		
		RemoveThingReply reply = new RemoveThingReply(DUMMY_REQUEST_ID+"x");
		assert !communicationsHandler.handle(reply);
		assert !com.isFinished()

		// now use proper correlation id
		reply = new RemoveThingReply(DUMMY_REQUEST_ID);
		assert communicationsHandler.handle(reply);
		assert com.isFinished()
	}
	
	@Test
	void 'assert multiple thing remove requests produce multiple communications'() {
		RemoveThingRequest requestOne = new RemoveThingRequest(DUMMY_REQUEST_ID+"one", DUMMY_THING_ID+"one");
		
		RemoveThingRequest requestTwo = new RemoveThingRequest(DUMMY_REQUEST_ID+"two", DUMMY_THING_ID+"two");

		assert communicationsHandler.activeCommunications.size() == 0;
		assert communicationsHandler.handle(requestOne);
		assert communicationsHandler.handle(requestTwo);
		assert communicationsHandler.activeCommunications.size() == 2;
	}
	
	@After
	void stop() {
		super.stop();
	}
}