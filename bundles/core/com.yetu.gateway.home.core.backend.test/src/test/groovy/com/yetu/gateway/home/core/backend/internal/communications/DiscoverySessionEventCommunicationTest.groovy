package com.yetu.gateway.home.core.backend.internal.communications

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.core.messaging.types.messages.AbstractMessage
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionExpired;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionFinished
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted

/**
 * @author Sebastian Garn
 */
class DiscoverySessionEventCommunicationTest extends AbstractCommunicationTest {
	public static String DUMMY_SESSION_ID = "abc";
	
	@Before
	void setUp() {
		super.setup();
	}
	
	@Test
	void 'assert handler creates DiscoverySessionEventCommunication on DiscoverySessionError and sends message to out queue'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		DiscoverySessionError message = new DiscoverySessionError(DUMMY_SESSION_ID, "test error", 0);

		assert messageSentToBackend == null
		assert communicationsHandler.handle(message);
		
		// communication should have been terminated immediately
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend != null
		assert messageSentToBackend instanceof DiscoverySessionError
	}
	
	@Test
	void 'assert handler creates DiscoverySessionEventCommunication on DiscoverySessionFinished and sends message to out queue'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		DiscoverySessionFinished message = new DiscoverySessionFinished(DUMMY_SESSION_ID);

		assert messageSentToBackend == null
		assert communicationsHandler.handle(message);
		
		// communication should have been terminated immediately
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend != null
		assert messageSentToBackend instanceof DiscoverySessionFinished
	}
	
	@Test
	void 'assert handler creates DiscoverySessionEventCommunication on DiscoverySessionStarted and sends message to out queue'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		DiscoverySessionStarted message = new DiscoverySessionStarted(DUMMY_SESSION_ID);

		assert messageSentToBackend == null
		assert communicationsHandler.handle(message);
		
		// communication should have been terminated immediately
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend != null
		assert messageSentToBackend instanceof DiscoverySessionStarted
	}
	
	@Test
	void 'assert handler creates DiscoverySessionEventCommunication on DiscoverySessionExpired and sends message to out queue'() {
		assert communicationsHandler.activeCommunications.size() == 0;

		DiscoverySessionExpired message = new DiscoverySessionExpired(DUMMY_SESSION_ID);

		assert messageSentToBackend == null
		assert communicationsHandler.handle(message);
		
		// communication should have been terminated immediately
		assert communicationsHandler.activeCommunications.size() == 0;

		assert messageSentToBackend != null
		assert messageSentToBackend instanceof DiscoverySessionExpired
	}
	
	
	@After
	void stop() {
		super.stop();
	}
}