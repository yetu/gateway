package com.yetu.gateway.home.core.monitoring

import com.yetu.gateway.home.core.monitoring.internal.operations.ListDirectoryContentsOperation;
import com.yetu.gateway.home.core.monitoring.internal.operations.RetrieveLogFileOperation;
import com.yetu.gateway.home.core.monitoring.internal.operations.RetrieveLogFilePartsOperation;
import com.yetu.gateway.home.core.monitoring.model.OperationResult

import org.eclipse.smarthome.test.OSGiTest
import org.junit.Before
import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.junit.matchers.JUnitMatchers.*

/**
 * Checks whether operations take care of restrictions.
 * 
 * @author Sebastian Garn
 */
class OperationListenerTest extends OSGiTest {
	@Before
	void setUp() {
		
	}

	@Test
	void 'assert list directory contents operation restricts which directories can be listed'() {
		ListDirectoryContentsOperation operation = new ListDirectoryContentsOperation();
		
		HashMap<String, String> configuration = new HashMap<String, String>();
		configuration.put("directory", System.getProperty("user.home"));
		
		OperationResult result = operation.execute(configuration, null);
		
		assertThat result.getStatus(), is(OperationResult.Status.FAILURE);
		
		// probably not the best check
		assertTrue result.getDescription().contains("restrictions");
	}
	
	@Test
	void 'assert retrieve log file operation'() {
		RetrieveLogFileOperation operation = new RetrieveLogFileOperation();
		
		HashMap<String, String> configuration = new HashMap<String, String>();
		configuration.put(RetrieveLogFileOperation.PARAMETER_FILENAME, System.getProperty("user.home"));
		
		OperationResult result = operation.execute(configuration, null);
		
		assertThat result.getStatus(), is(OperationResult.Status.FAILURE);
		
		// probably not the best check
		assertTrue result.getDescription().contains("restrictions");
	}
	
	@Test
	void 'assert retrieve log file parts operation'() {
		RetrieveLogFilePartsOperation operation = new RetrieveLogFilePartsOperation();
		
		HashMap<String, String> configuration = new HashMap<String, String>();
		configuration.put(RetrieveLogFileOperation.PARAMETER_FILENAME, System.getProperty("user.home"));
		configuration.put(RetrieveLogFilePartsOperation.PARAMETER_LINECOUNT, "5");
		configuration.put(RetrieveLogFilePartsOperation.PARAMETER_BOTTOM_START, "false");
		
		OperationResult result = operation.execute(configuration, null);
		
		assertThat result.getStatus(), is(OperationResult.Status.FAILURE);
		
		// probably not the best check
		assertTrue result.getDescription().contains("restrictions");
	}

}
