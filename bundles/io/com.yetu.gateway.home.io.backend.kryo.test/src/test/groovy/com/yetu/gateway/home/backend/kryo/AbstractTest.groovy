package com.yetu.gateway.home.backend.kryo

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.junit.matchers.JUnitMatchers.*

import org.eclipse.smarthome.test.OSGiTest
import org.osgi.framework.Bundle
import org.osgi.framework.BundleContext
import org.osgi.framework.BundleException
import org.osgi.framework.ServiceReference
import org.osgi.util.tracker.ServiceTracker


abstract class AbstractTest extends OSGiTest {
	public void restartBundle(String symbolicName, BundleContext currContext) {
		for (Bundle currBundle : currContext.getBundles()) {
			if (currBundle.getSymbolicName().compareTo(symbolicName) == 0) {
				try {
					currBundle.stop();
					currBundle.start();
					return
				} catch(BundleException bException) {
					
				}
			}
		}
		
		println "Bundle with name "+symbolicName+" not found"
	}
	
	public Object waitForService(Class serviceInterfaceClass, Class implementingServiceClass, int timeout) {
		boolean found = false
		long endsAt = Calendar.getInstance().getTimeInMillis() + timeout

		while (Calendar.getInstance().getTimeInMillis() < endsAt) {
			ServiceTracker tracker = new ServiceTracker(bundleContext, serviceInterfaceClass.getName(), null)
			tracker.open()
			tracker.waitForService(100)
			
			for (ServiceReference s : tracker.getServiceReferences()) {
				if (bundleContext.getService(s).getClass() == implementingServiceClass) {
					return bundleContext.getService(s)
				}
			}
		}
		
		return null
	}
}