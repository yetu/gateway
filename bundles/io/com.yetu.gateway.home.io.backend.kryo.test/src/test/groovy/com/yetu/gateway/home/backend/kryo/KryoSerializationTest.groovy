package com.yetu.gateway.home.backend.kryo

import static org.junit.Assert.*

import java.nio.channels.NotYetConnectedException

import org.junit.After
import org.junit.Before
import org.junit.BeforeClass;
import org.junit.Test

import com.yetu.gateway.home.backend.kryo.internal.utils.DummyBackend
import com.yetu.gateway.home.backend.kryo.internal.utils.DummyBackendConnection
import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor
import com.yetu.gateway.home.core.backend.types.ConnectionStateListener
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError
import com.yetu.gateway.home.io.backend.kryo.internal.KryoCommunicationHandler
import com.yetu.gatewayprotocol.kryo.ServerMessage.HandshakeReply

/**
 * This class takes care of testing the ProtobufCommunicationHandler together with the TCPBackendTransport
 * 
 * @author Sebastian Garn
 *
 */
class KryoSerializationTest extends AbstractTest {
	private static TEST_BACKEND_PORT = 5588;
	
	private DummyBackend backend;
	private KryoCommunicationHandler kryoHandler;
	
	@Before
	void setup() {
		backend = new DummyBackend();
		backend.start(TEST_BACKEND_PORT);

		kryoHandler = waitForService(RemoteCommunicationHandler.class, KryoCommunicationHandler.class, 5000)

		assert kryoHandler != null
	}
	
	@Test
	void 'assert kryo is able to serialize objects'() {
		// TODO: check whether kryo is able to serialize all gw messages (are they all registered?)
		assert true;
		
		backend.setRejectConnections(false)
		kryoHandler.setup(new ConnectionDescriptor("127.0.0.1",TEST_BACKEND_PORT))
		
		boolean connected = false;
		boolean msgRecvd = false;

		// connect with backend - gw tries to send the queued message
		kryoHandler.addConnectionStateListener(new ConnectionStateListener() {
			public void onConnectionLost() {
				connected = false;
			}
			
			public void onConnectionEstablished() {
				connected = true;
			}
		});
		
		// connect
		assert kryoHandler.start() == true;
		
		// wait for connection established
		waitFor({connected}, 1000)
		assert connected

		assert kryoHandler.send(new SetPropertyError(com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError.ErrorCode.ERROR_NOT_FOUND,"","","","","",""));
	}
	
	@After
	void finish() {
		backend.stop();
	}	
}