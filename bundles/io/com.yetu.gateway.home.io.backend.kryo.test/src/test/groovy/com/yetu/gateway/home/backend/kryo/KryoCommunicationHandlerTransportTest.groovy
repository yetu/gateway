package com.yetu.gateway.home.backend.kryo

import static org.junit.Assert.*

import java.nio.channels.NotYetConnectedException

import org.junit.After
import org.junit.Before
import org.junit.BeforeClass;
import org.junit.Test

import com.yetu.gateway.home.backend.kryo.internal.utils.DummyBackend
import com.yetu.gateway.home.backend.kryo.internal.utils.DummyBackendConnection
import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor
import com.yetu.gateway.home.core.backend.types.ConnectionStateListener
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError
import com.yetu.gateway.home.io.backend.kryo.internal.KryoCommunicationHandler
import com.yetu.gatewayprotocol.kryo.ServerMessage.HandshakeReply

/**
 * This class takes care of testing the ProtobufCommunicationHandler together with the TCPBackendTransport
 * 
 * @author Sebastian Garn
 *
 */
class KryoCommunicationHandlerTransportTest extends AbstractTest {
	private static TEST_BACKEND_PORT = 5588;
	
	private DummyBackend backend;
	private KryoCommunicationHandler kryoHandler;
	
	@Before
	void setup() {
		backend = new DummyBackend();
		backend.start(TEST_BACKEND_PORT);

		kryoHandler = waitForService(RemoteCommunicationHandler.class, KryoCommunicationHandler.class, 5000)

		assert kryoHandler != null
	}
	
	@Test
	void 'assert Gateway is not reconnecting multiple times at the same time'() {
		kryoHandler.setup(new ConnectionDescriptor("127.0.0.1",TEST_BACKEND_PORT));

		assert kryoHandler.start() == true;
		assert kryoHandler.start() == true;
		
		waitFor({backend.getConnections().size() > 1 }, 2000)

		// backend should have new connection
		assert backend.getConnections().size() == 1
	}

	@Test
	void 'assert Gateway is connecting properly'() {
		kryoHandler.setup(new ConnectionDescriptor("127.0.0.1",TEST_BACKEND_PORT));

		assert kryoHandler.start() == true;
		
		waitFor({backend.getConnections().size() != 0 }, 1000)

		// backend should have new connection
		assert backend.getConnections().size() == 1

		DummyBackendConnection connection = backend.getConnections().values().iterator().next()
	}

	@Test
	void 'assert kryo handler informs about connection loss and connection establishment'() {
		// this will cause the backend to immediately close the connection after a few ms
		backend.setRejectConnections(true)
		
		int connectionDownCount = 0
		int connectionUpCount = 0

		// connect with backend - gw tries to send the queued message
		kryoHandler.addConnectionStateListener(new ConnectionStateListener() {
			public void onConnectionLost() {
				connectionDownCount+=1;
			}
			
			public void onConnectionEstablished() {
				connectionUpCount+=1;
			}
		});
		
		kryoHandler.setup(new ConnectionDescriptor("127.0.0.1",TEST_BACKEND_PORT));
		assert kryoHandler.start() == true;

		waitFor({connectionDownCount == 1}, 1000)
		waitFor({connectionUpCount == 1}, 1000)

		assert kryoHandler.start() == true;
		
		waitFor({connectionDownCount == 2}, 1000)
		waitFor({connectionUpCount == 2}, 1000)
		
		assert connectionDownCount == 2
		assert connectionUpCount == 2
	}
	
	@Test
	void 'assert handler informs about incoming messages'() {
		backend.setRejectConnections(false)
		kryoHandler.setup(new ConnectionDescriptor("127.0.0.1",TEST_BACKEND_PORT))
		
		boolean connected = false;
		boolean msgRecvd = false;

		// connect with backend - gw tries to send the queued message
		kryoHandler.addConnectionStateListener(new ConnectionStateListener() {
			public void onConnectionLost() {
				connected = false;
			}
			
			public void onConnectionEstablished() {
				connected = true;
			}
		});
		
		// connect
		assert kryoHandler.start() == true;
		
		// wait for connection established
		waitFor({connected}, 1000)
		assert connected
		
		Thread msgReceiver = new Thread() {
			public void run() {
				IncomingMessage msg = kryoHandler.receive()
				msgRecvd = true;
			}
		}
		msgReceiver.start();
		
		// wait for a message - we should not get one
		waitFor({msgRecvd}, 500)
		assert !msgRecvd
		
		// dummy backend sends message
		DummyBackendConnection connection = backend.getConnections().values().iterator().next()
		
		HandshakeReply handshake = new HandshakeReply("123");
		assert connection.sendMessage(handshake);
		
		// we should have received a message
		waitFor({msgRecvd}, 500)
		assert msgRecvd
	}
	
	@Test
	void 'assert NotYetConnectedException gets thrown while receiving and going offline'() {
		backend.setRejectConnections(false)
		kryoHandler.setup(new ConnectionDescriptor("127.0.0.1",TEST_BACKEND_PORT))
		
		boolean connected = false;
		boolean receiveStop = false;
		
		// simple thread which polls the receive queue for new messages
		Thread msgReceiver = new Thread() {
			public void run() {
				while (!receiveStop) {
					try {
						
						IncomingMessage msg = kryoHandler.receive()
					} catch(InterruptedException e) {
						// ignore
					} catch(NotYetConnectedException nc) {
						receiveStop = true;
					}
				}
			}
		}
		
		// connect with backend - gw tries to send the queued message
		kryoHandler.addConnectionStateListener(new ConnectionStateListener() {
			public void onConnectionLost() {
				connected = false;
				msgReceiver.interrupt();
			}
			
			public void onConnectionEstablished() {
				connected = true;
			}
		});
		
		// connect
		assert kryoHandler.start() == true;
		
		// wait for connection established
		waitFor({connected}, 1000)
		assert connected
		
		msgReceiver.start();
		
		waitFor({receiveStop}, 500)
		assert !receiveStop;
		
		// cancel the backend connection
		waitFor({backend.getConnections() != 0}, 1000)
		DummyBackendConnection connection = backend.getConnections().values().iterator().next()
		connection.close();
		
		// closed connection should thread to terminate
		waitFor({receiveStop},500);
		assert receiveStop;
	}

	@After
	void finish() {
		backend.stop();
	}	
}