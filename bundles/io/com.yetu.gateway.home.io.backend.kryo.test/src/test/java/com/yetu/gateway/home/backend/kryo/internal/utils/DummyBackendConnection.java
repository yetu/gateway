package com.yetu.gateway.home.backend.kryo.internal.utils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;

import com.yetu.gatewayprotocol.kryo.GatewayMessage;
import com.yetu.gatewayprotocol.kryo.ServerMessage;
import com.yetu.kryo_support.KryoSupport;



public class DummyBackendConnection {
	private ChannelHandlerContext context;
	private GatewayMessage lastMessage;
	
	public DummyBackendConnection(ChannelHandlerContext context) {
		this.context = context;
	}
	
	public void setLastMessage(GatewayMessage lastMessage) {
		this.lastMessage = lastMessage;
	}
	
	public GatewayMessage getLastMessage() {
		return lastMessage;
	}
	
	/**
	 * Sends a message over this dummy connection
	 * @param ServerMessage message
	 * @return True if message was sent
	 */
	public boolean sendMessage(ServerMessage message) {
		byte[] result = KryoSupport.marshall(message);
		ByteBuf msg = Unpooled.copiedBuffer(result);
		
		ChannelFuture future = context.channel().writeAndFlush(msg); 
		try {
			future.sync();
		} catch (InterruptedException e) {
			System.out.println("Failed to send message: "+e.toString());
			return false;
		}
		
		return true;
	}
	
	public void close() {
		context.close();
	}
}
