package com.yetu.gateway.home.backend.kryo.internal.utils;

public class HexUtil {
	// TODO keep this class?
	
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static byte[] hexToBytes(String hex) {
		byte[] bytes = new byte[hex.length() / 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int h = hex.charAt(j*2) - 0x30;
			if (h > 10) { h = h - 7; }
			int l = hex.charAt(j*2+1) - 0x30;
			if (l > 10) { l = l - 7; };
			bytes[j] = (byte) (((h << 4) + l) & 0xFF); 
		}
		return bytes; 
	}

	
}
