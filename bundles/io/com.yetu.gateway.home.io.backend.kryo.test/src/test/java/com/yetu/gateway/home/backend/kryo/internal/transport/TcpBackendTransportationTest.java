package com.yetu.gateway.home.backend.kryo.internal.transport;

import static org.junit.Assert.*;

import org.junit.Test;

import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor;
import com.yetu.gateway.home.io.backend.kryo.internal.transport.KryoTransportation;
import com.yetu.gatewayprotocol.kryo.GatewayMessage;

public class TcpBackendTransportationTest {
	@Test
	public void testSendWhileOffline() {
		KryoTransportation transport = new KryoTransportation();
		transport.setup(new ConnectionDescriptor("127.0.0.1",5678));
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		GatewayMessage.HandshakeRequest handshake = new GatewayMessage.HandshakeRequest("","123","");
		assertFalse(transport.sendMessage(handshake));
	}
	
	@Test
	public void testTryToConnectTwice() {
		KryoTransportation transport = new KryoTransportation();
		transport.setup(new ConnectionDescriptor("127.0.0.1",5678));
		assertTrue(transport.connect());
		assertFalse(transport.connect());
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		GatewayMessage.HandshakeRequest handshake = new GatewayMessage.HandshakeRequest("","123","");
		assertFalse(transport.sendMessage(handshake));
		assertFalse(transport.sendMessage(handshake));
	}
}
