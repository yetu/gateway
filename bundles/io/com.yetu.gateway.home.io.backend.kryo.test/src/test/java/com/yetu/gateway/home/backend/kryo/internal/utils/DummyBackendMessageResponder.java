package com.yetu.gateway.home.backend.kryo.internal.utils;

import com.yetu.gatewayprotocol.kryo.GatewayMessage;

public interface DummyBackendMessageResponder {
	public void handleMessage(DummyBackendConnection connection, GatewayMessage message);
}