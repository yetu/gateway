package com.yetu.gateway.home.backend.kryo.internal.utils;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gatewayprotocol.kryo.GatewayMessage;
import com.yetu.kryo_support.KryoSupport;

public class DummyBackend {
	
	public static final Logger logger = LoggerFactory.getLogger(DummyBackend.class);
	
	private HashMap<ChannelHandlerContext, DummyBackendConnection> connections;
	private ChannelFuture channelFuture;
	private ServerBootstrap bootstrap;
	private boolean listen = true;
	private boolean rejectConnections = false;
	
	public DummyBackend() {
		this.connections = new HashMap<ChannelHandlerContext, DummyBackendConnection>();

		EventLoopGroup group = new NioEventLoopGroup();

		// configure server
        bootstrap = new ServerBootstrap();
        bootstrap.group(group);
        bootstrap.channel(NioServerSocketChannel.class);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000);
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
        	@Override
        	public void initChannel(SocketChannel ch) throws Exception {
        		ChannelPipeline pipeline = ch.pipeline();
        		pipeline.addLast("length-decoder" , new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, -4, 4)); 
        		pipeline.addLast("message-decoder", new ChannelInboundHandlerAdapter() {
        			@Override
        			public void channelRegistered(ChannelHandlerContext ctx) {
        				if (rejectConnections) {
        					ctx.close();
        				}
        				
        				logger.debug("New connection");
        				connections.put(ctx, new DummyBackendConnection(ctx));
        			}
        			
        			@Override
        			public void channelRead(ChannelHandlerContext ctx, Object msg) {
        				ByteBuf m = (ByteBuf) msg;
        				
        				try {
        					byte[] bytes = new byte[m.readableBytes()];
        					m.readBytes(bytes);

        					GatewayMessage gwMsg = (GatewayMessage)KryoSupport.unmarshall(bytes);
        					connections.get(ctx).setLastMessage(gwMsg);
        				} catch (Exception e) {
        					System.out.println("Unable to read from channel: "+e.toString());
        				} finally {
        					m.release();
        				}
        			}
        			
        			@Override
        			public void channelUnregistered(ChannelHandlerContext ctx) {
        				connections.get(ctx).close();
        				connections.remove(ctx);
        			}
        		});
        		pipeline.addLast("length-encoder" , new LengthFieldPrepender(4, true));
        	}
        	
        });
	}
	
	public void start(int port) {
        try {
	        // Start the server.
	        channelFuture = bootstrap.bind(port).sync();
        } catch (Exception e) {
        	System.out.println("Failed to create ServerSocket: "+e.toString());
        }
	}
	
	public boolean isListening() {
		if (listen && channelFuture != null && channelFuture.channel() != null && channelFuture.channel().isOpen()) {
			return true;
		}

		return false;
	}
	
	public HashMap<ChannelHandlerContext, DummyBackendConnection> getConnections() {
		return connections;
	}
	
	public boolean stop() {
		if (!isListening()) {
			return false;
		}
		
		channelFuture.channel().close();
		
		for (ChannelHandlerContext handler : connections.keySet()) {
			handler.close();
		}
		
		return true;
	}
	
	public void setRejectConnections(boolean value) {
		rejectConnections = value;
	}
}
