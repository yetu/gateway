package com.yetu.gateway.home.backend.kryo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.channels.NotYetConnectedException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.yetu.gateway.home.backend.kryo.internal.utils.DummyBackend;
import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor;
import com.yetu.gateway.home.core.backend.types.ConnectionStateListener;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;
import com.yetu.gateway.home.io.backend.kryo.internal.KryoCommunicationHandler;

/**
 * Some basic tests of the ProtobufCommunicationHandler functionalities
 * 
 * @author Sebastian Garn
 *
 */
public class KryoCommunicationHandlerTest {
	public static int DEFAULT_PORT = 5577;
	public DummyBackend dummyBackend;
	KryoCommunicationHandler handler;
	
	@Before
	public void setUp() {
		dummyBackend = new DummyBackend();
		dummyBackend.start(DEFAULT_PORT);
		
		handler = new KryoCommunicationHandler();
		handler.addConnectionStateListener(new ConnectionStateListener() {

			@Override
			public void onConnectionLost() {
				
			}

			@Override
			public void onConnectionEstablished() {
				
			}
		});
	}
	
	@Test(expected=NotYetConnectedException.class)
	public void testReceiveWhileOffline() {
		handler.setup(new ConnectionDescriptor("127.0.0.1", 5566));
		try {
			handler.receive();
		} catch (InterruptedException e) {
			assertTrue(false);
		}
	}
	
	@Test(expected=NotYetConnectedException.class)
	public void testSendWhileOffline() {
		handler.setup(new ConnectionDescriptor("127.0.0.1", 5566));
		assertFalse(handler.send(new HandshakeRequest("abc","123")));
	}
	
	@Test
	public void testSetupTwiceFails() {
		assertTrue(handler.setup(new ConnectionDescriptor("127.0.0.1", 5566)));
		assertFalse(handler.setup(new ConnectionDescriptor("127.0.0.1", 5566)));
	}
	
	@Test
	public void testStartTwiceFails() {
		handler.setup(new ConnectionDescriptor("127.0.0.1", 5566));
		assertTrue(handler.start());
		assertTrue(handler.start());
	}
	
	@Test
	public void testStartUnconfigured() {
		assertFalse(handler.start());
	}
	
	@Test
	public void testRestartCommunication() {
		handler.setup(new ConnectionDescriptor("127.0.0.1", 5566));
		assertTrue(handler.start());
		handler.stop();
		assertTrue(handler.start());
	}
	
	@After
	public void stop() {
		dummyBackend.stop();
	}
}
