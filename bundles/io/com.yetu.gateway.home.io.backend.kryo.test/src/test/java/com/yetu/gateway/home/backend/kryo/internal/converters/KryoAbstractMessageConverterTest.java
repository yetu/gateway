package com.yetu.gateway.home.backend.kryo.internal.converters;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HeartbeatRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest;
import com.yetu.gateway.home.io.backend.kryo.internal.converters.KryoAbstractMessageConverter;
import com.yetu.gatewayprotocol.kryo.ServerMessage;


public class KryoAbstractMessageConverterTest {
	private KryoAbstractMessageConverter converter;

	private String DEFAULT_CORRELATION_ID = "DUMMY_CORRELATION_ID";
	
	@Before
	public void setup() {
		converter = KryoAbstractMessageConverter.getInstance();	
	}
	
	@Test
	public void testHandshakeReplyConversion() {
		ServerMessage kryoMsg = new ServerMessage.HandshakeReply(DEFAULT_CORRELATION_ID); 
		IncomingMessage abstractedMsg = converter.convert(kryoMsg);

		assertThat(abstractedMsg, instanceOf(HandshakeReply.class));
		assertEquals(DEFAULT_CORRELATION_ID,((HandshakeReply)abstractedMsg).getCorrelationId());
	}
	
	@Test
	public void testHeartbeatConversion() {
		ServerMessage kryoMsg = new ServerMessage.Heartbeat(); 
		IncomingMessage abstractedMsg = converter.convert(kryoMsg);

		assertThat(abstractedMsg, instanceOf(HeartbeatRequest.class));
	}
	
	@Test
	public void testGatewayConfigurationRequestConversion() {
		ServerMessage kryoMsg = new ServerMessage.GatewayConfigurationRequest();
		
		IncomingMessage abstractedMsg = converter.convert(kryoMsg);

		assertThat(abstractedMsg, instanceOf(ConfigurationRequest.class));
	}
	
	@Test
	public void testStartDiscoverySessionRequest() {
		String sessionId = "abc";
		String requestId = "123";
		
		ServerMessage kryoMsg = new ServerMessage.StartDiscoverySessionRequest(requestId, sessionId);
		
		IncomingMessage abstractedMsg = converter.convert(kryoMsg);

		assertThat(abstractedMsg, instanceOf(StartDiscoverySessionRequest.class));
		
		StartDiscoverySessionRequest request = (StartDiscoverySessionRequest)abstractedMsg;
		assertEquals(requestId, request.getRequestId());
		assertEquals(sessionId, request.getSessionId());
	}
	
	@Test
	public void testStopDiscoverySessionRequestConversion() {
		String sessionId = "abc";
		String requestId = "123";
		
		ServerMessage kryoMsg = new ServerMessage.StopDiscoverySessionRequest(requestId, sessionId);
		
		IncomingMessage abstractedMsg = converter.convert(kryoMsg);

		assertThat(abstractedMsg, instanceOf(StopDiscoverySessionRequest.class));
		
		StopDiscoverySessionRequest request = (StopDiscoverySessionRequest)abstractedMsg;
		assertEquals(requestId, request.getRequestId());
		assertEquals(sessionId, request.getSessionId());
	}
	
	@Test
	public void testRemoveThingRequest() {
		String requestId = "abc";
		String thingId = "123";
		
		ServerMessage kryoMsg = new ServerMessage.RemoveThingRequest(requestId, thingId);
		
		IncomingMessage abstractedMsg = converter.convert(kryoMsg);

		assertThat(abstractedMsg, instanceOf(RemoveThingRequest.class));
		
		RemoveThingRequest request = (RemoveThingRequest)abstractedMsg;
		assertEquals(requestId, request.getRequestId());
		assertEquals(thingId, request.getThingId());
	}
	
	@Test
	public void testSetPropertyConversion() {
		String thingId = "123";
		String componentId = "xyz";
		String capabilityId = "cap";
		String propertyName = "prop";
		String value = "val";
		
		ServerMessage kryoMsg = new ServerMessage.SetProperty(thingId, componentId, capabilityId, propertyName, value);
		
		IncomingMessage abstractedMsg = converter.convert(kryoMsg);

		assertThat(abstractedMsg, instanceOf(SetProperty.class));

		SetProperty msg = (SetProperty)abstractedMsg;
		assertEquals(thingId, msg.getThingId());
		assertEquals(componentId, msg.getComponentId());
		assertEquals(capabilityId, msg.getCapabilityId());
		assertEquals(propertyName, msg.getPropertyName());
		assertEquals(value, msg.getValue());
	}
}
