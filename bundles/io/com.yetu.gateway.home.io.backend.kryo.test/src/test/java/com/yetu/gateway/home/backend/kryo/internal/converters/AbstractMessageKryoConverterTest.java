package com.yetu.gateway.home.backend.kryo.internal.converters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.yetu.abstraction.model.Action;
import com.yetu.abstraction.model.ActionParameter;
import com.yetu.abstraction.model.Attribute;
import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.ComponentType;
import com.yetu.abstraction.model.ConnectionDetails;
import com.yetu.abstraction.model.Event;
import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.Thing;
import com.yetu.abstraction.model.ThingModelFactory;
import com.yetu.abstraction.model.ThingStatus;
import com.yetu.abstraction.model.Unit;
import com.yetu.abstraction.model.ValueType;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ConfigurationReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionExpired;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionFinished;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.PropertyChanged;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError.ErrorCode;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StopDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAlreadyAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingDiscovered;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingNotAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoveFailure;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoved;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingStatusChanged;
import com.yetu.gateway.home.io.backend.kryo.internal.converters.AbstractMessageKryoConverter;
import com.yetu.gatewayprotocol.kryo.GatewayMessage;
import com.yetu.gatewayprotocol.kryo.thing.CapabilityDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.ComponentDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.ConnectionDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.PropertyDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.ThingDescriptor;


public class AbstractMessageKryoConverterTest {
	private AbstractMessageKryoConverter converter;
	
	private static String DUMMY_GATEWAY_ID = "abc";
	private static String DUMMY_GATEWAY_IMAGE_VERSION = "123";
	
	@Before
	public void setup() {
		converter = AbstractMessageKryoConverter.getInstance();	
	}
	
	@Test
	public void testHandshakeRequestConversion() {
		OutgoingMessage abstractedMsg = new HandshakeRequest(DUMMY_GATEWAY_ID, DUMMY_GATEWAY_IMAGE_VERSION);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.HandshakeRequest);

		GatewayMessage.HandshakeRequest handshake = (GatewayMessage.HandshakeRequest)kryoMsg;
		assertEquals(handshake.getGatewayId(), DUMMY_GATEWAY_ID);
		assertEquals(handshake.getAutomationStackVersion(), DUMMY_GATEWAY_IMAGE_VERSION);
	}
	
	@Test
	public void testConfigurationReplyConversion() {
		String runningSessionId = "234";
		
		ArrayList<Thing> things = new ArrayList<Thing>();

		Thing thingOne = createDummyThing("thingOne", "", "", "", "", "", "", "", "", "", "", ValueType.BOOLEAN,"","","");
		Thing thingTwo = createDummyThing("thingTwo", "", "", "", "", "", "", "", "", "", "", ValueType.BOOLEAN,"","","");
		
		things.add(thingOne);
		things.add(thingTwo);
		
		OutgoingMessage abstractedMsg = new ConfigurationReply(things, runningSessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.GatewayConfigurationReply);
		
		// we dont have to check the conversion since this is already done in another test
		GatewayMessage.GatewayConfigurationReply converted = (GatewayMessage.GatewayConfigurationReply)kryoMsg;
		assertEquals(converted.getSessionId(), runningSessionId);
		assertEquals(converted.getThings().length, things.size());
	}

	@Test
	public void testStartDiscoverySessionReplyConversion() {
		String correlationId = "abc";
		String sessionId = "123";
		
		OutgoingMessage abstractedMsg = new StartDiscoverySessionReply(correlationId, sessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.StartDiscoverySessionReply);
		
		GatewayMessage.StartDiscoverySessionReply convertedMessage = (GatewayMessage.StartDiscoverySessionReply)kryoMsg;
		assertEquals(convertedMessage.getCorrelationId(), correlationId);
		assertEquals(convertedMessage.getSessionId(), sessionId);
	}
	
	@Test
	public void testDiscoverySessionStartedConversion() {
		String sessionId = "123";
		
		OutgoingMessage abstractedMsg = new DiscoverySessionStarted(sessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.DiscoverySessionStarted);
		
		GatewayMessage.DiscoverySessionStarted convertedMessage = (GatewayMessage.DiscoverySessionStarted)kryoMsg;
		assertEquals(convertedMessage.getSessionId(), sessionId);
	}
	
	@Test
	public void testDiscoverySessionExpiredConversion() {
		String sessionId = "123";
		
		OutgoingMessage abstractedMsg = new DiscoverySessionExpired(sessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.DiscoverySessionExpired);
		
		GatewayMessage.DiscoverySessionExpired convertedMessage = (GatewayMessage.DiscoverySessionExpired)kryoMsg;
		assertEquals(convertedMessage.getSessionId(), sessionId);
	}
	
	@Test
	public void testDiscoverySessionFinishedConversion() {
		String sessionId = "123";
		
		OutgoingMessage abstractedMsg = new DiscoverySessionFinished(sessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.DiscoverySessionStopped);
		
		GatewayMessage.DiscoverySessionStopped convertedMessage = (GatewayMessage.DiscoverySessionStopped)kryoMsg;
		assertEquals(convertedMessage.getSessionId(), sessionId);
	}
	
	@Test
	public void testDiscoverySessionErrorConversion() {
		String sessionId = "123";
		String reason = "katze";
		int code = 1;
		
		OutgoingMessage abstractedMsg = new DiscoverySessionError(sessionId,reason, code);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.DiscoverySessionFailed);
		
		GatewayMessage.DiscoverySessionFailed convertedMessage = (GatewayMessage.DiscoverySessionFailed)kryoMsg;
		assertEquals(convertedMessage.getSessionId(), sessionId);
		assertEquals(convertedMessage.getCause(), reason);
		assertEquals(convertedMessage.getCode(), code+"");
	}
	
	@Test
	public void testStopDiscoverySessionReplyConversion() {
		String correlationId = "abc";
		String sessionId = "123";
		
		OutgoingMessage abstractedMsg = new StopDiscoverySessionReply(correlationId, sessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.StopDiscoverySessionReply);
		
		GatewayMessage.StopDiscoverySessionReply convertedMessage = (GatewayMessage.StopDiscoverySessionReply)kryoMsg;
		assertEquals(convertedMessage.getCorrelationId(), correlationId);
		assertEquals(convertedMessage.getSessionId(), sessionId);
	}
	
	@Test
	public void testRemoveThingReplyConversion() {
		String correlationId = "abc";
		
		OutgoingMessage abstractedMsg = new RemoveThingReply(correlationId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.RemoveThingReply);
		
		GatewayMessage.RemoveThingReply convertedMessage = (GatewayMessage.RemoveThingReply)kryoMsg;
		assertEquals(convertedMessage.getCorrelationId(), correlationId);
	}
	
	@Test
	public void testThingRemovedConversion() {
		String thingId = "123";
		
		OutgoingMessage abstractedMsg = new ThingRemoved(thingId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.ThingRemoved);
		
		GatewayMessage.ThingRemoved convertedMessage = (GatewayMessage.ThingRemoved)kryoMsg;
		assertEquals(convertedMessage.getThingId(), thingId);
	}
	
	@Test
	public void testThingRemoveFailureConversion() {
		String thingId = "123";
		ThingRemoveFailure.ErrorCode code = ThingRemoveFailure.ErrorCode.THING_NOT_FOUND;
		String cause = "abc";

		OutgoingMessage abstractedMsg = new ThingRemoveFailure(thingId, cause, code);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.ThingRemoveFailure);
		
		GatewayMessage.ThingRemoveFailure convertedMessage = (GatewayMessage.ThingRemoveFailure)kryoMsg;
		assertEquals(convertedMessage.getThingId(), thingId);
		assertEquals(convertedMessage.getCause(), cause);
		assertEquals(convertedMessage.getCode(), GatewayMessage.ThingRemoveFailure.ErrorCode.THING_NOT_FOUND);
	}
	
	@Test
	public void testPropertyChangedConversion() {
		String thingId = "123";
		String componentId = "xyz";
		String capabilityId = "cap";
		String propertyName = "prop";
		Date date = Calendar.getInstance().getTime();
		String value = "val";
		
		OutgoingMessage abstractedMsg = new PropertyChanged(thingId, componentId, capabilityId, propertyName, date, value);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.PropertyChanged);
		
		GatewayMessage.PropertyChanged convertedMessage = (GatewayMessage.PropertyChanged)kryoMsg;

		assertEquals(convertedMessage.getThingId(),thingId);
		assertEquals(convertedMessage.getComponentId(),componentId);
		assertEquals(convertedMessage.getCapabilityId(),capabilityId);
		assertEquals(convertedMessage.getTimestamp().toString(),date.toString());
		assertEquals(convertedMessage.getPropertyName(),propertyName);
		assertEquals(convertedMessage.getValue(),value);
	}
	
	@Test
	public void testSetPropertyErrorConversion() {
		ErrorCode errorCode = ErrorCode.ERROR_CAPABILITIY_NOT_FOUND;
		String cause = "katze";
		String thingId = "123";
		String componentId = "xyz";
		String capabilityId = "cap";
		String propertyName = "prop";
		String value = "val";
		
		OutgoingMessage abstractedMsg = new SetPropertyError(errorCode, cause, thingId, componentId, capabilityId, propertyName, value);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.SetPropertyError);
		
		GatewayMessage.SetPropertyError convertedMessage = (GatewayMessage.SetPropertyError)kryoMsg;
		assertEquals(convertedMessage.getThingId(), thingId);
		assertEquals(convertedMessage.getComponentId(), componentId);
		assertEquals(convertedMessage.getCapabilityId(), capabilityId);
		assertEquals(convertedMessage.getPropertyName(), propertyName);
		assertEquals(convertedMessage.getValue(), value);
		assertEquals(convertedMessage.getCause(), cause);
		assertEquals(convertedMessage.getCode().name(), errorCode.name());
	}
	
	@Test
	public void testThingAddedConversion() {
		String thingId = "thingid";
		String thingName = "thingname";
		String manufacturer = "manufacturer";
		String componentId = "componentid";
		String componentName = "componentname";
		String componentTypeId = "componenttypeid";
		String capabilityId = "capabilityd";
		String propertyName = "propertyname";
		String propertyValue = "propertyvalue";
		String unitId = "unitid";
		String unitSymbol = "unitsymbol";
		String connectionDetailsName = "DummyConnectionDetails";
		String connectionDetailsKey = "technology";
		String connectionDetailsValue = "dummy";
		ValueType valueType = ValueType.BOOLEAN;
		
		Thing thing = createDummyThing(thingId, thingName, manufacturer, componentId, componentName, componentTypeId, capabilityId, propertyName, propertyValue, unitId, unitSymbol, valueType, connectionDetailsName, connectionDetailsKey, connectionDetailsValue);
		
		// create a dummy thing
		String sessionId = "111";

		OutgoingMessage abstractedMsg = new ThingAdded(thing, sessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.ThingAdded);
		
		GatewayMessage.ThingAdded convertedMessage = (GatewayMessage.ThingAdded)kryoMsg;
		
		assertNotEquals(convertedMessage.getThing(), null);
		ThingDescriptor thingDescriptor = convertedMessage.getThing();
		assertEquals(thingDescriptor.getId(), thingId);
		assertEquals(thingDescriptor.getName(), thingName);
		assertEquals(thingDescriptor.getManufacturer(), manufacturer);
		assertEquals(thingDescriptor.getMainComponent().getId(), componentId);
		assertEquals(thingDescriptor.getDisplayComponentType(), componentTypeId);
		
		assertEquals(thingDescriptor.getComponents().length, 1);
		ComponentDescriptor componentDescriptor = thingDescriptor.getComponents()[0];
		assertEquals(componentDescriptor.getId(), componentId);
		assertEquals(componentDescriptor.getName(), componentName);
		
		assertEquals(componentDescriptor.getComponentType(), componentTypeId);
				
		assertEquals(componentDescriptor.getCapabilities().length, 1);
		CapabilityDescriptor capabilityDescriptor = componentDescriptor.getCapabilities()[0];
		assertEquals(capabilityDescriptor.getId(), capabilityId);
		
		assertEquals(capabilityDescriptor.getProperties().length, 1);
		PropertyDescriptor propertyDescriptor = capabilityDescriptor.getProperties()[0];
		assertEquals(propertyDescriptor.getName(), propertyName);
		assertEquals(propertyDescriptor.getValue(), propertyValue);
		assertEquals(propertyDescriptor.getUnit(), unitId);
		
		// TODO check conversion of actions and events (waiting for implementation)
		
		// check conversion of connection details
		assertEquals(convertedMessage.getThing().getConnections().length, 1);
		ConnectionDescriptor connectionDescriptor = convertedMessage.getThing().getConnections()[0];
		assertEquals(connectionDescriptor.getId(), connectionDetailsName);
		assertEquals(connectionDescriptor.getAttributes().length, 1);
		assertEquals(connectionDescriptor.getAttributes()[0].getKey(), connectionDetailsKey);
		assertEquals(connectionDescriptor.getAttributes()[0].getValue(), connectionDetailsValue);
	}

	@Test
	public void testThingNotAddedErrorConversion() {
		String code = "111";
		String cause = "abc";
		
		OutgoingMessage abstractedMsg = new ThingNotAdded(code, cause);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.ThingNotAddedError);
		
		GatewayMessage.ThingNotAddedError convertedMessage = (GatewayMessage.ThingNotAddedError)kryoMsg;
		assertEquals(convertedMessage.getCause(), cause);
		assertEquals(convertedMessage.getCode(), code);
	}
	
	@Test
	public void testThingDiscoveredConversion() {
		String thingId = "123";
		String sessionId = "111";
		
		OutgoingMessage abstractedMsg = new ThingDiscovered(sessionId, thingId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.ThingDiscovered);
		
		GatewayMessage.ThingDiscovered convertedMessage = (GatewayMessage.ThingDiscovered)kryoMsg;
		assertEquals(convertedMessage.getThingId(), thingId);
		assertEquals(convertedMessage.getSessionId(), sessionId);
	}
	
	@Test
	public void testThingAlreadyAddedConversion() {
		String thingId = "123";
		String sessionId = "111";
		
		OutgoingMessage abstractedMsg = new ThingAlreadyAdded(thingId, sessionId);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.ThingAlreadyAddedError);
		
		GatewayMessage.ThingAlreadyAddedError convertedMessage = (GatewayMessage.ThingAlreadyAddedError)kryoMsg;
		assertEquals(convertedMessage.getThingId(), thingId);
		assertEquals(convertedMessage.getSessionId(), sessionId);
	}
	
	@Test
	public void testThingStatusChangedConversion() {
		String thingId = "123";
		ThingStatus state = ThingStatus.ONLINE;
		
		OutgoingMessage abstractedMsg = new ThingStatusChanged(thingId, state);
		
		GatewayMessage kryoMsg = converter.convert(abstractedMsg);
		assertTrue(kryoMsg instanceof GatewayMessage.ThingStatusChanged);
		
		GatewayMessage.ThingStatusChanged convertedMessage = (GatewayMessage.ThingStatusChanged)kryoMsg;
		assertEquals(convertedMessage.getThingId(), thingId);
		assertEquals(convertedMessage.getStatus(), ThingDescriptor.Status.AVAILABLE);
	}
	
	
	/**
	 * Creates a simple thing instance with a component that has a capability, property and unit
	 * @param prefix
	 * @return
	 */
	private Thing createDummyThing(String thingId, String thingName, String manufacturer, String componentId, String componentName, String componentTypeId, String capabilityId, String propertyName, String propertyValue, String unitId, String unitSymbol, ValueType valueType, String connectionDetailsName, String connectionDetailsKey, String connectionDetailsValue) {
		ComponentType componentType = ThingModelFactory.eINSTANCE.createComponentType();
		componentType.setId(componentTypeId);

		Unit unit = ThingModelFactory.eINSTANCE.createUnit();
		unit.setId(unitId);
		unit.setSymbol(unitSymbol);
		unit.setValueType(valueType);
		
		Property property = ThingModelFactory.eINSTANCE.createProperty();
		property.setName(propertyName);
		property.setUnit(unit);
		property.setValue((Object)propertyValue);
		
		Capability capability = ThingModelFactory.eINSTANCE.createCapability(capabilityId);
		capability.getProperties().add(property);
		
		Component component = ThingModelFactory.eINSTANCE.createComponent(componentId,componentType);
		component.setName(componentName);
		component.getCapabilities().add(capability);

		Thing thing = ThingModelFactory.eINSTANCE.createThing();
		thing.setId(thingId);
		thing.setManufacturer(manufacturer);
		thing.setName(thingName);
		thing.getComponents().add(component);
		thing.setMainComponent(component);
		thing.setDisplayType(componentType);
		

		// add dummy connection details
		ConnectionDetails connectionDetails = ThingModelFactory.eINSTANCE.createConnectionDetails();
		connectionDetails.setName(connectionDetailsName);
		
		Attribute technology = ThingModelFactory.eINSTANCE.createAttribute();
		technology.setName(connectionDetailsKey);
		technology.setValue(connectionDetailsValue);
		
		connectionDetails.getProperties().add(technology);
		thing.getConnectionDetails().add(connectionDetails);
		
		return thing;
	}
	
	
	// comparison - not needed right now
	
	public boolean thingsEqual(Thing thingOne, Thing thingTwo) {
		// check thing attributes
		if (thingOne.getName().compareTo(thingTwo.getName()) != 0) return false;
		else if (thingOne.getManufacturer().compareTo(thingTwo.getManufacturer()) != 0) return false;
		
		// check connection details
		if (thingOne.getConnectionDetails().size() != thingTwo.getConnectionDetails().size()) return false;
		for (ConnectionDetails currConnectionDetail : thingOne.getConnectionDetails()) {
			boolean found = false;
			
			for (ConnectionDetails potentialMatch : thingTwo.getConnectionDetails()) {
				if (connectionDetailsEqual(currConnectionDetail,potentialMatch)) {
					found = true;
					break;
				}
			}
			
			if (!found) return false;
		}
		
		// check components
		if (thingOne.getComponents().size() != thingOne.getComponents().size()) return false;
		for (Component currComponent : thingOne.getComponents()) {
			boolean found = false;
			
			for (Component potentialMatch : thingTwo.getComponents()) {
				if (componentsEqual(currComponent,potentialMatch)) {
					found = true;
					break;
				}
			}
			
			if (!found) return false;
		}
		
		// check properties
		if (thingOne.getProperties().size() != thingOne.getProperties().size()) return false;
		for (Attribute currAttribute : thingOne.getProperties()) {
			boolean found = false;
			for (Attribute potentialMatch : thingTwo.getProperties()) {
				if (attributesEqual(currAttribute, potentialMatch)) {
					found = true;
					break;
				}
			}
			
			if (!found) return false;
		}
		
		return true;
	}
	
	public boolean attributesEqual(Attribute attributeOne, Attribute attributeTwo) {
		if (attributeOne.getName().compareTo(attributeTwo.getName()) == 0) return false;
		else if (attributeOne.getValue().compareTo(attributeTwo.getValue()) == 0) return false;
		
		return true;
	}
	
	public boolean connectionDetailsEqual(ConnectionDetails detailOne, ConnectionDetails detailTwo) {
		if (detailOne.getName().compareTo(detailTwo.getName()) != 0) return false;
		else if (detailOne.getProperties().size() != detailTwo.getProperties().size()) return false;
		
		// check for same attributes
		for (Attribute currAttribute : detailOne.getProperties()) {
			boolean found = false;
			
			for (Attribute potentialMatch : detailTwo.getProperties()) {
				if (currAttribute.getName().compareTo(potentialMatch.getName()) == 0 && currAttribute.getValue().compareTo(potentialMatch.getValue()) == 0) {
					found = true;
					break;
				}
			}
			
			if (!found) return false;
		}
		
		return true;
	}
	
	/**
	 * Compares two components
	 * @param componentOne
	 * @param componentTwo
	 * @return true if equal
	 */
	public boolean componentsEqual(Component componentOne, Component componentTwo) {
		if (componentOne.getId().compareTo(componentTwo.getId()) != 0) return false;
		else if (componentOne.getName().compareTo(componentTwo.getName()) != 0) return false;
		else if (!componentTypesEqual(componentOne.getType(), componentTwo.getType())) return false;
		
		// check for same capabilities
		if (componentOne.getCapabilities().size() != componentTwo.getCapabilities().size()) return false;
		for (Capability currCapability : componentOne.getCapabilities()) {
			boolean found = false;
			
			for (Capability potentialMatch : componentOne.getCapabilities()) {
				if (capabilitiesEqual(currCapability,potentialMatch)) {
					found = true;
					break;
				}
			}
			
			if (!found) return false;
		}
		
		return true;
	}
	
	public boolean componentTypesEqual(ComponentType componentTypeOne, ComponentType componentTypeTwo) {
		if (componentTypeOne.getId().compareTo(componentTypeTwo.getId()) != 0) return false;
		return true;
	}
	
	/**
	 * Compares two capabilities
	 * @param capabilityOne
	 * @param capabilityTwo
	 * @return true if equal
	 */
	public boolean capabilitiesEqual(Capability capabilityOne, Capability capabilityTwo) {
		if (capabilityOne.getId().compareTo(capabilityTwo.getId()) != 0) return false;
		
		// compare actions
		if (capabilityOne.getActions().size() != capabilityTwo.getActions().size()) return false; 
		for (Action currAction : capabilityOne.getActions()) {
			boolean found = false;
			
			for (Action potentialMatch : capabilityTwo.getActions()) {
				if (actionsEqual(currAction, potentialMatch)) found = true;
			}
			
			if (!found) return false;
		}
		
		// compare properties
		if (capabilityOne.getProperties().size() != capabilityTwo.getProperties().size()) return false;
		for (Property currProperty : capabilityOne.getProperties()) {
			boolean found = false;
			
			for (Property potentialMatch : capabilityTwo.getProperties()) {
				if (propertiesEqual(currProperty, potentialMatch)) found = true;
			}
			
			if (!found) return false;
		}
		
		// compare events
		if (capabilityOne.getEvents().size() != capabilityTwo.getEvents().size()) return false;
		for (Event currEvent : capabilityOne.getEvents()) {
			boolean found = false;
			
			for (Event potentialMatch : capabilityTwo.getEvents()) {
				if (eventsEqual(currEvent, potentialMatch)) found = true;
			}
			
			if (!found) return false;
		}

		return true;
	}
	
	public boolean eventsEqual(Event eventOne, Event eventTwo) {
		return eventOne == eventTwo;
	}
	
	/**
	 * Compares two actions
	 * @param actionOne
	 * @param actionTwo
	 * @return true if equal
	 */
	public boolean actionsEqual(Action actionOne, Action actionTwo) {
		if (actionOne.getName().compareTo(actionTwo.getName()) != 0) return false;
		else if (actionOne.getParameter().size() != actionTwo.getParameter().size()) return false;
		
		for (ActionParameter currParameter : actionOne.getParameter()) {
			boolean found = false;
			
			for (ActionParameter potentialMatch : actionTwo.getParameter()) {
				if (actionParametersEqual(currParameter, potentialMatch)) found = true;
			}
			
			if (!found) return false;
		}

		return true;
	}
	

	/**
	 * Compares two properties
	 * @param propertyOne
	 * @param propertyTwo
	 * @return true if equal
	 */
	public boolean propertiesEqual(Property propertyOne, Property propertyTwo) {
		if (propertyOne.getName().compareTo(propertyTwo.getName()) != 0) return false;
		else if (!unitsEqual(propertyOne.getUnit(), propertyTwo.getUnit())) return false; 
		else if (propertyOne.isWritable() != propertyTwo.isWritable()) return false;

		return true;
	}
	
	/**
	 * Compares two action parameters
	 * @param parameterOne
	 * @param parameterTwo
	 * @return true if equal
	 */
	public boolean actionParametersEqual(ActionParameter parameterOne, ActionParameter parameterTwo) {
		if (parameterOne.getName().compareTo(parameterTwo.getName()) != 0) return false;
		else if (!unitsEqual(parameterOne.getUnit(),parameterTwo.getUnit())) return false;
				
		return true;
	}
	
	/**
	 * Compares two units
	 * @param unitOne
	 * @param unitTwo
	 * @return true if equal
	 */
	public boolean unitsEqual(Unit unitOne, Unit unitTwo) {
		if (unitOne.getId().compareTo(unitTwo.getId()) != 0) return false;
		else if (unitOne.getSymbol().compareTo(unitTwo.getSymbol()) != 0) return false;
		else if (!unitOne.getValueType().equals(unitTwo.getValueType())) return false;
		
		return true;
	}
}
