package com.yetu.gateway.home.io.backend.kryo.internal.types;

import java.util.Calendar;

import com.yetu.gatewayprotocol.kryo.GatewayMessage;

/**
 * This class is a Decorator for a GatewayEvent, adding some metadata like Retry Counter, 
 * Timestamp of last sending and a result
 * 
 * This is a simple bean class, for simplification getters/setters are omitted, members 
 * are directly accessed.
 *
 */
public class GatewayMessageWrapper {

	/** timestamp of last sending */ 
	private long timestamp; 
	
	/** the actual event */ 
	private GatewayMessage message;
	
	/** message id */ 
	private String messageId;
	
	/** notify this listener when a message was finally sent or reached the maximum retry count */ 
	public TransportationEventListener messageSentCallback;
	
	public GatewayMessageWrapper(String messageID, GatewayMessage message) {
		this.timestamp = Calendar.getInstance().getTimeInMillis(); // no try until now 
		this.message = message; 
		this.messageId = messageID; 
	}
	
	public GatewayMessage getGatewayMessage() {
		return message;
	}
	
	public String getMessageId() {
		return messageId;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
}
