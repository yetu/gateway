package com.yetu.gateway.home.io.backend.kryo.internal;

import java.nio.channels.NotYetConnectedException;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor;
import com.yetu.gateway.home.core.backend.types.ConnectionStateListener;
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.io.backend.kryo.internal.converters.AbstractMessageKryoConverter;
import com.yetu.gateway.home.io.backend.kryo.internal.converters.KryoAbstractMessageConverter;
import com.yetu.gateway.home.io.backend.kryo.internal.transport.KryoTransportation;
import com.yetu.gateway.home.io.backend.kryo.internal.types.TransportationEvent;
import com.yetu.gateway.home.io.backend.kryo.internal.types.TransportationEventListener;
import com.yetu.gatewayprotocol.kryo.GatewayMessage;
import com.yetu.gatewayprotocol.kryo.ServerMessage;

/**
 * Handler using tcp backend transport to connect to remote machine.
 * 
 * @author Sebastian Garn
 *
 */

public class KryoCommunicationHandler implements RemoteCommunicationHandler {

	public static final Logger logger = LoggerFactory.getLogger(KryoCommunicationHandler.class);
	
	/** Contains all necessary parameters for setting up connection **/
	private ConnectionDescriptor connectionDescriptor;
	
	/** A list of listeners interested in connection state events */
	private ArrayList<ConnectionStateListener> connectionStateListeners;

	/** Indicates whether handler was configured or not */
	private boolean configured;
	
	private KryoTransportation transport;
	
	private LinkedBlockingQueue<ServerMessage> incomingMessageQueue;
	
	public KryoCommunicationHandler() {
		connectionStateListeners = new ArrayList<ConnectionStateListener>();
		incomingMessageQueue = new LinkedBlockingQueue<ServerMessage>();
		configured = false;
	}
	
	public void onActivate() {
		logger.debug("Kryo bundle activated");
	}
	
	public void onDeactivate() {
		logger.debug("Kryo bundle deactivated");
		shutdown();
	}

	@Override
	public synchronized boolean setup(ConnectionDescriptor connectionDescriptor) {
		if (configured) {
			logger.warn("Communication handler was already configured");
			logger.warn("Transport is: "+transport);
			return false;
		}
		
		transport = new KryoTransportation();
		transport.setup(connectionDescriptor);
		transport.addTransportationEventListener(new TransportationEventListener() {
			@Override
			public void onTransportationEvent(TransportationEvent event) {
				logger.debug("Received event: "+event.getType());
	
				switch (event.getType()) {
					case CONNECTION_DOWN: {
						onConnectionLost();
						break;
					}
					case CONNECTION_UP: {
						logger.debug("Received event CONNECTION UP");
						onConnectionEstablished();
						break;
					}
					case CONNECTION_ERROR: {
						logger.debug("Connection error occured");
	
						onConnectionLost();
						break;
					}
					case MESSAGE_RECEIVED: {
						onIncomingMessage(event.getServerMessage());
						break;
					}
					case MESSAGE_SENT: {
						logger.debug("Message sent");
						break;
					}
					default:
						break;
				}
			}
		});
		
		this.configured = true;
		this.connectionDescriptor = connectionDescriptor;
		
		return true;
	}
	
	@Override
	public boolean start() {
		logger.debug("Start called");
		
		if (!configured) {
			logger.error("Communication handler not configured");
			return false;
		}
		
		if (transport.isConnecting()) {
			logger.debug("Already connecting");
			return true;
		}
		
		logger.debug("Init connection");
		return transport.connect();
	}

	/**
	 * Called whenever the connection was initiated
	 */
	private void onConnectionEstablished() {
		logger.debug("Connection to "+connectionDescriptor.getIp()+" on port "+connectionDescriptor.getPort()+" established.");
		
		synchronized(connectionStateListeners) {
			for (ConnectionStateListener currListener : connectionStateListeners) {
				logger.debug("Inform: "+currListener);
				currListener.onConnectionEstablished();
			}
		}

	}
	
	/**
	 * Called whenever the connection was lostxxxx
	 */
	private void onConnectionLost() {
		logger.warn("No connection to "+connectionDescriptor.getIp()+" on port "+connectionDescriptor.getPort()+".");

		synchronized(connectionStateListeners) {
			for (ConnectionStateListener currListener : connectionStateListeners) {
				logger.debug("Inform: "+currListener);
				currListener.onConnectionLost();
			}
		}

		stop();
	}
	
	/**
	 * Put message to queue if possible
	 * 
	 * @param ServerMessage message
	 */
	private void onIncomingMessage(ServerMessage message) {		
		if (!incomingMessageQueue.offer(message)) {
			logger.debug("Failed to receive message.");	
		}
		else {
			logger.debug("Message enqueued");
		}
	}
	
	@Override
	public IncomingMessage receive() throws NotYetConnectedException, InterruptedException {
		if (transport == null || !transport.isConnected()) {
			throw new NotYetConnectedException();
		}
		
		ServerMessage message = incomingMessageQueue.take();
		IncomingMessage inMessage = KryoAbstractMessageConverter.getInstance().convert(message);

		logger.debug("Abstracted message: "+inMessage);
		return inMessage;
	}

	@Override
	public boolean send(OutgoingMessage message) {
		logger.debug("Passing message to transport: "+message);
		GatewayMessage gatewayMessage = AbstractMessageKryoConverter.getInstance().convert(message);

		if (transport == null || !transport.isConnected()) {
			logger.error("Unable to send message: "+gatewayMessage+". Gateway offline. Transport: "+transport);
			throw new NotYetConnectedException();
		}

		return transport.sendMessage(gatewayMessage);
	}

	@Override
	public void addConnectionStateListener(ConnectionStateListener listener) {
		synchronized(connectionStateListeners) {
			if (connectionStateListeners.contains(listener)) return;

			connectionStateListeners.add(listener);
		}
	}

	@Override
	public void removeConnectionStateListener(ConnectionStateListener listener) {
		synchronized(connectionStateListeners) {
			if (!connectionStateListeners.contains(listener)) return;
			
			connectionStateListeners.remove(listener);
		}
	}

	@Override
	public void stop() {
		if (transport != null) {
			logger.debug("Stopping kryo communication");
			transport.stop();
		}
	}
	
	@Override
	public void signalizeFaultyConnection() {
		logger.warn("Faulty connection reported. Informing listeners and stopping transport.");
		onConnectionLost();
	}

	@Override
	public void shutdown() {
		if (transport != null) {
			logger.debug("Shutdown kryo communication");
			transport.shutdown();
		}
	}
}
