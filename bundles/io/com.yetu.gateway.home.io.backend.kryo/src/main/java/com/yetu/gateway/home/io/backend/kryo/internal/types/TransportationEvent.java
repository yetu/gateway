package com.yetu.gateway.home.io.backend.kryo.internal.types;

import com.yetu.gatewayprotocol.kryo.GatewayMessage;
import com.yetu.gatewayprotocol.kryo.ServerMessage;

public class TransportationEvent {

	public static enum TransportationEventType {
		CONNECTION_UP,
		CONNECTION_DOWN,
		CONNECTION_ERROR,
		MESSAGE_RECEIVED,
		MESSAGE_SENT,
		MESSAGE_ERROR
	}
	
	private ServerMessage serverMessage = null; 
	private GatewayMessage gatewayMessage = null; 
	private TransportationEventType type = null; 
	
	public TransportationEvent(TransportationEventType type) {
		this.type = type; 
	}

	public TransportationEvent(TransportationEventType type, ServerMessage serverMessage) {
		this.serverMessage = serverMessage; 
		this.type = type; 
	}

	public TransportationEvent(TransportationEventType type, GatewayMessage gatewayMessage) {
		this.gatewayMessage = gatewayMessage; 
		this.type = type; 
	}

	public ServerMessage getServerMessage() {
		return serverMessage;
	}

	public GatewayMessage getGatewayMessage() {
		return gatewayMessage;
	}

	public TransportationEventType getType() {
		return type;
	}
	
}
