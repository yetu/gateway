package com.yetu.gateway.home.io.backend.kryo.internal.transport;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Slf4JLoggerFactory;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor;
import com.yetu.gateway.home.io.backend.kryo.internal.types.TransportationEvent;
import com.yetu.gateway.home.io.backend.kryo.internal.types.TransportationEvent.TransportationEventType;
import com.yetu.gateway.home.io.backend.kryo.internal.types.TransportationEventListener;
import com.yetu.gatewayprotocol.kryo.GatewayMessage;
import com.yetu.gatewayprotocol.kryo.ServerMessage;
import com.yetu.gatewayprotocol.kryo.YetuMessage;
import com.yetu.kryo_support.KryoSupport;

/**
 * This class is responsible for the actuall tcp communication. 
 * All netty-related code is here. 
 * 
 * @author andreas.breuer@itemis.de, sebastian.garn@yetu.com
 */
public class KryoTransportation {
	static {
		// register all gateway and server messages so kryo is able to parse them properly
		YetuMessage.registerMessages();
	}

	private static Logger logger = LoggerFactory.getLogger(KryoTransportation.class); 

	/**
	 * If this property is set (e.g. by passing -Ddebug.backendprotocol=true as an
	 * arg to the VM), an additional Handler is registered in the pipeline which 
	 * prints out the hexadecimal content of the received packets.  
	 */
	public static final String PROTOCOL_DEBUG_PROPERTY="debug.backendprotocol"; 

	/** the bootstrap used for connection */ 
	protected Bootstrap bootstrap; 

	/** Eventloop group, internally used by netty */ 
	private EventLoopGroup workerGroup; 

	/** the Event Channel */ 
	private Channel channel;

	private ConnectionDescriptor connectionDescriptor;

	/** Event Listeners for Transportation Events */ 
	protected List<TransportationEventListener> listeners = new CopyOnWriteArrayList<TransportationEventListener>();
	
	private boolean connecting = false;

	public KryoTransportation() {
		InternalLoggerFactory.setDefaultFactory(new Slf4JLoggerFactory());
	}

	public void setup(ConnectionDescriptor connectionDescriptor) {
		this.connectionDescriptor = connectionDescriptor; 
	}

	/**
	 * Connects with remote machine
	 */
	public synchronized boolean connect() {
		if (connecting) {
			logger.warn("Already connecting");
			return false;
		}
		
		connecting = true;
		
		logger.debug("Starting connection to yetu GatewayService");
		workerGroup = new NioEventLoopGroup();
		bootstrap = new Bootstrap();
		bootstrap.group(workerGroup);
		bootstrap.channel(NioSocketChannel.class);
		bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
		bootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000); // 10sec timeout
		bootstrap.handler(new ChannelInitializer<SocketChannel>() {
			@Override
			public void initChannel(SocketChannel ch) throws Exception {
				buildChannelPipeline(ch);
			}
		});
		
		logger.debug("Connecting to "+connectionDescriptor.getIp()+" on port "+connectionDescriptor.getPort()+"...");
		ChannelFuture f = bootstrap.connect(connectionDescriptor.getIp(), connectionDescriptor.getPort());
		f.addListener(new ChannelFutureListener() {
			public void operationComplete(ChannelFuture future) throws Exception {
				if (future.isSuccess() && future.isDone()) {
					channel = future.channel();
					connecting = false;
				} else {
					logger.debug("Operation complete: channel has invalid state");
					fireTransportationEvent(new TransportationEvent(TransportationEventType.CONNECTION_DOWN));
					connecting = false;
				}
			}
		});
		
		return true;
	}

	public void shutdown() {
		stop();
		listeners.clear();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void stop() {
		logger.info("Stopping yetu GatewayService connection");
		
		if (channel != null) {
			ChannelFuture future = channel.close();
			future.awaitUninterruptibly();
		}
		
		if (workerGroup != null) {
			Future f = workerGroup.shutdownGracefully();
			f.addListener(new GenericFutureListener() {
				@Override
				public void operationComplete(Future future) throws Exception {
					
				}
			}); 
		}
		
		connecting = false;
	}

	public void reset() {
		if (channel != null) {
			ChannelFuture future = channel.disconnect();
			future.awaitUninterruptibly();
		}

		channel = null; 
	}

	public boolean sendMessage(GatewayMessage message) {
		ChannelFuture handshakeFuture = null;
		
		// change the initial size if necessary, buffer size gets increased on demand
		byte[] serialized = KryoSupport.marshall(message);
		
		if (!isConnected()) {
			return false;
		}

		handshakeFuture = channel.writeAndFlush(Unpooled.copiedBuffer(serialized));

		try {
			handshakeFuture.sync();
		} catch (InterruptedException e) {
			logger.error("Interrupted while sending message", e);
			return false;
		}

		logger.debug(">> MESSAGE SENT: {}", message);
		return true;
	}

	public boolean isConnected () {
		return channel != null && channel.isOpen();   
	}

	/** 
	 * Build up the Channel Pipeline for Protocol processing
	 */
	protected ChannelPipeline buildChannelPipeline(Channel ch) {
		ChannelPipeline pipeline = ch.pipeline();
		
		// if Debug is required ... 
		if (System.getProperty(PROTOCOL_DEBUG_PROPERTY) != null) {
			pipeline.addLast("debug-handler", new LoggingHandler(KryoTransportation.class, LogLevel.DEBUG)); 
		}

		if (connectionDescriptor.isSsl()) { 
			SslHandler handler = generateSslHandler(connectionDescriptor.getKeystoreFilename(), connectionDescriptor.getKeystorePassword(), connectionDescriptor.getTruststoreFilename(), connectionDescriptor.getTruststorePassword());
			if (handler != null) {
				pipeline.addFirst(handler);
			}
		}

		// Inbound Messages 
		pipeline.addLast("length-decoder" , new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, -4, 4)); 
		pipeline.addLast ("message-decoder", new ChannelInboundHandlerAdapter() {
			@Override
			public void channelRead(ChannelHandlerContext ctx, Object msg) {
				ByteBuf byteMessage = (ByteBuf)msg;

				try {
					ServerMessage message = (ServerMessage)KryoSupport.unmarshall(byteMessage.nioBuffer());
					
					logger.debug("<< MESSAGE RECEIVED: {}", message); 
					fireTransportationEvent(new TransportationEvent(TransportationEventType.MESSAGE_RECEIVED, message));
				} catch(Exception e) {
					logger.debug("Failed to receive message",e);
				} finally {
					byteMessage.release();
				}
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void channelActive(ChannelHandlerContext ctx) throws Exception {
				super.channelActive(ctx);
				logger.warn("Channel created");
				fireTransportationEvent(new TransportationEvent(TransportationEventType.CONNECTION_UP));
			}
			
			/**
			 * {@inheritDoc}
			 */
			@Override
			public void channelInactive(ChannelHandlerContext ctx) throws Exception {
				super.channelInactive(ctx);
				logger.warn("Channel lost");
				logger.debug("Listener count is: "+listeners.size());
				fireTransportationEvent(new TransportationEvent(TransportationEventType.CONNECTION_DOWN));
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
				logger.error("Exception in communication on the EventChannel", cause);
				ctx.close();
				fireTransportationEvent(new TransportationEvent(TransportationEventType.CONNECTION_ERROR));
			}
		});
		// Outbound Messages 
		pipeline.addLast("length-encoder" , new LengthFieldPrepender(4, true)); 
		return pipeline; 
	}

	/**
	 * Create an SSL handler stage to add to the pipeline (must be the first stage added)
	 * @return The SSL handler, or null if there was an error creating it
	 */
	private SslHandler generateSslHandler(String keystoreFilename, String keystorePassword, String truststoreFilename, String truststorePassword) {
		try {
			logger.info("Attempting to enable SSL connection to the backend");

			// See the README for the stud-gateway-yetu Chef cookbook for instructions on how to generate .jks
			KeyStore ks = KeyStore.getInstance("JKS");
			KeyStore ts = KeyStore.getInstance("JKS");

			char[] ksPassphrase = keystorePassword.toCharArray();
			char[] tsPassphrase = truststorePassword.toCharArray();

			ks.load(new FileInputStream(keystoreFilename), ksPassphrase);
			ts.load(new FileInputStream(truststoreFilename), tsPassphrase);			

			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, ksPassphrase);

			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(ts);

			SSLContext sslc = SSLContext.getInstance("TLS");     
			sslc.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

			SSLEngine engine = sslc.createSSLEngine();
			engine.setEnableSessionCreation(true);
			engine.setUseClientMode(true);
			return new SslHandler(engine);
		} catch (Exception ex) {
			logger.error("SSL setup failed, disabling SSL", ex);
			return null;
		}
	}

	private void fireTransportationEvent(TransportationEvent event) {
		for (TransportationEventListener listener : listeners) {
			listener.onTransportationEvent(event);
		}
	}

	public void addTransportationEventListener(TransportationEventListener listener) {
		listeners.add(listener); 
	}

	public void removeTransportationEventListener(TransportationEventListener listener) {
		listeners.remove(listener);
	}
	
	public boolean isConnecting() {
		return connecting;
	}
}
