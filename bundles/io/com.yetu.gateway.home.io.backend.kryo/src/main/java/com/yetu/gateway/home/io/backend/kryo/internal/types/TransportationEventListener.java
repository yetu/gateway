package com.yetu.gateway.home.io.backend.kryo.internal.types;

/**
 * Listener Interface for receiving Notifications when the Backend<->Gateway 
 * connection is actually setup and the handshake was performed successfully 
 *
 * @author andreas.breuer@itemis.de
 */
public interface TransportationEventListener {

	public void onTransportationEvent (TransportationEvent event); 

}
