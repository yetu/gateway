package com.yetu.gateway.home.io.backend.kryo.internal.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HeartbeatRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest;
import com.yetu.gatewayprotocol.kryo.ServerMessage;

public class KryoAbstractMessageConverter {
	public static final Logger logger = LoggerFactory.getLogger(KryoAbstractMessageConverter.class);
	public static KryoAbstractMessageConverter instance;
	
	public static KryoAbstractMessageConverter getInstance() {
		if (instance == null) {
			instance = new KryoAbstractMessageConverter();
		}
		
		return instance;
	}
	
	/**
	 * Convenience method in case the specific message type is unknown
	 * @param ServerMessage message: message to convert
	 * @return Converted message or null if no translation was found 
	 */
	public IncomingMessage convert(ServerMessage message) {
		if (message instanceof ServerMessage.HandshakeReply) return convert((ServerMessage.HandshakeReply)message);
		else if (message instanceof ServerMessage.Heartbeat) return convert((ServerMessage.Heartbeat)message);
		else if (message instanceof ServerMessage.GatewayConfigurationRequest) return convert((ServerMessage.GatewayConfigurationRequest)message);
		else if (message instanceof ServerMessage.StartDiscoverySessionRequest) return convert((ServerMessage.StartDiscoverySessionRequest)message);
		else if (message instanceof ServerMessage.StopDiscoverySessionRequest) return convert((ServerMessage.StopDiscoverySessionRequest)message);
		else if (message instanceof ServerMessage.RemoveThingRequest) return convert((ServerMessage.RemoveThingRequest)message);
		else if (message instanceof ServerMessage.SetProperty) return convert((ServerMessage.SetProperty)message);
		else {
			logger.error("Unable to convert message due to unknown type: "+message);
		}
		return null;
	}
	
	public HandshakeReply convert(ServerMessage.HandshakeReply kryoMessage) {
		logger.debug("Converting kryo ServerMessage.HandshakeReply to internal HandshakeReply");
		
		HandshakeReply abstractMessage = new HandshakeReply(kryoMessage.getCorrelationId());
		return abstractMessage;
	}
	
	public HeartbeatRequest convert(ServerMessage.Heartbeat kryoMessage) {
		logger.debug("Converting kryo ServerMessage.Heartbeat to internal HeartbeatRequest");
		
		HeartbeatRequest abstractMessage = new HeartbeatRequest();
		return abstractMessage;
	}
	
	public ConfigurationRequest convert(ServerMessage.GatewayConfigurationRequest kryoMessage) {
		logger.debug("Converting kryo ServerMessage.GatewayConfigurationRequest to internal ConfigurationRequest");
		
		ConfigurationRequest abstractMessage = new ConfigurationRequest();
		return abstractMessage;
	}
	
	public StartDiscoverySessionRequest convert(ServerMessage.StartDiscoverySessionRequest kryoMessage) {
		logger.debug("Converting kryo ServerMessage.StartDiscoverySessionRequest to internal StartDiscoverySessionRequest");
		
		StartDiscoverySessionRequest abstractMessage = new StartDiscoverySessionRequest(kryoMessage.getRequestId(), kryoMessage.getSessionId());
		return abstractMessage;
	}
	
	public StopDiscoverySessionRequest convert(ServerMessage.StopDiscoverySessionRequest kryoMessage) {
		logger.debug("Converting kryo ServerMessage.StopDiscoverySessionRequest to internal StopDiscoverySessionRequest");
		
		StopDiscoverySessionRequest abstractMessage = new StopDiscoverySessionRequest(kryoMessage.getRequestId(), kryoMessage.getSessionId());
		return abstractMessage;
	}
	
	public RemoveThingRequest convert(ServerMessage.RemoveThingRequest kryoMessage) {
		logger.debug("Converting kryo ServerMessage.RemoveThingRequest to internal RemoveThingRequest");
		
		RemoveThingRequest abstractMessage = new RemoveThingRequest(kryoMessage.getRequestId(), kryoMessage.getThingId());
		return abstractMessage;
	}
	
	public SetProperty convert(ServerMessage.SetProperty kryoMessage) {
		logger.debug("Converting kryo ServerMessage.SetProperty to internal SetProperty");

		SetProperty abstractMessage = new SetProperty(kryoMessage.getThingId(), kryoMessage.getComponentId(), kryoMessage.getCapabilityId(), kryoMessage.getPropertyName(), kryoMessage.getValue());
		return abstractMessage;
	}
}
