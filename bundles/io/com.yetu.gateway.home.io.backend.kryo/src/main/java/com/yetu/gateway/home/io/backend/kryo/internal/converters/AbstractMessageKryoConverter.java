package com.yetu.gateway.home.io.backend.kryo.internal.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.abstraction.model.Attribute;
import com.yetu.abstraction.model.Capability;
import com.yetu.abstraction.model.Component;
import com.yetu.abstraction.model.ConnectionDetails;
import com.yetu.abstraction.model.Property;
import com.yetu.abstraction.model.Thing;
import com.yetu.abstraction.model.ThingStatus;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ConfigurationReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionExpired;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionFinished;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.DiscoverySessionStarted;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.PropertyChanged;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.RemoveThingReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.SetPropertyError;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StartDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.StopDiscoverySessionReply;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingAlreadyAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingDiscovered;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingNotAdded;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoveFailure;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingRemoved;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.ThingStatusChanged;
import com.yetu.gatewayprotocol.kryo.GatewayMessage;
import com.yetu.gatewayprotocol.kryo.GatewayMessage.ThingRemoveFailure.ErrorCode;
import com.yetu.gatewayprotocol.kryo.thing.CapabilityDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.ComponentDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.ConnectionDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.PropertyDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.ThingDescriptor;
import com.yetu.gatewayprotocol.kryo.thing.ThingDescriptor.Status;

public class AbstractMessageKryoConverter {
	public static final Logger logger = LoggerFactory.getLogger(AbstractMessageKryoConverter.class);
	public static AbstractMessageKryoConverter instance;
	
	public static AbstractMessageKryoConverter getInstance() {
		if (instance == null) {
			instance = new AbstractMessageKryoConverter();
		}
		
		return instance;
	}
	
	/**
	 * Convenience method in case the specific message type is unknown
	 * @param ServerMessage message: message to convert
	 * @return Converted message or null if no translation was found 
	 */
	public GatewayMessage convert(OutgoingMessage message) {
		if (message instanceof HandshakeRequest) return convert((HandshakeRequest)message);
		else if (message instanceof ConfigurationReply) return convert((ConfigurationReply)message);
		else if (message instanceof StartDiscoverySessionReply) return convert((StartDiscoverySessionReply)message);
		else if (message instanceof DiscoverySessionStarted) return convert((DiscoverySessionStarted)message);
		else if (message instanceof DiscoverySessionFinished) return convert((DiscoverySessionFinished)message);
		else if (message instanceof DiscoverySessionError) return convert((DiscoverySessionError)message);
		else if (message instanceof DiscoverySessionExpired) return convert((DiscoverySessionExpired)message);
		else if (message instanceof StopDiscoverySessionReply) return convert((StopDiscoverySessionReply)message);
		else if (message instanceof RemoveThingReply) return convert((RemoveThingReply)message);
		else if (message instanceof ThingRemoved) return convert((ThingRemoved)message);
		else if (message instanceof ThingRemoveFailure) return convert((ThingRemoveFailure)message);
		else if (message instanceof PropertyChanged) return convert((PropertyChanged)message);
		else if (message instanceof SetPropertyError) return convert((SetPropertyError)message);
		else if (message instanceof ThingAdded) return convert((ThingAdded)message);
		else if (message instanceof ThingAlreadyAdded) return convert((ThingAlreadyAdded)message);
		else if (message instanceof ThingNotAdded) return convert((ThingNotAdded)message);
		else if (message instanceof ThingDiscovered) return convert((ThingDiscovered)message);
		else if (message instanceof ThingStatusChanged) return convert((ThingStatusChanged)message);
		else {
			logger.error("Unable to convert message due to unknown type: "+message);
		}
		return null;
	}
	
	public GatewayMessage.HandshakeRequest convert(HandshakeRequest abstractMessage) {
		logger.debug("Converting internal HandshakeRequest to kryo HandshakeRequest");
		GatewayMessage.HandshakeRequest kryoMessage = new GatewayMessage.HandshakeRequest(abstractMessage.getRequestId(), abstractMessage.getGatewayId(), abstractMessage.getGatewayImageVersion());
		logger.debug(kryoMessage.getTimestamp()+"");
		logger.debug("Kryo is: {}",kryoMessage);
		return kryoMessage;
	}
	
	public GatewayMessage.GatewayConfigurationReply convert(ConfigurationReply abstractMessage) {
		logger.debug("Converting internal ConfigurationReply to kryo GatewayConfigurationReply");
		
		if (abstractMessage.getThings() == null) {
			return new GatewayMessage.GatewayConfigurationReply(null, abstractMessage.getRunningSessionId());
		}
		
		ThingDescriptor [] thingDescriptors = new ThingDescriptor[abstractMessage.getThings().size()];
		
		// convert things to thing descriptors
		int currThingIndex = 0;
		for (Thing currThing : abstractMessage.getThings()) {
			logger.debug("Converting thing to kryo thing descriptor: "+currThing);
			ThingDescriptor thingDescriptor = toThingDescriptor(currThing);
			thingDescriptors[currThingIndex] = thingDescriptor;
			
			currThingIndex++;
		}
		
		GatewayMessage.GatewayConfigurationReply kryoMessage = new GatewayMessage.GatewayConfigurationReply(thingDescriptors, abstractMessage.getRunningSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.StartDiscoverySessionReply convert(StartDiscoverySessionReply abstractMessage) {
		logger.debug("Converting internal StartDiscoverySessionReply to kryo StartDiscoverySessionReply");
		GatewayMessage.StartDiscoverySessionReply kryoMessage = new GatewayMessage.StartDiscoverySessionReply(abstractMessage.getCorrelationId(), abstractMessage.getSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.DiscoverySessionStarted convert(DiscoverySessionStarted abstractMessage) {
		logger.debug("Converting internal DiscoverySessionStarted to kryo DiscoverySessionStarted");
		GatewayMessage.DiscoverySessionStarted kryoMessage = new GatewayMessage.DiscoverySessionStarted(abstractMessage.getSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.DiscoverySessionStopped convert(DiscoverySessionFinished abstractMessage) {
		logger.debug("Converting internal DiscoverySessionFinished to kryo DiscoverySessionStopped");
		GatewayMessage.DiscoverySessionStopped kryoMessage = new GatewayMessage.DiscoverySessionStopped(abstractMessage.getSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.DiscoverySessionFailed convert(DiscoverySessionError abstractMessage) {
		logger.debug("Converting internal DiscoverySessionError to kryo DiscoverySessionFailed");
		GatewayMessage.DiscoverySessionFailed kryoMessage = new GatewayMessage.DiscoverySessionFailed(abstractMessage.getSessionId(), abstractMessage.getErrorCode()+"", abstractMessage.getErrorMessage());
		return kryoMessage;
	}
	
	public GatewayMessage.DiscoverySessionExpired convert(DiscoverySessionExpired abstractMessage) {
		logger.debug("Converting internal DiscoverySessionExpired to kryo DiscoverySessionExpired");
		GatewayMessage.DiscoverySessionExpired kryoMessage = new GatewayMessage.DiscoverySessionExpired(abstractMessage.getSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.StopDiscoverySessionReply convert(StopDiscoverySessionReply abstractMessage) {
		logger.debug("Converting internal StopDiscoverySessionReply to kryo StopDiscoverySessionReply");
		GatewayMessage.StopDiscoverySessionReply kryoMessage = new GatewayMessage.StopDiscoverySessionReply(abstractMessage.getCorrelationId(), abstractMessage.getSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.RemoveThingReply convert(RemoveThingReply abstractMessage) {
		logger.debug("Converting internal RemoveThingReply to kryo RemoveThingReply");
		GatewayMessage.RemoveThingReply kryoMessage = new GatewayMessage.RemoveThingReply(abstractMessage.getCorrelationId());
		return kryoMessage;
	}
	
	public GatewayMessage.ThingRemoved convert(ThingRemoved abstractMessage) {
		logger.debug("Converting internal ThingRemoved to kryo ThingRemoved");
		GatewayMessage.ThingRemoved kryoMessage = new GatewayMessage.ThingRemoved(abstractMessage.getThingId());
		return kryoMessage;
	}
	
	public GatewayMessage.ThingRemoveFailure convert(ThingRemoveFailure abstractMessage) {
		logger.debug("Converting internal ThingRemoveFailure to kryo ThingRemoveFailure");
		
		ErrorCode errorCode = null;
		
		switch(abstractMessage.getErrorCode()) {
			case THING_NOT_FOUND:
				errorCode = ErrorCode.THING_NOT_FOUND; 
				break;
		}
		
		GatewayMessage.ThingRemoveFailure kryoMessage = new GatewayMessage.ThingRemoveFailure(abstractMessage.getThingId(), errorCode, abstractMessage.getCause());
		return kryoMessage;
	}
	
	public GatewayMessage.PropertyChanged convert(PropertyChanged abstractMessage) {
		logger.debug("Converting internal PropertyChanged to kryo PropertyChanged");
		GatewayMessage.PropertyChanged kryoMessage = new GatewayMessage.PropertyChanged(
				abstractMessage.getThingId(),
				abstractMessage.getComponentId(),
				abstractMessage.getCapabilityId(),
				abstractMessage.getPropertyName(),
				abstractMessage.getTimeWhenChanged(),
				abstractMessage.getValue());
		return kryoMessage;
	}
	
	public GatewayMessage.SetPropertyError convert(SetPropertyError abstractMessage) {
		logger.debug("Converting internal SetPropertyError to kryo SetPropertyError");
		
		GatewayMessage.SetPropertyError.ErrorCode code = null;
		
		switch(abstractMessage.getErrorCode()) {
			case ERROR_NOT_FOUND:
				code = GatewayMessage.SetPropertyError.ErrorCode.ERROR_NOT_FOUND;
				break;
			case ERROR_THING_NOT_FOUND:
				code = GatewayMessage.SetPropertyError.ErrorCode.ERROR_THING_NOT_FOUND;
				break;
			case ERROR_COMPONENT_NOT_FOUND:
				code = GatewayMessage.SetPropertyError.ErrorCode.ERROR_COMPONENT_NOT_FOUND;
				break;
			case ERROR_CAPABILITIY_NOT_FOUND:
				code = GatewayMessage.SetPropertyError.ErrorCode.ERROR_CAPABILITIY_NOT_FOUND;
				break;
			case ERROR_PROPERTY_NOT_FOUND:
				code = GatewayMessage.SetPropertyError.ErrorCode.ERROR_PROPERTY_NOT_FOUND;
				break;
			case ERROR_VALUE_IS_NOT_VALID:
				code = GatewayMessage.SetPropertyError.ErrorCode.ERROR_VALUE_IS_NOT_VALID;
				break;
			case ERROR_UNKNOWN:
				code = GatewayMessage.SetPropertyError.ErrorCode.ERROR_UNKNOWN;
				break;
		}
		
		GatewayMessage.SetPropertyError kryoMessage = new GatewayMessage.SetPropertyError(
				abstractMessage.getThingId(),
				abstractMessage.getComponentId(),
				abstractMessage.getCapabilityId(),
				abstractMessage.getPropertyName(),
				abstractMessage.getValue(),
				code,
				abstractMessage.getCause());
		return kryoMessage;
	}

	public GatewayMessage.ThingAdded convert(ThingAdded abstractMessage) {
		logger.debug("Converting internal ThingAdded to kryo ThingAdded");
	
		ThingDescriptor thingDescriptor = toThingDescriptor(abstractMessage.getThing());
		GatewayMessage.ThingAdded kryoMessage = new GatewayMessage.ThingAdded(thingDescriptor, abstractMessage.getSessionId());

		logger.debug("Kryo message is {} ",kryoMessage);
		
		return kryoMessage;
	}
	
	public GatewayMessage.ThingAlreadyAddedError convert(ThingAlreadyAdded abstractMessage) {
		logger.debug("Converting internal ThingAlreadyAddedError to kryo ThingAlreadyAddedError");
		GatewayMessage.ThingAlreadyAddedError kryoMessage = new GatewayMessage.ThingAlreadyAddedError(abstractMessage.getThingId(), abstractMessage.getSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.ThingNotAddedError convert(ThingNotAdded abstractMessage) {
		logger.debug("Converting internal ThingNotAddedError to kryo ThingNotAddedError");
		GatewayMessage.ThingNotAddedError kryoMessage = new GatewayMessage.ThingNotAddedError(abstractMessage.getCode(), abstractMessage.getCause());
		return kryoMessage;
	}
	
	public GatewayMessage.ThingDiscovered convert(ThingDiscovered abstractMessage) {
		logger.debug("Converting internal ThingDiscovered to kryo ThingDiscovered");
		GatewayMessage.ThingDiscovered kryoMessage = new GatewayMessage.ThingDiscovered(abstractMessage.getThingId(), abstractMessage.getSessionId());
		return kryoMessage;
	}
	
	public GatewayMessage.ThingStatusChanged convert(ThingStatusChanged abstractMessage) {
		logger.debug("Converting internal ThingStatusChanged to kryo ThingStatusChanged");
		Status status = convertThingStatus(abstractMessage.getStatus());

		GatewayMessage.ThingStatusChanged kryoMessage = new GatewayMessage.ThingStatusChanged(abstractMessage.getThingId(), status);
		
		return kryoMessage;
	}
	
	// helpers to convert a thing into a thing descriptor
	
	private ThingDescriptor toThingDescriptor(Thing thing) {
		// transform components into component descriptors
		ComponentDescriptor[] componentDescriptors = null;
		
		if (thing.getComponents() != null) {
			componentDescriptors = new ComponentDescriptor[thing.getComponents().size()];
			
			int currComponentIndex = 0;
			for (Component currComponent : thing.getComponents()) {
				componentDescriptors[currComponentIndex] = toComponentDescriptor(currComponent);			
				currComponentIndex++;
			}
		}
		
		// transform properties
		com.yetu.gatewayprotocol.kryo.thing.Attribute[] properties = null;
		
		if (thing.getProperties() != null) {
			properties = new com.yetu.gatewayprotocol.kryo.thing.Attribute[thing.getProperties().size()];

			int currElem = 0;
			for (Attribute currProperty : thing.getProperties()) {
				properties[currElem] = new com.yetu.gatewayprotocol.kryo.thing.Attribute(currProperty.getName(), currProperty.getValue());
				currElem++;
			}
		}
		
		// transform connection details
		ConnectionDescriptor[] connectionDescriptors = null;
		
		if (thing.getConnectionDetails() != null) {
			connectionDescriptors = new ConnectionDescriptor[thing.getConnectionDetails().size()];

			int currConnectionDetailIndex = 0;
			for (ConnectionDetails currConnectionDetail : thing.getConnectionDetails()) {
				connectionDescriptors[currConnectionDetailIndex] = toConnectionDescriptor(currConnectionDetail);
				logger.debug("Converted to kryo connection descriptor: "+connectionDescriptors[currConnectionDetailIndex]);
				currConnectionDetailIndex++;
			}
		}

		ComponentDescriptor mainComponent = null;
		
		if (thing.getMainComponent() != null) {
			mainComponent = toComponentDescriptor(thing.getMainComponent());
		}
		
		String displayTypeId = null;
		if (thing.getDisplayType() != null){
			displayTypeId = thing.getDisplayType().getId();
		}
		
		Status status = convertThingStatus(thing.getStatus());
		
		return new ThingDescriptor(thing.getId(), thing.getName(), thing.getManufacturer(), displayTypeId, mainComponent, properties, connectionDescriptors, componentDescriptors, status);
	}
	
	private ComponentDescriptor toComponentDescriptor(Component component) {
		CapabilityDescriptor[] capabilityDescriptors = null;
		
		if (component.getCapabilities() != null) {
			capabilityDescriptors = new CapabilityDescriptor[component.getCapabilities().size()];
			 
			int currCapIndex = 0;
			for (Capability currCapability : component.getCapabilities()) {
				capabilityDescriptors[currCapIndex] = toCapabilityDescriptor(currCapability);
				currCapIndex++;
			}
		}

		ComponentDescriptor descriptor = new ComponentDescriptor(component.getId(), component.getName(), component.getType().getId(), capabilityDescriptors);
		
		return descriptor;
	}
	
	private CapabilityDescriptor toCapabilityDescriptor(Capability capability) {
		PropertyDescriptor [] propertyDescriptors = null;
		
		
		if (capability.getProperties() != null) {
			propertyDescriptors = new PropertyDescriptor[capability.getProperties().size()];
			
			int currPropertyIndex = 0;
			for (Property currProperty : capability.getProperties()) {
				propertyDescriptors[currPropertyIndex] = toPropertyDescriptor(currProperty);
				currPropertyIndex++;
			}
		}
		
		// TODO also parse actions and events
		
		return new CapabilityDescriptor(capability.getId(), propertyDescriptors);
	}
	
	private PropertyDescriptor toPropertyDescriptor(Property property) {
		String value = null;
		if (property.getValue() != null) {
			value = property.getValue().toString();
		}
		
		return new PropertyDescriptor(property.getName(), property.getUnit().getId(), value, property.isWritable());
	}
	
	private ConnectionDescriptor toConnectionDescriptor(ConnectionDetails connectionDetails) {
		com.yetu.gatewayprotocol.kryo.thing.Attribute[] attributes = null;

		if (connectionDetails.getProperties() != null) {
			attributes = new com.yetu.gatewayprotocol.kryo.thing.Attribute[connectionDetails.getProperties().size()];
			
			int currElem = 0;
			for (Attribute currAttribute : connectionDetails.getProperties()) {
				attributes[currElem] = new com.yetu.gatewayprotocol.kryo.thing.Attribute(currAttribute.getName(), currAttribute.getValue());
				currElem++;
			}
		}

		return new ConnectionDescriptor(connectionDetails.getName(), attributes);
	}
	
	private Status convertThingStatus(ThingStatus thingStatus) {
		Status status;
		
		// convert internal status to kryo status
		switch (thingStatus) {
			case ONLINE:
				status = ThingDescriptor.Status.AVAILABLE;
				break;
			case OFFLINE:
				status = ThingDescriptor.Status.UNAVAILABLE;
				break;
			case UNKNOWN:
				status = ThingDescriptor.Status.UNKNOWN;
				break;
			default:
				logger.error("Unknown thing status passed. Using default status.");
				status = ThingDescriptor.Status.UNKNOWN;
		}
		
		return status;
	}
}
