package com.yetu.gateway.home.io.zwave.mock.internal;

import java.io.IOException;

import com.domoone.zwave.io.ZWaveConnection;
import com.domoone.zwave.simulation.ZWaveMockedConnection;
import com.domoone.zwave.simulation.ZWaveStreamNotificationRule;
import com.yetu.gateway.home.io.zwave.ZWaveConnectionFactory;
import com.yetu.gateway.home.io.zwave.mock.device.DeviceSimulator;
public class ZWaveMockedConnectionFactoryImpl implements ZWaveConnectionFactory{

	DeviceSimulator deviceSimulator;
	
	public ZWaveMockedConnectionFactoryImpl() {
	
	}
	
	@Override
	public ZWaveConnection createSerialConnection(String portname) throws IOException {
		return deviceSimulator.getMockedConnection();
	}
	
	
	public void setDeviceSimulator(DeviceSimulator deviceSimulator){
		this.deviceSimulator = deviceSimulator;
	}
	
	
	public void unsetDeviceSimulator(DeviceSimulator deviceSimulator){
		this.deviceSimulator = null;
	}
	
	
}
