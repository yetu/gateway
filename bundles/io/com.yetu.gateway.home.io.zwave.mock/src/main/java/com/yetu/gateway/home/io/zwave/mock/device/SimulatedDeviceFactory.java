package com.yetu.gateway.home.io.zwave.mock.device;

import com.domoone.zwave.simulation.ZWaveMockedConnection;
import com.yetu.gateway.home.io.zwave.mock.internal.device.DlinkWallPlug;

public class SimulatedDeviceFactory {

	public final static String DEV_DLINK_WALLPLUG = "DLINK_WP";
	
	
	private ZWaveMockedConnection connection;
	private int currNodeId = 2;
	
	public SimulatedDeviceFactory(ZWaveMockedConnection connection){
		this.connection = connection;
	}
	
	public AbstractMockedZWaveDevice createDevice(String deviceType){
		
		if ("DLINK_WP".equalsIgnoreCase(deviceType)){
			return new DlinkWallPlug(getNodeId(), connection);
		}
		
		
		return null;
	}
	
	
	protected int getNodeId(){
		return currNodeId++;
	}
}
