package com.yetu.gateway.home.io.zwave.mock.internal.device;

import com.domoone.zwave.simulation.ZWaveMockedConnection;
import com.domoone.zwave.simulation.ZWaveStreamNotificationListener;
import com.domoone.zwave.simulation.ZWaveStreamNotificationRule;
import com.domoone.zwave.simulation.ZWaveStreamReplyRule;
import com.yetu.gateway.home.io.zwave.mock.device.AbstractMockedZWaveDevice;

public class DlinkWallPlug extends AbstractMockedZWaveDevice {

	private boolean property_on    = false;
	private int		property_power = 0;
	
	ZWaveStreamNotificationListener streamNotificationListener;
	
	public DlinkWallPlug(int id, ZWaveMockedConnection connection) {
		super(id, connection);		
	}

	@Override
	public void setProperty(String property, String value) throws Exception {
		if ("on".equalsIgnoreCase(property)){
			setOn("true".equalsIgnoreCase(value));
		}
		if ("power".equalsIgnoreCase(property)){
			setPower(Integer.parseInt(value));
		}
		
	}

	@Override
	public String getProperty(String property) throws Exception {
		if ("on".equalsIgnoreCase(property)){
			return ""+isOn();
		}
		if ("power".equalsIgnoreCase(property)){
			return ""+getPower();
		}
		return null;
	}

	@Override
	public String getNodeInformation() {
		
		// 04 BASIC_TYPE_ROUTING_SLAVE
		// 10 GENERIC_TYPE_SWITCH_BINARY	
		// 01 SPECIFIC_TYPE_POWER_SWITCH_BINARY
		// cmd_class 1
		// ...
		// cmd_class n
		
		return "04 10 01 5e 86 72 98 5a 85 59 73 25 20 27 32 70 71 75 7a";
	}

	@Override
	public void addReplyRules() {  					
		ZWaveStreamReplyRule rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+" 02 72 04 00 ## 00 ##");  // manufacture specific 		
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 08 72 05 01 08 00 01 00 11 #LRC");			
		getConnection().addStreamRule(rule);		
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+" 02 86 11 00 ## 00 ##");  // CMD_CLASS_VERSION GET  - 
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 0b 86 12 03 03 53 01 0a 01 00 01 0a #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 5e 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x5e (COMMAND_CLASS_ZWAVEPLUS_INFO)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 5e 02 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 86 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x86 (COMMAND_CLASS_VERSION)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 86 02 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 72 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x86 (COMMAND_CLASS_VERSION)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 72 02 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 98 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x98 (COMMAND_CLASS_SECURITY)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 98 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 5a 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x5a (COMMAND_CLASS_DEVICE_RESET_LOCALLY)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 5a 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 85 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x85 (COMMAND_CLASS_ASSOCIATION)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 85 02 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 59 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x59 (COMMAND_CLASS_ASSOCIATION_GRP_INFO)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 59 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 25 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x25 (COMMAND_CLASS_SWITCH_BINARY)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 25 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 73 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x73 (COMMAND_CLASS_POWERLEVEL)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 73 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 20 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x20 (COMMAND_CLASS_BASIC)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 20 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 27 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x27 (COMMAND_CLASS_SWITCH_ALL)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 27 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 70 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x70 (COMMAND_CLASS_METER)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 70 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 32 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x32 (COMMAND_CLASS_CONFIGURATION)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 32 03 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 71 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x71 (COMMAND_CLASS_ALARM)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 71 01 #LRC");			
		getConnection().addStreamRule(rule);
		
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 75 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x75 (COMMAND_CLASS_PROTECTION)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 75 02 #LRC");			
		getConnection().addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 0a 00 13 "+getByteStr(getNodeId())+"86 13 7a 00 ## 00 ##");  // CMD_CLASS_VERSION GET - 0x7a (COMMAND_CLASS_FIRMWARE_UPDATE_MD)
		rule.addReaction("01 0e 00 04 00 "+getByteStr(getNodeId())+" 04 86 14 7a 02 #LRC");			
		getConnection().addStreamRule(rule);
		
		
		
		/*
2015-32-25 10:06:51 | libzwave 0.2.67 | DEBUG | Sending Data: 01 0c 00 13 03 04 85 01 01 01 00 03 00 60 | CMD_ZWAVE_SEND_DATA
2015-32-25 10:06:52 | libzwave 0.2.67 | DEBUG |       ... Received ACK
2015-32-25 10:06:52 | libzwave 0.2.67 | DEBUG |       ... Received 01 04 01 13 01 e8 | CMD_ZWAVE_SEND_DATA
2015-32-25 10:06:52 | libzwave 0.2.67 | DEBUG | send ACK
2015-32-25 10:06:52 | libzwave 0.2.67 | DEBUG |       ... Received 01 05 00 13 03 00 ea | CMD_ZWAVE_SEND_DATA
2015-32-25 10:06:52 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:02 | libzwave 0.2.67 | DEBUG |       ... Received 01 14 00 04 00 03 0e 32 02 21 34 00 00 00 00 00 00 00 00 00 00 c7 | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:02 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:02 | libzwave 0.2.67 | DEBUG |       ... Received 01 09 00 04 00 03 03 25 03 00 d4 | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:02 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:05 | libzwave 0.2.67 | DEBUG |       ... Received 01 14 00 04 00 03 0e 32 02 21 34 00 00 00 02 00 00 00 00 00 00 c5 | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:05 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:05 | libzwave 0.2.67 | DEBUG |       ... Received 01 09 00 04 00 03 03 25 03 ff 2b | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:05 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:07 | libzwave 0.2.67 | DEBUG |       ... Received 01 14 00 04 00 03 0e 32 02 21 34 00 00 00 00 00 00 00 00 00 00 c7 | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:07 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:07 | libzwave 0.2.67 | DEBUG |       ... Received 01 09 00 04 00 03 03 25 03 00 d4 | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:07 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:11 | libzwave 0.2.67 | DEBUG |       ... Received 01 14 00 04 00 03 0e 32 02 21 34 00 00 00 00 00 00 00 00 00 00 c7 | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:11 | libzwave 0.2.67 | DEBUG | send ACK
2015-33-25 10:06:11 | libzwave 0.2.67 | DEBUG |       ... Received 01 09 00 04 00 03 03 25 03 ff 2b | CMD_APPL_COMMAND_HANDLER
2015-33-25 10:06:11 | libzwave 0.2.67 | DEBUG | send ACK
		 */
		
		
		// 01 0a 00 13 02 02 25 02 00 02 00 c3
	}

	@Override
	public void addNotificationRules() {
		streamNotificationListener = new ZWaveStreamNotificationListener() {
			
			@Override
			public void onStreamTransmitted(String message) {
				handleStreamNotification(message);
				
			}
		};
		// 01 0b 00 13 06 03 25 01 ff 00 06 00 3f
		
		ZWaveStreamNotificationRule nrule = new ZWaveStreamNotificationRule("01 0a 00 13 "+getByteStr(getNodeId())+" 02 25 02 ## ## ## ##",streamNotificationListener);  // CMD_SERIAL_GET_INIT_DATA
		getConnection().addStreamRule(nrule);
		//01 0b 00 13 02 03 25 01 ff 00 02 00 3f
		nrule = new ZWaveStreamNotificationRule("01 0b 00 13 "+getByteStr(getNodeId())+" 03 25 01 ## ## ## ## ##",streamNotificationListener);  // CMD_SERIAL_GET_INIT_DATA
		getConnection().addStreamRule(nrule);
	}
	
	protected void handleStreamNotification(String message){
		if (message.startsWith("01 0a 00 13 "+getByteStr(getNodeId())+" 02 25 02")){
			sendOnValue();
		}
		if (message.startsWith("01 0b 00 13 "+getByteStr(getNodeId())+" 03 25 01")){
			// setting on and off value
			int onValue = getByteAt(8, message);
			this.property_on = (onValue == 0xff);
		}
	}

	@Override
	public void onStreamTransmitted(String message) {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void handleCommand(String command) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
	private boolean isOn(){
		return property_on;
	}
	
	private void setOn(boolean value){
		if (property_on == value){
			return;
		}
		property_on = value;
		sendOnValue();
		sendMeterValue();
	}
	
	private void sendMeterValue(){
		//
	}
	
	private void sendOnValue(){
		String strOn;
		if (isOn()){
			strOn = "ff";
		} else {
			strOn="00";
		}
		getConnection().send("01 09 00 04 00 "+getByteStr(getNodeId())+" 03 25 03 "+strOn+" #LRC");
	}
	
	private int getPower(){
		return property_power;
	}
	
	private void setPower(int power){
		if (power == property_power){
			return;
		}
		property_power = power;
	}

}
