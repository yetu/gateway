package com.yetu.gateway.home.io.zwave.mock.device;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.ZWaveControllerMode;
import com.domoone.zwave.simulation.ZWaveMockedConnection;
import com.domoone.zwave.simulation.ZWaveStreamNotificationListener;
import com.domoone.zwave.simulation.ZWaveStreamNotificationRule;
import com.domoone.zwave.simulation.ZWaveStreamReplyRule;
import com.yetu.gateway.home.core.testing.TestEndpoint;
import com.yetu.gateway.home.core.testing.TestEndpointRegistry;
import com.yetu.gateway.home.core.testing.TestEndpoint.Method;


public class DeviceSimulator {
	private static Logger logger = LoggerFactory.getLogger(DeviceSimulator.class);
	
	
	protected static final String ENDPOINT_ID_DEVICE_SIMULATOR = "com.yetu.gateway.home.io.zwave.mock.simulator";
	protected static final String ENDPOINT_ID_NODE = "com.yetu.gateway.home.io.zwave.mock.nodes";
	
	protected static final String PARAMETER_CONTROLLER_MODE = "mode";
	
	protected static final String PARAMETER_NODE_ID = "node";
	protected static final String PARAMETER_PROPERTY = "property";
	protected static final String PARAMETER_VALUE = "value";
	protected static final String PARAMTER_DEVICE_TYPE = "devicetype";
	
	protected ZWaveMockedConnection connection = null;
	protected SimulatedDeviceFactory deviceFactory = null;
	protected ZWaveStreamNotificationListener streamNotificationListener;
	private List<AbstractMockedZWaveDevice> mockedDevices = null;
	private TestEndpointRegistry testEndpointRegistry;
	private ZWaveControllerMode currControlerMode = ZWaveControllerMode.CNTRL_MODE_NOT_CONNECTED;
	
	public DeviceSimulator(){		
		mockedDevices = new ArrayList<>();
		deviceFactory = new SimulatedDeviceFactory(getMockedConnection());
	}
	
	public synchronized ZWaveMockedConnection getMockedConnection(){
		if (connection == null) {
			connection = new ZWaveMockedConnection();
			prepareSerialConnection();
			setControllerMode(ZWaveControllerMode.CNTRL_MODE_NORMAL);
		}		
		return connection;
	}

	
	public void setTestEndpointRegistry(TestEndpointRegistry testEndpointRegistry){
		this.testEndpointRegistry = testEndpointRegistry;
		generateTestEndpoints();
	}
	
	public void unsetTestEndpointRegistry(TestEndpointRegistry testEndpointRegistry){
		this.testEndpointRegistry = null;
	}
	
	private void generateTestEndpoints(){
		
		logger.debug("Generating Endpoints");
		
		TestEndpoint addDevice = new TestEndpoint(ENDPOINT_ID_DEVICE_SIMULATOR, "createDevice", Method.UPDATE, new String[]{PARAMTER_DEVICE_TYPE}, "creates a new device. To add this device, set Controller to learn mode and use method pressButton") {
			@Override
			public String handle(Map<String, String[]> parameters) throws Exception {
			  return addDevice(parameters);
			}
		};
		
		TestEndpoint setNodeProperty = new TestEndpoint(ENDPOINT_ID_NODE, "setProperty", Method.UPDATE, new String[]{"node","property","value"}, "Sets the property of the simulated device with given value") {
			@Override
			public String handle(Map<String, String[]> parameters) throws Exception {
			  return setProperty(parameters);
			}
		};
		
		TestEndpoint getNodeProperty = new TestEndpoint(ENDPOINT_ID_NODE, "getProperty", Method.UPDATE, new String[]{"node","property"}, "Get the current value of a property") {
			@Override
			public String handle(Map<String, String[]> parameters) throws Exception {
				return (getProperty(parameters));
			}
		};
		
		TestEndpoint wakeup = new TestEndpoint(ENDPOINT_ID_NODE, "wakeup", Method.UPDATE, new String[]{"node"}, "let the node wake up") {
			@Override
			public String handle(Map<String, String[]> parameters) throws Exception {
				return wakeup(parameters);
			}
		};
		
		TestEndpoint pressButton = new TestEndpoint(ENDPOINT_ID_NODE, "pressButton", Method.UPDATE, new String[]{PARAMETER_NODE_ID}, "let the node sending it's NIF") {
			@Override
			public String handle(Map<String, String[]> parameters) throws Exception {
				return pressButton(parameters);
			}
		};
		
		testEndpointRegistry.addTestEndpoint(addDevice);
		testEndpointRegistry.addTestEndpoint(wakeup);
		testEndpointRegistry.addTestEndpoint(setNodeProperty);
		testEndpointRegistry.addTestEndpoint(getNodeProperty);
		testEndpointRegistry.addTestEndpoint(pressButton);
	}


	
	private int getNodeId(Map<String, String[]> parameters) throws Exception{	
		return Integer.parseInt( getParameter(parameters, PARAMETER_NODE_ID));	
	}
	
	private String getParameter(Map<String, String[]> parameters, String parameter){
		String[] params = parameters.get(parameter);
		if (params == null){
			// TODO: throw exception
		}
		if (params.length == 1){
			return params[0];
		}
		// TODO throw exception
		return "";
	}
	
	
	
	private String setProperty(Map<String, String[]> parameters) throws Exception{
		return setProperty(getNodeId(parameters),  getParameter(parameters, PARAMETER_PROPERTY), getParameter(parameters, PARAMETER_VALUE));
	}
	
	private String addDevice(Map<String,String[]> parameters) throws Exception{
		AbstractMockedZWaveDevice dev = deviceFactory.createDevice(getParameter(parameters, PARAMTER_DEVICE_TYPE));
		this.mockedDevices.add(dev);
		if (dev != null){
			return "nodeId="+dev.getNodeId();
		}
		return "ERROR";
	}
	
	private String setProperty(int nodeId, String property, String value) throws Exception{
		logger.debug("Setting property {} of node {} to {}", property, nodeId, value);
		AbstractMockedZWaveDevice dev = this.getMockedDevice(nodeId);
		if (dev == null){
			// TODO: throw exception
		}
		dev.setProperty(property, value);
		return "";
	}
	
	private String getProperty(Map<String, String[]> parameters) throws Exception{
		return getProperty(getNodeId(parameters), getParameter(parameters,PARAMETER_PROPERTY));
	}
	
	private String getProperty(int nodeId, String property) throws Exception{
		AbstractMockedZWaveDevice dev = this.getMockedDevice(nodeId);
		if (dev == null){
			// TODO: throw exception
		}		
		return dev.getProperty(property);
	}
	
	private String wakeup(Map<String, String[]> parameters) throws Exception{
		return wakeup(getNodeId(parameters));
	}
	
	private String wakeup(int nodeId) throws Exception{
		logger.debug("sending wakeup to node {}",nodeId);
		AbstractMockedZWaveDevice dev = this.getMockedDevice(nodeId);
		if (dev == null){
			// TODO: throw exception
		}
		
		return "";
	}
	
	private String pressButton(Map<String, String[]> parameters) throws Exception{
		return pressButton(getNodeId(parameters));
	}
	
	private String pressButton(int nodeId){
		logger.debug("sending pressButton to node {}",nodeId);
		AbstractMockedZWaveDevice dev = this.getMockedDevice(nodeId);
		if (dev == null){
			// TODO: throw exception
		}
		simulateLearnButtonPress(dev);
		return "";
	}
	
	
	
	protected String generateInitDataStream(){
		
		// each bit within the message content represents the existence of a node id -> if bit 0, 2 and 5 are set, then there exist nodes with the ids 1, 3 and 6. 
		// this method generates the correct init stream for the controller depending on existing mocked devices dynamically
		// example for no existing node: "01 25 01 02 05 00 1d 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 03 01 #LRC";
		
		// because the maximum amount of node ids is 235, we need a 235 bit number wich is represented by 29 bytes. 
		// thus, we have to generate 29 bytes. 
		byte[] bytes = new byte[29];  
		int currentByte = 0;	// 
		
		// tmp represents the current byte where the bits will be set. 
	    byte tmp = 0;		
		tmp |= 1 << 0; // there exists always the controller with id = 1, thus, bit on position 0 will be set to 1
		
		// for all possible nodes do
		for (int i = 1; i< 235; i++){
			// if all 8 bits of a byte are set
			if (i % 8 == 0){
				// set the byte
				bytes[currentByte] = tmp;
				// increase the current Byte
				currentByte++;
				// set the new byte to 0
				tmp = 0;
			}
			// if there exists no device with id i
			if (null == getMockedDevice(i+1)){ // remember: nodeIds start with 1, bits start with 0
				// set the corresponding bit in the byte to zero
			   tmp |= 0 << (i % 8);
			} else {
				// set the corresponding bit in the byte to one
			   tmp |= 1 << (i % 8);
			}
		}
		
		// generating the complete init stream
		String retStr = "01 25 01 02 05 00 1d";
		for (int i= 0; i< 29; i++){
			retStr = retStr +" "+AbstractMockedZWaveDevice.getByteStr(bytes[i]);
		}
		retStr = retStr + " 03 01 #LRC";
		logger.debug("generated initDataStream {}",retStr);
		return retStr;
		
		
	}


	protected void handleStreamNotification(String message){
		if ("01 03 00 02 fe".equalsIgnoreCase(message)){
			connection.send(generateInitDataStream());
		}
		
		if (message.startsWith("01 05 00 4b 01")){ // CMD_ZWAVE_REMOVE_NODE_FROM_NETWORK - start
			setControllerMode(ZWaveControllerMode.CNTRL_MODE_EXCLUSION);
		}
		
		if (message.startsWith("01 05 00 4b 05")){ // CMD_ZWAVE_REMOVE_NODE_FROM_NETWORK - cancel
			setControllerMode(ZWaveControllerMode.CNTRL_MODE_NORMAL);
		}
		
		if (message.startsWith("01 05 00 4a 81")){ // CMD_ZWAVE_ADD_NODE_TO_NETWORK - start
			setControllerMode(ZWaveControllerMode.CNTRL_MODE_INCLUSION);
		}
		
		if (message.startsWith("01 05 00 4a 05")){ // CMD_ZWAVE_ADD_NODE_TO_NETWORK - cancel
			setControllerMode(ZWaveControllerMode.CNTRL_MODE_NORMAL);
		}
		
		
		
	}

	protected void setControllerMode(ZWaveControllerMode controllerMode){
		if (controllerMode == this.currControlerMode){
			return;
		}
		logger.debug("ControllerMode has changed from {} to {}",currControlerMode,controllerMode);
		this.currControlerMode = controllerMode;
	}

	protected void prepareSerialConnection(){
		
		// adding notification rules to mock
		streamNotificationListener = new ZWaveStreamNotificationListener() {
				
			@Override
			public void onStreamTransmitted(String message) {
				handleStreamNotification(message);
				
			}
		};
		
		
		ZWaveStreamNotificationRule nrule = new ZWaveStreamNotificationRule("01 03 00 02 fe",streamNotificationListener);  // CMD_SERIAL_GET_INIT_DATA
		connection.addStreamRule(nrule);
		
		
		nrule = new ZWaveStreamNotificationRule("01 05 00 4b 01 ## ##",streamNotificationListener);  // CMD_ZWAVE_REMOVE_NODE_FROM_NETWORK -  start
		connection.addStreamRule(nrule);
		
		nrule = new ZWaveStreamNotificationRule("01 05 00 4b 05 ## ##",streamNotificationListener);  // CMD_ZWAVE_REMOVE_NODE_FROM_NETWORK  - cancel
		connection.addStreamRule(nrule);
		
		nrule = new ZWaveStreamNotificationRule("01 05 00 4a 81 ## ##",streamNotificationListener);  // CMD_ZWAVE_ADD_NODE_TO_NETWORK  - start
		connection.addStreamRule(nrule);
		
		nrule = new ZWaveStreamNotificationRule("01 05 00 4a 05 ## ##",streamNotificationListener);   // CMD_ZWAVE_ADD_NODE_TO_NETWORK  - cancel
		connection.addStreamRule(nrule);
		
		
		
		
		// added reply rules to serial Mock
		ZWaveStreamReplyRule rule = new ZWaveStreamReplyRule("01 03 00 15 e9");  // CMD_ZWAVE_GET_VERSION
		rule.addReaction("01 10 01 15 5a 2d 57 61 76 65 20 33 2e 34 32 00 01 93");			
		connection.addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 03 00 07 fb");  // CMD_SERIAL_GET_CAPABILITIES
		rule.addReaction("01 2b 01 07 04 02 01 15 00 02 00 03 fe 00 16 80 0c 00 00 00 e3 97 7d 80 07 00 00 80 00 00 00 00 00 00 00 00 00 00 02 00 00 80 07 00 2e");			
		connection.addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 03 00 20 dc");  // CMD_MEMORY_GET_ID (HomeId)
		rule.addReaction("01 08 01 20 d2 7f ff 78 01 fd");			
		connection.addStreamRule(rule);		
		
		rule = new ZWaveStreamReplyRule("01 04 00 41 01 bb");  // CMD_ZWAVE_GET_NODE_PROTOCOL_INFO
		rule.addReaction("01 09 01 41 93 16 00 02 02 01 32");			
		connection.addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 04 01 42 02 ##");   // CMD_ZWAVE_SET_DEFAULT
		rule.addReaction("01 04 00 42 02 #LRC");
		connection.addStreamRule(rule);
		
		// ---------- ControllerModes ----------- 
		
		rule = new ZWaveStreamReplyRule("01 05 00 4b 01 %funcId_remove_start ##");  // CMD_ZWAVE_REMOVE_NODE_FROM_NETWORK -  start
		rule.addReaction("01 07 00 4b %funcId_remove_start 01 00 00 #LRC");			
		connection.addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 05 00 4b 05 %funcId_remove_cancel ##");  // CMD_ZWAVE_REMOVE_NODE_FROM_NETWORK  - cancel
		rule.addReaction("01 07 00 4b %funcId_remove_cancel 06 00 00 #LRC");			
		connection.addStreamRule(rule);
			
		rule = new ZWaveStreamReplyRule("01 05 00 4a 81 %funcId_add_start ##");  // CMD_ZWAVE_ADD_NODE_TO_NETWORK  - start
		rule.addReaction("01 07 00 4a %funcId_add_start 01 00 00 #LRC");			
		connection.addStreamRule(rule);
		
		rule = new ZWaveStreamReplyRule("01 05 00 4a 05 %funcId_add_cancel ##");  // CMD_ZWAVE_ADD_NODE_TO_NETWORK  - cancel
		rule.addReaction("01 07 00 4a %funcId_add_cancel 06 00 00 #LRC");			
		connection.addStreamRule(rule);
		
	}
	
	protected AbstractMockedZWaveDevice getMockedDevice(int id){
		for (AbstractMockedZWaveDevice dev : mockedDevices){
			if (dev.getNodeId() == id){
				return dev;
			}
		}
		return null;	
	}
	
	
	protected void simulateLearnButtonPress(AbstractMockedZWaveDevice device){
		
		logger.debug("simulating button press on device {}",device.getClass().getName() + " "+device.getNodeId());
		
		String niStr = " 10 "+device.getNodeInformation();  // MR: I'm really not sure for what this parameter stands : 10 -> its always the same
		switch (currControlerMode){
			case CNTRL_MODE_INCLUSION:
				connection.send("01 17 00 4a %funcId_add_start 03 "+device.getNodeIdStr()+niStr+" #LRC");
				connection.send("01 07 00 4a %funcId_add_start 05 "+device.getNodeIdStr()+" 00 #LRC");		
				return;
			case CNTRL_MODE_EXCLUSION:
				connection.send("01 17 00 4b %funcId_remove_start 03 "+device.getNodeIdStr()+niStr+" #LRC");
				connection.send("01 17 00 4b %funcId_remove_start 06 "+device.getNodeIdStr()+niStr+" #LRC");
				return;
			default:
				connection.send("01 16 00 49 84 "+device.getNodeIdStr()+niStr+" #LRC");				
				
		}
		
	}
	
}
