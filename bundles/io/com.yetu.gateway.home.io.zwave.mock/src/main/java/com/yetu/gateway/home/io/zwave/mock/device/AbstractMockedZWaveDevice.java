package com.yetu.gateway.home.io.zwave.mock.device;

import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.cmd.ZWaveNodeCmdFactory;
import com.domoone.zwave.simulation.ZWaveMockedConnection;
import com.domoone.zwave.simulation.ZWaveStreamNotificationListener;


public abstract class AbstractMockedZWaveDevice implements ZWaveStreamNotificationListener{

	Logger logger = LoggerFactory.getLogger(AbstractMockedZWaveDevice.class);
	
	ZWaveMockedConnection connection;
	int nodeId;
	ZWaveNodeCmdFactory nodeCmdFactory = null;
	
	public AbstractMockedZWaveDevice(int id, ZWaveMockedConnection connection){
		logger.debug("generating node with id {}",id);
		this.connection = connection;
		this.nodeId = id;
		addReplyRules();
		addNotificationRules();
	}
	
	public void setNodeCmdFactory(ZWaveNodeCmdFactory nodeCmdFactory){
		this.nodeCmdFactory = nodeCmdFactory;
	}
	
	public String getNodeIdStr(){
		String idStr = ""+Integer.toHexString(nodeId);
		if (idStr.length()<2){
			idStr = "0"+idStr;
		}
		return idStr;
	}
	
	
	protected ZWaveMockedConnection getConnection(){
		return connection;
	}
	
	public int getNodeId(){
		return nodeId;
	}
	
	public static String getByteStr(int value){
		String str = ""+Integer.toHexString(value);
		if (str.length()<2){
			str = "0"+str;
		}
		return str;
	}
	
	public static int getByteAt(int index, String message){
		StringTokenizer tok = new StringTokenizer(message," ");
		if (tok.countTokens() < index){
			return -1;
		}
		int i = 0;
		while (i<index){
			tok.nextToken();
			i++;
		}
		return Integer.parseInt(tok.nextToken(),16);
	}
	
	abstract public void setProperty(String property, String value) throws Exception;
	abstract public String getProperty(String property) throws Exception;
	abstract public String getNodeInformation(); 
	abstract public void addReplyRules();
	abstract public void addNotificationRules();
	abstract public void onStreamTransmitted(String message);
	abstract public void handleCommand(String command) throws Exception;
}