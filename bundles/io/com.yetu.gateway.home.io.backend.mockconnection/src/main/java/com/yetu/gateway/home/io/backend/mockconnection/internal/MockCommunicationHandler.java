package com.yetu.gateway.home.io.backend.mockconnection.internal;

import java.nio.channels.NotYetConnectedException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.core.backend.types.ConnectionDescriptor;
import com.yetu.gateway.home.core.backend.types.ConnectionStateListener;
import com.yetu.gateway.home.core.backend.types.RemoteCommunicationHandler;
import com.yetu.gateway.home.core.messaging.types.messages.IncomingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.OutgoingMessage;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ConfigurationRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HandshakeReply;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.HeartbeatRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.RemoveThingRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.ResetRequest.Entity;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.SetProperty;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StartDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.incoming.StopDiscoverySessionRequest;
import com.yetu.gateway.home.core.messaging.types.messages.outgoing.HandshakeRequest;
import com.yetu.gateway.home.core.testing.TestEndpoint;
import com.yetu.gateway.home.core.testing.TestEndpointRegistry;

/**
 * @author Sebastian Garn
 *
 */

public class MockCommunicationHandler implements RemoteCommunicationHandler  {

	public static final Logger logger = LoggerFactory.getLogger(MockCommunicationHandler.class);
	
	/** A list of listeners interested in connection state events */
	private ArrayList<ConnectionStateListener> connectionStateListeners;
	
	LinkedBlockingQueue<IncomingMessage> incomingMessageQueue;
	
	private boolean autoHandshake = true;
	
	public static String BUNDLE_NAME = "com.yetu.gateway.home.io.backend.mockconnection";
	
	private ArrayList<TestEndpoint> testEndpoints;
	
	public MockCommunicationHandler() {
		connectionStateListeners = new ArrayList<ConnectionStateListener>();
		incomingMessageQueue = new LinkedBlockingQueue<IncomingMessage>();

		testEndpoints = new ArrayList<TestEndpoint>();
		testEndpoints.add(stopConnection);
		testEndpoints.add(receiveHandshakeReply);
		testEndpoints.add(receiveConfigurationRequest);
		testEndpoints.add(receiveStartDiscoverySessionRequest);
		testEndpoints.add(receiveStopDiscoverySessionRequest);
		testEndpoints.add(receiveSetProperty);
		testEndpoints.add(receiveHeartbeatRequest);
		testEndpoints.add(receiveRemoveThingRequest);
		testEndpoints.add(setAutoHandhake);
		testEndpoints.add(getAutoHandhake);
		testEndpoints.add(receiveResetRequest);
	}
	
	public void onActivate() {
		logger.debug("Mock bundle activated");
	}
	
	public void onDeactivate() {
		logger.debug("Mock bundle deactivated");
		stop();
	}

	@Override
	public boolean setup(ConnectionDescriptor connectionDescriptor) {
		return true;
	}
	
	@Override
	public synchronized boolean start() {
		onConnectionEstablished();

		return true;
	}

	/**
	 * Called whenever the connection was initiated
	 */
	private void onConnectionEstablished() {
		logger.debug("Mock connection established");
		
		synchronized(connectionStateListeners) {
			for (ConnectionStateListener currListener : connectionStateListeners) {
				logger.debug("Inform: "+currListener);
				currListener.onConnectionEstablished();
			}
		}
	}
	
	/**
	 * Called whenever the connection was lost
	 */
	private void onConnectionLost() {
		logger.warn("Mock connection lost");

		synchronized(connectionStateListeners) {
			for (ConnectionStateListener currListener : connectionStateListeners) {
				logger.debug("Inform: "+currListener);
				currListener.onConnectionLost();
			}
		}

		stop();
	}
	
	/**
	 * Put message to queue if possible
	 * 
	 * @param ServerMessage message
	 */
	private void onIncomingMessage(IncomingMessage message) {		
		if (!incomingMessageQueue.offer(message)) {
			logger.debug("Failed to receive message.");	
		}
		else {
			logger.debug("Message enqueued");
		}
	}
	
	@Override
	public IncomingMessage receive() throws NotYetConnectedException, InterruptedException {
		IncomingMessage message = incomingMessageQueue.take();

		logger.debug("Abstracted message: "+message);
		return message;
	}

	@Override
	public boolean send(OutgoingMessage message) {
		logger.debug("Sending message via mock connection: "+message);
		
		// some default behaviour
		if (autoHandshake) {
			if (message instanceof HandshakeRequest) {
				HandshakeRequest request = (HandshakeRequest)message;
				onIncomingMessage(new HandshakeReply(request.getRequestId()));
			}
		}
	
		return true;
	}

	@Override
	public void addConnectionStateListener(ConnectionStateListener listener) {
		synchronized(connectionStateListeners) {
			if (connectionStateListeners.contains(listener)) return;

			connectionStateListeners.add(listener);
		}
	}

	@Override
	public void removeConnectionStateListener(ConnectionStateListener listener) {
		synchronized(connectionStateListeners) {
			if (!connectionStateListeners.contains(listener)) return;
			
			connectionStateListeners.remove(listener);
		}
	}

	@Override
	public void stop() {
		
	}

	@Override
	public void signalizeFaultyConnection() {
		logger.warn("Faulty connection reported. Informing listeners and stopping transport.");
		onConnectionLost();
	}
	

	public void addTestEndpointRegistry(TestEndpointRegistry registry) {
		for (TestEndpoint currEndpoint : testEndpoints) {
			registry.addTestEndpoint(currEndpoint);
		}
	}
	
	public void removeTestEndpointRegistry(TestEndpointRegistry registry) {
		for (TestEndpoint currEndpoint : testEndpoints) {
			registry.removeTestEndpoint(currEndpoint);
		}
	}
	
	
	/**
	 * TestEndpoint declarations
	 */
	
	TestEndpoint stopConnection = new TestEndpoint(BUNDLE_NAME, "stopConnection", TestEndpoint.Method.UPDATE, new String[0], "Stops the mock connection") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			logger.debug("Stopping mock connection");
			onConnectionLost();
			return "";
		}
	};
	
	TestEndpoint receiveHandshakeReply = new TestEndpoint(BUNDLE_NAME, "receiveHandshakeReply", TestEndpoint.Method.CREATE, new String[]{"correlationid"}, "Simulates a handshake reply coming from backend.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			onIncomingMessage(new HandshakeReply(parameters.get("correlationId")[0]));
			return "";
		}
	};

	TestEndpoint receiveConfigurationRequest = new TestEndpoint(BUNDLE_NAME, "receiveConfigurationRequest", TestEndpoint.Method.CREATE, new String[0], "Simulates a handshake reply coming from backend.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			onIncomingMessage(new ConfigurationRequest());
			return "";
		}
	};
			
	TestEndpoint setAutoHandhake = new TestEndpoint(BUNDLE_NAME, "setAutoHandshake", TestEndpoint.Method.UPDATE, new String[]{"enabled"}, "Enables/disabled autohandshake. Enabled by default.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			String enabled = parameters.get("enabled")[0];
			autoHandshake = Boolean.parseBoolean(enabled);
			return "";
		}
	};
			
	TestEndpoint getAutoHandhake = new TestEndpoint(BUNDLE_NAME, "getAutoHandshake", TestEndpoint.Method.GET, new String[0], "Retrieve the autohandshake state.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			return autoHandshake+"";
		}
	};
	
	TestEndpoint receiveSetProperty = new TestEndpoint(BUNDLE_NAME, "receiveSetProperty", TestEndpoint.Method.CREATE, new String[]{"thingid","componentid","capabilityid","propertyname","value"}, "Simulates a SetProperty message coming from backend") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			onIncomingMessage(
					new SetProperty(
							parameters.get("thingid")[0],
							parameters.get("componentid")[0],
							parameters.get("capabilityid")[0],
							parameters.get("propertyname")[0],
							parameters.get("value")[0]
					));
			return "";
		}
	};
	
	TestEndpoint receiveHeartbeatRequest = new TestEndpoint(BUNDLE_NAME, "receiveHeartbeatRequest", TestEndpoint.Method.CREATE, new String[0], "Simulates a heartbeat request coming from backend.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			onIncomingMessage(new HeartbeatRequest());
			return "";
		}
	};
	
	TestEndpoint receiveRemoveThingRequest = new TestEndpoint(BUNDLE_NAME, "receiveRemoveThingRequest", TestEndpoint.Method.CREATE, new String[]{"requestid","thingid"}, "Simulates a remove thing request coming from backend.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			onIncomingMessage(new RemoveThingRequest(parameters.get("requestid")[0], parameters.get("thingid")[0]));
			return "";
		}
	};
	
	TestEndpoint receiveStartDiscoverySessionRequest = new TestEndpoint(BUNDLE_NAME, "receiveStartDiscoverySessionRequest", TestEndpoint.Method.CREATE, new String[]{"requestid","sessionid"}, "Simulates a start discovery session request coming from backend.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			onIncomingMessage(new StartDiscoverySessionRequest(parameters.get("requestid")[0], parameters.get("sessionid")[0]));
			return "";
		}
	};
	
	TestEndpoint receiveStopDiscoverySessionRequest = new TestEndpoint(BUNDLE_NAME, "receiveStopDiscoverySessionRequest", TestEndpoint.Method.CREATE, new String[]{"requestid","sessionid"}, "Simulates a stop discovery session request coming from backend.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			onIncomingMessage(new StopDiscoverySessionRequest(parameters.get("requestid")[0], parameters.get("sessionid")[0]));
			return "";
		}
	};
	
	TestEndpoint receiveResetRequest = new TestEndpoint(BUNDLE_NAME, "receiveResetRequest", TestEndpoint.Method.CREATE, new String[]{"requestid","entities"}, "Simulates a reset request coming from backend.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			String[] strEntities = parameters.get("entities");
			Entity[] entities = new Entity[strEntities.length];
			
			for (int i = 0; i < strEntities.length; i++) {
				entities[i] = Entity.valueOf(strEntities[i]);
			}
			
			onIncomingMessage(new ResetRequest(parameters.get("requestid")[0], entities));
			return "";
		}
	};

	@Override
	public void shutdown() {
		
	}
}
