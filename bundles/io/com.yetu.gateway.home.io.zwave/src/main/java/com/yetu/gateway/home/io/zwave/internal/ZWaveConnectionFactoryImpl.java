package com.yetu.gateway.home.io.zwave.internal;

import java.io.IOException;

import com.domoone.zwave.io.ZWaveConnection;
import com.yetu.gateway.home.io.zwave.ZWaveConnectionFactory;
import com.yetu.gateway.home.io.zwave.ZWaveSerialConnection;

public class ZWaveConnectionFactoryImpl implements ZWaveConnectionFactory{

	

	@Override
	public ZWaveConnection createSerialConnection(String portname) throws IOException {
		ZWaveSerialConnection connection = new ZWaveSerialConnection(portname);
		connection.connect();
		return connection;
	}
	
}
