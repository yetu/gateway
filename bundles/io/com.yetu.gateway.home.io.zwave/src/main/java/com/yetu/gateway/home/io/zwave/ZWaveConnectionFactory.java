package com.yetu.gateway.home.io.zwave;

import java.io.IOException;

import com.domoone.zwave.io.ZWaveConnection;

public interface ZWaveConnectionFactory {
		
	public ZWaveConnection createSerialConnection(String portname) throws IOException;
	
}
