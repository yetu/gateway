package com.yetu.gateway.home.io.zwave;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.domoone.zwave.io.ZWaveConnection;


public class ZWaveSerialConnection implements ZWaveConnection {
	
	SerialPort serialPort = null;
	String portName = null;
	
	public ZWaveSerialConnection(String portName){
		this.portName = portName;		
	}
	
	public String getPortName(){
		return portName;
	}
	
	public void connect() throws IOException{
		
		try {
			CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
			if (portIdentifier.isCurrentlyOwned()) {
				throw new IOException("The port " + portName + " is currenty used by "
						+ portIdentifier.getCurrentOwner());
			}
			CommPort port = portIdentifier.open(this.getClass().getName(), 2000);
			if (!(port instanceof SerialPort)) {
				throw new IOException(portName + " is not a serial port");
			}
			serialPort = (SerialPort) port;
			serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		} catch (NoSuchPortException exc) {
			throw new IOException("There exists no serial port with the name "+portName,exc);
		} catch (PortInUseException exc) {
			throw new IOException("The serial port with the name "+portName+" is currently in use and can't be opened",exc);
		} catch (Exception exc){
			throw new IOException("Unable to open serial port "+portName,exc);
		}
	}
	
	@Override
	public InputStream getInputStream(){
		try {
			if (serialPort != null){
				return serialPort.getInputStream();
			}
		} catch (IOException exc){
			
		}
		return null;
	}
	
	@Override
	public OutputStream getOutputStream(){
		try {
			if (serialPort != null){
				return serialPort.getOutputStream();
			}
		} catch (IOException exc){
			
		}
		return null;
	}
	

	public void disconnect() {
		if (serialPort != null) {
			serialPort.close();
		}
	}
}
