package com.yetu.gateway.home.binding.mockdriver;

import org.eclipse.smarthome.core.thing.ThingTypeUID;

public class MockBindingConstants {
	public static final String BINDING_ID = "mockdriver";
	
	public static final String CONFIG_PROPERTY_DEVICE_ID = "deviceId";
	
	public final static ThingTypeUID THING_TYPE_MOCK_LAMP = new ThingTypeUID(BINDING_ID, "lamp");
	
	/** available test endpoints **/
	
	public static final String ENDPOINT_ID_MOCK_DISCOVERY = "com.yetu.gateway.home.binding.mockdriver.discoveryservice";
	
	public static final String TESTENDPOINT_DISCOVER_DEVICE = "discoverDevice";
	public static final String TESTENDPOINT_DISCOVER_DEVICE_PARAM_DEVICEID = "deviceid";
	
	public static final String TESTENDPOINT_IS_SCANNING = "isScanning";
	
	public static final String TESTENDPOINT_ABORT_SCANNING = "abortScanning";
	
	public static final String ENDPOINT_ID_MOCK_DEVICES = "com.yetu.gateway.home.binding.mockdriver.devices";
	
	public static final String TESTENDPOINT_TRIGGER_PROPERTY_CHANGE = "triggerPropertyChange";
	public static final String TESTENDPOINT_TRIGGER_PROPERTY_CHANGE_PARAM_THINGUID = "thinguid";
	public static final String TESTENDPOINT_TRIGGER_PROPERTY_CHANGE_PARAM_PROPERTY = "property";
	public static final String TESTENDPOINT_TRIGGER_PROPERTY_CHANGE_PARAM_VALUE = "value";
	
	public static final String TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE = "triggerThingStatusChange";
	public static final String TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_THINGUID = "thinguid";
	public static final String TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_NEW_STATUS = "newstatus";
	
	public static final String TESTENDPOINT_GET_THINGUIDS = "getThingUIDs";
}
