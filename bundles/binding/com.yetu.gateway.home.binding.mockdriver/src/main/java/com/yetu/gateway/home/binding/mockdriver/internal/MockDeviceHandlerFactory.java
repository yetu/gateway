package com.yetu.gateway.home.binding.mockdriver.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.yetu.gateway.home.binding.mockdriver.MockBindingConstants;
import com.yetu.gateway.home.core.testing.TestEndpoint;
import com.yetu.gateway.home.core.testing.TestEndpoint.Method;
import com.yetu.gateway.home.core.testing.TestEndpointRegistry;

public class MockDeviceHandlerFactory extends BaseThingHandlerFactory {
	public static final Logger logger = LoggerFactory.getLogger(MockDeviceHandlerFactory.class);

	public final static Collection<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = Lists.newArrayList(MockBindingConstants.THING_TYPE_MOCK_LAMP);
	
	private TestEndpointRegistry testEndpointRegistry;
	private ArrayList<TestEndpoint> testEndpoints;
	private HashMap<String, Thing> things;
		
	public MockDeviceHandlerFactory() {
		testEndpoints = new ArrayList<TestEndpoint>();
		testEndpoints.add(getThingUIDs);
		testEndpoints.add(triggerPropertyChange);
		testEndpoints.add(triggerThingStatusChange);
		
		things = new HashMap<String, Thing>();
	}

	@Override
	public boolean supportsThingType(ThingTypeUID thingTypeUID) {
		logger.debug("Do you support: "+thingTypeUID);
	    return SUPPORTED_THING_TYPES_UIDS.contains(thingTypeUID);
	}

	@Override
	protected ThingHandler createHandler(Thing thing) {
		logger.debug("Create handler: "+thing);
		
		// save thing for test endpoint
		things.put(thing.getUID().getAsString(), thing);
		
		ThingTypeUID thingTypeUID = thing.getThingTypeUID();
		if (thingTypeUID.equals(MockBindingConstants.THING_TYPE_MOCK_LAMP)) {
			return new MockDeviceHandler(thing);
		}
		return null;
	}

	public void addTestEndpointRegistry(TestEndpointRegistry registry) {
		testEndpointRegistry = registry;
		
		for (TestEndpoint currEndpoint : testEndpoints) {
			testEndpointRegistry.addTestEndpoint(currEndpoint);
		}
	}
	
	public void removeTestEndpointRegistry(TestEndpointRegistry registry) {
		testEndpointRegistry = null;
	}

	/**
	 * Test endpoints
	 */

	TestEndpoint triggerPropertyChange = new TestEndpoint(
			MockBindingConstants.ENDPOINT_ID_MOCK_DEVICES,
			MockBindingConstants.TESTENDPOINT_TRIGGER_PROPERTY_CHANGE,
			Method.UPDATE,
			new String[]{ MockBindingConstants.TESTENDPOINT_TRIGGER_PROPERTY_CHANGE_PARAM_THINGUID,MockBindingConstants.TESTENDPOINT_TRIGGER_PROPERTY_CHANGE_PARAM_PROPERTY,MockBindingConstants.TESTENDPOINT_TRIGGER_PROPERTY_CHANGE_PARAM_VALUE},
			"Allows to trigger a property change event") {

		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			String thingUID = parameters.get("thinguid")[0];
			String property = parameters.get("property")[0];
			String propertyValue = parameters.get("value")[0];
			
			if (!things.containsKey(thingUID)) {
				throw new Exception("Thing not found");
			}
			
			Thing thing = things.get(thingUID);
			if (!(thing.getHandler() instanceof MockDeviceHandler)) {
				throw new Exception("ThingHandler "+thing.getHandler()+" of thing with uid  "+thingUID+" is invalid");
			}

			MockDeviceHandler mockHandler = (MockDeviceHandler)thing.getHandler();
			boolean success = mockHandler.triggerPropertyChange(property, propertyValue);
			
			if (!success) {
				throw new Exception("Failed to execute request.");
			}

			return "";			
		}
	};
	
	TestEndpoint triggerThingStatusChange = new TestEndpoint(
			MockBindingConstants.ENDPOINT_ID_MOCK_DEVICES,
			MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE,
			Method.UPDATE,
			new String[]{ MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_THINGUID,MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_NEW_STATUS},
			"Allows to trigger a thing status change event. The status can be e.g. ONLINE, OFFLINE, UNINITIALIZED") {

		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			String thingUID = parameters.get(MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_THINGUID)[0];
			String newThingStatus = parameters.get(MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_NEW_STATUS)[0];
			
			if (!things.containsKey(thingUID)) {
				throw new Exception("Thing not found: "+thingUID);
			}
			
			Thing thing = things.get(thingUID);
			if (!(thing.getHandler() instanceof MockDeviceHandler)) {
				throw new Exception("ThingHandler "+thing.getHandler()+" of thing with uid  "+thingUID+" is invalid");
			}
			
			ThingStatus newStatus;
			try {
				newStatus = ThingStatus.valueOf(newThingStatus);	
			} catch(Exception e) {
				throw new Exception("Unsupported thing status. "+newThingStatus);
			}

			MockDeviceHandler mockHandler = (MockDeviceHandler)thing.getHandler();
			boolean success = mockHandler.triggerStatusChange(newStatus);
			
			if (!success) {
				throw new Exception("Failed to execute request.");
			}

			return "";			
		}
	};

	TestEndpoint getThingUIDs = new TestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DEVICES, MockBindingConstants.TESTENDPOINT_GET_THINGUIDS, Method.GET, new String[0], "Gives back all device ids which have a handler") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			String result = "";

			for (String currUid : things.keySet()) {
				result += currUid+"\n";
			}

			return result;
		}
	};
}
