package com.yetu.gateway.home.binding.mockdriver.internal;

import java.util.ArrayList;
import java.util.Map;

import org.eclipse.smarthome.config.discovery.AbstractDiscoveryService;
import org.eclipse.smarthome.config.discovery.DiscoveryResult;
import org.eclipse.smarthome.config.discovery.DiscoveryResultBuilder;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;
import com.yetu.gateway.home.binding.mockdriver.MockBindingConstants;
import com.yetu.gateway.home.binding.mockdriver.internal.devicetypes.MockLamp;
import com.yetu.gateway.home.core.testing.TestEndpoint;
import com.yetu.gateway.home.core.testing.TestEndpoint.Method;
import com.yetu.gateway.home.core.testing.TestEndpointRegistry;

public class MockDeviceDiscoveryService extends AbstractDiscoveryService {
	
	public static final Logger logger = LoggerFactory.getLogger(MockDeviceDiscoveryService.class);
	
	private TestEndpointRegistry testEndpointRegistry;
	private ArrayList<TestEndpoint> testEndpoints;
	
	private boolean scanning = false;

	public MockDeviceDiscoveryService() {
		// add supported thing typs
		super(Sets.newHashSet(MockBindingConstants.THING_TYPE_MOCK_LAMP), 60);

		testEndpoints = new ArrayList<TestEndpoint>();
		testEndpoints.add(discoverDevice);
		testEndpoints.add(stopScan);
	}

	@Override
	protected void startScan() {
		logger.debug("Start scan called");
		scanning = true;
	}

	private void addDevice(String deviceId) {
		logger.debug("AddDevice called");
		
		MockLamp lamp = new MockLamp(deviceId);
		
		// generate a thing id from its unique device id and thing type id 
		ThingUID thingUID = new ThingUID(MockBindingConstants.THING_TYPE_MOCK_LAMP, lamp.getUniqueId());
        String label = "DummyLabelOf"+deviceId;

        DiscoveryResult discoveryResult = DiscoveryResultBuilder.create(thingUID).withLabel(label).withProperty(MockBindingConstants.CONFIG_PROPERTY_DEVICE_ID, lamp.getUniqueId()).build();
		
        thingDiscovered(discoveryResult);
	}
	
	@Override
	public synchronized void abortScan() {
		logger.debug("Scan stopped");

		super.abortScan();
		scanning = false;
	}

	public void addTestEndpointRegistry(TestEndpointRegistry registry) {
		testEndpointRegistry = registry;
		
		for (TestEndpoint currEndpoint : testEndpoints) {
			testEndpointRegistry.addTestEndpoint(currEndpoint);
		}
	}
	
	public void removeTestEndpointRegistry(TestEndpointRegistry registry) {
		testEndpointRegistry = null;
	}
	
	/**
	 * Declaration of test endpoints
	 */
	TestEndpoint discoverDevice = new TestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DISCOVERY, MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE, Method.CREATE, new String[]{MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE_PARAM_DEVICEID}, "Lets the mock driver discover a device with given device id.") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			addDevice(parameters.get(MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE_PARAM_DEVICEID)[0]);
			logger.debug("Discover a device");
			return "";
		}
	};
	
	TestEndpoint isScanning = new TestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DISCOVERY, MockBindingConstants.TESTENDPOINT_IS_SCANNING, Method.GET, new String[0], "Checks whether this binding is scanning for devices") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			logger.debug("Is scanning called");
			return scanning+"";
		}
	};
	
	TestEndpoint stopScan = new TestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DISCOVERY, MockBindingConstants.TESTENDPOINT_ABORT_SCANNING, Method.UPDATE, new String[0], "Allows to abort a running scan") {
		@Override
		public String handle(Map<String, String[]> parameters) throws Exception {
			logger.debug("Abort scan called");
			stopScan();
			return "";
		}
	};
}
