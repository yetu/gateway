package com.yetu.gateway.home.binding.mockdriver.internal;

import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.ThingStatusInfo;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockDeviceHandler extends BaseThingHandler  {
	public static final Logger logger = LoggerFactory.getLogger(MockDeviceHandler.class);

	public MockDeviceHandler(Thing thing) {
		super(thing);
	}

	@Override
	public void handleCommand(ChannelUID channelUID, Command command) {
		logger.debug("Handle command "+channelUID+" with cmd: "+command);
		
		if (command instanceof OnOffType) {
			this.updateState(channelUID, (State)command);
		}
	}

	/**
	 * Simulates a changed property of the device
	 * @param property
	 * @param propertyValue
	 * @return
	 */
	public boolean triggerPropertyChange(String property, String propertyValue) {
		Channel channel = null;
		State state = null;
		
		if (property.compareToIgnoreCase("on") == 0) {
			state = OnOffType.valueOf(propertyValue);
			channel = getThing().getChannel("on");
		}
		
		if (channel == null){
			logger.error("Unable to find channel for property {} with value {}",property,propertyValue);
			return false;
		}
		if (state == null){
			logger.error("Unable to generate correct state for property {} with given value {}",property,propertyValue);
			return false;
		}
		
		this.updateState(channel.getUID(),state);
		return true;
	}
	
	public boolean triggerStatusChange(ThingStatus newStatus) {
		logger.debug("Setting new state of thing. "+newStatus);
		thing.setStatusInfo(new ThingStatusInfo(newStatus, ThingStatusDetail.NONE, ""));
		
		return true;
	}
}
