package com.yetu.gateway.home.binding.mockdriver.internal.devicetypes;

public class MockLamp {
	private String uniqueId;
	
	public MockLamp(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	public String getUniqueId() {
		return uniqueId;
	}
}
