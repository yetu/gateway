package org.eclipse.smarthome.binding.nest.handler;

import org.eclipse.smarthome.binding.nest.internal.NestCommunicator;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.binding.BaseBridgeHandler;
import org.eclipse.smarthome.core.types.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NestBridgeHandler extends BaseBridgeHandler{

	private static Logger logger = LoggerFactory.getLogger(NestBridgeHandler.class);
	
	public NestBridgeHandler(Bridge bridge) {
		super(bridge);
	}

	@Override
	public void handleCommand(ChannelUID channelUID, Command command) {
		if ("access_token".equalsIgnoreCase(channelUID.getId())){
			String accessToken = null;
			if (command instanceof StringType){
				accessToken = command.toString();
			}
			if (accessToken != null){
				if (accessToken.length() > 4){
						logger.debug("received access token {}...{}",accessToken.substring(0,4),accessToken.substring(accessToken.length()-4));						
						NestCommunicator.getInstance().setAccessToken(accessToken);
				}				
			}
		}
		
	}
	

}
