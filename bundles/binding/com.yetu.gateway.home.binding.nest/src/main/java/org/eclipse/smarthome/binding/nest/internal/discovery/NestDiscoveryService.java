package org.eclipse.smarthome.binding.nest.internal.discovery;

import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;

import org.eclipse.smarthome.binding.nest.NestBindingConstants;
import org.eclipse.smarthome.binding.nest.internal.NestCommunicationListener;
import org.eclipse.smarthome.binding.nest.internal.NestCommunicator;
import org.eclipse.smarthome.binding.nest.internal.NestException;
import org.eclipse.smarthome.binding.nest.internal.NestCommunicator.NestCommunicatorState;
import org.eclipse.smarthome.binding.nest.internal.NestIdMapper;
import org.eclipse.smarthome.binding.nest.internal.messages.AbstractDevice;
import org.eclipse.smarthome.binding.nest.internal.messages.DataModel;
import org.eclipse.smarthome.binding.nest.internal.messages.SmokeCOAlarm;
import org.eclipse.smarthome.binding.nest.internal.messages.DataModel.Devices;
import org.eclipse.smarthome.binding.nest.internal.messages.Thermostat;
import org.eclipse.smarthome.config.discovery.AbstractDiscoveryService;
import org.eclipse.smarthome.config.discovery.DiscoveryResult;
import org.eclipse.smarthome.config.discovery.DiscoveryResultBuilder;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class NestDiscoveryService extends AbstractDiscoveryService{

	
	
	public NestDiscoveryService(){
		super(NestBindingConstants.DEFAULT_DISCOVERY_TIMEOUT);		
		
	}
	
	private Logger logger = LoggerFactory.getLogger(NestDiscoveryService.class);

	
	
	
	@Override
    public Set<ThingTypeUID> getSupportedThingTypes() {
        return NestBindingConstants.SUPPORTED_THING_TYPES;
    }
	
	protected void activate(Map<String,Object> configProperties){
		Map<String, Object> newConfigProperties = new HashMap<>();
		newConfigProperties.putAll(configProperties);		
		newConfigProperties.put(CONFIG_PROPERTY_BACKGROUND_DISCOVERY_ENABLED, true);
		super.activate(newConfigProperties);
		NestCommunicator.getInstance().addNestCommunicationListener(new NestCommunicationListener() {
			
			@Override
			public void onStateChange(NestCommunicatorState state) {
				processNestCommunicatorStateChange(state);
				
			}
			
			@Override
			public void onPropertyChange(String propertyId, Object newState) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onNewDeviceFound(AbstractDevice device) {
				
				processDeviceFound(device);
			}
		});
	}
	
	private void processDeviceFound(AbstractDevice device){
		if (device instanceof Thermostat){
			thingDiscovered(generateDiscoveryResult((Thermostat)device));
			return;
		} else if (device instanceof SmokeCOAlarm){
			thingDiscovered(generateDiscoveryResult((SmokeCOAlarm)device));
			return;
		}
		
		logger.warn("processing new device found, but do not know what to do. "+
		"Device {} is whether a thermostat nor a smoke detector",device);
		
	}
	
	private void processNestCommunicatorStateChange(NestCommunicatorState state) {
		switch (state){
			case NCS_NORMAL:
				startScan();
				break;
			default:
				break;
		}
		
	}
	
	protected void searchForDevices() {
		logger.debug("Polling nest api for new devices");
		
		try {
			if (NestCommunicator.getInstance().hasAccessToken()){
				processDataModel(NestCommunicator.getInstance().getDataModel());
			} else {				
				logger.debug("stopping Nest Scan - no access token available");
				reportBridge();
			}
		} catch (NestException nestException) {
			logger.debug("Reading DataModel failed");
			if (scanListener != null){
				scanListener.onErrorOccurred(nestException);
			}
		} 			
		
	}
	
	@Override
	protected void startBackgroundDiscovery() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				reportBridge();
				
			}
		}, 60000);
		
	}
	
	private void reportBridge(){
		logger.debug("creating nest bridge");
		thingDiscovered(getBridgeDiscoveryResult(new ThingUID(NestBindingConstants.THING_TYPE_UID_NEST_WEB_ACCESS_BRIDGE,"oauth"), "Nest OAuth Service Bridge"));
		
	}
	
	private DiscoveryResult getBridgeDiscoveryResult(ThingUID thingUID, String label){

		Map<String, Object> properties = new HashMap<>();
		
		return DiscoveryResultBuilder.create(thingUID)
				.withProperties(properties)
				.withLabel(label)
				.build();
	}
	
	private void processDataModel(DataModel dataModel){
		logger.debug("processing received DataModel");
		Devices devices = null;

		try {
			devices = dataModel.getDevices();			
		} catch (NestException exc){
			if (scanListener != null){
				scanListener.onErrorOccurred(exc);
				return;
			}
		}

		if (devices == null) {
			logger.error("Failed to retrieve list of devices");
			return;
		}

		logger.debug("got {} thermostats and {} smoke CO sensors",devices.getThermostats().size(), devices.getSmoke_co_alarms().size());
		
		if (devices.getThermostats() != null) {
			for (Entry<String,Thermostat> entry : devices.getThermostats().entrySet()){
				thingDiscovered(generateDiscoveryResult(entry.getValue()));
			}	
		}
		
		if (devices.getSmoke_co_alarms() != null) {
			for (Entry<String,SmokeCOAlarm> entry : devices.getSmoke_co_alarms().entrySet()){
				thingDiscovered(generateDiscoveryResult(entry.getValue()));
			}	
		}
		
	}

	
	private DiscoveryResult generateDiscoveryResult(Thermostat thermostat){
		ThingUID thingUID = new ThingUID(NestBindingConstants.BINDING_ID,NestBindingConstants.THING_TYPE_UID_NEST_THERMOSTAT.getId(), NestIdMapper.getInstance().getDeviceIdbyNestDeviceId(thermostat.getDevice_id()));
		return generateDiscoveryResult("Nest Thermostat "+thermostat.getName(), thingUID);
	}

	private DiscoveryResult generateDiscoveryResult(SmokeCOAlarm smokeSensor){
		ThingUID thingUID = new ThingUID(NestBindingConstants.BINDING_ID,NestBindingConstants.THING_TYPE_UID_NEST_SMOKE_SENSOR.getId(), NestIdMapper.getInstance().getDeviceIdbyNestDeviceId(smokeSensor.getDevice_id()));
		return generateDiscoveryResult("Nest Smoke sensor "+smokeSensor.getName(), thingUID);
	}

	private DiscoveryResult generateDiscoveryResult(String label, ThingUID thingUID){
		Map<String, Object> properties = new HashMap<>();
		
		return DiscoveryResultBuilder.create(thingUID)
				.withProperties(properties)
				.withLabel(label)
				.build();
	}

	@Override
	protected void startScan() {
		logger.debug("Ignoring event since api gets poll within background task");
	}
}
