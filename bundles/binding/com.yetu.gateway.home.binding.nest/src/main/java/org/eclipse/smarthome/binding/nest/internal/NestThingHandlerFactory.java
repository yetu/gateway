package org.eclipse.smarthome.binding.nest.internal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.smarthome.binding.nest.NestBindingConstants;
import org.eclipse.smarthome.binding.nest.handler.NestBridgeHandler;
import org.eclipse.smarthome.binding.nest.handler.NestThermostatHandler;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NestThingHandlerFactory extends BaseThingHandlerFactory  {
	
	private static Logger logger = LoggerFactory.getLogger(NestThingHandlerFactory.class);
	
	
			
	private Map<String, ThingHandler> generatedNestThingHandlers;
	
	public NestThingHandlerFactory(){
		this.generatedNestThingHandlers = new HashMap<String, ThingHandler>();
	}
	
	@Override
	public boolean supportsThingType(ThingTypeUID thingTypeUID) {
		return NestBindingConstants.SUPPORTED_THING_TYPES.contains(thingTypeUID);
	}

	public boolean isNestDeviceAbstracted(String nestDeviceId){
		return (generatedNestThingHandlers.containsKey(nestDeviceId));
	}
	
	@Override
	protected ThingHandler createHandler(Thing thing) {
		logger.debug("create Handler called");
		ThingHandler handler = null;
		if ("thermostat".equalsIgnoreCase(thing.getThingTypeUID().getId())){
			// it's a thermostat :)
			logger.debug("creating new NestThermostatHandler for thing {}",thing.getUID());
			handler =  new NestThermostatHandler(thing);
		}
		if ("webservice".equalsIgnoreCase(thing.getThingTypeUID().getId())){
			if (thing instanceof Bridge){
				logger.debug("creating new NestBridgeHandler for thing {}",thing.getUID());
				handler = new NestBridgeHandler((Bridge)thing);
			}
		}
		if (handler == null){
			// no handler found :(
			logger.warn("Unable to find handler for given thing {}",thing.getUID());			
		} else {
			generatedNestThingHandlers.put(thing.getUID().getId(), handler);
		}
		return handler;
	}

}
