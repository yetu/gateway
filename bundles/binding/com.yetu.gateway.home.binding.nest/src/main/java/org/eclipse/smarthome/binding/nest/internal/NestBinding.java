package org.eclipse.smarthome.binding.nest.internal;

import java.util.Dictionary;

import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.smarthome.core.storage.Storage;
import org.eclipse.smarthome.core.storage.StorageService;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NestBinding implements ManagedService {
	private static final String NEST_ID_STORAGE_NAME = "nestDeviceIdMapping";
	protected static final ObjectMapper JSON = new ObjectMapper();
	Logger logger = LoggerFactory.getLogger(NestBinding.class);
	
	protected StorageService storageService;
	protected Storage<String> storage;
	
	
	public NestBinding() {
		NestCommunicator.getInstance();
	}
	
	@Override
	public void updated(Dictionary<String, ?> properties)
			throws ConfigurationException {
		// TODO Auto-generated method stub
		
	}
	
	
	// +++++++++++++++++++++++++++++ Nest Id Mapping ++++++++++++++++++++++++++++++++++++
	
		public void setStorageService(StorageService storageService){
			this.storageService = storageService;
			storage = storageService.getStorage(NEST_ID_STORAGE_NAME);
			NestIdMapper.getInstance().setStorage(storage);
		}
		
		public void unsetStorageService(StorageService storageService){
			storage = null;
			storageService = null;
		}
			
		// ---------------------------- Nest ID Mapping -------------------------------------
		
	
	
}
