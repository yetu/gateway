package org.eclipse.smarthome.binding.nest;

import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingTypeUID;

import com.google.common.collect.Sets;

public class NestBindingConstants {

	public static final String BINDING_ID = "nest";
	
	public final static ThingTypeUID THING_TYPE_UID_NEST_THERMOSTAT = new ThingTypeUID(BINDING_ID,"thermostat");
	
	public final static ThingTypeUID THING_TYPE_UID_NEST_SMOKE_SENSOR = new ThingTypeUID(BINDING_ID,"smokesensor");
	
	public final static ThingTypeUID THING_TYPE_UID_NEST_WEB_ACCESS_BRIDGE = new ThingTypeUID(BINDING_ID,"webservice");
	
	
	public final static Set<ThingTypeUID> SUPPORTED_THING_TYPES = Sets.newHashSet(THING_TYPE_UID_NEST_THERMOSTAT,THING_TYPE_UID_NEST_SMOKE_SENSOR,THING_TYPE_UID_NEST_WEB_ACCESS_BRIDGE);
	
	// Discovery
	public static final int DEFAULT_DISCOVERY_TIMEOUT = 300; // in seconds
	public static final long DEFAULT_NEST_REFRESH_INTERVAL = 30000; // in milliseconds
	
}
