package org.eclipse.smarthome.binding.nest.internal;

import org.eclipse.smarthome.core.storage.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NestIdMapper {

	private static Logger logger = LoggerFactory.getLogger(NestIdMapper.class);
	private static NestIdMapper instance = null;
	private Storage<String> storage;
	
	protected NestIdMapper(){
		
	}
	
	public static NestIdMapper getInstance(){
		if (instance == null){
			logger.debug("generating instance of NestIdMapping");
			instance = new NestIdMapper();
		}
		return instance;
	}
	
	
	public void setStorage(Storage<String> storage){
		logger.debug("Storage was assigned");
		this.storage = storage;	
	}
	
	
	public String getDeviceIdbyNestDeviceId(String nestDeviceId){
		String id = null;
		for (String key : storage.getKeys()){
			if (nestDeviceId.equalsIgnoreCase(storage.get(key))){
				logger.debug("found existing id {} for nestDeviceId {}",key,nestDeviceId);
				id = key;
			}
		}
		if (id == null){
			logger.debug("found no existing id for nestDeviceId {}",nestDeviceId);
			id = ""+(storage.getValues().size()+1);
			logger.debug("generating id {} for nest device with id {}",id,nestDeviceId);
			storage.put(id,nestDeviceId);
		}		
		return id;
	}
	
	
	
	public String getNestDeviceId(String id){
		return storage.get(id);
	}
}
