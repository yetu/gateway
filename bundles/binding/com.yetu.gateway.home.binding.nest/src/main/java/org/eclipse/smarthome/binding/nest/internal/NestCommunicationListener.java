package org.eclipse.smarthome.binding.nest.internal;

import org.eclipse.smarthome.binding.nest.internal.NestCommunicator.NestCommunicatorState;
import org.eclipse.smarthome.binding.nest.internal.messages.AbstractDevice;

public interface NestCommunicationListener {
	public void onStateChange(NestCommunicatorState state);
	public void onPropertyChange(String propertyId, Object newState);	
	public void onNewDeviceFound(AbstractDevice device);
}
