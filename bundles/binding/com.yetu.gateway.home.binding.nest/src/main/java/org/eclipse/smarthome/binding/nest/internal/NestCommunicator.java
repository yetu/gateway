package org.eclipse.smarthome.binding.nest.internal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.PropertyUtils;
import org.eclipse.smarthome.binding.nest.NestBindingConstants;
import org.eclipse.smarthome.binding.nest.NestSetPropertyException;
import org.eclipse.smarthome.binding.nest.internal.messages.AbstractDevice;
import org.eclipse.smarthome.binding.nest.internal.messages.DataModel;
import org.eclipse.smarthome.binding.nest.internal.messages.DataModelElement;
import org.eclipse.smarthome.binding.nest.internal.messages.DataModelRequest;
import org.eclipse.smarthome.binding.nest.internal.messages.DataModelResponse;
import org.eclipse.smarthome.binding.nest.internal.messages.SmokeCOAlarm;
import org.eclipse.smarthome.binding.nest.internal.messages.Thermostat;
import org.eclipse.smarthome.binding.nest.internal.messages.UpdateDataModelRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NestCommunicator {

	//public final static String ACCESS_TOKEN = "c.hO62AAuL6yR1ysHX8nlf3pcjPcz4vA4eigjPIHyNCBUz5O3bBPgng1sx66rOGp7dNjOxU4y5N0bIZYezphzvwOQlg5DgziBXoko7Z4dvzokGjm8peZLKQsChKSgcbUe8lmFQoDHBb8GDnQhC";
	//c.3SKIX50IXPmB3qXpEjsX06SEMBPYSgQYmNapAKZkEDXQSxMgtm8UQz29BiOpmN3D1pYT2yCLjAdId72wUICQtBANojbKcMLHo4gbMFIJcmtwh7mGtLrwsvXiKeeQa2zVMXpRF5PEPaDWxfVN
	
	private static Logger logger = LoggerFactory.getLogger(NestCommunicator.class);	
	private static NestCommunicator instance = null;
	private long refreshInterval;
	private NestCommunicatorState currentState;
	private NestReader nestReader;
	private DataModel currentDataModel = null;
	private List<NestCommunicationListener> communicatorListeners;
	private String accessToken = null;
	

	
	// ++++++++++++++++++++++++++ INITIALIZATION ++++++++++++++++++++++++++++++++
	public static NestCommunicator getInstance(){
		if (instance == null){
			instance = new NestCommunicator();
		}
		
		return instance;
	}
	
	public NestCommunicator(){		
		this(NestBindingConstants.DEFAULT_NEST_REFRESH_INTERVAL);
	}
	
	public NestCommunicator(long refreshInterval){
		communicatorListeners = new ArrayList<>();
		this.refreshInterval = refreshInterval;
		setState(NestCommunicatorState.NCS_INITIALIZING);
		nestReader = new NestReader();
		nestReader.start();
		accessToken = loadAccessToken();
		if (accessToken == null){			
			setState(NestCommunicatorState.NCS_ERROR_NO_ACCESS_TOKEN);
		}
	}
		
	// --------------------------- INITIALIZATION --------------------------------
	// +++++++++++++++++++++++++++ OPERATIONAL +++++++++++++++++++++++++++++++++++
	
	public void setRefreshInterval(long milliseconds){
		this.refreshInterval = milliseconds;
	}
	public NestCommunicatorState getState(){
		return currentState;
	}
	
	private void setState(NestCommunicatorState state){	
		if (currentState == state){
			return;
		}
		if (currentState != null) {
			logger.debug("State changed from {} to {}",currentState.name(),state.name());
		} else {
			logger.debug("State changed to {}",state.name());
		}
		this.currentState = state;

		synchronized(communicatorListeners){
			for (NestCommunicationListener listener : getCommunicationListeners()){
				listener.onStateChange(currentState);
			}
			
		}
	}
	
	public enum NestCommunicatorState{
		NCS_INITIALIZING,
		NCS_NORMAL,	
		NCS_STOPPED,
		NCS_ERROR_NO_ACCESS_TOKEN,
		NCS_ERROR_NO_CONNECTION,
		NCS_ERROR_UNKNOWN;
	
	}
	
	public void addNestCommunicationListener(NestCommunicationListener listener){
		synchronized (communicatorListeners){
			if (communicatorListeners.contains(listener)){
				return;
			}
			communicatorListeners.add(listener);
		}
	}
	
	private List<NestCommunicationListener> getCommunicationListeners(){
		List<NestCommunicationListener> listeners = new ArrayList<>();
		listeners.addAll(communicatorListeners);
		return listeners;
	}
	
	public void dispatchPropertyChangeEvent(String propertyName, Object value){
	
		for (NestCommunicationListener listener : getCommunicationListeners()){
			listener.onPropertyChange(propertyName, value);
		}		
	}
	
	private void dispatchNewDeviceFoundEvent(AbstractDevice device){
		for (NestCommunicationListener listener : getCommunicationListeners()){
			listener.onNewDeviceFound(device);
		}	
	}
	
	public boolean hasAccessToken() {
		return accessToken != null;
	}
	
	public void setAccessToken(String accessToken) {
		storeAccessToken(accessToken);
		this.accessToken = accessToken;
		this.readNest();
	}
	
	private File getAccessTokenFilePath(){		
		String sep = System.getProperty("file.separator");
		String path = System.getProperty("user.dir")+sep+"runtime"+sep+"conf"+sep+"nestaccess.tok";		
		return new File(path);
	}
	
	private void storeAccessToken(String accessToken){
		FileWriter fw;
		
		try {
			fw = new FileWriter(getAccessTokenFilePath());
			fw.write(accessToken);
			fw.flush();
			fw.close();
		} catch (IOException exc) {			
			logger.error("unable to store access token for nest api.",exc);
		}
		
	}
	
	private String loadAccessToken(){
		BufferedReader br;
		try {
			File file = getAccessTokenFilePath();
			if (!file.exists()){
				return null;
			}
			br = new BufferedReader(new FileReader(file));
			String accessToken = br.readLine();			
			br.close();
			if (accessToken != null){
				if (accessToken.length()==0){
					accessToken = null;
				}
			}
			return accessToken;
		} catch (IOException exc){
			logger.error("unable to load access token for nest api.",exc);
		}
		return null;
	}
	
	public String getAccessToken(){
		//return "c.hO62AAuL6yR1ysHX8nlf3pcjPcz4vA4eigjPIHyNCBUz5O3bBPgng1sx66rOGp7dNjOxU4y5N0bIZYezphzvwOQlg5DgziBXoko7Z4dvzokGjm8peZLKQsChKSgcbUe8lmFQoDHBb8GDnQhC";
		return accessToken;
	}
	
	// --------------------------- OPERATIONAL ----------------------------------
	// +++++++++++++++++++++++++++   READING DATA +++++++++++++++++++++++++++++++
	
	
	private synchronized void readNest() throws NestException {
		if (!hasAccessToken()){
			return;
		}
		logger.debug("gonna try to read Nest Data");
		DataModelRequest dmreq = new DataModelRequest(getAccessToken());
		DataModelResponse dmres = dmreq.execute();

		if (dmres.isError()) {
			
			logger.error("Error retrieving data model: {}", dmres.getError());
			setState(NestCommunicatorState.NCS_ERROR_UNKNOWN);
			throw new NestException("Error reading data model. "+dmres.getError(), null);
		}
		
		
		
		//DataModel updateDataModel = currentDataModel.updateDataModel(property, newState)
		Map<String, Object> changeMap = getPropertyChanges(dmres);
		
		// TODO update currentDataModel 
		currentDataModel = dmres;
		
		for (Entry<String, Object> entry : changeMap.entrySet()){
			dispatchPropertyChangeEvent(entry.getKey(),entry.getValue());
		}
		
		if (dmres != null){
			setState(NestCommunicatorState.NCS_NORMAL);
		}
		logger.debug("finished reading nest data");
	}

	
	private HashMap<String,Object> getPropertyChanges(DataModel newDataModel){
		
		HashMap<String,Object> resultMap = new HashMap<>();	
		resultMap.putAll(getPropertyChanges(newDataModel.getDevices().getAll().values()));
		return resultMap;
	}
	
	private HashMap<String, Object> getPropertyChanges(Collection<AbstractDevice> abstractDevices){
		HashMap<String,Object> resultMap = new HashMap<>();
		AbstractDevice currentDevice;
		for (AbstractDevice device : abstractDevices){
			try {
				currentDevice = getNestDevice(device.getDevice_id());
				if (currentDevice == null){
					logger.debug("found new Thermostat {}",PropertyUtils.describe(device));		
					dispatchNewDeviceFoundEvent(device);
				} else {
						resultMap.putAll(getPropertyChanges(currentDevice, device));			
				}
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return resultMap;
	}
	

	
	@SuppressWarnings("unchecked")
	private HashMap<String,Object> getPropertyChanges(AbstractDevice currentDevice, AbstractDevice newDevice){
		//logger.debug("GetPropertyChanges of \r\nThermostat 1 {}\r\nThermostat 2 {}",currentThermostat,newThermostat);
		
		
		HashMap<String,Object> resultMap = new HashMap<>();
		
		try {
			Map<String,Object> cDeviceMap = PropertyUtils.describe(currentDevice);
			Map<String, Object> nDeviceMap = PropertyUtils.describe(newDevice);
			
			Object oldValue;
			Object newValue;
			for (Entry<String,Object> nDeviceEntry : nDeviceMap.entrySet()){
				// get property value of same property from old map
				oldValue = cDeviceMap.get(nDeviceEntry.getKey());
				newValue = nDeviceEntry.getValue();
				//logger.debug("for property {} new value is {} and old value is {}",nThermEntry.getKey(),newValue,oldValue);
				// compare the values
				if (newValue==null){
					// it's a new value? wierd
					// TODO: handle this
				} else {					
					if (!isSame(newValue, oldValue)){
						// property has changed -> put to result map
						String propertyName = null;
						if (newDevice instanceof Thermostat){
							propertyName = "devices.thermostats_by_id."+newDevice.getDevice_id()+"."+nDeviceEntry.getKey();
						} else if (newDevice instanceof SmokeCOAlarm){							
							propertyName = "devices.smoke_co_alarms_by_id."+newDevice.getDevice_id()+"."+nDeviceEntry.getKey();
						}
						
						if (propertyName != null){
							logger.debug("discovered property change for property {} with new value {}",propertyName,newValue);							
							resultMap.put(propertyName,newValue);
						} else {
							logger.warn("unknown Nest Device type {}",newDevice.getClass().getName());
						}
					}
				}
				
			}
			
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return resultMap;
	}
	
	private boolean isSame(Object value1, Object value2){
		if (value1 == null){
			return (value2 == null);
		}
		if (value2 == null){
			return false;
		}
		if (value1 instanceof DataModelElement){
			// TODO: string comparison is a workaround for complex values
			// a recursive comparison is not needed yet, refactor if necessary
			return value1.toString().equals(value2.toString());
		}
		
		return value1.equals(value2);		
	}
	
//	private Map<String, Object> getRecursiveChanges(DataModelElement oldDataModel, DataModelElement newDataModel){
//		
//	}
	
	public DataModel getDataModel() throws NestException{
		if (currentDataModel == null){
			readNest();
		}
		return currentDataModel;
	}
	
	public AbstractDevice getNestDevice(String id){
		AbstractDevice device = getThermostat(id);
		if (device == null){
			device = getSmokeSensor(id);
		}
		return device;
	}
	
	public Thermostat getThermostat(String id){		
		if  (currentDataModel == null){
			return null;
		}
		
		synchronized(currentDataModel){
			if ( currentDataModel.getThermostats() != null){
				for (Thermostat tempTherm : currentDataModel.getThermostats().values()){
					if (id.equals(tempTherm.getDevice_id())){
						return tempTherm;
					}
				}
			}
		}		
		return null;
	}
	
	public SmokeCOAlarm getSmokeSensor(String id){
		if  (currentDataModel == null){
			return null;
		}
		
		synchronized(currentDataModel){
			if ( currentDataModel.getThermostats() != null){
				for (SmokeCOAlarm tempSensor : currentDataModel.getSmoke_co_alarms().values()){
					if (id.equals(tempSensor.getDevice_id())){
						return tempSensor;
					}
				}
			}
		}		
		return null;
	}
	

	
	
	private class NestReader extends Thread{
	
		private boolean keepAlive = true;
		
		public void doStop(){
			keepAlive = false;
			this.interrupt();
		}
		
		public void run(){
			while (keepAlive){				
				try {
					readNest();
				} catch (Exception exception){
					logger.warn("Problems during reading nest data");
				}
				
				try {								
					Thread.sleep(refreshInterval);
				} catch (InterruptedException e) {
					// do not do something
				}
			
			}
			setState(NestCommunicatorState.NCS_STOPPED);
		}
		
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++ SETTING DATA ++++++++++++++++++++++++++++++++++++
	
	public void setProperty(String property, Object newState) throws NestSetPropertyException{
		if (currentDataModel == null){
			// TODO: read Nest DataModel first
			return;
		}
	
		try {
			DataModel updateDataModel = currentDataModel.updateDataModel(property, newState);
			updateNest(updateDataModel);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
	}
	
	private void updateNest(DataModel dataModel){
		logger.debug("updating nest with {}",dataModel);
		UpdateDataModelRequest request = new UpdateDataModelRequest(getAccessToken(),dataModel);
		DataModelResponse response = request.execute();
		if (response.isError()) {
			logger.error("Error updating data model: {}", response);
		}		
	}

	


	
	// --------------------------------------------- SETTING DATA ------------------------------------
	
	
	
}
