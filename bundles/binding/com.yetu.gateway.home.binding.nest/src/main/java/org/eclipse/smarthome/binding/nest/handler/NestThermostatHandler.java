package org.eclipse.smarthome.binding.nest.handler;

import java.math.BigDecimal;

import org.eclipse.smarthome.binding.nest.NestSetPropertyException;
import org.eclipse.smarthome.binding.nest.internal.NestCommunicationListener;
import org.eclipse.smarthome.binding.nest.internal.NestCommunicator;
import org.eclipse.smarthome.binding.nest.internal.NestCommunicator.NestCommunicatorState;
import org.eclipse.smarthome.binding.nest.internal.NestIdMapper;
import org.eclipse.smarthome.binding.nest.internal.messages.AbstractDevice;
import org.eclipse.smarthome.binding.nest.internal.messages.Thermostat;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class NestThermostatHandler extends BaseThingHandler {
	
	private static final double MIN_DESIRED_TEMPERATURE = 9.0;
	private static final double MAX_DESIRED_TEMPERATURE = 32.0;
	
	
	private static Logger logger = LoggerFactory.getLogger(NestThermostatHandler.class);

	

	public NestCommunicationListener communicationListener;
	
	
	
//	private String desiredTemperatePropertyName;
//	private String currentTemperatePropertyName;
//	private String currentHumidityPropertyName;
	
	public NestThermostatHandler(Thing thing) {
		super(thing);	
		//desiredTemperatePropertyName = "devices.thermostats_by_id."+getThing().getUID().getId()+".target_temperature_c";
		//currentTemperatePropertyName = "devices.thermostats_by_id."+getThing().getUID().getId()+".ambient_temperature_c";
		//currentHumidityPropertyName = "devices.thermostats_by_id."+getThing().getUID().getId()+".humidity";
		
		communicationListener = new NestCommunicationListener() {
			
			@Override
			public void onStateChange(NestCommunicatorState state) {
				processNestCommunicatorStateChange(state);
				
			}
			@Override
			public void onPropertyChange(String propertyId, Object newState) {
				processPropertyChange(propertyId, newState);
			}
			@Override
			public void onNewDeviceFound(AbstractDevice device) {
				// Ignoring
				
			}
			
		};
		NestCommunicator.getInstance().addNestCommunicationListener(communicationListener);
		
		new ChannelInitializerThread().start();		
		
	}
	
	private String getDesiredTemperaturePropertyName(){
		return "devices.thermostats_by_id."+NestIdMapper.getInstance().getNestDeviceId(getThing().getUID().getId())+".target_temperature_c";
	}
	
	private String getCurrentTemperaturePropertyName(){
		return "devices.thermostats_by_id."+NestIdMapper.getInstance().getNestDeviceId(getThing().getUID().getId())+".ambient_temperature_c";
	}

	private String getCurrentHumidityPropertyName(){
		return "devices.thermostats_by_id."+NestIdMapper.getInstance().getNestDeviceId(getThing().getUID().getId())+".humidity";
	}
	
	private void processPropertyChange(String propertyId, Object newState) {
		logger.debug("Thing {} status {}",getThing().getThingTypeUID(), getThing().getStatus());
		ChannelUID channelUID = null; 
		State state = null;	
		
		if (getDesiredTemperaturePropertyName().equalsIgnoreCase(propertyId)){			
			channelUID = new ChannelUID(getThing().getUID()+":desired_temperature");			
		} else if (getCurrentTemperaturePropertyName().equalsIgnoreCase(propertyId)){			
			channelUID = new ChannelUID(getThing().getUID()+":current_temperature");
		} else if (getCurrentHumidityPropertyName().equalsIgnoreCase(propertyId)){			
			channelUID = new ChannelUID(getThing().getUID()+":humidity");			
		}
		
		if (channelUID == null){
			logger.warn("Unable to process property change. No channel found for property {}.", propertyId);
			return;
		}
		
		if (newState instanceof BigDecimal){
			state = new DecimalType((BigDecimal)newState);
			logger.debug("updating channel {} with state {}",channelUID,state);
			updateState(channelUID, state);
		} else {
			logger.warn("Unable to update channel {} related to property {}. Given state {} is not of type BigDecimal",channelUID, propertyId, newState);
			return;
		}
	}
	
	private void processNestCommunicatorStateChange(NestCommunicatorState state){
		logger.debug("received nest communiator state change {}",state);
		switch (state) {
		case NCS_NORMAL:
			updateChannels();
			break;
		case NCS_ERROR_NO_CONNECTION:			
		case NCS_STOPPED:
		case NCS_ERROR_NO_ACCESS_TOKEN:
			// TODO: change Thing state to disconnected
		default:
			
		}
	}
	
	private void updateChannels(){
		logger.debug("updating channels");
		Thermostat thermostat = NestCommunicator.getInstance().getThermostat(NestIdMapper.getInstance().getNestDeviceId(getThing().getUID().getId()));
		if (thermostat != null){
			updateState(new ChannelUID(getThing().getUID()+":desired_temperature"), new DecimalType((BigDecimal)thermostat.getTarget_temperature_c()) );
			updateState(new ChannelUID(getThing().getUID()+":current_temperature"), new DecimalType((BigDecimal)thermostat.getAmbient_temperature_c()) );
			updateState(new ChannelUID(getThing().getUID()+":humidity"), new DecimalType((BigDecimal)thermostat.getHumidity()) );			
		}	
	}
	

	
	@Override
	public void handleCommand(ChannelUID channelUID, Command command) {
		logger.debug("Handling command '{}' for channel '{}'",command,channelUID);		
		if ("desired_temperature".equalsIgnoreCase(channelUID.getId())){			
			if (command instanceof DecimalType){			
				double newValue = getCorrectDesiredTemperature(((DecimalType)command).doubleValue()) ;
				updateNest(getDesiredTemperaturePropertyName(),BigDecimal.valueOf(newValue));
				return;
			} else {
				logger.warn("Unable to update Nest thermostat {}. Command {} is not of Type DecimalType",getThing().getUID().getId(), command);
			}
		} else {
			logger.warn("Unable to update Nest thermostat {}. Channel {} is not valid for setting properties",getThing().getUID().getId(), channelUID);
		}
		
	}
	
	private double getCorrectDesiredTemperature(double value){
		
		if (value < MIN_DESIRED_TEMPERATURE){
			logger.debug("{} is not a valid value for desired temperature. Value will be set to min ({})",value,MIN_DESIRED_TEMPERATURE);
			return MIN_DESIRED_TEMPERATURE;
		}
		
		if (value > MAX_DESIRED_TEMPERATURE){
			logger.debug("{} is not a valid value for desired temperature. Value will be set to max ({})",value,MAX_DESIRED_TEMPERATURE);
			return MAX_DESIRED_TEMPERATURE;
		}
		
		// only x.0 and x.5 are allowed		
		double newValue = (Math.round(value * 2))/2.0;
		// now it's valid, but give do a log when value has been changed by our algorithm
		if (newValue != value){
			logger.debug("{} is not a valid value for desired temperature. Value will be set to {}.",value,newValue);
		}
				
		return newValue;
	}
	
	private void updateNest(String property, Object newState){
		logger.debug("sending new state {} of property {} to NestCommunicator",newState, property );
		try {
			NestCommunicator.getInstance().setProperty(property, newState);
		} catch (NestSetPropertyException exc){
			logger.error("unable to set property {} with new state {}. {}",property,newState,exc.getMessage(),exc);
		}
	}
	
	
	private class ChannelInitializerThread extends Thread{
		public void run(){
			boolean repeat = true;
			
			while (repeat){
				try {
					//logger.debug("checking Channels linked");
					if (getThing().getStatus() == ThingStatus.ONLINE){
						
						if (channelsHaveItems()){
						//	logger.debug("NU! status is ONLINE and Channels are linked! -> updating");
							updateChannels();
							repeat = false;
						}
					}
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					repeat = false;
				}				
			}
			//logger.debug("finished ChannelInitilizerThread");
		}
		
		private boolean channelsHaveItems(){
			for (Channel channel : getThing().getChannels()){
				if (!channel.getLinkedItems().isEmpty()){
					return true;
				}
			}
			return false;
		}
	}
	

	

}
