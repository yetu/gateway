package com.yetu.gateway.home.binding.zwave.internal.thingtypes.assaabloy;

import java.util.List;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class YaleDoorLockThingTypeGenerator  extends ZWaveSpecificThingTypeGenerator {

	@Override
	protected void addChannels(ThingUID uid,List<ChannelDefinition> channelDefinitions,	List<ChannelGroupDefinition> channelGroupDefinitions) {
		addLockChannel(uid, channelDefinitions, channelGroupDefinitions);
		addBatteryChannel(uid, channelDefinitions, channelGroupDefinitions);
	}

	@Override
	public String getManufacturerName() {
		return "Yale";
	}

	@Override
	public int getManufacturerId() {
		return 0x129; // 297
	}

	@Override
	public int getProductTypeId() {
		return 0x06;
	}

	@Override
	public int getProductId() {		
		return 0; // it's really 0
	}

	@Override
	public String getProductName() {
		return "Door Lock";
	}

	@Override
	public String getTypeName() {
		return "door_lock";
	}

	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_LOCK;
	}

}
