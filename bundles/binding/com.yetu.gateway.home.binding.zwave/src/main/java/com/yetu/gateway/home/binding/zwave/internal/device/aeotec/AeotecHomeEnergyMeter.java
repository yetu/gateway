package com.yetu.gateway.home.binding.zwave.internal.device.aeotec;

import org.slf4j.LoggerFactory;

import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericMeter;

public class AeotecHomeEnergyMeter extends GenericMeter {

	public AeotecHomeEnergyMeter(ZWaveNode node) {
		super(node);
		logger = LoggerFactory.getLogger(AeotecHomeEnergyMeter.class);
	}
	

	@Override
	protected void initZWaveNode() {
		// TODO Auto-generated method stub
		associateNode(01,01);
		associateNode(02,01);
		sendConfiguration(5, ZWaveCmdParamType.WORD,1); // send report when consumptions changes at least 1W for clamp 1
		sendConfiguration(8, ZWaveCmdParamType.BYTE,1); // send report when consumptions changes at least 1% for clamp 1
		
	}


	@Override
	public void requestValues() {
		super.requestValues();
		
	}
	
}
