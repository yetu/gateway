package com.yetu.gateway.home.binding.zwave.internal.device;

public interface ZWaveDeviceEventListener {
	public void onDeviceControllerEvent(ZWaveDeviceEvent event);
}
