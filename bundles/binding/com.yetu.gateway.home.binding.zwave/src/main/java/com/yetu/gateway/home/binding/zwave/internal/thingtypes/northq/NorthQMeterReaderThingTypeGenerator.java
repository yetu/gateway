package com.yetu.gateway.home.binding.zwave.internal.thingtypes.northq;

import java.util.List;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class NorthQMeterReaderThingTypeGenerator extends ZWaveSpecificThingTypeGenerator{

//	private void addMeterChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
//		
//		String componentId = "meter";
//		String componentType = "METER";
//		String capability = "MEASUREMENT";
//	
//		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
//		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
//		
//		Set<String> tags = new HashSet<String>();
//		//tags.add("abstraction:"+componentId+":"+componentType+":"+capability+":unit=WATTHOUR");
//		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=WATTHOUR");
//		
//		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"Electricity Meter","Energy", tags,stateDescription,null);		
//		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
//		
//		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
//		
//		List<ChannelDefinition> groupChannels = new ArrayList<>();	
//		groupChannels.add(def);
//		
//		
//		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Meter","Meter", groupChannels);
//		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
//		groupDefs.add(gd);
//	}
	
	
	
	@Override
	public int getManufacturerId() {
	
		return 0x96;
	}

	@Override
	public int getProductTypeId() {
	
		return 0x01;
	}

	@Override
	public int getProductId() {
		
		return 0x01;
	}

	@Override
	public String getTypeName() {
		return "northq_meter_reader";
	}

	@Override
	protected void addChannels(ThingUID uid, List<ChannelDefinition> channelDefinitions,List<ChannelGroupDefinition> channelGroupDefinitions) {
		addMeterChannel(uid, channelDefinitions, channelGroupDefinitions);		
		addBatteryChannel(uid, channelDefinitions, channelGroupDefinitions);
	}

	@Override
	public String getManufacturerName() {
		return "NorthQ";
	}

	@Override
	public String getProductName() {
		return "Meter Reader";
	}

	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_METER;
	}

}
