package com.yetu.gateway.home.binding.zwave.internal.thingtypes;


import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ThingType;

import com.domoone.zwave.node.ZWaveNode;

/**
 * The Z-Wave binding is generating all thing types at runtime. This is needed, because the z-wave protocol supports devices to be 
 * self descriptive. Thus, all devices could be supported by the binding although they are not known by the binding. A ZWaveThingTypeGenerator
 * generates a thing type out of a given ZWaveNode. All available ThingTypeGenerators will be added to the ZWaveThingTypeGeneratorRepository. 
 * @author Mathias Runge
 *
 */
public interface ZWaveThingTypeGenerator {
	
	public ThingUID generateThingUID(ZWaveNode node);
	public ThingTypeUID getThingTypeUID();	
	public ThingType generateThingType(ThingUID uid);
}
