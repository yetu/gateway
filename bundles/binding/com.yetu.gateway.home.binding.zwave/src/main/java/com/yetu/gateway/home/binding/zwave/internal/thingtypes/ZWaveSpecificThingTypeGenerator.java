package com.yetu.gateway.home.binding.zwave.internal.thingtypes;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupType;
import org.eclipse.smarthome.core.thing.type.ChannelGroupTypeUID;
import org.eclipse.smarthome.core.thing.type.ChannelType;
import org.eclipse.smarthome.core.thing.type.ChannelTypeUID;
import org.eclipse.smarthome.core.thing.type.ThingType;
import org.eclipse.smarthome.core.types.StateDescription;
import org.eclipse.smarthome.core.types.StateOption;

import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.ZWaveBindingConstants;

public abstract class ZWaveSpecificThingTypeGenerator implements ZWaveThingTypeGenerator{	

	private static final String PROTOCOL 				= "protocol";
	
	private static final String ZWAVE_NODE_ID			= "zwave_node_id";
	private static final String ZWAVE_MANUFACTURER_ID 	= "zwave_manufacturer_id";
	private static final String ZWAVE_PRODUCT_ID 		= "zwave_product_id";
	private static final String ZWAVE_PRODUCT_TYPE_ID 	= "zwave_product_type_id";
	
	public static final String DEFAULT_COMPONENT_ID_BATTERY				= "battery";
	public static final String DEFAULT_COMPONENT_ID_SOCKET				= "socket";
	public static final String DEFAULT_COMPONENT_ID_METER				= "meter";
	public static final String DEFAULT_COMPONENT_ID_SENSOR_TEMPERATURE	= "temperature";
	public static final String DEFAULT_COMPONENT_ID_SENSOR_HUMIDITY		= "humidity";
	public static final String DEFAULT_COMPONENT_ID_SENSOR_ILLUMINANCE	= "illuminance";
	public static final String DEFAULT_COMPONENT_ID_SENSOR_ELECTRICITY	= "power";
	public static final String DEFAULT_COMPONENT_ID_SENSOR_MOTION		= "motion";
	public static final String DEFAULT_COMPONENT_ID_SENSOR_CONTACT		= "contact";
	public static final String DEFAULT_COMPONENT_ID_LAMP					= "lamp";
	public static final String DEFAULT_COMPONENT_ID_LOCK				= "lock";
	
	
	@Override
	public ThingType generateThingType(ThingUID uid){
		
		List<String> bridges = getBridges();		
		Map<String,String> properties = getProperties();
		properties.put(ZWAVE_NODE_ID, uid.getId());
		
		List<ChannelDefinition> channelDefinitions = new ArrayList<>();
		List<ChannelGroupDefinition> channelGroupDefinitions = new ArrayList<>();
		
		addChannels(uid,channelDefinitions,channelGroupDefinitions);
		
		return new ThingType(uid.getThingTypeUID(), bridges, getLabel(),getDescription(), channelDefinitions, channelGroupDefinitions, properties, null);
	}
	
	public ThingUID generateThingTypeUID(ZWaveNode node){		
		return new ThingUID(ZWaveBindingConstants.ZWAVE_BINDING_ID,getTypeName(),""+node.getNodeId());
	}
	
	
	public ThingTypeUID getThingTypeUID(){
		return new ThingTypeUID(ZWaveBindingConstants.ZWAVE_BINDING_ID, getTypeName());
	}
	
	@Override
	public ThingUID generateThingUID(ZWaveNode node) {		
		return new ThingUID(ZWaveBindingConstants.ZWAVE_BINDING_ID,getTypeName(),""+node.getNodeId());
	}
	
	protected String getLabel(){
		return getManufacturerName()+" "+getProductName();
	}
	
	protected String getDescription(){
		return "";
	}

	protected Map<String,String> getProperties(){
		Map<String,String> properties = new HashMap<String, String>();

		properties.put(PROTOCOL,"Z-WAVE");
		properties.put(ZWAVE_MANUFACTURER_ID, ""+getManufacturerId());
		properties.put(ZWAVE_PRODUCT_ID, ""+getProductId());
		properties.put(ZWAVE_PRODUCT_TYPE_ID, ""+getProductTypeId());	
		
		properties.put("abstraction_thing_name",getManufacturerName()+" "+getProductName());
		properties.put("abstraction_main_comp",getMainComponentId());
		
		
		return properties;
	}
	
	protected List<String> getBridges(){
		return new ArrayList<>();
	}
	
	protected void addBatteryChannel(ThingUID uid, List<ChannelDefinition> channelDefinitions, List<ChannelGroupDefinition> channelGroupDefinitions){
		addBatteryChannel(DEFAULT_COMPONENT_ID_BATTERY, uid, channelDefinitions,channelGroupDefinitions);
	}
	
	protected void addBatteryChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefinitions, List<ChannelGroupDefinition> channelGroupDefinitions){
		
		
		//String componentId = "battery";
		String componentType = "BATTERY";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(100.0),new BigDecimal(1.0),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType.toUpperCase()+":"+capability.toUpperCase()+":unit=PERCENTAGE");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=PERCENTAGE");
		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"light sensor","Light", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, getTypeName(),"light sensor", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		channelGroupDefinitions.add(gd);
	}
	
	protected void addMeterChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		addMeterChannel(DEFAULT_COMPONENT_ID_METER,uid, channelDefs, groupDefs);
	}
	
	protected void addMeterChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		//String componentId = "meter";
		String componentType = "METER";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType+":"+capability+":unit=WATTHOUR");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=WATTHOUR");
		
		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"Electricity Meter","Energy", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Meter","Meter", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);
	}
	
	protected void addSensorContactChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		addSensorContactChannel(DEFAULT_COMPONENT_ID_SENSOR_CONTACT,uid, channelDefs, groupDefs);
	}
	
	protected void addSensorContactChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		
		//String componentId = "sensorcontact";
		String componentType = "SENSOR";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType.toUpperCase()+":"+capability.toUpperCase()+":unit=BOOLEAN");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=BOOLEAN");
		ChannelType ct = new ChannelType(ctuid,false,"Switch",capability.toLowerCase(),"Sensor Contact","Switch", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, getTypeName(),"Sensor Contact", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);
	}
	
	protected void addSensorTemperatureChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		addSensorTemperatureChannel(DEFAULT_COMPONENT_ID_SENSOR_TEMPERATURE,uid, channelDefs, groupDefs);
	}
	
	protected void addSensorTemperatureChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		
		//String componentId = "temperature";
		String componentType = "SENSOR";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType.toUpperCase()+":"+capability.toUpperCase()+":unit=TEMPERATURE");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=TEMPERATURE");
		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"temperature sensor","Temperature", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, getTypeName(),"Sensor for Temperature", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);
	}
	
	
	protected void addSensorMotionChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		addSensorMotionChannel(DEFAULT_COMPONENT_ID_SENSOR_MOTION, uid, channelDefs, groupDefs);
	}
	
	protected void addSensorMotionChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		
		//String componentId = "sensormotion";
		String componentType = "SENSOR";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType.toUpperCase()+":"+capability.toUpperCase()+":unit=BOOLEAN");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=BOOLEAN");
		ChannelType ct = new ChannelType(ctuid,false,"Switch",capability.toLowerCase(),"Sensor Motion","Switch", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, getTypeName(),"Sensor Motion", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);
	}
	
	protected void addSocketChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		addSocketChannel(DEFAULT_COMPONENT_ID_SOCKET, uid, channelDefs, groupDefs);
	}
	
	protected void addSocketChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		// generate Switchabel for Socket
		StateDescription stateDescription = new StateDescription(new BigDecimal(0),new BigDecimal(1),new BigDecimal(1),"", false, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":switchable");
		
		Set<String> tags = new HashSet<>();	
	
		tags.add("#abstraction:comp_id="+componentId+";comp_type=SOCKET;cap_id=SWITCHABLE");
	
		ChannelType ct = new ChannelType(ctuid,false,"Switch","switchable","descr","Switch", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition("switchable",ct);
	
		

		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+":socket");
		
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Socket","Socket", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition("socket",cgt);
	
		
		//channelDefs.add(def);
		groupDefs.add(gd);
	}
	
	protected void addSensorIlluminanceChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		addSensorIlluminanceChannel(DEFAULT_COMPONENT_ID_SENSOR_ILLUMINANCE, uid, channelDefs, groupDefs);
	}
	
	protected void addSensorIlluminanceChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		String componentType = "SENSOR";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType.toUpperCase()+":"+capability.toUpperCase()+":unit=PERCENTAGE");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=ILLUMINANCE");
		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"light sensor","Light", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, getTypeName(),"light sensor", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);
	}
	
	protected void addSensorElectricityChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		addSensorElectricityChannel(DEFAULT_COMPONENT_ID_SENSOR_ELECTRICITY, uid, channelDefs, groupDefs);
	}
	
	protected void addSensorElectricityChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		
		String componentType = "SENSOR";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType+":"+capability+":unit=WATT");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=WATT");
		
		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"Electricity Meter","Energy", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Meter","Meter", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);		
		
	}
	
	protected void addLockChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){		
		addLockChannel(DEFAULT_COMPONENT_ID_LOCK, uid, channelDefs, groupDefs);
	}
	
	protected void addLockChannel(String componentId,ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){

		String componentType = "LOCK";
		String capability = "LOCKABLE";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType+":"+capability+":unit=WATT");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=BOOLEAN");
		
		ChannelType ct = new ChannelType(ctuid,false,"Switch",capability.toLowerCase(),"Lock","Lock", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Lock","Lock", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);		
	}
	
	
	abstract protected void addChannels(ThingUID uid,List<ChannelDefinition> channelDefinitions,List<ChannelGroupDefinition> channelGroupDefinitions);
	abstract public String getManufacturerName();
	abstract public int getManufacturerId();
	abstract public int getProductTypeId();
	abstract public int getProductId();
	abstract public String getProductName();
	abstract public String getTypeName();
	abstract public String getMainComponentId();
	
	
}
