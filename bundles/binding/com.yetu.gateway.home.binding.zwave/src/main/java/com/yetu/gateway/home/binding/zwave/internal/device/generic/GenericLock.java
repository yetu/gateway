package com.yetu.gateway.home.binding.zwave.internal.device.generic;

import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;

import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportDoorLockOperation;
import com.domoone.zwave.cmd.report.ZWaveReportDoorLockOperation.DoorLockMode;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;

public class GenericLock  extends ZWaveDeviceController{

	public GenericLock(ZWaveNode node) {
		super(node);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleUpdate(String componentId, String componentType,	String capability, State newState) {
		logger.debug("hanndling update '{}' for capability '{}' of component '{}' ",newState,capability,componentId);
		if ("lock".equalsIgnoreCase(componentType)){
			if ("lockable".equalsIgnoreCase(capability)){
				logger.debug("handling update for lock lockable state {}",newState);
			}
		}		
		
	}

	@Override
	public void handleCommand(String componentId, String componentType, String capability, Command command) {
		logger.debug("handling command '{}' for capability '{}' of subthing '{}' ",command,capability,componentId);
		if ("lock".equalsIgnoreCase(componentType)){
			if ("lockable".equalsIgnoreCase(capability)){			
				if ("TRUE".equalsIgnoreCase(command.toString())){
					lock();
				}
				if ("ON".equalsIgnoreCase(command.toString())){
					lock();
				}
				if ("FALSE".equalsIgnoreCase(command.toString())){
					unlock();
				}
				if ("OFF".equalsIgnoreCase(command.toString())){
					unlock();
				}
			}
		}
		
		
	}

	protected void lock(){
		//logger.debug("lock door lock");
		getZWaveNode().sendData(cmdFactory.generateCmd_DoorLock_set(true));
		requestLockState();
	}
	
	protected void unlock(){
		//logger.debug("lock door lock");
		getZWaveNode().sendData(cmdFactory.generateCmd_DoorLock_set(false));
		requestLockState();
	}
	
	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		try {
			// 
			
			switch (cmd.getCommandClassKey()){
				case 0x62: // COMMAND_CLASS_DOOR_LOCK
					if (cmd.getCommandKey() == 0x03){ // DOOR_LOCK_OPERATION_REPORT
						evalOperationReport(ZWaveReportFactory.generateDoorLockOperationReport(cmd));
					}					
					break;					
				
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
		
	}

	protected void updateLockValue(boolean locked){
		if (capabilityStateChangeListener != null){
			State newState;
			if (locked){
				newState = OnOffType.ON;	
			} else {
				newState = OnOffType.OFF;
			}
			capabilityStateChangeListener.onCapabilityStateChange("lock","lockable", newState);
		}
	}
	
	private void evalOperationReport(ZWaveReportDoorLockOperation report){
		//62 03 ff 00 00 fe c2
		updateLockValue(report.getDoorLockMode() == DoorLockMode.DLM_SECURED);
	}
	
	@Override
	protected void initZWaveNode() {
		associateNode(01,01);
		
	}

	@Override
	public void requestValues() {
		requestLockState();
	}
	
	public void requestLockState(){
		logger.debug("requesting lock values");
		getZWaveNode().sendData(cmdFactory.generateCmd_DoorLock_get());
	}

}
