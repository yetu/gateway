package com.yetu.gateway.home.binding.zwave.internal.thingtypes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ThingType;
import org.eclipse.smarthome.core.thing.type.ThingTypeRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.aeotec.AeotecHEMThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.assaabloy.YaleDoorLockThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.dlink.DLinkDCHZ110ThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.dlink.DLinkDCHZ120ThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.dlink.DLinkDCHZ210ThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.dlink.DLinkEnergyClampThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro.FibaroDoorWindowContactThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro.FibaroMotionDetectorThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro.FibaroSmokeDetectorThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro.FibaroWallPlugThingTypeGenerator;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.northq.NorthQMeterReaderThingTypeGenerator;


/**
 * All Z-Wave thing type generators will be added to this repository and could be accessed
 * via ZWaveThingTypeGeneratorRepository.getInstance().whatever
 * @author Mathias Runge
 *
 */
public class ZWaveThingTypeFactory{

	private static Logger logger = LoggerFactory.getLogger(ZWaveThingTypeFactory.class);	
	private static ZWaveThingTypeFactory instance = null;
	
	protected ZWaveGenericThingTypeGenerator genericZWaveThingTypeGenerator;
	protected List<ZWaveSpecificThingTypeGenerator> specificThingTypeGeneratorList;
	protected ThingTypeRegistry thingTypeRegistry;
	
	// +++++++++++++++++++ supported thing type generators ++++++++++++++++++++++++++++
	
	/**
	 * Please add all thing type generators here. 
	 */
	protected void registerAllSpecificGenerators(){
		// adding Fibaro Generators
		this.specificThingTypeGeneratorList.add(new FibaroWallPlugThingTypeGenerator());
		this.specificThingTypeGeneratorList.add(new FibaroSmokeDetectorThingTypeGenerator());
		this.specificThingTypeGeneratorList.add(new FibaroMotionDetectorThingTypeGenerator());
		this.specificThingTypeGeneratorList.add(new FibaroDoorWindowContactThingTypeGenerator());
		// adding AeoTec Generators
		this.specificThingTypeGeneratorList.add(new AeotecHEMThingTypeGenerator());
		// adding D-Link Generators
		this.specificThingTypeGeneratorList.add(new DLinkDCHZ110ThingTypeGenerator());
		this.specificThingTypeGeneratorList.add(new DLinkDCHZ120ThingTypeGenerator());
		this.specificThingTypeGeneratorList.add(new DLinkDCHZ210ThingTypeGenerator());
		this.specificThingTypeGeneratorList.add(new DLinkEnergyClampThingTypeGenerator());
		// adding NorthQ Generators
		this.specificThingTypeGeneratorList.add(new NorthQMeterReaderThingTypeGenerator());
		
		this.specificThingTypeGeneratorList.add(new YaleDoorLockThingTypeGenerator());
	}
	
	
	
	
	// -------------------- supported thing type generators ------------------------------
	
	public synchronized static ZWaveThingTypeFactory getInstance(){
		if (instance == null){
			instance = new ZWaveThingTypeFactory();
		}
		return instance;
	}
	
	
	protected ZWaveThingTypeFactory(){
		specificThingTypeGeneratorList = new ArrayList<>();
		genericZWaveThingTypeGenerator = new ZWaveGenericThingTypeGenerator();
		registerAllSpecificGenerators();
	}
	
	/**
	 * Searches for the thing type generator that is responsible for this node (identified by manufacturer details) and request the 
	 * thing UID.
	 * @param node for which the thing type UID needs to be generated
	 * @return ThingUID related to given node
	 */
	public ThingUID generateThingUID(ZWaveNode node){
		logger.debug("generating ThingUID for ZWaveNode {}",node.getNodeId());
		for (ZWaveSpecificThingTypeGenerator gen : specificThingTypeGeneratorList){
			if ((gen.getManufacturerId() == node.getManufactureId()) && (gen.getProductTypeId() == node.getProductTypeId()) && (gen.getProductId() == node.getProductId())){
				return gen.generateThingUID(node);
			}
		}
		logger.debug("no specific thing type generator was found, thing type will be generic");
		return genericZWaveThingTypeGenerator.generateThingUID(node);
	}
	
	
	public Set<ThingTypeUID> getSupportedThingTypeUIDs(){
		Set<ThingTypeUID> supportedUIDs = new HashSet<>();
		supportedUIDs.add(genericZWaveThingTypeGenerator.getThingTypeUID());
		for (ZWaveSpecificThingTypeGenerator gen : specificThingTypeGeneratorList){
			supportedUIDs.add(gen.getThingTypeUID());
		}
		return supportedUIDs;
	}
	
	public ZWaveThingTypeGenerator getTypeGenerator(ThingUID thingUID){
		for (ZWaveSpecificThingTypeGenerator gen : specificThingTypeGeneratorList){
			if (gen.getThingTypeUID().equals(thingUID.getThingTypeUID())){
				return gen;
			}
		}
		return genericZWaveThingTypeGenerator;
	}
	
	public ThingType generateThingType(ThingUID thingUID){
		ZWaveThingTypeGenerator generator = getTypeGenerator(thingUID);
		if (generator == null){
			return null;
		}
		
		return generator.generateThingType(thingUID);
	}






	
	
}
