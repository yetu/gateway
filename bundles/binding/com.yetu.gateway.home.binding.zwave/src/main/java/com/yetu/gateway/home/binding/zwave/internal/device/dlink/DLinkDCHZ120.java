package com.yetu.gateway.home.binding.zwave.internal.device.dlink;

import java.math.BigDecimal;

import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.report.ZWaveReportNotification;
import com.domoone.zwave.cmd.report.ZWaveReportNotification.NotificationType;
import com.domoone.zwave.cmd.report.ZWaveReportSensorMultilevel;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class DLinkDCHZ120 extends ZWaveDeviceController{
	
	public DLinkDCHZ120(ZWaveNode node) {		
		super(node);
		logger = LoggerFactory.getLogger(DLinkDCHZ120.class);
	}

	@Override
	protected void initZWaveNode() {
		
		associateNode(1,1);
	//	associateNode(2,1);
	//	associateNode(3,1);
	}
	
	@Override
	public void handleUpdate(String componentId, String componentType,
			String capability, State newState) {
		// it's a set of sensors
		
	}

	@Override
	public void handleCommand(String componentId, String componentType,
			String capability, Command command) {
		// it's a set of sensors		
	}

	@Override
	public void requestValues() {
		//		
	}
	
	
	protected void updateMotionValue(boolean value){
		
		// setting Binary Value -> Motion Sensor
		if (capabilityStateChangeListener != null){
			State newState;
			if (value){
				newState = OnOffType.ON;	
			} else {
				newState = OnOffType.OFF;
			}
			capabilityStateChangeListener.onCapabilityStateChange(ZWaveSpecificThingTypeGenerator.DEFAULT_COMPONENT_ID_SENSOR_MOTION,"measurement", newState);
		}
	}
	
	protected void updateTemperatureValue(double value){
		capabilityStateChangeListener.onCapabilityStateChange(ZWaveSpecificThingTypeGenerator.DEFAULT_COMPONENT_ID_SENSOR_TEMPERATURE,"measurement", new DecimalType(BigDecimal.valueOf(value)));
	}
	
	protected void updateLuminanceValue(double value){
		capabilityStateChangeListener.onCapabilityStateChange(ZWaveSpecificThingTypeGenerator.DEFAULT_COMPONENT_ID_SENSOR_ILLUMINANCE,"measurement", new DecimalType(BigDecimal.valueOf(value)));
	}
	
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		logger.debug("evaluating received ZWaveNodeCmd cmd {}",cmd.getCommand().getName());	
		try {
			switch (cmd.getCommandClassKey()){
				case 0x71:
					evaulateNotification(ZWaveReportFactory.generateNotificationReport(cmd));
					break;
				case 0x31: // COMMAND_CLASS_SENSOR_MULTILEVEL
					evaluateSensorData(ZWaveReportFactory.generateSensorMultilevelReport(cmd));
					break;
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
	}
	
	
	protected void evaulateNotification(ZWaveReportNotification report){
		logger.debug("evaluation NotificationReport with notification type {} and event {}",report.getNotificationType().name(), report.getEvent() );
		if (report.getNotificationType() == NotificationType.HOME_SECURTIY){
			if ((report.getEvent() == 0x08) || (report.getEvent() == 0x07)){
				updateMotionValue(true);
			}
			if (report.getEvent() == 0x03){
			
				// Tamper Alarm -> throw event
			}
		}
	}

	protected void evaluateSensorData(ZWaveReportSensorMultilevel report){
		switch (report.getSensorType()){
		case 1:
			
			double temperature = (double)report.getValue()/(double)report.getPrecission();
			if (report.getScale() == 1){
				// temperature was measured in Fahrenheit
				// transform into Celsius
				temperature = (temperature - 32.0) * 5.0/9.0;
				// value could have more digits after transforming -> reduce to precission again.
				temperature = (double)( Math.round(temperature * report.getPrecission()) )/(double)report.getPrecission();
			}
			updateTemperatureValue(temperature);
			
			break;
		case 3:
			updateLuminanceValue(((double)report.getValue()/(double)report.getPrecission()));			
			break;
			default:
				// other sensor types are not supported !?
				break;
		}
	
	}
}
