package com.yetu.gateway.home.binding.zwave.internal.thingtypes.aeotec;

import java.util.List;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class AeotecHEMThingTypeGenerator extends ZWaveSpecificThingTypeGenerator{



	@Override
	public int getManufacturerId() {
		return 0x86; // 134
	}

	@Override
	public int getProductTypeId() {
		return 0x02;
	}

	@Override
	public int getProductId() {
		return 0x1C; // 28
	}

	@Override
	public String getTypeName() {
		return "aeotec_hem_2nd";
	}

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		addSensorElectricityChannel(uid, channelDefinitions, channelGroupDefinitions);
		addMeterChannel(uid, channelDefinitions, channelGroupDefinitions);
		
	}

	@Override
	public String getManufacturerName() {
		return "AeoTec";
	}

	@Override
	public String getProductName() {
		return "HEM";
		
	}

	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_METER;
	}
}
