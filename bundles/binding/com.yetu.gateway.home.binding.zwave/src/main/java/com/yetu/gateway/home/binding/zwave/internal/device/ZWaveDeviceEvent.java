package com.yetu.gateway.home.binding.zwave.internal.device;

public class ZWaveDeviceEvent {

	private DeviceEventType eventType;
	private ZWaveDeviceController devCntrl;
	
	public ZWaveDeviceEvent(DeviceEventType eventType, ZWaveDeviceController devCntrl){
		this.devCntrl = devCntrl;
		this.eventType = eventType;
	}
	
	public DeviceEventType getDeviceEventType(){
		return eventType;
	}
	
	public ZWaveDeviceController getDeviceController(){
		return devCntrl;
	}
		
	
	public enum DeviceEventType{
		DET_LOW_BATTERY,
		DET_OFFLINE,
		DET_ONLINE,
		DET_SLEEPING,
		DET_UNKNOWN_ERROR
	}
	
}
