package com.yetu.gateway.home.binding.zwave.internal.device.dlink;

import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericMeter;

public class DLinkEnergyClamp extends GenericMeter {
	
	
	
	public DLinkEnergyClamp(ZWaveNode node) {
		super(node);		
	}


	@Override
	protected void initZWaveNode() {
		// TODO Auto-generated method stub
		associateNode(01,01);
		associateNode(02,01);
			
	}


	@Override
	public void requestValues() {
		super.requestValues();
		
	}
}
