package com.yetu.gateway.home.binding.zwave.internal.thingtypes.dlink;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupType;
import org.eclipse.smarthome.core.thing.type.ChannelGroupTypeUID;
import org.eclipse.smarthome.core.thing.type.ChannelType;
import org.eclipse.smarthome.core.thing.type.ChannelTypeUID;
import org.eclipse.smarthome.core.types.StateDescription;
import org.eclipse.smarthome.core.types.StateOption;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class DLinkDCHZ110ThingTypeGenerator extends ZWaveSpecificThingTypeGenerator {


	
	
	
	protected void addSensorIlluminanceChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		
		String componentId = "illuminance";
		String componentType = "SENSOR";
		String capability = "MEASUREMENT";
	
		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
		
		Set<String> tags = new HashSet<String>();
		//tags.add("abstraction:"+componentId+":"+componentType.toUpperCase()+":"+capability.toUpperCase()+":unit=PERCENTAGE");
		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=PERCENTAGE");
		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"light sensor","Light", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
		
		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, getTypeName(),"light sensor", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
		groupDefs.add(gd);
	}
	
	
	@Override
	public int getManufacturerId() {
		
		return 0x108; // 264
	}

	@Override
	public int getProductTypeId() {
		return 0x02;
	}

	@Override
	public int getProductId() {
		return 0x0e; // 14
	}

	@Override
	public String getTypeName() {
		return "dlink_dch_z110";
	}

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		
		addBatteryChannel(uid, channelDefinitions, channelGroupDefinitions);
		addSensorContactChannel(uid, channelDefinitions, channelGroupDefinitions);
		addSensorTemperatureChannel(uid, channelDefinitions, channelGroupDefinitions);
		addSensorIlluminanceChannel(uid, channelDefinitions, channelGroupDefinitions);
	}

	@Override
	public String getManufacturerName() {
		return "D-Link";
	}

	@Override
	public String getProductName() {
		return "DCH Z110";
	}

	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_SENSOR_CONTACT;
	}
}
