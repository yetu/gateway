package com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro;

import java.util.List;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class FibaroDoorWindowContactThingTypeGenerator extends ZWaveSpecificThingTypeGenerator {

//	@Override
//	public ThingType generateThingType(ThingUID uid) {
//		List<ChannelDefinition> channelDefinitions = new ArrayList<>();
//		List<ChannelGroupDefinition> channelGroupDefinitions = new ArrayList<>();
//		
//		this.addSensorChannels(uid, channelDefinitions, channelGroupDefinitions);
//				
//		List<String> bridges = null;
//		
//		String label = "Fibaro Door Window Contact Sensor";
//		String description = "";
//		
//		return new ThingType(uid.getThingTypeUID(), bridges, label, description, channelDefinitions, channelGroupDefinitions, null);
//	}

	
	
	@Override
	public int getManufacturerId() {
		return 0x10f; // 271
	}

	@Override
	public int getProductTypeId() {
		return 0x700; // 2048
	}

	@Override
	public int getProductId() {
		return 0x1000; // 4096
	}

	@Override
	public String getTypeName() {
		return "fibaro_contact_sensor";
	}

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		addSensorContactChannel(uid, channelDefinitions, channelGroupDefinitions);
		//addSensorTemperatureChannel(uid,channelDefinitions, channelGroupDefinitions);
		addBatteryChannel(uid, channelDefinitions, channelGroupDefinitions);		
		
	}

	
	@Override
	public String getManufacturerName() {
		return "Fibaro";
	}

	@Override
	public String getProductName() {
		return "Door/Window Sensor";
	}
	
	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_SENSOR_CONTACT;
	}
}
