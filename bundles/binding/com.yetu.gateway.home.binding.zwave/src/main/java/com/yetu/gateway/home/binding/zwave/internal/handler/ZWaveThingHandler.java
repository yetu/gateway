package com.yetu.gateway.home.binding.zwave.internal.handler;

import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.ThingStatusInfo;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceEvent;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceEventListener;


public class ZWaveThingHandler extends BaseThingHandler implements CapabilityStateChangeListener, ZWaveDeviceEventListener{

	protected static Logger logger = LoggerFactory.getLogger(ZWaveThingHandler.class);
		
	public static final int STATE_CONFIGURING   = 0;
	public static final int STATE_READY			= 1;
	public static final int STATE_UNINITIALIZED	= 3;
	public static final int STATE_NO_NODE		= 4;
	
	protected ZWaveDeviceController devCntrl = null;
		
	public ZWaveThingHandler(Thing thing) {
		this(thing,null);	
	}
	
	public ZWaveThingHandler(Thing thing, ZWaveDeviceController devCntrl) {
		super(thing);
		//thing.setHandler(this);
		setThingStatus(ThingStatus.UNINITIALIZED);
		printThingDetails();
		if (devCntrl != null){
			setDeviceController(devCntrl);		
		} 
	}
	
	@Override
	public void initialize() {
		 setThingStatus(ThingStatus.INITIALIZING);
	}
	
	protected void setThingStatus(ThingStatus thingStatus ){
		setThingStatus(thingStatus,ThingStatusDetail.NONE,"");
	}
	
	protected void setThingStatus(ThingStatus thingStatus, ThingStatusDetail statusDetail, String description){
		logger.debug("setting thing status '{}' , '{}', '{}'",thingStatus, statusDetail, description);
		this.getThing().setStatusInfo(new ThingStatusInfo(thingStatus, statusDetail, description));
	}
	
	protected void setDeviceController(ZWaveDeviceController devCntrl){
		this.devCntrl = devCntrl;
		devCntrl.setCapabilityStateChangeListener(this);
		devCntrl.setDeviceEventListener(this);
		if (devCntrl.getLastActivity() == null){

			setThingStatus(ThingStatus.OFFLINE);
			//devCntrl.requestValues();
		} else {
			setThingStatus(ThingStatus.ONLINE);
		}
	}
	
	 @Override
	 public void handleUpdate(ChannelUID channelUID, State newState) {
		 logger.debug("handling update for {} with State {}",channelUID,newState);	
		 ChannelMapping channelMapping = getChannelMapping(channelUID);
			 
	     devCntrl.handleUpdate(channelMapping.getComponentId(), channelMapping.getComponentType(), channelMapping.getCapability(), newState);
		
	 }

	@Override
	public void handleCommand(ChannelUID channelUID, Command command) {
		logger.debug("handling command for {} with State {}",channelUID,command);
		
		
		
		ChannelMapping channelMapping = getChannelMapping(channelUID);
		
		if (channelMapping == null){
			logger.error("Unable to generate Channel Mapping out of ChannelUID {}",channelUID);
		}
		 if (devCntrl != null){
			 devCntrl.handleCommand(channelMapping.getComponentId(), channelMapping.getComponentType(), channelMapping.getCapability(), command);
		 } else {
			 logger.warn("no device controller added to handler");
		 }
	}
	
	@Override
	public void onCapabilityStateChange(String componentId, String capability,	State newState) {
		if (getThing().getStatus() != ThingStatus.ONLINE){
			setThingStatus(ThingStatus.ONLINE);
		}
		String channelId = componentId+"#"+capability.toLowerCase();
		Channel channel = getThing().getChannel(channelId);
		
		if (channel != null){
			this.updateState(channel.getUID(), newState);
		} else {
			logger.warn("thing {} does not have a channel {} but received capability update from assigned z-wave device controller",getThing().getUID(),channelId);
		}
	}
	
	private void printThingDetails(){
		Thing thing = getThing();
		String channelStr = "";
		
		
		for (Channel channel : thing.getChannels()){
			channelStr += "          "+channel.getUID().getId() +" ("+channel.getUID()+")"+"\r\n";
		}
		
		
		logger.debug(""+"\r\n"+
					 "------------------  Thing Details --------------------"+"\r\n"+
					 "     UID   = "+thing.getThingTypeUID()+"\r\n"+		
					 "    Status = "+thing.getStatusInfo()+"\r\n"+
					 "     "+"\r\n"+
					 "     capabilities"+"\r\n"+
					 ""+channelStr+"\r\n"+
					 "------------------------------------------------------");
	}
	
	
	private ChannelMapping getChannelMapping(ChannelUID channelUID){
		ChannelMapping mapping = null;
		if (getThing() != null){
			Channel channel = getThing().getChannel(channelUID.getId());
			if (channel != null){
				mapping = new ChannelMapping(channel);
			}
		}
		return mapping;
	}

	@Override
	public void onDeviceControllerEvent(ZWaveDeviceEvent event) {
		switch(event.getDeviceEventType()){
			case DET_ONLINE:
				setThingStatus(ThingStatus.ONLINE);
				break;
			case DET_OFFLINE:
				setThingStatus(ThingStatus.OFFLINE);
				break;
			default:
				break;
		}
		
	}
	

	
	
}
