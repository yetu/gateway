package com.yetu.gateway.home.binding.zwave.internal.device.dlink;

import java.math.BigDecimal;

import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.State;

import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.report.ZWaveReportMeter;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericSwitchBinary;

public class DLinkDCHZ210 extends GenericSwitchBinary{

	public DLinkDCHZ210(ZWaveNode node) {
		super(node);		
	}

	
	protected void initZWaveNode(){		
		associateNode(1, 1);		// on off		
		associateNode(2, 1);		// energy consumption		
		
	}
	
	@Override
	protected void updateOnValue(boolean value){
		logger.debug("updating on/off value {}",value);
		State newState;
		if (value){
			newState = OnOffType.ON;	
		} else {
			newState = OnOffType.OFF;
		}
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("socket","switchable", newState);
		}
	}
	
	protected void updateConsumptionValue(double value){	
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("consumption","measurement", new DecimalType(BigDecimal.valueOf(value)));
		}
	}
	
	protected void updatePowerValue(double value){
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("power","measurement", new DecimalType(BigDecimal.valueOf(value)));
		} 
	}
	
	
	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		super.evaluateReceivedNodeCmd(cmd);
		try {
			switch (cmd.getCommandClassKey()){
				case 0x32 : // COMMAND_CLASS_METER_REPORT
					evaluateReport(ZWaveReportFactory.generateMeterReport(cmd));
					break;	
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
		
	}
	
	protected void evaluateReport(ZWaveReportMeter report){
		
		double value;
		if (report.getPrecission() > 0){		
			value = (double)report.getValue()/(double)report.getPrecission();			
		} else {
			value = (double)report.getValue();
		}		
		switch (report.getScale()){
			case ZWaveReportMeter.SCALE_KWH:
			case ZWaveReportMeter.SCALE_KVAH:
				updateConsumptionValue(value*1000);				
				break;
			case ZWaveReportMeter.SCALE_W:
				updatePowerValue(value);
				break;
			default:
				
				break;
		}
		
	}
	
	@Override
	public void requestValues() {
		super.requestValues();
		// request meter values
	}
	
	
	
}
