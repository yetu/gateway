package com.yetu.gateway.home.binding.zwave.internal.handler;

import org.eclipse.smarthome.core.types.State;

public interface CapabilityStateChangeListener {
	public void onCapabilityStateChange(String componentId, String capability, State newState);
}
