package com.yetu.gateway.home.binding.zwave.internal.io;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.utils.logger.LogTag;
import com.domoone.utils.xml.XmlConvertionException;
import com.domoone.zwave.ZWaveCntrl;
import com.domoone.zwave.cmd.ZWaveCmdClassSpecification;
import com.domoone.zwave.event.ZWaveEventListener;
import com.yetu.gateway.home.binding.zwave.ZWaveBindingConstants;

public class ZWaveControlAccess {
	private static ZWaveControlAccess instance = null;
	private static Logger logger = LoggerFactory.getLogger(ZWaveControlAccess.class);
	
	ZWaveCmdClassSpecification zwaveSpec = null; 
	ZWaveCntrl zwaveCntrl = null;
	ZWaveEventListener zwaveEventListener = null;
	
	public synchronized static ZWaveControlAccess getInstance(){
		
		if (instance == null){
			instance = new ZWaveControlAccess();
		}
		
		return instance;
		
	}
	
	protected ZWaveControlAccess(){
		loadZWaveSpecifications();
		createZWaveController();
	}
	
	/**
	 * All z-wave device classes, parameters, etc. are stored in one specification file in an XML format. This file is not part of the binding for
	 * license reasons and can be loaded from http://z-wavesupport.sigmadesigns.com/modules/TSStart/. You need an developer account for this. 
	 */
	protected void loadZWaveSpecifications(){
		
		String filename = getDefaultZWaveSpecificationFilePath();
		logger.debug("Loading Z-Wave specification {}",filename);
		try {
			zwaveSpec = new ZWaveCmdClassSpecification(filename);
		} catch (IOException e) {
			logger.error("unable to load specification file",e);
		} catch (XmlConvertionException e) {
			logger.error("Unable to convert XML content to Z-Wave specification",e);
		}
	}
	
	/**
	 * return  the default file path where the z-wave specification file is located.  
	 * @return file path where the z-wave specification file is located (including the filename)
	 */
	public String getDefaultZWaveSpecificationFilePath(){
		String sep = System.getProperty("file.separator");
		String path = System.getProperty("user.dir")+sep+"conf"+sep+"spec"+sep+ZWaveBindingConstants.ZWAVE_SPECIFICATION_FILENAME;
		return path;
	}
	
	public ZWaveCntrl getZWaveCntrl(){
		return zwaveCntrl;
	}
	
	/**
	 * The libzwave supports the usage of any logging mechanism with the help of a wrapper. Thus, a wrapper for the log4j logger needs to be 
	 * submitted for getting internal logs out of the z-wave library. 
	 */
	protected void setLog4JLoggerToLibZwave(){
		com.domoone.utils.logger.Logger log = new com.domoone.utils.logger.Logger() {
			
			@Override
			public void log(LogTag tag, String message, Throwable throwable) {
				
				String msg = "libzwave "+ZWaveCntrl.getVersion()+" | "+message;
				
				  switch (tag){
				  case INFO: logger.info(msg); break;
				  case WARN: logger.warn(msg); break;
				  case DEBUG: logger.debug(msg); break;
				  case ERROR: 
					  if (throwable != null){
						  logger.error(msg,throwable);
					  } else {
						  logger.error(msg);
					  }
				  }
				
			}
			
			@Override
			public void log(LogTag tag, String message) {
				log(tag,message,null);
				
			}
		};
		
		ZWaveCntrl.setLogger(log);
		ZWaveCntrl.doLogging(true);
		
	}
	/**
	 * Creating a controller instance which represents the communication interface for all Z-Wave devices. The ZWaveCntrl comes with the libzwave.jar. 
	 */
	protected void createZWaveController(){
		// bind logger to ZWaveCntrl
		setLog4JLoggerToLibZwave();		
		
		// create instance of ZWaveCntrl
			
		if (zwaveSpec != null){
			zwaveCntrl = new ZWaveCntrl(zwaveSpec);
		} else {			
			logger.warn("Z-Wave controller will be initialized without Z-Wave specifications");
			zwaveCntrl = new ZWaveCntrl();
		}
			
				
	}
	


}
