package com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupType;
import org.eclipse.smarthome.core.thing.type.ChannelGroupTypeUID;
import org.eclipse.smarthome.core.thing.type.ChannelType;
import org.eclipse.smarthome.core.thing.type.ChannelTypeUID;
import org.eclipse.smarthome.core.thing.type.ThingType;
import org.eclipse.smarthome.core.types.StateDescription;
import org.eclipse.smarthome.core.types.StateOption;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class FibaroSmokeDetectorThingTypeGenerator extends ZWaveSpecificThingTypeGenerator {

//	@Override
//	public ThingType generateThingType(ThingUID uid) {
//		List<ChannelDefinition> channelDefinitions = new ArrayList<>();
//		List<ChannelGroupDefinition> channelGroupDefinitions = new ArrayList<>();
//		
//		this.addAlarmChannels(uid, channelDefinitions, channelGroupDefinitions);
//				
//		List<String> bridges = null;
//		
//		String label = "Fibaro Smoke Detector";
//		String description = "Smoke detector.";
//		
//		return new ThingType(uid.getThingTypeUID(), bridges, label, description, channelDefinitions, channelGroupDefinitions, null);
//	}

	private void addAlarmChannels(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		// generate alarm_cap for alarm
		StateDescription stateDescription = new StateDescription(new BigDecimal(0),new BigDecimal(1),new BigDecimal(1),"", true, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":alarm_cap");
		
		
		ChannelType ct = new ChannelType(ctuid,false,"Switch","alarm_cap","descr","Alarm", null,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition("alarm_cap",ct);
		

		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+":alarm");
		
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Alarm","Smoke Detector", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition("alarm",cgt);
		
		
		//channelDefs.add(def);
		groupDefs.add(gd);
	}
	
	@Override
	public int getManufacturerId() {
		return 0x10f; // 271
	}

	@Override
	public int getProductTypeId() {
		return 0xc00; // 3072
	}

	@Override
	public int getProductId() {
		return 0x1000; // 4096
	}

	@Override
	public String getTypeName() {
		return "fibaro_smoke_detector";
	}

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getManufacturerName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProductName() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public String getMainComponentId() {
		return "alarm";
	}
	
}
