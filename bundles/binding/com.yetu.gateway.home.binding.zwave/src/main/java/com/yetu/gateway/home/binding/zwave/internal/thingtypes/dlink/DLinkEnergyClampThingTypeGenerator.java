package com.yetu.gateway.home.binding.zwave.internal.thingtypes.dlink;

import java.util.List;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class DLinkEnergyClampThingTypeGenerator extends ZWaveSpecificThingTypeGenerator{

	@Override
	public int getManufacturerId() {
		return 0x108; // 264
	}

	@Override
	public int getProductTypeId() {
		return 0x06;  // 06
	}

	@Override
	public int getProductId() {
		return 0x1A; // 26
	}

	@Override
	public String getTypeName() {
		return "dlink_energyclamp";
	}

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		addSensorElectricityChannel(uid, channelDefinitions, channelGroupDefinitions);
		addMeterChannel(uid, channelDefinitions, channelGroupDefinitions);
		addBatteryChannel(uid, channelDefinitions, channelGroupDefinitions);
	}

	@Override
	public String getManufacturerName() {
		return "D-Link";
	}

	@Override
	public String getProductName() {
		return "Energy Clamp";
		
	}

	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_METER;
	}
}
