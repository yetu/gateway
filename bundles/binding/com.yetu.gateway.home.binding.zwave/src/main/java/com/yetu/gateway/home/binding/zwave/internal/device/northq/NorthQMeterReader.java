package com.yetu.gateway.home.binding.zwave.internal.device.northq;

import java.math.BigDecimal;
import java.util.Date;

import org.eclipse.smarthome.core.library.types.DecimalType;

import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportConfiguration;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericMeter;

public class NorthQMeterReader extends GenericMeter{

	
	
	
	public NorthQMeterReader(ZWaveNode node) {
		super(node);		
	}
	
	
	@Override
	protected void initZWaveNode() {
		associateNode(01,01);
		associateNode(02,01);
		getZWaveNode().sendData(cmdFactory.generateCmd_Time_Parameters_Set(new Date()));
		setWakeUpInterval(60*15); // 15 minutes are the min for this device	
		
		// TODO: Attention. impulse factor needs to be set by user according 
		setImpulseFactor(10000);
		setMeterType(1);
	}
	
	@Override
	protected void updateUsageValue(double value){	
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("meter","measurement", new DecimalType(BigDecimal.valueOf(value)));
		}
	}
	
	@Override
	protected void updateConsumptionValue(double value){
		//
	}
	
	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		super.evaluateReceivedNodeCmd(cmd);
		try {
			switch (cmd.getCommandClassKey()){
			case 0x70: // COMMAND_CLASS_CONFIGURATION
				if (cmd.getCommandKey() == 0x06){ // REPORT
					evalConfigurationReport(ZWaveReportFactory.generateConfigurationReport(cmd));
				}
				break;
			case 0x84:  // COMMAND_CLASS_WAKEUP
				if (cmd.getCommandKey() == 0x07){ // Wakeup Notification
					requestMeterData();
				}
				break;

				default:
					break;
			}	
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
		
	}
	
	protected void evalConfigurationReport(ZWaveReportConfiguration confRep){
		int paramId = confRep.getParamId();
		int value = confRep.getValue();
		logger.debug("received config parameter {} = {}",paramId,value);
		switch (paramId){
		case 1: 
			logger.debug("received impulse factor {}",value);			
			break;
		case 9:
			logger.debug("received value offset {}",value);		
			break;
		case 2:
			logger.debug("received meter type {}",value);
			break;
		case 12:
			logger.debug("received serial number {}",transformSerialNumber(value));			
			default : 
				break;
		
		}
	}
	
	protected String transformSerialNumber(int value){
		String sn = Integer.toHexString(value);
		while (sn.length() < 8){
			sn = "0"+sn;
		}
		return sn;
	}

	protected void requestMeterData(){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Meter_Get_V2((byte)0));
	}
	
	protected void requestImpulseFactor(){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Get(1));
	}
	
	protected void requestOffset(){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Get(9));
	}
	
	protected void requestMeterType(){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Get(2));
	}
	
	protected void requestSerialNumber(){
		logger.debug("requesting serial number (12)");		
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Get(12));
	}
	
	/**
	 * The parameter value when real-time mode was started and ended. A configuration report containe the number of seconds * 20
	 * left until the real-time mode is over. A configuration report with value 0 notifies that the power reader is going to normal mode.
	 * The wake up no more information has no effect in real-time mode.In order to tell the Power REader to go to sleep from real-time mode,
	 * set this parameter to value 0.
	 */
	protected void requestRealTimeRegister(){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Get(11));
	}
	
	/**
	 * Sets the amount of impulses for one kWh. Read the value at the meter. 
	 * @param impulseFactor
	 */
	protected void setImpulseFactor(int impulseFactor){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set(1, ZWaveCmdParamType.BIT_32, impulseFactor));
		requestImpulseFactor();
	}
	
	/**
	 * Sets the total impulse count. If you want to set the total meter consumption (number on the meter) then use
	 * total_meter_consumption*10*impulseFactor to set the correct offset.
	 * @param offset
	 */
	protected void setOffset(int offset){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set(9, ZWaveCmdParamType.BIT_32, offset));
		this.requestOffset();
	}
	
	/**
	 * sets meter type to electric or mechanical
	 * @param meterType
	 */
	protected void setMeterType(int meterType){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set(2, ZWaveCmdParamType.BYTE, meterType));
		this.requestMeterType();
	}
	
	
	
	/**
	 * Set this register to 0 in order to make the Power Reader start automatic calibration when placed on electricity mechanical meters.
	 * This Register is not used on Gas Readers.
	 * 
	 * @param threshold
	 */
	protected void setFilterInhibitThreshold(int threshold){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set(7, ZWaveCmdParamType.BYTE, threshold));
	}
	
	/**
	 * At every wake up notification, the Power REader stays alive for 3 seconds. If the controlling node can't guarantee a response
	 * for the Power REader within 3 seconds, then this parameter should be set to the number of seconds that it will take the controlling 
	 * node to send a response .
	 * @param seconds for staying alive after wake up notification
	 */
	protected void setKeepAlivePeriod(int seconds){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set(10, ZWaveCmdParamType.WORD, seconds));
	}
	
	protected void stopRealTimeMode(){
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set(11, ZWaveCmdParamType.WORD, 0));
	}
	
	
	@Override
	public void requestValues() {
//		requestImpulseFactor();
//		requestMeterType();
//		requestOffset();
//		requestSerialNumber();
//		
		
	}


}
