package com.yetu.gateway.home.binding.zwave.internal.thingtypes;

import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ThingType;

import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.ZWaveBindingConstants;

public class ZWaveGenericThingTypeGenerator implements ZWaveThingTypeGenerator{

	public ThingUID generateThingUID(ZWaveNode node){		
		return new ThingUID(ZWaveBindingConstants.ZWAVE_BINDING_ID,ZWaveBindingConstants.GENERIC_DEVICE_TYPE,""+node.getNodeId());
	}
	
	public ThingTypeUID getThingTypeUID(){
		return new ThingTypeUID(ZWaveBindingConstants.ZWAVE_BINDING_ID,ZWaveBindingConstants.GENERIC_DEVICE_TYPE);
	}

	@Override
	public ThingType generateThingType(ThingUID uid) {
		// TODO Auto-generated method stub
		return null;
	}
}
