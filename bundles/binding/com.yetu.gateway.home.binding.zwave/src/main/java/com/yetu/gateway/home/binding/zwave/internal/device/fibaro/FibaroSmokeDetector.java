package com.yetu.gateway.home.binding.zwave.internal.device.fibaro;

import org.slf4j.LoggerFactory;

import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericSensorAlarm;

public class FibaroSmokeDetector extends GenericSensorAlarm {

	public FibaroSmokeDetector(ZWaveNode node) {
		super(node);
		logger = LoggerFactory.getLogger(FibaroSmokeDetector.class);
	}

}
