package com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro;

import java.util.List;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class FibaroMotionDetectorThingTypeGenerator extends ZWaveSpecificThingTypeGenerator {


	
	@Override
	public int getManufacturerId() {
		return 0x10f; // 271
	}

	@Override
	public int getProductTypeId() {
		return 0x800; // 2048
	}

	@Override
	public int getProductId() {
		return 0x1001; // 4097
	}

	@Override
	public String getTypeName() {
		return "fibaro_motion_detector";
	}

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		
		addSensorMotionChannel(uid,channelDefinitions, channelGroupDefinitions);
		addSensorTemperatureChannel(uid, channelDefinitions, channelGroupDefinitions);
		addSensorIlluminanceChannel(uid, channelDefinitions, channelGroupDefinitions);
		addBatteryChannel(uid, channelDefinitions, channelGroupDefinitions);
	}

	@Override
	public String getManufacturerName() {
		return "Fibaro";
	}

	@Override
	public String getProductName() {
		return "Motion Sensor";
	}
	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_SENSOR_MOTION;
	}
}