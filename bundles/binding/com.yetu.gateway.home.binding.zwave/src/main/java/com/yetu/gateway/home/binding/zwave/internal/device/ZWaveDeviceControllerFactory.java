package com.yetu.gateway.home.binding.zwave.internal.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.aeotec.AeotecHomeEnergyMeter;
import com.yetu.gateway.home.binding.zwave.internal.device.dlink.DLinkDCHZ110;
import com.yetu.gateway.home.binding.zwave.internal.device.dlink.DLinkDCHZ120;
import com.yetu.gateway.home.binding.zwave.internal.device.dlink.DLinkDCHZ210;
import com.yetu.gateway.home.binding.zwave.internal.device.dlink.DLinkEnergyClamp;
import com.yetu.gateway.home.binding.zwave.internal.device.fibaro.FibaroDoorWindowSensor;
import com.yetu.gateway.home.binding.zwave.internal.device.fibaro.FibaroMotionDetector;
import com.yetu.gateway.home.binding.zwave.internal.device.fibaro.FibaroSmokeDetector;
import com.yetu.gateway.home.binding.zwave.internal.device.fibaro.FibaroWallplug;
import com.yetu.gateway.home.binding.zwave.internal.device.northq.NorthQMeterReader;
import com.yetu.gateway.home.binding.zwave.internal.device.yale.YaleDoorLock;

public class ZWaveDeviceControllerFactory {
	
	protected static Logger logger = LoggerFactory.getLogger(ZWaveDeviceControllerFactory.class);

	public static ZWaveDeviceController generateDeviceController(ZWaveNode node){
		
		logger.debug("");
		logger.debug("----------- Device Generation ------------");
		if (node.getGenericDeviceType() != null){
			logger.debug("   generic device type : 0x"+Integer.toHexString(node.getGenericDeviceType().getKey()));
		} else {
			logger.debug("   generic device type : UNKNOWN");
		}
		logger.debug("        manufacture ID : 0x"+Integer.toHexString(node.getManufactureId()));
		logger.debug("       product type ID : 0x"+Integer.toHexString(node.getProductTypeId()));
		logger.debug("            product ID : 0x"+Integer.toHexString(node.getProductId()));
		logger.debug("");
		
		
		ZWaveDeviceController devCntrl = null;
		if (node.getManufactureId() >= 0){
			switch (node.getManufactureId()){
			
			case 0x02: // Danfoss
				//devCntrl = generateDanfossDevice(node);
				break;
			case 0x86:	// AEON Labs
				devCntrl = generateAeonTecDeviceCntrl(node);
				break;
			case 0x60: // Everspring
				//devCntrl = generateEverspringDevice(node);
				break;
			case 0x96: // NorthQ
				devCntrl = generateNorthQDeviceCntrl(node);
				break;
			case 0x108: // D-Link
				devCntrl = generateDLinkDeviceCntrl(node);
				break;
			case 0x109: // Vision Security
				//devCntrl = generateVisionDevice(node);
				break;
			case 0x10F: // Fibaro
				devCntrl = generateFibaroDeviceCntrl(node);
				break;	
			case 0x129: // AssaAbloy - Yale
				devCntrl = generateYaleDeviceCntrl(node);
				break;
			default:				
					break;
			}
		} 			
		if (devCntrl != null){
			logger.debug("created device controller of class {}",devCntrl.getClass().getName());
		} else {
			logger.warn("unable to create device controller for node {}",node.getNodeId());
		}
		
		return devCntrl;
	}
	
	
	// ++++++++++++++++++++++++++ NorthQ +++++++++++++++++++++++++++++++++
	
	
	protected static ZWaveDeviceController generateNorthQDeviceCntrl(ZWaveNode node){
		switch (node.getProductTypeId()){
			case 0x01:
				if (node.getProductId() == 0x01){
					return generateNorthQMeterReader(node);
				}
				break;
			default:
				break;
		}
		return null;
	}
	
	protected static ZWaveDeviceController generateNorthQMeterReader(ZWaveNode node){
		ZWaveDeviceController devCntrl = new NorthQMeterReader(node);
		return devCntrl;
	}
	
	// -------------------------- NorthQ ---------------------------------
	// ++++++++++++++++++++++++++ D-Link +++++++++++++++++++++++++++++++++
	
	protected static ZWaveDeviceController generateDLinkDeviceCntrl(ZWaveNode node){
		ZWaveDeviceController devCntrl = null;
		
		switch(node.getProductTypeId()){
		case 0x01:
			if (node.getProductId() == 0x11){
				devCntrl = generateDLinkDCHz210(node);
			}
		case 0x02:
			if (node.getProductId() == 0x0e){
				devCntrl = generateDLinkDCHz110(node);
			}
			if (node.getProductId() == 0x0d){
				devCntrl = generateDLinkDCHz120(node);
			}
		case 0x06:
			if (node.getProductId() == 0x1A){
				devCntrl = generateDLinkEnergyClamp(node);
			}
		default:
			break;
	}
		
		return devCntrl;
	}
	
	protected static ZWaveDeviceController generateDLinkEnergyClamp(ZWaveNode node){
		ZWaveDeviceController devCntrl = new DLinkEnergyClamp(node);
		return devCntrl;
	}
	

	protected static ZWaveDeviceController generateDLinkDCHz210(ZWaveNode node){
		ZWaveDeviceController devCntrl = new DLinkDCHZ210(node);
		return devCntrl;
	}
	
	protected static ZWaveDeviceController generateDLinkDCHz110(ZWaveNode node){
		ZWaveDeviceController devCntrl = new DLinkDCHZ110(node);
		return devCntrl;
	}
	
	protected static ZWaveDeviceController generateDLinkDCHz120(ZWaveNode node){
		ZWaveDeviceController devCntrl = new DLinkDCHZ120(node);
		return devCntrl;
	}
	// -------------------------- D-Link
	// ++++++++++++++++++++++++++ AEOTEC +++++++++++++++++++++++++++++++++
	protected static ZWaveDeviceController generateAeonTecDeviceCntrl(ZWaveNode node){
		ZWaveDeviceController devCntrl = null;
		switch(node.getProductTypeId()){
			case 0x02:
				if (node.getProductId() == 0x05){
					//dev = generateAL_DSB05_ZWEU(node);
				}
				if (node.getProductId() == 0x1C){
					devCntrl = generateAeotecHomeEnergyMeter(node);
				}
				break;
			default:
				break;
		}
		return devCntrl;
	}
	
	protected static ZWaveDeviceController generateAeotecHomeEnergyMeter(ZWaveNode node){
		ZWaveDeviceController devCntrl = new AeotecHomeEnergyMeter(node);
		return devCntrl;
	}

	// -------------------------- AEOTEC --------------------------------
	// ++++++++++++++++++++++++++ FIBARO +++++++++++++++++++++++++++++++++
	
	protected static ZWaveDeviceController generateFibaroDeviceCntrl(ZWaveNode node){		
		ZWaveDeviceController devCntrl = null;
		switch(node.getProductTypeId()){
			case 0x600:
					if (node.getProductId() == 0x1000){
						devCntrl = generateFibaroWallPlug(node);
					}
				break;
			case 0xb00:
				if (node.getProductId() == 0x1001){
				//	devCntrl = generateFibaroFloodSensor(node);
				}
				break;
			case 0x800:
				if (node.getProductId() == 0x1001){
					devCntrl = generateFibaroMotionDetector(node);
				}
			case 0x700:
				if (node.getProductId() == 0x1000){
					devCntrl = generateFibaroDoorWindowSensor(node);
				}
				break;
			case 0xC00:
				if (node.getProductId() == 0x1000){
					devCntrl = generateFibaroSmokeSensor(node);
				}
			default:
						break;
					
		}
		return devCntrl;
	}
	
	protected static ZWaveDeviceController generateFibaroWallPlug(ZWaveNode node){
		ZWaveDeviceController devCntrl = new FibaroWallplug(node);
		return devCntrl;
	}

	protected static ZWaveDeviceController generateFibaroSmokeSensor(ZWaveNode node){
		ZWaveDeviceController devCntrl = new FibaroSmokeDetector(node);
		return devCntrl;
	}
	
	protected static ZWaveDeviceController generateFibaroMotionDetector(ZWaveNode node){
		ZWaveDeviceController devCntrl = new FibaroMotionDetector(node);
		return devCntrl;
	}
	
	protected static ZWaveDeviceController generateFibaroDoorWindowSensor(ZWaveNode node){
		ZWaveDeviceController devCntrl = new FibaroDoorWindowSensor(node);
		return devCntrl;
	}
	
	
	// +++++++++++++++++++++++++++++ Assa Abloy  / Yale +++++++++++++++++++++++++++++++++++++++
	
	
	protected static ZWaveDeviceController generateYaleDeviceCntrl(ZWaveNode node){
		ZWaveDeviceController devCntrl = null;
		switch(node.getProductTypeId()){
		case 0x06:
			if (node.getProductId() == 0x00){
				devCntrl = generateYaleDoorLock(node);
			}
		}
		return devCntrl;
	}

	
	protected static ZWaveDeviceController generateYaleDoorLock(ZWaveNode node){
		ZWaveDeviceController devCntrl = new YaleDoorLock(node);
		return devCntrl;
	}
}
