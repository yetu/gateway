package com.yetu.gateway.home.binding.zwave.internal.device;


import java.util.Date;

import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.ZWaveNodeCmdFactory;
import com.domoone.zwave.cmd.report.ZWaveReportBattery;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.report.ZWaveReportWakeUpInterval;
import com.domoone.zwave.event.ZWaveNodeDataEvent;
import com.domoone.zwave.event.ZWaveNodeEvent;
import com.domoone.zwave.event.ZWaveNodeEventListener;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceEvent.DeviceEventType;
import com.yetu.gateway.home.binding.zwave.internal.handler.CapabilityStateChangeListener;
import com.yetu.gateway.home.binding.zwave.internal.handler.CommandProcessor;


/**
 * 
 * Controlling a Z-Wave device should be independent from handling the device. Thus, there exist ZWaveThingHandler and 
 * ZWaveDeviceController. This is important to prevent dependencies between the bundles. Every Z-Wave device could be initialized
 * without having the Thing-Registry up and running and vice versa. 
 * 
 * @author Mathias Runge
 *
 */
public abstract class ZWaveDeviceController implements CommandProcessor{
	
	protected Logger logger = LoggerFactory.getLogger(ZWaveDeviceController.class);
	
	protected boolean wakeupIntervalSet = false;
	protected Date lastBatteryRequest = null;
	protected int state;
	protected Date lastActivity = null;
	protected CapabilityStateChangeListener capabilityStateChangeListener = null;
	protected ZWaveDeviceEventListener eventListener = null;
	
	protected final static int DEFAULT_WAKEUP_INTERVAL = 900000; //15 min
	
	protected ZWaveNode zwaveNode = null;
	protected ZWaveNodeCmdFactory cmdFactory = null;
	
	public ZWaveDeviceController(ZWaveNode node){
		this.zwaveNode = node;
		node.addEventListener(new ZWaveNodeEventListener() {
			
			@Override
			public void onNodeEvent(ZWaveNodeEvent event) {
				evaluateNodeEvent(event);				
			}
		});			
	}
	
	public void setCapabilityStateChangeListener(CapabilityStateChangeListener listener){
		this.capabilityStateChangeListener = listener;
	}
	
	protected synchronized void evaluateNodeEvent(ZWaveNodeEvent event){
		
		switch (event.getEventType()){	
			case NODE_EVENT_STATUS_CHANGED:
				// onStatusChanged();
			case NODE_EVENT_WAKEUP:
				requestBatteryStateIfNeeded();
				
				//dispatchWakeUpEvent();
				if (getZWaveNode().supportsClassWakeUp()){
					zwaveNode.setNodeToSleep();
				}
				break;
			case NODE_EVENT_SLEEP:
				//dispatchSleepEvent();
				break;
			case NODE_EVENT_DATA_RECEIVED:
					if (event instanceof ZWaveNodeDataEvent){
						ZWaveNodeCmd cmd = ((ZWaveNodeDataEvent)event).getNodeCmd();
						checkInternalReceivedNodeCmd(cmd);
						evaluateReceivedNodeCmd(cmd);
					}
				break;
			default:
				break;
		}		
		setLastActivity();				
	}	
	
	protected void setLastActivity(){
		logger.debug("updating last activity");
		boolean noActivityYet = false;
		if (lastActivity == null){
			noActivityYet = true;
		}
		lastActivity = new Date();
		if (noActivityYet){
			dispatchDeviceEvent(new ZWaveDeviceEvent(DeviceEventType.DET_ONLINE, this));
		}
	}
	
	public Date getLastActivity(){
		return lastActivity;
	}
	
	public ZWaveNode getZWaveNode(){
		return zwaveNode;
	}
	
	protected boolean hasZWaveNode(){
		return (zwaveNode != null);
	}

	protected void setState(int newState){
		if (newState != state){
			state = newState;
		}
	}
	
	public void setCmdFactory(ZWaveNodeCmdFactory cmdFactory){
		this.cmdFactory = cmdFactory;
	}

	public void setDeviceEventListener(ZWaveDeviceEventListener listener){
		this.eventListener = listener;
	}
	
	/**
	 * Init will be called after a new z-wave node has been added (not restored). The abstract method initZWaveNode() will be called
	 * and must be implemented by each device controller.
	 */
	public void init(){	
		initZWaveNode();
		requestValues();
		if (zwaveNode.supportsClassWakeUp()){
			if (!isWakeupIntervalSet()){				
				setDefaultWakeupInterval();
				requestWakeupInterval();
			}
		}	
		requestBatteryStateIfNeeded();
	}
	
	protected void setWakeUpInterval(int seconds){	
		logger.debug("updating wakeup interval {}",seconds);
		getZWaveNode().sendData(cmdFactory.generateCmd_WakeUpInterval_Set(seconds,1));
		this.wakeupIntervalSet = true;		
	}
	
	protected void setDefaultWakeupInterval(){
		setWakeUpInterval(DEFAULT_WAKEUP_INTERVAL);
	}
	
	protected void requestBatteryState(){
		lastBatteryRequest = new Date();
		logger.debug("requesting battery state");
		getZWaveNode().sendData(cmdFactory.generateCmd_Battery_Get());
	}

	
	protected void associateNode(int group, int nodeId){		
		logger.debug("sending association (group = {}, node = {})",group,nodeId);
		getZWaveNode().sendData(cmdFactory.generateCmd_Association_Set(group, nodeId));
	}
	
	protected void requestWakeupInterval(){
		logger.debug("requesting wakeup interval");
		getZWaveNode().sendData(cmdFactory.generateCmd_WakUpInterval_Get());		
	}
	
	
	
	protected boolean isWakeupIntervalSet(){
		return wakeupIntervalSet;
	}
	
	protected void processNodeEvent(ZWaveNodeEvent event){
		switch (event.getEventType()){	
			case NODE_EVENT_STATUS_CHANGED:
				// onStatusChanged();
			case NODE_EVENT_WAKEUP:
				requestBatteryStateIfNeeded();
				//dispatchWakeUpEvent();
				if (getZWaveNode().supportsClassWakeUp()){
					zwaveNode.setNodeToSleep();
				}
				break;
			case NODE_EVENT_SLEEP:
				//dispatchSleepEvent();
				break;
			case NODE_EVENT_DATA_RECEIVED:
				if (event instanceof ZWaveNodeDataEvent){
					ZWaveNodeCmd cmd = ((ZWaveNodeDataEvent)event).getNodeCmd();
					checkInternalReceivedNodeCmd(cmd);
					evaluateReceivedNodeCmd(cmd);
				}
			default: 
				break;
		}
				
	}
	
	protected void checkInternalReceivedNodeCmd(ZWaveNodeCmd cmd){
		try {
			switch (cmd.getCommandClassKey()){
			case 0x80:	// COMMAND_CLASS_BATTERY
				if (cmd.getCommandKey() == 0x03){
					evaluateBatteryReport(cmd);	
				}				
				break;
			case 0x84:	// COMMAND_CLASS_WAKEUP
				if (cmd.getCommandKey() == 0x06){
					evaluateWakeUpIntervalReport(cmd);
				}
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
	}
	
	protected void evaluateWakeUpIntervalReport(ZWaveNodeCmd nodeCmd){
		try {
			ZWaveReportWakeUpInterval intRep = ZWaveReportFactory.generateWakeUpIntervalReport(nodeCmd);
			this.updateWakeupInterval(intRep.getInterval());
		} catch (Exception exc){
			exc.printStackTrace();
		}
	}
	
	protected void evaluateBatteryReport(ZWaveNodeCmd nodeCmd){
		try {
			ZWaveReportBattery batRep = ZWaveReportFactory.generateBatteryReport(nodeCmd);
			this.updateBattery(batRep.getBattery());
		} catch (Exception exc){
			exc.printStackTrace();
		}
	}
	
	protected void updateBattery(int battery){
		logger.debug("updating battery value {}",battery);
		if (capabilityStateChangeListener != null){			
			capabilityStateChangeListener.onCapabilityStateChange("battery","measurement", new DecimalType(battery));
		}
	}
	
	protected void updateWakeupInterval(int interval){
		logger.debug("updating wakeup interval {}",interval);
	}
	
	protected void sendConfiguration(int paramId, ZWaveCmdParamType type, int value){
		logger.debug("sending configuration to device paramId = {}, cmdType = {}, value = {}",paramId,type.name(),value);
		this.getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set(paramId, type, value));
	}
	
	
	protected void requestBatteryStateIfNeeded(){
		if (getZWaveNode().supportsClassBattery()){	
			if (lastBatteryRequest == null){
				this.requestBatteryState();
				return;
			}
			Date now = new Date();
			// if last request was 24h and more ago;
			if (now.getTime() - lastBatteryRequest.getTime() > 1000 * 60 * 60 * 24){
				this.requestBatteryState();
			}
		}
	}
	
	protected void onCapabilityChange(String componentId, String capability, State newState){
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange(componentId, capability, newState);
		}
	}
	
	protected void dispatchDeviceEvent(ZWaveDeviceEvent event){
		if (eventListener != null){
			eventListener.onDeviceControllerEvent(event);
		}
	}
	
	abstract protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd);
	abstract protected void initZWaveNode();
	public abstract void requestValues();
	
}
