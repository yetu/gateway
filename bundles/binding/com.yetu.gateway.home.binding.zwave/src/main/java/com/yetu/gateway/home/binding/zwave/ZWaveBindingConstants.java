package com.yetu.gateway.home.binding.zwave;

import org.eclipse.smarthome.core.thing.ThingTypeUID;


public class ZWaveBindingConstants {
	public static final String ZWAVE_BINDING_ID = "yetuzwave";
	public static final String ZWAVE_SPECIFICATION_FILENAME = "zwave_cmd_classes.xml";
	public static final int	   ZWAVE_NODE_ID_DIGITS = 3;

	public static final ThingTypeUID 		ZWAVE_GENERIC_THING_TYPE = new ThingTypeUID(ZWAVE_BINDING_ID,"generic");
	
//	public final static Set<ThingTypeUID> 	SUPPORTED_THING_TYPES_UIDS =ImmutableSet.of(ZWAVE_GENERIC_THING_TYPE);
	
	public final static String SERIAL_PORT = "port";
	
	public final static int DEFAULT_DISCOVERY_TIMEOUT = 300;
	
	public final static String MANUFACTURE_ID 			= "manufacture_id";
	public final static String PRODUCT_ID 				= "product_id";
	public final static String PRODUCT_TYPE_ID 			= "product_type_id";
	public final static String GENERIC_DEVICE_TYPE 		= "generic_device_type";
	public final static String SPECIFIC_DEVICE_TYPE 	= "specific_device_type";
}
