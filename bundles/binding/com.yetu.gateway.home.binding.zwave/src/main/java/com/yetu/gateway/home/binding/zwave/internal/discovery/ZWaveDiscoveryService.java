package com.yetu.gateway.home.binding.zwave.internal.discovery;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.config.discovery.AbstractDiscoveryService;
import org.eclipse.smarthome.config.discovery.DiscoveryResult;
import org.eclipse.smarthome.config.discovery.DiscoveryResultBuilder;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.ZWaveCntrl;
import com.domoone.zwave.ZWaveControllerMode;
import com.domoone.zwave.ZWaveException;
import com.domoone.zwave.event.ZWaveEvent;
import com.domoone.zwave.event.ZWaveEventListener;
import com.domoone.zwave.event.ZWaveNodeEvent;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.ZWaveBindingConstants;
import com.yetu.gateway.home.binding.zwave.internal.io.ZWaveControlAccess;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveThingTypeFactory;
import com.yetu.gateway.home.binding.zwave.internal.utils.ZWaveBindingUtils;


/**
 * Discovery Service sets the connected Z-Wave controller into inclusion mode, waiting for devices to be paired. Discovery mode will be automatically
 * stopped after one device was paired. To change this, please change the underlying libzwave behavior. 
 * 
 * @author Mathias Runge (initial contribution)
 *
 */
public class ZWaveDiscoveryService  extends AbstractDiscoveryService{

	private final Logger logger = LoggerFactory.getLogger(ZWaveDiscoveryService.class);

	private ZWaveCntrl cntrl;
	
	public ZWaveDiscoveryService() {
		super(ZWaveBindingConstants.DEFAULT_DISCOVERY_TIMEOUT);
		setZWaveController(ZWaveControlAccess.getInstance().getZWaveCntrl());
	}
	
	
	/**
	 * The ZWaveDiscoveryService only works with the libzwave. Thus, a special controller instance need to be
	 * assign to this service whereas this service do not initialize the controller. 
	 * @param controller Same z-wave controller instance which will also be used by ZWaveThingHandlerFactory
	 */
	public void setZWaveController(ZWaveCntrl controller){
		this.cntrl = controller;
		// disable the timeout on controller side because ESH is handling timeout
		cntrl.setControllerModeTimeout(0);
		// add control listener to be informed when new nodes are added (and other events)
	
		cntrl.addCntrlListener(new ZWaveEventListener() {			
			@Override
			public void onZWaveEvent(ZWaveEvent event) {
				processZWaveEvent(event);
				
			}
		});
	}
	
	/**
	 * Starts a z-wave discovery by setting the z-wave controller to inclusion mode. 
	 */
	@Override
	protected void startScan(){		
		logger.debug("start scan ...");
		// start ZWave discovery
		if (cntrl != null){			
			if (cntrl.isInitialized()) {
				// do a manufacture interview, but do not request all command class versions
				cntrl.setInclusionMode(true, false);
			} else {
				logger.error("Unable to start scan for Z-Wave. Z-Wave controller is not initialized. (mode = "+cntrl.getControllerMode().name()+")");
				scanListener.onErrorOccurred(new ZWaveException("Z-Wave Controller is not initialized. Starting discovery failed!"));
			}
		} else {
			if (scanListener != null){
				logger.error("Unable to start scan for Z-Wave. Z-Wave controller was not injected yet");
				scanListener.onErrorOccurred(new ZWaveException("Z-Wave Controller was not injected for ZWaveDiscoveryService"));
			}
		}
	}


	/**
	 * Stops the scan mode and internally sets the z-wave controller back to normal mode. 
	 */
	@Override	
    protected void stopScan() {
		logger.debug("stopping the scan ...");		
		if (cntrl != null){
			// stopping ZWave discovery
			cntrl.setNormalMode();
		}
    	super.stopScan();
    }
	

	/**
	 * Aborts a z-wave discovery session and set the z-wave controller back to normal mode.
	 */
	@Override
	 public synchronized void abortScan() {
		logger.debug("aborting scan");
		super.abortScan();
		if (cntrl != null){
			if (cntrl.isInitialized()){
				cntrl.setNormalMode();
			}
		}
    }
	
	/**
	 * ZWaveDiscoveryService and ZWaveThingHandlerFactory are using the ZWaveThingTypeGeneratorRepository for reporting the supported thing types.
	 * see ZWaveThingTypeGeneratorRepository for further details.  
	 */
	@Override
	public Set<ThingTypeUID> getSupportedThingTypes() {
		return ZWaveThingTypeFactory.getInstance().getSupportedThingTypeUIDs();		
	}
	
	/**
	 * report a new added z-wave node as discovery result. Manufacturer details and device details will be stored in configuration properties and
	 * are needed for the ZWaveThingHandlerFactory
	 * @param node
	 */
	private void submitDiscoveryResults(ZWaveNode node){	
		logger.debug("submitting discovery result ("+node.getNodeId()+","+node.getManufactureId()+","+node.getProductTypeId()+","+node.getProductId()+")");
		
		String nodeId = ZWaveBindingUtils.getNumberString(node.getNodeId(),ZWaveBindingConstants.ZWAVE_NODE_ID_DIGITS);
		
		ThingUID uid = ZWaveThingTypeFactory.getInstance().generateThingUID(node);
		
			
		logger.debug("generated id is "+uid);
		
		Map<String, Object> properties = new HashMap<>();
		
		properties.put(ZWaveBindingConstants.MANUFACTURE_ID ,node.getManufactureId());
		properties.put(ZWaveBindingConstants.PRODUCT_TYPE_ID,node.getProductTypeId());
		properties.put(ZWaveBindingConstants.PRODUCT_ID,node.getProductId());	
		if (node.getGenericDeviceType() != null){
			properties.put(ZWaveBindingConstants.GENERIC_DEVICE_TYPE,node.getGenericDeviceType().getName());
		}
		if (node.getSpecificDeviceType() != null){
			properties.put(ZWaveBindingConstants.SPECIFIC_DEVICE_TYPE,node.getSpecificDeviceType().getName());
		}
		DiscoveryResult result = DiscoveryResultBuilder.create(uid)
				.withProperties(properties)
				.withLabel("new Z-Wave device ("+nodeId+")")
				.build();
		
		thingDiscovered(result);
	}
	
	/**
	 * processes incoming Z-Wave events from the Z-Wave controller. This method will also be called, when a new z-wave node was added
	 * or the interview (receiving manufacturer details) was finished.  
	 * @param event
	 */
	protected void processZWaveEvent(ZWaveEvent event){
		ZWaveNode node;
		switch (event.getEventType()){
		case NODE_EVENT_NODE_ADDED :	
			logger.debug("new z-wave was added, will be interviewed now");
			break;
		case NODE_EVENT_NODE_ALREADY_ADDED:
			logger.debug("node is already added to network");
			if (cntrl.getControllerMode() == ZWaveControllerMode.CNTRL_MODE_INCLUSION){
				logger.debug("Z-Wave controller is in inclusion mode. Handling node as new added device.");
				node = ((ZWaveNodeEvent)event).getNode();
				submitDiscoveryResults(node);
			}
			break;
		case NODE_EVENT_NIF_RECEIVED:
			logger.debug("LRN button was pressed on Z-Wave device which is already paired with z-wave controller.");
		case NODE_EVENT_INTERVIEW_FINISHED:
			if (event instanceof ZWaveNodeEvent){				
				logger.debug("z-wave device interview finished");				
				node = ((ZWaveNodeEvent)event).getNode();
				submitDiscoveryResults(node);			
			}
			break;
		default:
				
		}
	}	
	
}
