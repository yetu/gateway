package com.yetu.gateway.home.binding.zwave.internal.utils;

public class ZWaveBindingUtils {
	
	public static String getNumberString(int number, int digits){
		String result = ""+number;
		while (result.length() < digits){
			result = "0"+result;
		}
		return result;
	}
	
}
