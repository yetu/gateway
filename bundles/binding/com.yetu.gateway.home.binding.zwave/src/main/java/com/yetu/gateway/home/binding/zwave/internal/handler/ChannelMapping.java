package com.yetu.gateway.home.binding.zwave.internal.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ChannelUID;

public class ChannelMapping {

	private static final String ABSTRACTION_IDENTIFIER = "#abstraction:";
	
	
	String componentId;
	String componentType;
	String capability;
	Channel channel;
	
	public ChannelMapping(Channel channel){
		this.channel = channel;
		setDetails();
	}
	
	private void setDetails(){
		String tag = getAbstractionTag();		
		if (tag != null){
			setDetails(tag);
		} else {
			setDetails(channel.getUID());
		}
	}
	
	private void setDetails(String tag){
		
		Map<String,String> attributeValues = new HashMap<>();
		String abstraction = tag.substring(ABSTRACTION_IDENTIFIER.length());
		
		StringTokenizer tok = new StringTokenizer(abstraction,";");
		
		if (tok.countTokens() != 0){	
			while (tok.hasMoreTokens()){
				String str = tok.nextToken();
				String key = str.substring(0,str.indexOf("="));
				String value = str.substring(str.indexOf("=")+1,str.length());
				attributeValues.put(key, value);
			}
		}
		
		
			
			componentId = attributeValues.get("comp_id");
			componentType = attributeValues.get("comp_type");
			capability = attributeValues.get("cap_id");
			
			
		
	}
	
	private void setDetails(ChannelUID channelUid){
		/*
		 * private String extractCapabilityFromChannel(ChannelUID uid){
		String id = uid.getId();
		if (id.contains("#")){
			id = id.subSequence(id.indexOf('#')+1, id.length()).toString();
			return id;
		}
		return uid.getId();
	}
	
	private String extractcomponentIdFromChannel(Channel channel){
		String id = uid.getId();
		if (id.contains("#")){
			String componentId = id.subSequence(0,id.indexOf('#')).toString();
			return componentId;
		} else {
			if (uid.getGroupId() != null){
				return uid.getGroupId().toString();
			}
		}
		return "nocomponentIdDefined"; 
		
	}
		 */
	}
	
	private String getAbstractionTag(){
		Set<String> tags = channel.getDefaultTags();
		for (String tag : tags){
			if (tag.startsWith(ABSTRACTION_IDENTIFIER)){
				return tag;
			}
		}
		return null;
	}

	public String getComponentId() {
		return componentId;
	}

	public String getComponentType() {
		return componentType;
	}

	public String getCapability() {
		return capability;
	}

	public Channel getChannel() {
		return channel;
	}
	
}
