package com.yetu.gateway.home.binding.zwave.internal.handler;

import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;

public interface CommandProcessor {
	
	public void handleUpdate(String componentId, String componentType, String capability, State newState);	
	public void handleCommand(String componentId, String componentType, String capability, Command command);
	
}
