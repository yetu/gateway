package com.yetu.gateway.home.binding.zwave.internal.device.yale;

import com.domoone.utils.logger.LogTag;
import com.domoone.zwave.ZWaveCntrl;
import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportAlarm;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericLock;

public class YaleDoorLock extends GenericLock{

	public YaleDoorLock(ZWaveNode node) {
		super(node);
		// TODO Auto-generated constructor stub
	}
	

	@Override
	protected void lock(){		
		getZWaveNode().sendData(cmdFactory.generateCmd_DoorLock_set(true));		
	}
	
	@Override
	protected void unlock(){
		getZWaveNode().sendData(cmdFactory.generateCmd_DoorLock_set(false));		
	}
	
	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		super.evaluateReceivedNodeCmd(cmd);
		try {			
			switch (cmd.getCommandClassKey()){				
				case 0x71:
					evaulateAlarm(ZWaveReportFactory.generateAlarmReport(cmd) );
				break;
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
		
	}
	
	protected void evaulateAlarm(ZWaveReportAlarm report){
		ZWaveCntrl.log(LogTag.DEBUG, "Received Alarm (type = "+report.getAlarmType()+" | level = "+report.getAlarmLevel()+")");
		
		switch (report.getAlarmType()){
			case 0x70:	// Master Code changed / User Added
				break;
			case 0xa1:	// Tamper Alarm
				break;
			case 0x16: // Manual Unlock
				updateLockValue(false);
				break;
			case 0x19:	// RF Operate Unlock
				updateLockValue(false);
				break;
			case 0x15:	// Manual Lock
				updateLockValue(true);
				break;
			case 0x18:  // RF Operate Lock
				updateLockValue(true);
				break;
			case 0x12:	// Keypad Lock
				updateLockValue(true);
				break;
			case 0x13:	// Keypad Unlock
				updateLockValue(false);
				break;
			case 0x09:	// Deadbold Jammed
				break;
			case 0xA9:	// Low Battery /Too low to operate			
				break;
			case 0xA8:  // Low Battery / critical Battery level
				break;
			case 0xA7:	// Low Battery 
				break;
			case 0x1B:	// Auto Lock Operated Locked
				updateLockValue(true);
				break;
			case 0x71:	// Error - duplicate pin code
				break;
			case 0x82:  // RF module power cycled
				break;
			case 0x21:	// user deleted
				break;
			default:
				// unknown alarm type
				break;
		}
		
		
	}
	
	
	/**
	 * Sets the audio mode.
	 * @param mode  1 = Silent
	 * 				2 = Low
	 * 				3 = High (default)
	 */
	public void setAudioMode(int mode){
		sendConfiguration(01, ZWaveCmdParamType.BYTE,mode);
	}
	
	/**
	 * 
	 * @param relock default true  for products 0x02 and 0x04
	 * 						 false for products 0x01 and 0x03 
	 * 				default for productId 0x00 not given
	 */
	public void setAutoRelock(boolean relock){
		if (relock){
			sendConfiguration(2,ZWaveCmdParamType.BYTE,0x00); 
		} else {
			sendConfiguration(2,ZWaveCmdParamType.BYTE,0xff); 
		}
	}
	
	
	
	/**
	 * Sets the relock time in seconds after a successful code entry;
	 * @param relocktime in seconds 5-255
	 */
	public void setRelockTime(int relocktime){
		sendConfiguration(3,ZWaveCmdParamType.BYTE,relocktime);
	}

	/**
	 * The number of invalid conde entries lock will accept before sending TAMPER Alarm. 
	 * When number of wrong code entries is exceeded, lock will disable keypad for the time specified by shutdown time parameter.
	 * @param limit 1-7 (5 default)
	 */
	public void setWrongCodeEntryLimit(int limit){
		sendConfiguration(4, ZWaveCmdParamType.BYTE,limit);
	}
	
	
	/**
	 * Sets the language. 
	 * @param language	1 = Englisch (default)
	 * 					2 = Spanish
	 * 					3 = French
	 */
	public void setLanguage(int language){
		sendConfiguration(5,ZWaveCmdParamType.BYTE,language);
	}
	
	
	/**
	 * Number of seconds unit will be inoperable after number of wrong code entries is exceeded
	 * @param shutdownTime 1-255 (default 30)
	 */
	public void setShutdownTime(int shutdownTime){
		sendConfiguration(7, ZWaveCmdParamType.BYTE,shutdownTime);
	}
	
	/**
	 * Sets the operation Mode
	 * @param mode	00	Normal Mode
	 * 				01	Vacation Mode - all user codes disabled
	 * 				02	Privacy Mode  - all user codes disabled and RF Lock/Unlock disabled
	 */
	public void setOperatingMode(int mode){
		sendConfiguration(8, ZWaveCmdParamType.BYTE, mode);
	}
	
}
