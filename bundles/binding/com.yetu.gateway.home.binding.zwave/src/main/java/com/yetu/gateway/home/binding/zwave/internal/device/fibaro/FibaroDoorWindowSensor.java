package com.yetu.gateway.home.binding.zwave.internal.device.fibaro;

import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericSensorBinary;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class FibaroDoorWindowSensor extends GenericSensorBinary{

	public FibaroDoorWindowSensor(ZWaveNode node) {
		super(node);
		logger = LoggerFactory.getLogger(FibaroDoorWindowSensor.class);
	}
	
	@Override
	protected void updateSensorValue(boolean value){
		if (capabilityStateChangeListener != null){
			State newState;
			if (value){
				newState = OnOffType.ON;	
			} else {
				newState = OnOffType.OFF;
			}
			capabilityStateChangeListener.onCapabilityStateChange(ZWaveSpecificThingTypeGenerator.DEFAULT_COMPONENT_ID_SENSOR_CONTACT,"measurement", newState);
		}
	}
}
