package com.yetu.gateway.home.binding.zwave.internal.handler;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.thing.type.ThingType;
import org.eclipse.smarthome.core.thing.type.ThingTypeRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.ZWaveCntrl;
import com.domoone.zwave.ZWaveControllerMode;
import com.domoone.zwave.cmd.ZWaveNodeCmdFactory;
import com.domoone.zwave.event.ZWaveEvent;
import com.domoone.zwave.event.ZWaveEventListener;
import com.domoone.zwave.event.ZWaveNodeEvent;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceControllerFactory;
import com.yetu.gateway.home.binding.zwave.internal.io.ZWaveControlAccess;
import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveThingTypeFactory;



public class ZWaveThingHandlerFactory extends BaseThingHandlerFactory{

	
	private static Logger logger = LoggerFactory.getLogger(ZWaveThingHandlerFactory.class);
	
	//private ThingTypeRegistry thingTypeRegistry = null; 
	
	ZWaveCntrl zwaveController;
	ZWaveNodeCmdFactory cmdFactory;
	List<ZWaveDeviceController> devCntrls = null;	
	List<ZWaveThingHandler> thingHandlers = null;
	
	public ZWaveThingHandlerFactory() {
		super();
		devCntrls = new ArrayList<>();		
		thingHandlers = new ArrayList<>();
		addZWaveController();
		if (zwaveController != null){
			cmdFactory = new ZWaveNodeCmdFactory(zwaveController.getCommandClassSpecifications());
			if (zwaveController.getControllerMode() != ZWaveControllerMode.CNTRL_MODE_INITIALIZING){
				assignDevControllers();
			}
		}		
	}
		
	
	public Set<ThingTypeUID> getSupportedThingTypeUIDs(){		
		return ZWaveThingTypeFactory.getInstance().getSupportedThingTypeUIDs();
	}
	
	protected void addZWaveController(){
		zwaveController = ZWaveControlAccess.getInstance().getZWaveCntrl();
		zwaveController.addCntrlListener(new ZWaveEventListener() {
			
			@Override
			public void onZWaveEvent(ZWaveEvent event) {
				processZWaveEvent(event);
				
			}
		});
	}
	
	protected void processZWaveEvent(ZWaveEvent event){
		switch (event.getEventType()){
			case CNTRL_EVENT_INIT_COMPLETED:
				assignDevControllers();
				break;
			case NODE_EVENT_INTERVIEW_FINISHED:
				try {
					zwaveController.saveConfiguration();
				} catch (Exception exc){
					logger.error("Unable to store z-wave configuration", exc);
				}
				if (event instanceof ZWaveNodeEvent){
					ZWaveNode node = ((ZWaveNodeEvent)event).getNode();
					ZWaveDeviceController devCntrl = ZWaveDeviceControllerFactory.generateDeviceController(node);
					
					if (devCntrl != null){
						devCntrl.setCmdFactory(cmdFactory);
						devCntrl.init();
						 ZWaveThingHandler handler = getCachedThingHandler(node.getNodeId());
						 if (handler == null){
							 cacheDeviceController(devCntrl);
						 } else {
							 handler.setDeviceController(devCntrl);
						 }
							 
						
					}
				}
				break;
			default:
				// do nothing
				
		}
	}
	
	protected void assignDevControllers(){
		ZWaveDeviceController cntrl;
		ZWaveThingHandler handler = null;
		for (ZWaveNode node : zwaveController.getNodeList()){
			if (node.getNodeId() > 1){
				cntrl = ZWaveDeviceControllerFactory.generateDeviceController(node);
			 	if(cntrl != null) {
			 		cntrl.setCmdFactory(cmdFactory);
				 	cacheDeviceController(cntrl);
				 	handler = getCachedThingHandler(node.getNodeId());
				 	if (handler != null){
					 	handler.setDeviceController(cntrl);
				 	}			 		
			 	}
			}
		}
	}
	
	protected void cacheDeviceController(ZWaveDeviceController controller){
		synchronized (devCntrls) {
			devCntrls.add(controller);
		}		
	}

	protected void cacheThingHandler(ZWaveThingHandler handler){
		synchronized (handler) {
			thingHandlers.add(handler);
		}
	}
	
	@Override
	public boolean supportsThingType(ThingTypeUID thingTypeUID) {	
		logger.debug("supportsThingType(ThingTypeUID thingTypeUID)  is called in ZWaveThingHandlerFactory");
		for (ThingTypeUID ttUID :ZWaveThingTypeFactory.getInstance().getSupportedThingTypeUIDs()){
			if (ttUID.equals(thingTypeUID)){
				return true;
			}
		}		
		logger.debug("{} is not a supported thing type",thingTypeUID);		
		return false;
	}

	@Override
	protected ThingHandler createHandler(Thing thing) {
		// TODO Auto-generated method stub		
		logger.debug("createHandler(Thing thing) is called");
		
		ZWaveNode node = zwaveController.getNode(Integer.parseInt(thing.getUID().getId()));
		return createHandler(thing,node);
		
	}
	
	
	protected ZWaveDeviceController getCachedControllerIfexists(ZWaveNode node){
		synchronized (devCntrls) {
			for (ZWaveDeviceController cntrl : devCntrls){
				if (cntrl.getZWaveNode() == node){
					return cntrl;
				}
			}
		}
		return null;
	}
	
	protected ZWaveThingHandler createHandler(Thing thing, ZWaveNode node){
		ZWaveThingHandler handler = getCachedThingHandler(thing);
		if (handler == null){
			logger.debug("didn't found cached Handler -> will create one");
			handler = new ZWaveThingHandler(thing);
			thingHandlers.add(handler);
		}
		
		if (node != null){
			ZWaveDeviceController devCntrl = getCachedControllerIfexists(node);			
			if (devCntrl == null){
				logger.warn("No device controller exists for node {}. That's weird, device controller usually will"+
							"be generated directly after node was interviewed or loaded. Maybe interview is not finished correctly?"+
						" Is the configuration file broken or missing?", node.getNodeId());
				
			} else {
				devCntrl.setCmdFactory(cmdFactory);
				handler.setDeviceController(devCntrl);
			}
		
		} else {
			logger.debug("no z-wave node was given. maybe z-wave controller is not initialized yet. In this case Z-wave device controller will be assigned later");
		}
		return handler;
	}
	
	protected ZWaveThingHandler getCachedThingHandler(Thing thing){
		synchronized (thingHandlers) {
			for (ZWaveThingHandler handler : thingHandlers){
				if (handler.getThing().getUID().equals(thing.getUID())){
					return handler;
				}
			}
		}
		return null;
	}
	
	protected ZWaveThingHandler getCachedThingHandler(int nodeId){
		String id = ""+nodeId;
		synchronized (thingHandlers) {
			for (ZWaveThingHandler handler : thingHandlers){
				if (handler.getThing().getUID().getId().equalsIgnoreCase(id)){
					return handler;
				}
			}
		}
		return null;
	}
		
	protected int extractNodeIdFromUID(ThingUID thingUID){
		try {
			return Integer.parseInt(thingUID.getId());
		} catch (NumberFormatException exc){
			logger.error("Error parsing node id from thingUID {}",thingUID,exc);
		}
		return 0;
	}
	
	 /**
     * Creates a thing based on given thing type uid.
     * 
     * @param thingTypeUID
     *            thing type uid (can not be null)
     * @param thingUID
     *            thingUID (can not be null)
     * @param configuration
     *            (can not be null)
     * @return thing (can be null, if thing type is unknown)
     */
	@Override
    protected Thing createThing(ThingTypeUID thingTypeUID, Configuration configuration, ThingUID thingUID) {
		logger.debug("createThing(uid, conf, thinguid) was called");
    	return createThing(thingTypeUID, configuration, thingUID, null);
    }
    
    /**
     * Creates a thing based on given thing type uid.
     * 
     * @param thingTypeUID
     *            thing type uid (should not be null)
     * @param thingUID
     *            thingUID (should not be null)
     * @param configuration
     *            (should not be null)
     * @param bridgeUID
     *            (can be null)
     * @return thing (can be null, if thing type is unknown)
     */
	@Override
    public Thing createThing(ThingTypeUID thingTypeUID, Configuration configuration, ThingUID thingUID, ThingUID bridgeUID) {
		
		ThingType thingType = ZWaveThingTypeFactory.getInstance().generateThingType(thingUID);
		
		// we generated the thing type, add thing type to thing type registry
//		if (thingTypeRegistry != null){
//			thingTypeRegistry.getThingTypes()
//		} else {
//			logger.error("Thing type for {} not added to thingTypeRegistry because it is null. This could leed to the problem, that no items will be generated automatically");
//		}
//		
		
		logger.debug("createThing(uid, conf, thinguid) was called");
        logger.debug("ThingTypeUID = "+thingTypeUID.getAsString());
        logger.debug("ThingUID = "+thingUID.getAsString());
        
        if (thingType != null) {
            Thing thing = ThingFactory.createThing(thingType, thingUID, configuration, bridgeUID,
                    getConfigDescriptionRegistry());     
            
            
           
            return thing;
        } else {
            return null;
        }
    }
	
//	public void setThingTypeRegistry(ThingTypeRegistry thingTypeRegistry){
//		this.thingTypeRegistry = thingTypeRegistry;
//	}
//	
//	public void unsetThingTypeRegistry(ThingTypeRegistry thingTypeRegistry){
//		this.thingTypeRegistry = null;
//	}
//	
	
	
}
