package com.yetu.gateway.home.binding.zwave.internal.thingtypes.fibaro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupType;
import org.eclipse.smarthome.core.thing.type.ChannelGroupTypeUID;
import org.eclipse.smarthome.core.thing.type.ChannelType;
import org.eclipse.smarthome.core.thing.type.ChannelTypeUID;
import org.eclipse.smarthome.core.types.StateDescription;
import org.eclipse.smarthome.core.types.StateOption;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class FibaroWallPlugThingTypeGenerator extends ZWaveSpecificThingTypeGenerator {
	
	@Override
	public int getManufacturerId() {		
		return 0x10f; //271
	}

	@Override
	public int getProductTypeId() {		
		return 0x600; //1536
	}

	@Override
	public int getProductId() {		
		return 0x1000; //4096
	}

	@Override
	public String getTypeName() {
		return "fibaro_wallplug";
	}

	
	
//	@Override
//	public ThingType generateThingType(ThingUID uid) {
//		List<ChannelDefinition> channelDefinitions = new ArrayList<>();
//		List<ChannelGroupDefinition> channelGroupDefinitions = new ArrayList<>();
//		
//		this.addSocketChannels(uid, channelDefinitions, channelGroupDefinitions);
//		this.addLightChannels(uid, channelDefinitions, channelGroupDefinitions);
//		this.addMeterChannels(uid,channelDefinitions,channelGroupDefinitions);
//				
//		List<String> bridges = null;
//		
//		String label = "Fibaro Wallplug";
//		String description = "Powerplug that also measures electric consumption.";
//		
//		return new ThingType(uid.getThingTypeUID(), bridges, label, description, channelDefinitions, channelGroupDefinitions, null);
//	}
	
	
	
	@Override
	protected void addSensorIlluminanceChannel(String componentId, ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
		
		// generate Switchabel for Socket
		StateDescription stateDescription = new StateDescription(new BigDecimal(0),new BigDecimal(1),new BigDecimal(1),"", false, new ArrayList<StateOption>());
		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":switchable");		
		Set<String> tags = new HashSet<>();	
		//tags.add("abstraction:led:LAMP:SWITCHABLE");
		tags.add("#abstraction:comp_id="+componentId+";comp_type=LAMP;cap_id=SWITCHABLE");
		ChannelType ct = new ChannelType(ctuid,false,"Switch","switchable","LED ring of the wallplug","Light", tags,stateDescription,null);		
		ChannelDefinition def = new ChannelDefinition("switchable",ct);		

		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+":light");
		
		List<ChannelDefinition> groupChannels = new ArrayList<>();	
		groupChannels.add(def);
		
		stateDescription = new StateDescription(new BigDecimal(0),new BigDecimal(8),new BigDecimal(1),"", false, new ArrayList<StateOption>());
		ctuid = new ChannelTypeUID(uid.getAsString()+":colorable");		
		tags = new HashSet<>();	
	//	tags.add("#abstraction:led:LAMP:COLORABLE");
		tags.add("#abstraction:comp_id="+componentId+";comp_type=LAMP;cap_id=COLORABLE");
		ct = new ChannelType(ctuid,false,"Number","colorable","Color of the LED ring (0-8 whereas 0 is default)","Color",tags,stateDescription,null);		
		def = new ChannelDefinition("colorable",ct);		
		
		groupChannels.add(def);
		
		
		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Light","Light", groupChannels);
		ChannelGroupDefinition gd = new ChannelGroupDefinition("light",cgt);
		
		
		//channelDefs.add(def);
		groupDefs.add(gd);
	}
	
	

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		addSocketChannel(uid, channelDefinitions, channelGroupDefinitions);
		//addMeterChannel(uid, channelDefinitions, channelGroupDefinitions);
		addSensorIlluminanceChannel("led",uid, channelDefinitions, channelGroupDefinitions);
		addSensorElectricityChannel(uid, channelDefinitions, channelGroupDefinitions);
	}

	@Override
	public String getManufacturerName() {
		return "Fibaro";
	}

	@Override
	public String getProductName() {
		return "Wallplug";
	}
	
	@Override
	protected Map<String,String> getProperties(){
		Map<String,String> properties =super.getProperties();
		
		properties.put("abstraction_main_comp","socket");
		
		return properties;
	}
	
	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_SOCKET;
	}
	
}
