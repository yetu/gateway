package com.yetu.gateway.home.binding.zwave.internal;


import java.io.File;
import java.util.Dictionary;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.ZWaveCntrl;
import com.domoone.zwave.event.ZWaveEvent;
import com.domoone.zwave.event.ZWaveEventListener;
import com.domoone.zwave.io.ZWaveConnection;
import com.yetu.gateway.home.binding.zwave.ZWaveBindingConstants;
import com.yetu.gateway.home.binding.zwave.internal.io.ZWaveControlAccess;
import com.yetu.gateway.home.io.zwave.ZWaveConnectionFactory;


/**
 * 
 * @author Mathias Runge (initial conribution)
 *
 */
public class ZWaveBinding implements ManagedService{
	protected static final String NODE_CONFIG_FILENAME = "zwave_nodes.xml";
	private static Logger logger  = LoggerFactory.getLogger(ZWaveBinding.class);
	
	private ZWaveConnectionFactory connectionFactory = null;
	private static ZWaveCntrl 			  zwaveCntrl = null;	
	private static ZWaveEventListener 	  zwaveEventListener = null;
	private static ZWaveConnection  conn = null;
	private String serialPort = null;
	
	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
		// FIXME: For some reason, this method will be called 3 times. 2 times with valid configuration parameters. 
	
		
		if (properties == null){
			return;
		}
	
		String port =(String)properties.get(ZWaveBindingConstants.SERIAL_PORT);
		if (port != null){
			logger.debug("calling initZWaveController() with port {}",port);
			serialPort = port;
			activateIfPossible();
		}		
	}
	
	
	private void activateIfPossible(){
		if (serialPort == null){
			return;
		}
		if (connectionFactory == null){
			return;
		}
		
		initZWaveController();
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// +++++++++++++++++++++++++++++++++++++++++++ Z-Wave Controlling ++++++++++++++++++++++++++++++++++++++
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	
	public void setZWaveConnectionFactory(ZWaveConnectionFactory connectionFactory){
		this.connectionFactory = connectionFactory;
		activateIfPossible();
	}
	
	public void unsetZWaveConnectionFactory(ZWaveConnectionFactory cunnectionFactory){
		this.connectionFactory = null;
	}
	
	
	/**
	 * The physical Z-Wave controller could be USB to serial plug, a network resource. In this case, a serial connection
	 * with the current portName will be opened and the in- and output streams will be delivered to the z-wave controller.  
	 * @param portName
	 */
	protected void initZWaveController(){
		
		
		if (conn != null){
			logger.warn("Connection to {} is already established. Cancel initialization of Z-Wave Controller",serialPort);
			return;
		}
		zwaveCntrl = ZWaveControlAccess.getInstance().getZWaveCntrl();
		zwaveCntrl.setConfigurationFilePath(getNodeConfigFileName());
		loadNodeConfiguration();
		// add Listener to instance
				zwaveEventListener = new ZWaveEventListener() {
					
					@Override
					public void onZWaveEvent(ZWaveEvent event) {
						processZWaveEvent(event);
						
					}
				};
				zwaveCntrl.addCntrlListener(zwaveEventListener);		
		
		logger.debug("try to init zwave controller on port {}",serialPort);
		// initialize serial connection
		// new ZWaveSerialConnection();
		zwaveCntrl.setSecurityEnabled(true);
		try {
			conn = connectionFactory.createSerialConnection(serialPort);
			
			//conn.connect();
			// init the zwaveCntrl
			zwaveCntrl.init(conn.getInputStream(),conn.getOutputStream());			
		} catch (Exception exc){
			logger.error("unable to init serial connection to zwave controller on port {}",serialPort,exc);
		}
		
	}
	
	/**
	 * Processes z-wave events that were sent from the z-wave controller. 
	 * @param event
	 */
	protected void processZWaveEvent(ZWaveEvent event){
	
		// logger.debug("processing ZWaveEvent {}",event.getEventType());
	}
	
	
	/**
	 * Specifies the Z-Wave node configuration file name where the ZWaveCntrl schould store the configurations.
	 * @return filename for Z-Wave node configuration
	 */
	protected String getNodeConfigFileName(){		
		String sep = System.getProperty("file.separator");
		String path = System.getProperty("user.dir")+sep+"runtime"+sep+"conf"+sep+NODE_CONFIG_FILENAME;
		logger.debug("Z-Wave Node Configuration file is '"+path+"'.");
		return path;
	}

	
	/**
	 * Loads the Z-Wave node configuration
	 */
	public synchronized void loadNodeConfiguration() {		
		try {
			File f = new File(getNodeConfigFileName());
			if (f.exists()){
				logger.debug("Z-Wave configuration will be loaded now.");
				zwaveCntrl.loadConfiguration();				
			} else {
				logger.warn("No z-wave node configuration file was found ("+f.getAbsolutePath()+"). That's ok for the first start.");
				logger.debug("New z-wave node configuration file will be generated now");
				zwaveCntrl.saveConfiguration();
			}
			
		} catch (Exception exc){
			logger.warn("Unable to load configuration - "+exc.getMessage(),exc);
			//throw new ZWaveException("Unable to load configuration - "+exc.getMessage(),exc);
		}
	}
	
	
	/**
	 * Stores the Z-Wave node configuration.
	 */
	public synchronized void saveNodeConfiguration(){		
		try {
			zwaveCntrl.saveConfiguration();
			logger.debug("Z-Wave configuration will be saved now.");
		} catch (Exception exc){
			logger.error("Unable to save configuration - {}",exc.getMessage(),exc);
			
		}
	}
	
	
	
	
}
