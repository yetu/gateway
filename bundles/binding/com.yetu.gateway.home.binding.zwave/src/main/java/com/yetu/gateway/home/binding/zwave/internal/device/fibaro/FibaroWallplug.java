package com.yetu.gateway.home.binding.zwave.internal.device.fibaro;

import java.math.BigDecimal;

import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReport;
import com.domoone.zwave.cmd.report.ZWaveReportConfiguration;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.report.ZWaveReportSensorMultilevel;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.generic.GenericSwitchBinary;

public class FibaroWallplug extends GenericSwitchBinary{
	
	private int colorWhenOn;
	private int colorWhenOff;

	private int desiredColorWhenOn = -1;
	private int desiredColorWhenOff = -1;
	
	public FibaroWallplug(ZWaveNode node) {
		super(node);
		logger = LoggerFactory.getLogger(FibaroWallplug.class);
	}

	
	
	
	protected void initZWaveNode(){
		//associateNode(3, 1);		// FIXME: not recommended -> implemented only for IFA purpose. 
		//        unable to remove device when group 3 is associated with primary controller (01)
		//requestSensorData();	
		associateNode(1, 1);		// on off
		//requestOnState();
		associateNode(2, 1);		// energy consumption		
		//requestColorOnData();
		//requestColorOffData();
	}
	
	@Override
	protected void updateOnValue(boolean value){
		logger.debug("updating on/off value {}",value);
		State newState;
		if (value){
			newState = OnOffType.ON;	
		} else {
			newState = OnOffType.OFF;
		}
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("socket","switchable", newState);
		}
	}
	
	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd){
		super.evaluateReceivedNodeCmd(cmd);
		try {
			switch (cmd.getCommandClassKey()){
				case 0x31: // COMMAND_CLASS_SENSOR_MULTILEVEL
					if (cmd.getCommandKey() == 0x05){ // REPORT
						evalReport(ZWaveReportFactory.generateSensorMultilevelReport(cmd));
					}					
					break;	
				case 0x70: // COMMAND_CLASS_CONFIGURATION
					if (cmd.getCommandKey() == 0x06){ // REPORT
						evalReport(ZWaveReportFactory.generateConfigurationReport(cmd));
					}
					break;
						
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
	}
	
	@Override
	protected void evalReport(ZWaveReport report){
		super.evalReport(report);
		if (report instanceof ZWaveReportSensorMultilevel){
			ZWaveReportSensorMultilevel rep = (ZWaveReportSensorMultilevel)report;
			double value = rep.getValue();
			if (rep.getPrecission() != 0){
				value = value / (double)rep.getPrecission();
			}
			updatePowerValue(value);
		}
		if (report instanceof ZWaveReportConfiguration){
			ZWaveReportConfiguration conf = (ZWaveReportConfiguration)report;
			if (conf.getParamId() == 61){
				updateColorWhenOn(conf.getValue());	
			}
			if (conf.getParamId() == 62){
				updateColorWhenOff(conf.getValue());			
			}
		}
	}
	
	protected void updateColorWhenOff(int value){
		logger.debug("updating ring color when off {}",value );
		this.colorWhenOff = value;
		if (this.desiredColorWhenOff == -1){
			if (value < 8){
				desiredColorWhenOff = value;
			} else {
				if (desiredColorWhenOn != -1){
					desiredColorWhenOff = desiredColorWhenOn - 1;	
					if (desiredColorWhenOff < 0){
						desiredColorWhenOff = 0;
					}
				}
			}	
			
		}
	}
	
	protected void updateColorWhenOn(int value){
		logger.debug("updating ring color when on {}",value );
		this.colorWhenOn = value;
		if ((this.desiredColorWhenOn == -1) && (value < 9)){
			desiredColorWhenOn = value;
			if (desiredColorWhenOff < 0){
				desiredColorWhenOff = desiredColorWhenOn - 1;	
				if (desiredColorWhenOff < 0){
					desiredColorWhenOff = 0;
				}
			}
		}
	}
	
	protected void updatePowerValue(double value){
		logger.debug("updating on/off value {}",value);
		State newState = new DecimalType(BigDecimal.valueOf(value));		
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("consumption","measurement", newState);
		}
		
	}
	
	protected void requestSensorData(){	
		logger.debug("requesting sensor value from device");
		getZWaveNode().sendData(cmdFactory.generateCmd_SensorMultilevel_Get());		
		
	}
	
	protected void requestColorOnData(){	
		logger.debug("requesting value for color when device is on");
		getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Get((byte)(61)));		
	}

	protected void requestColorOffData(){	
		logger.debug("requesting value for color when device is off");
		getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Get((byte)(62)));		
	}

	/**
	 * Sets the color of the LED ring when the plug is turned on.
	 * @param color 0 : LED ring illumination color changes in predefined steps, depending on power consumption 
	 * 				1 : LED ring illumination color changes continuously over the full RGB spectrum, depending on power consumption
	 * 				2 : white
	 * 				3 : red
	 * 				4 : green
	 * 				5 : blue
	 * 				6 : yellow
	 * 				7 : cyan
	 * 				8 : magenta
	 * 				9 : LED ring illumination turned off
	 */
	protected void setColorOn(int color){
		logger.debug("setting color when on {}",color);
		desiredColorWhenOn = color;
		getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set((byte)(61),ZWaveCmdParamType.BYTE,color));		
		requestColorOnData();
	}
	
	
	/**
	 * Sets the color of the LED ring when the plug is turned off.
	 * @param color 0 : LED ring illumination shows the last measured power before it was turned off
	 * 				1 : white
	 * 				2 : red
	 * 				3 : green
	 * 				4 : blue
	 * 				5 : yellow
	 * 				6 : cyan
	 * 				7 : magenta
	 * 				8 : LED ring illumination turned off
	 */
	protected void setColorOff(int color){
		logger.debug("setting color when off {}", color);
		desiredColorWhenOff = color;
		getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set((byte)(62),ZWaveCmdParamType.BYTE,color));		
		requestColorOffData();
	}
	
	
	protected void turnOnLed(){		
		setColorOn(desiredColorWhenOn);
		setColorOff(desiredColorWhenOff);
	}
	
	protected void turnOffLed(){
		getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set((byte)(62),ZWaveCmdParamType.BYTE,8));		 // turn off led when socket is off
		getZWaveNode().sendData(cmdFactory.generateCmd_Configuration_Set((byte)(61),ZWaveCmdParamType.BYTE,9));		 // turn off led when socket is on
	}
	
	/**
	 * {@inheritDoc}
	 */

	@Override
	public void handleUpdate(String componentId, String componentType, String capability, State newState) {
		super.handleUpdate(componentId,componentType, capability, newState);		
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public void handleCommand(String componentId, String componentType, String capability, Command command) {
		super.handleCommand(componentId,componentType, capability, command);
		if ("lamp".equalsIgnoreCase(componentType)){
			if ("switchable".equalsIgnoreCase(capability)){			
				if ("OFF".equalsIgnoreCase(command.toString())){
					turnOffLed();
				}
				if ("ON".equalsIgnoreCase(command.toString())){
					turnOnLed();
				}
			}
			if ("colorable".equalsIgnoreCase(capability)){
				
			}
		}
	}
	

	@Override
	public void requestValues() {
		super.requestValues();
		requestSensorData();
		requestColorOffData();
		requestColorOnData();
	}
	
}
