package com.yetu.gateway.home.binding.zwave;

import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;


public interface ZWaveThingUIDGenerator {
	public ThingUID generateThingUID(int manufacturerId, int productTypeId, int productId, int nodeId);
	public Set<ThingTypeUID> getSupportedThingTypeUIDs();
}
