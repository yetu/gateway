package com.yetu.gateway.home.binding.zwave.internal.device.generic;

import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;

import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportBasic;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.report.ZWaveReportSensorBinary;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;

public class GenericSensorBinary extends ZWaveDeviceController{

	public GenericSensorBinary(ZWaveNode node) {
		super(node);		
	}


	@Override
	public void handleUpdate(String componentId, String componentType, String capability, State newState) {
		
	}

	@Override
	public void handleCommand(String componentId, String componentType, String capability, Command command) {
			
	}
	
	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		try {
			switch (cmd.getCommandClassKey()){
				case 0x30: // COMMAND_CLASS_SENSOR_BINARY
						if (cmd.getCommandKey() == 0x03){
							evaluateReport(ZWaveReportFactory.generateSensorBinaryReport(cmd));
						}
					break;					
				case 0x20: // COMMAND_CLASS_BASIC
						if (cmd.getCommandKey() == 0x03){ // REPORT
							evaluateReport(ZWaveReportFactory.generateBasicReport(cmd));
						}
						if (cmd.getCommandKey() == 0x01){ // SET
							updateSensorValue(ZWaveCmdParamType.toInteger(cmd.getParamValue(0))!=0);
						}
					break;
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
	}

	@Override
	protected void initZWaveNode() {
		if (getZWaveNode().supportsCommandClass(0x85)){
			associateNode(1,1);
		}	
		
	}
	
	protected void evaluateReport(ZWaveReportSensorBinary report){
		updateSensorValue(report.getValue());
	}
	
	protected void evaluateReport(ZWaveReportBasic report){
		updateSensorValue(report.getValue()!=0);
	}
	
	
	protected void updateSensorValue(boolean value){
		if (capabilityStateChangeListener != null){
			State newState;
			if (value){
				newState = OnOffType.ON;	
			} else {
				newState = OnOffType.OFF;
			}
			capabilityStateChangeListener.onCapabilityStateChange("sensor","measurement", newState);
		}
	}
	
	protected void requestSensorBinary(){		
		getZWaveNode().sendData(cmdFactory.generateCmd_SensorBinary_Get());
	}

	@Override
	public void requestValues() {
		requestSensorBinary();
	}
	
}
