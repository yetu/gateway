package com.yetu.gateway.home.binding.zwave.internal.device.generic;

import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.cmd.report.ZWaveReportSensorAlarm;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;

public class GenericSensorAlarm extends ZWaveDeviceController {

	public GenericSensorAlarm(ZWaveNode node) {
		super(node);
		logger = LoggerFactory.getLogger(GenericSensorAlarm.class);
	}

	@Override
	public void handleUpdate(String componentId, String componentType, String capability, State newState) {
		
	}

	@Override
	public void handleCommand(String componentId, String componentType, String capability, Command command) {
			
	}
	
	protected void updateAlarm(boolean value){
		logger.debug("updating motion state",value);
		State newState;
		if (value){
			newState = OnOffType.ON;	
		} else {
			newState = OnOffType.OFF;
		}
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("sensor","motion", newState);
		}
	}

	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		try {
			switch (cmd.getCommandClassKey()){
				case 0x9C:  // COMMAND_CLASS_SENSOR_ALARM
					if (cmd.getCommandKey() == 0x02) { // REPORT
						ZWaveReportSensorAlarm report = ZWaveReportFactory.generateSensorAlarmReport(cmd);
						updateAlarm(report.isAlarm());
					}
					break;
				case 0x20: // COMMAND_CLASS_BASIC
					if (cmd.getCommandKey() == 0x01){ //BASIC_SET
						updateAlarm(0 != ZWaveCmdParamType.toInteger(cmd.getParamValue(0)));	
					}
					break;
				default:
			}	
		} catch (Exception exc){
			logger.error("Unable to interprete incoming ZWaveNodeCommand",exc);
		}		
	}	

	@Override
	protected void initZWaveNode() {
		associateNode(1, 1);
	}

	@Override
	public void requestValues() {
		// TODO Auto-generated method stub
	}
}
