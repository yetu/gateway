package com.yetu.gateway.home.binding.zwave.internal.device.generic;

import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;

import com.domoone.zwave.cmd.ZWaveCmdParamType;
import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReport;
import com.domoone.zwave.cmd.report.ZWaveReportBasic;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.report.ZWaveReportSwitchBinary;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;


public class GenericSwitchBinary extends ZWaveDeviceController{

	public GenericSwitchBinary(ZWaveNode node) {
		super(node);		
	}

	/**
	 * {@inheritDoc}
	 */

	
	
	protected void updateOnValue(boolean value){
		logger.debug("updating on/off value {}",value);
		State newState;
		if (value){
			newState = OnOffType.ON;	
		} else {
			newState = OnOffType.OFF;
		}
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("socket","switchable", newState);
		}
	}
	
	protected void evalReport(ZWaveReport report){
		if (report instanceof ZWaveReportBasic){
			updateOnValue(((ZWaveReportBasic)report).getValue() != 0);
		}
		if (report instanceof ZWaveReportSwitchBinary){
			updateOnValue(((ZWaveReportSwitchBinary)report).getValue());
		}
	}

	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		try {
			switch (cmd.getCommandClassKey()){
				case 0x25: // COMMAND_CLASS_SWITCH_BINARY
					if (cmd.getCommandKey() == 0x03){ // REPORT
						evalReport(ZWaveReportFactory.generateSwitchBinaryReport(cmd));
					}					
					break;					
				case 0x20: // COMMAND_CLASS_BASIC
					if (cmd.getCommandKey() == 0x01){ // SET
						updateOnValue(ZWaveCmdParamType.toInteger(cmd.getParamValue(0))!=0);
					}
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
		
	}

	@Override
	protected void initZWaveNode() {
		if (getZWaveNode().supportsCommandClass(0x85)){
			associateNode(01, 01);
		}
		requestOnState();		
	}
	
	protected void requestOnState(){	
		logger.debug("requesting on/off state");
		getZWaveNode().sendData(cmdFactory.generateCmd_SwitchBinary_Get());
	}
	
	protected void turnOn(){
		logger.debug("turn device on");
		getZWaveNode().sendData(cmdFactory.generateCmd_SwitchBinary_Set(255));
		requestOnState();
	}
	
	protected void turnOff(){
		logger.debug("turn device off");
		getZWaveNode().sendData(cmdFactory.generateCmd_SwitchBinary_Set(0));
		requestOnState();
	}
	

	@Override
	public void handleUpdate(String componentId, String componentType, String capability, State newState) {
		logger.debug("hanndling update '{}' for capability '{}' of component '{}' ",newState,capability,componentId);
		if ("socket".equalsIgnoreCase(componentType)){
			if ("switchable".equalsIgnoreCase(capability)){
				logger.debug("handling update for socket switchable state {}",newState);
			}
		}		
	}

	@Override
	public void handleCommand(String componentId, String componentType, String capability, Command command) {
		logger.debug("hanndling command '{}' for capability '{}' of subthing '{}' ",command,capability,componentId);
		if ("socket".equalsIgnoreCase(componentType)){
			if ("switchable".equalsIgnoreCase(capability)){			
				if ("OFF".equalsIgnoreCase(command.toString())){
					turnOff();
				}
				if ("ON".equalsIgnoreCase(command.toString())){
					turnOn();
				}
			}
		}
		
	}

	@Override
	public void requestValues() {
		requestOnState();
		
	}
	
}
