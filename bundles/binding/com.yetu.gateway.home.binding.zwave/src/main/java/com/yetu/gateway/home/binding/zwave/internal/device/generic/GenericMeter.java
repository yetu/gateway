package com.yetu.gateway.home.binding.zwave.internal.device.generic;

import java.math.BigDecimal;

import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.LoggerFactory;

import com.domoone.zwave.cmd.ZWaveNodeCmd;
import com.domoone.zwave.cmd.report.ZWaveReportFactory;
import com.domoone.zwave.cmd.report.ZWaveReportMeter;
import com.domoone.zwave.node.ZWaveNode;
import com.yetu.gateway.home.binding.zwave.internal.device.ZWaveDeviceController;

public class GenericMeter extends ZWaveDeviceController{

	public GenericMeter(ZWaveNode node) {
		super(node);
		logger = LoggerFactory.getLogger(GenericMeter.class);
	}


	@Override
	protected void initZWaveNode() {
		associateNode(01,01);
		associateNode(02,01);
	}
	
	@Override
	public void handleUpdate(String componentId, String componentType, String capability, State newState) {
		// just measurements
	}

	@Override
	public void handleCommand(String componentId, String componentType, String capability, Command command) {
		// just measurements
	}

	protected void updateUsageValue(double value){	
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("meter","measurement", new DecimalType(BigDecimal.valueOf(value)));
		}
	}
	
	protected void updateConsumptionValue(double value){
		if (capabilityStateChangeListener != null){
			capabilityStateChangeListener.onCapabilityStateChange("power","measurement", new DecimalType(BigDecimal.valueOf(value)));
		} 
	}
	
	@Override
	protected void evaluateReceivedNodeCmd(ZWaveNodeCmd cmd) {
		try {
			switch (cmd.getCommandClassKey()){
				case 0x32 : // COMMAND_CLASS_METER_REPORT
					evaluateReport(ZWaveReportFactory.generateMeterReport(cmd));
					break;	
				default:
					break;
			}
		} catch (Exception exc){
			exc.printStackTrace();
			// FIXME
		}
		
	}
	
	
	
	
	protected void evaluateReport(ZWaveReportMeter report){
	
		double value;
		if (report.getPrecission() > 0){		
			value = (double)report.getValue()/(double)report.getPrecission();			
		} else {
			value = (double)report.getValue();
		}		
		switch (report.getScale()){
			case ZWaveReportMeter.SCALE_KWH:
			case ZWaveReportMeter.SCALE_KVAH:
				updateUsageValue(value*1000);				
				break;
			case ZWaveReportMeter.SCALE_W:
				updateConsumptionValue(value);
				break;
			default:
				
				break;
		}
		
	}


	@Override
	public void requestValues() {
		requestMeterValues();		
	}
	
	protected void requestMeterValues(){
		getZWaveNode().sendData(cmdFactory.generateCmd_Meter_Get_V2((byte)0));
		getZWaveNode().sendData(cmdFactory.generateCmd_Meter_Get_V2((byte)3));
	}


}
