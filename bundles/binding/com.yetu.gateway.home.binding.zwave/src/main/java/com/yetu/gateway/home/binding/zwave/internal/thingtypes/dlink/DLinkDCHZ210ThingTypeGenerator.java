package com.yetu.gateway.home.binding.zwave.internal.thingtypes.dlink;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.type.ChannelDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupDefinition;
import org.eclipse.smarthome.core.thing.type.ChannelGroupType;
import org.eclipse.smarthome.core.thing.type.ChannelGroupTypeUID;
import org.eclipse.smarthome.core.thing.type.ChannelType;
import org.eclipse.smarthome.core.thing.type.ChannelTypeUID;
import org.eclipse.smarthome.core.types.StateDescription;
import org.eclipse.smarthome.core.types.StateOption;

import com.yetu.gateway.home.binding.zwave.internal.thingtypes.ZWaveSpecificThingTypeGenerator;

public class DLinkDCHZ210ThingTypeGenerator extends ZWaveSpecificThingTypeGenerator{

//	@Override
//	public ThingType generateThingType(ThingUID uid) {
//		List<ChannelDefinition> channelDefinitions = new ArrayList<>();
//		List<ChannelGroupDefinition> channelGroupDefinitions = new ArrayList<>();
//		
//		this.addSocketChannels(uid, channelDefinitions, channelGroupDefinitions);
//		this.addMeterChannels(uid, channelDefinitions, channelGroupDefinitions);
//		this.addSensorElectricityChannel(uid, channelDefinitions, channelGroupDefinitions);
//		List<String> bridges = null;
//		
//		String label = "DLink Wallplug DCH-Z210";
//		String description = "";
//		
//		return new ThingType(uid.getThingTypeUID(), bridges, label, description, channelDefinitions, channelGroupDefinitions, null);
//	}

//	private void addSocketChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
//		
//		// generate Switchabel for Socket
//		StateDescription stateDescription = new StateDescription(new BigDecimal(0),new BigDecimal(1),new BigDecimal(1),"", false, new ArrayList<StateOption>());
//		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":switchable");
//		
//		Set<String> tags = new HashSet<>();	
//		//tags.add("abstraction:socket:SOCKET:SWITCHABLE");
//		tags.add("#abstraction:comp_id=socket;main_comp=socket;thing_name=DLink Wallplug DCH-Z210;comp_type=SOCKET;cap_id=SWITCHABLE");
//		ChannelType ct = new ChannelType(ctuid,false,"Switch","switchable","descr","Switch", tags,stateDescription,null);		
//		ChannelDefinition def = new ChannelDefinition("switchable",ct);
//	
//		
//
//		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+":socket");
//		
//		
//		List<ChannelDefinition> groupChannels = new ArrayList<>();	
//		groupChannels.add(def);
//		
//		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Socket","Socket", groupChannels);
//		ChannelGroupDefinition gd = new ChannelGroupDefinition("socket",cgt);
//	
//		
//		//channelDefs.add(def);
//		groupDefs.add(gd);
//	}
	
//	private void addMeterChannel(ThingUID uid, List<ChannelDefinition> channelDefs, List<ChannelGroupDefinition> groupDefs){
//		
//		String componentId = "consumption";
//		String componentType = "METER";
//		String capability = "MEASUREMENT";
//	
//		StateDescription stateDescription = new StateDescription(new BigDecimal(0.0),new BigDecimal(2500.0),new BigDecimal(0.1),"", true, new ArrayList<StateOption>());
//		ChannelTypeUID ctuid = new ChannelTypeUID(uid.getAsString()+":"+capability.toLowerCase());		
//		
//		Set<String> tags = new HashSet<String>();
//	//	tags.add("abstraction:"+componentId+":"+componentType+":"+capability.toUpperCase()+":unit=WATTHOUR");
//		tags.add("#abstraction:comp_id="+componentId+";comp_type="+componentType+";cap_id="+capability+";prop_unit=WATTHOUR");
//		
//		ChannelType ct = new ChannelType(ctuid,false,"Number",capability.toLowerCase(),"Electricity Meter","Energy", tags,stateDescription,null);		
//		ChannelDefinition def = new ChannelDefinition(capability.toLowerCase(),ct);		
//		
//		ChannelGroupTypeUID cgtUID = new ChannelGroupTypeUID(uid.getAsString()+componentId);
//		
//		List<ChannelDefinition> groupChannels = new ArrayList<>();	
//		groupChannels.add(def);
//		
//		
//		ChannelGroupType cgt = new ChannelGroupType(cgtUID, true, "Meter","Meter", groupChannels);
//		ChannelGroupDefinition gd = new ChannelGroupDefinition(componentId,cgt);
//		groupDefs.add(gd);		
//		
//	}
	
	
	
	
	@Override
	public int getManufacturerId() {
		return 0x108; // 264
	}

	@Override
	public int getProductTypeId() {
		return 0x1;
	}

	@Override
	public int getProductId() {
		return 0x11;
	}

	@Override
	public String getTypeName() {
		return "dlink_dch_z210";
	}

	@Override
	protected void addChannels(ThingUID uid,
			List<ChannelDefinition> channelDefinitions,
			List<ChannelGroupDefinition> channelGroupDefinitions) {
		addSocketChannel(uid, channelDefinitions, channelGroupDefinitions);
		addSensorElectricityChannel(uid, channelDefinitions, channelGroupDefinitions);
	//	addMeterChannel(uid, channelDefinitions, channelGroupDefinitions);
	}

	@Override
	public String getManufacturerName() {
		return "D-Link";
	}

	@Override
	public String getProductName() {
		return "DCH 210";
	}

	@Override
	public String getMainComponentId() {
		return DEFAULT_COMPONENT_ID_SOCKET;
	}
}
