package com.yetu.gateway.home.binding.remotemanagement.cumulocity;

public class CumulocityBundleConstants {
	public static String CONFIG_PARAM_HOST = "host";
	public static String CONFIG_PARAM_PORT = "port";
	public static String CONFIG_PARAM_DEVICE_ID = "externaldeviceid";
	public static String CONFIG_PARAM_LOGIN = "login";
	public static String CONFIG_PARAM_PASSWORD = "password";
	public static String CONFIG_HARDWARE_ID = "hardwareid";
	
	/** How often online state is updated in minutes **/
	public static int ONLINE_STATE_UPDATE_INTERVAL = 2;
	
	public static String PROTOCOL = "https";
	
	public static int RECONNECT_DELAY = 5000;
	
	/** how many items will be kept if cumulocity is offline **/
	public static int MAX_OFFLINE_CACHE = 200;
}
