package com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.zip.GZIPOutputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.AbstractContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.svenson.JSONParser;

import com.yetu.gateway.home.core.monitoring.model.FileDescriptor;

/**
 * Takes care of uploading files
 * @author Sebastian Garn
 *
 */
public class FileHandler {
	private static Logger logger = LoggerFactory.getLogger(FileHandler.class);
	
	private static int BUFFER_SIZE = 1024;
	
	private String protocol;
	private String host;
	private int port;
	private String encodedLogin;
	
	public FileHandler(String protocol, String host, int port, String encodedLogin) {
		this.protocol = protocol;
		this.host = host;
		this.port = port;
		this.encodedLogin = encodedLogin;
	}
	
	public FileDescriptor storeAsTextFile(String content, String storageFilename) {
		if (storageFilename == null || storageFilename.length() == 0) {
			logger.warn("Invalid parameters. Could not upload file");
			return null;
		}

		try {
	        String contentType = "text/plain";
	        StringBody contentToTransmit = new StringBody(content,contentType,null);

			return upload(contentToTransmit, content.length(), contentType, storageFilename);
		} catch(Exception e) {
			logger.error("Failed to send file",e);
			return null;
		}
	}

	public FileDescriptor sendFile(File fileToSend, String storageFilename, boolean zipBeforeSending) {
		if (fileToSend  == null || storageFilename == null || storageFilename.length() == 0) {
			logger.warn("Invalid parameters. Could not upload file");
			return null;
		}
		else if (!fileToSend.exists() || fileToSend.isDirectory()) {
			logger.warn("Invalid file type or file not found");
			return null;
		}

		// get file extension
		String filenameParts[] = fileToSend.getName().split("\\.");
        String extension = filenameParts[filenameParts.length - 1];
        File tempFile = null;
        
        if (zipBeforeSending) {
			logger.debug("Zip it");
			try {
		        if (!extension.equals("gz") && ! extension.equals("zip")) {
		        	logger.debug("Creating temp file");
		        	// create a temp file where we can put in the zipped content
		        	tempFile = File.createTempFile("upload", extension + ".gz");
	                compressFile(fileToSend, tempFile);
	                fileToSend = tempFile;
	                storageFilename += ".gz";
	                logger.debug(fileToSend.getAbsolutePath());
	            }
			} catch(Exception e) {
				logger.error("Failed to compress file", e);
				return null;
			}
        }
	    
        // determine the mime type
        String contentType = "";
        
        switch(extension) {
        	case "gz":
        	case "zip":
        		contentType = "application/zip";
        		break;
        	case "log":
        	case "txt":
        		contentType = "text/plain";
        		break;
        	default:
        		contentType = "application/octet-stream";
        }
		
		FileBody contentToTransmit = new FileBody(fileToSend, contentType);

		FileDescriptor result = upload(contentToTransmit, fileToSend.length(), contentType, storageFilename);
		
		if (tempFile != null) {
			tempFile.delete();
		}
		
		return result;
	}

	public FileDescriptor sendPartOfFile(File fileToSend, int lineCount, boolean startFromBottom, String storageFilename) {
		if (fileToSend  == null || storageFilename == null || storageFilename.length() == 0) {
			logger.warn("Invalid parameters. Could not upload file");
			return null;
		}
		else if (!fileToSend.exists() || fileToSend.isDirectory()) {
			logger.warn("Invalid file type or file not found");
			return null;
		}
		
		try {
			AdvancedFileReader fileReader = new AdvancedFileReader(fileToSend);
			String fileContent;

			if (startFromBottom) fileContent = fileReader.readFromBottom(lineCount);
			else fileContent = fileReader.readFromTop(lineCount);
			
			String contentType = "text/plain";
			StringBody contentToTransmit = new StringBody(fileContent, contentType, null);

			return upload(contentToTransmit, fileContent.length(), contentType, storageFilename);
		} catch(FileNotFoundException fException) {
			logger.error("Unable to find file",fException);
			return null;
		} catch(IOException ioException) {
			logger.error("Failed to read or send file",ioException);
			return null;
		} catch(Exception exception) {
			logger.error("Failed to send file",exception);
			return null;
		}
	}

	private FileDescriptor upload(AbstractContentBody contentBody, long contentLength, String contentType, String storageFilename) {
		logger.info("Uploading content to "+storageFilename);

		DefaultHttpClient client = new DefaultHttpClient();

		HttpPost post;
		
		// ignore port if it is https: this is a workaround for this crappy apache http client
		if (protocol.compareTo("https") == 0 && port == 443) {
			post = new HttpPost(protocol+"://"+host+"/inventory/binaries");
		}
		else {
			post = new HttpPost(protocol+"://"+host+":"+port+"/inventory/binaries");
		}

        post.addHeader("Authorization", "Basic "+encodedLogin);
        post.addHeader("Accept", "application/vnd.com.nsn.cumulocity.managedObject+json");

		try {
			// generate request object
	        StringBody fileDescriptor = new StringBody("{\"name\":\"" + storageFilename + "\",\"type\":\"" + contentType + "\"}","application/json",null);
	        StringBody fileSize = new StringBody("" + contentLength);
	        
	        MultipartEntity reqEntity = new MultipartEntity();
	        reqEntity.addPart("object", fileDescriptor);
	        reqEntity.addPart("filesize", fileSize);
	        reqEntity.addPart("file", contentBody);
	        
	        post.setEntity(reqEntity);
	        
	        // send it
	        HttpResponse response = client.execute(post);
	        
	        if (response.getEntity() == null || response.getStatusLine().getStatusCode() != 201) {
				logger.error("Invalid response! Code: " + response.getStatusLine().getStatusCode());
				if (response.getEntity() != null) {
					logger.error("Return value: "+getResponse(response.getEntity().getContent()));
				}
				else {
					logger.error("Return value is empty");
				}
				return null;
			}
	        
	        // parse the response
			Object result = JSONParser.defaultJSONParser().parse(getResponse(response.getEntity().getContent()));
			logger.info("File uploaded. Result is: "+result);

			@SuppressWarnings("unchecked")
			HashMap<String, String> resultMap = (HashMap<String, String>)result;

			String pathToFile = protocol+"://"+host+":"+port+"/inventory/binaries/"+resultMap.get("id");
			FileDescriptor descriptor = new FileDescriptor(pathToFile);
			
			// wait and close connection
			EntityUtils.consume(response.getEntity());

			return descriptor;
		} catch(UnsupportedEncodingException fException) {
			logger.error("Invalid encoding",fException);
			return null;
		} catch(IOException ioException) {
			logger.error("Failed to read or send file",ioException);
			return null;
		} catch(Exception e) {
			logger.error("Failed to send file",e);
			return null;
		}
	}
	
	/**
	 * Compresses the contents of an input file and writes the results to outFile
	 * @param inFile - File: non-zipped input file
	 * @param outFile - File: file which contains the zipped content
	 * @throws IOException
	 */
	private static void compressFile(File inFile, File outFile) throws IOException {
        InputStream inStream = new FileInputStream(inFile);
        OutputStream outStream = new GZIPOutputStream(new FileOutputStream(outFile));
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytes;

        while ((bytes = inStream.read(buffer)) > 0) {
            outStream.write(buffer, 0, bytes);
        }

        outStream.flush();
        outStream.close();
        inStream.close();
    }
	
	/**
	 * Returns the response of the http request
	 * @param connection
	 * @return
	 */
	public String getResponse(InputStream connection) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection));
	    String line = "";
	    StringBuilder builder = new StringBuilder();
	    
	    try {
		    while((line = reader.readLine()) != null) {
		        builder.append(line);
		    }
	    } catch(Exception e) {
	    	logger.debug("Error while reading. "+e.toString());
	    }
	    
	    return builder.toString();
	}
}
