package com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CumulocityBindingActivator implements BundleActivator {
	private static Logger logger = LoggerFactory.getLogger(CumulocityBindingActivator.class);
	
	public void start(BundleContext context) throws Exception {
		logger.debug("Starting cumulocity remote management binding");
	}

	public void stop(BundleContext bc) throws Exception {
		logger.debug("Stopping cumulocity remote management binding");
	}
}
