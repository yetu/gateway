package com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class AdvancedFileReader {
	private static String LINEBREAK = "\n";
	private static int AVERAGE_LINE_LENGTH = 80;
	private File file;
	
	public AdvancedFileReader(File file) throws FileNotFoundException {
		if (!file.exists() || file.isDirectory()) throw new FileNotFoundException();
		this.file = file;
	}
	
	/**
	 * Reads a file starting at the end
	 * @param filename - String: location of the file
	 * @param lineCount - int: how many lines you would like to read
	 * @return String requested lines
	 */
	public String readFromBottom(int lineCount) throws IOException {
		String[] tmpResult = new String[lineCount];
		ArrayList<String> result = new ArrayList<String>();
		
		if (lineCount < 1) return null;
		RandomAccessFile randomFile;

		randomFile = new RandomAccessFile(file.getAbsolutePath(), "r");			
		char currChar;
		
		for (long currPos = file.length(); currPos > 0; currPos--) {
			// move pointer to pos and read
			randomFile.seek(currPos-1);
			currChar = (char)randomFile.readByte();
			
			if (currChar == 10 && randomFile.readLine() != null) {
				// move back one char
				randomFile.seek(currPos);
				lineCount--;
				tmpResult[lineCount] = randomFile.readLine();
			}
			
			// all lines read
			if (lineCount == 0) {
				break;
			}
		}
		
		// add the first line of document if we reached beginning and did not have added all lines
		if (lineCount != 0) {
			randomFile.seek(0);
			tmpResult[lineCount-1] = randomFile.readLine();
		}
		
		randomFile.close();
		
		// remove null entries
		for (int i = 0; i < tmpResult.length; i++) {
			if (tmpResult[i] != null) {
				result.add(tmpResult[i]);
			}
		}
		
		return arrayToString(result);
	}
	
	/**
	 * Reads a file starting at the beginning
	 * @param lineCount - int: how many lines you would like to read
	 * @return String - requested lines
	 */
	public String readFromTop(int lineCount) throws IOException {
		if (lineCount < 1) return "";
		StringBuilder result = new StringBuilder(lineCount*AVERAGE_LINE_LENGTH);
		BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()));
		
		String currLine;
		
		while ((currLine = reader.readLine()) != null && lineCount > 0) {
			result.append(currLine);
			result.append(LINEBREAK);
			--lineCount;
		}
		
		reader.close();
		
		return result.toString();
	}
	
	private String arrayToString(List<String> in) {
		if (in == null || in.size() == 0) return "";
		
		StringBuilder result = new StringBuilder(in.size()*AVERAGE_LINE_LENGTH);
		
		for (String s : in) {
			result.append(s);
			result.append(LINEBREAK);
		}
		
		return result.toString();
	}
}
