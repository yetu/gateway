package com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal;

import java.io.File;
import java.security.Security;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import c8y.IsDevice;
import c8y.RequiredAvailability;
import c8y.SupportedOperations;

import com.cumulocity.model.Agent;
import com.cumulocity.model.ID;
import com.cumulocity.model.authentication.CumulocityCredentials;
import com.cumulocity.model.idtype.GId;
import com.cumulocity.model.operation.OperationStatus;
import com.cumulocity.rest.representation.identity.ExternalIDRepresentation;
import com.cumulocity.rest.representation.inventory.ManagedObjectRepresentation;
import com.cumulocity.rest.representation.operation.OperationRepresentation;
import com.cumulocity.sdk.client.Platform;
import com.cumulocity.sdk.client.PlatformImpl;
import com.cumulocity.sdk.client.SDKException;
import com.cumulocity.sdk.client.devicecontrol.DeviceControlApi;
import com.cumulocity.sdk.client.identity.IdentityApi;
import com.cumulocity.sdk.client.inventory.InventoryApi;
import com.cumulocity.sdk.client.notification.Subscriber;
import com.cumulocity.sdk.client.notification.Subscription;
import com.cumulocity.sdk.client.notification.SubscriptionListener;
import com.sun.jersey.core.util.Base64;
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.CumulocityBundleConstants;
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils.FileHandler;
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils.ManagementItemTransmitter;
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils.OperationExecutor;
import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.model.Alarm;
import com.yetu.gateway.home.core.monitoring.model.DeviceProperty;
import com.yetu.gateway.home.core.monitoring.model.Event;
import com.yetu.gateway.home.core.monitoring.model.FileDescriptor;
import com.yetu.gateway.home.core.monitoring.model.ManagementItem;
import com.yetu.gateway.home.core.monitoring.model.Measurement;
import com.yetu.gateway.home.core.monitoring.model.OperationListener;


/**
 * This class provides the functionalities for sending events and alarms to the cumulocity service. 
 * 
 * @author Sebastian Garn
 */
public class CumulocityRemoteGatewayManagementAccessor implements RemoteGatewayManagementAccessor, ManagedService {
	private static Logger logger = LoggerFactory.getLogger(CumulocityRemoteGatewayManagementAccessor.class);

	private String hardwareId;
	
	/** used to queue items whenever gateway is offline **/ 
	private ArrayList<ManagementItem> queue;

	private ArrayList<OperationListener> operationListeners;
	boolean isOnline;
	
	private GId inventoryDeviceId;
	private String externalDeviceId;

	private Platform platform;
	private Subscriber<GId, OperationRepresentation> operationSubscriber;
	private InventoryApi inventory;
	private ManagedObjectRepresentation deviceRepresentation;

	private Thread onlineStateThread;
	private boolean isConnecting;
	private boolean isFirstConnection;
	
	private boolean stopBinding;
	
	private FileHandler fileHandler;
	private ManagementItemTransmitter itemTransmitter; 
	
	public void activate(ComponentContext context) throws Exception {
		logger.debug("Started cumulocity remote management accessor");
		operationListeners = new ArrayList<OperationListener>();
		queue = new ArrayList<ManagementItem>();
		
		stopBinding = false;
		isOnline = false;
		isConnecting = false;
		isFirstConnection = true;
	}

	public void deactivate(ComponentContext context) throws Exception {
		logger.debug("Stopping cumulocity remote management accessor");
		isConnecting = false;
		stopBinding = true;
		
		if (operationSubscriber != null) {
			operationSubscriber.disconnect();
		}
		
		operationListeners.clear();
		queue.clear();
	}
	
	/**
	 * Called once as soon as configuration file gets loaded.
	 * 
	 * @param apiurl - String:  url including tentant id (e.g. yetu.cumulocity.com)
	 * @param port - int: used port
	 * @param externalDeviceId - String: external id which maps to device in inventory
	 * @param login - String: device login
	 * @param password - String: device password
	 */
	public void init(String apiurl, int port, String externalDeviceId, String hardwareId, String login, String password) {
		logger.debug("Init cumulocity: "+login+" "+password);
		
		logger.debug("Assigning bounce castle provider");
		Security.addProvider(new BouncyCastleProvider());

		platform = new PlatformImpl(CumulocityBundleConstants.PROTOCOL+"://"+apiurl+":"+port, new CumulocityCredentials(login, password));
		
		this.externalDeviceId = externalDeviceId;
		this.hardwareId = hardwareId;

		// necessary for upload
		String encodedLogin = new String(Base64.encode((login+":"+password).getBytes()));
		
		// setup file handler for uploading files
		fileHandler = new FileHandler(CumulocityBundleConstants.PROTOCOL, apiurl, port, encodedLogin);
		
		// connects this to cumulocity
		setupConnection();
	}
	
	/**
	 * Tries to establish a connection to cumulocity. Gets called whenever the connection to cumulocity gets lost
	 */
	public synchronized void setupConnection() {
		if (isConnecting) {
			logger.debug("Already trying to connect");
			return;
		}

		isConnecting = true;
		
		logger.info("Setting up connection to backend");
		
		setOnlineState(false);
		inventoryDeviceId = null;
		
		Thread connect = new Thread() {
			public void run() {
				// check connectivity by retrieving inventory id
				while (inventoryDeviceId == null && !stopBinding) {
					try {
						logger.debug("Trying to connect");
						inventoryDeviceId = getInventoryId(hardwareId, externalDeviceId);
					} catch(Exception e) {
						logger.debug("Failed to connect. Reconnecting in "+CumulocityBundleConstants.RECONNECT_DELAY+" ms. Reason: "+e.toString());
					}
					
					if (inventoryDeviceId == null) {
						try {
							Thread.sleep(CumulocityBundleConstants.RECONNECT_DELAY);
						} catch(Exception sleepException) { }
					}
				}

				logger.debug("Connected.");
				
				if (stopBinding) return;

				onConnected();
				
				isConnecting = false;
			}
		};
		connect.start();
	}

	/**
	 * Whenever connection was established to cumulocity
	 */
	private void onConnected() {
		// do the following once
		if (isFirstConnection) {
			// registers this agent once
			setupOperationListener();
			
			// periodically updates online state
			setupOnlineStateThread();
			
			isFirstConnection = false;
		}
		
		logger.debug("Platform is: "+platform);
		inventory = platform.getInventoryApi();
		deviceRepresentation = getDeviceRepresentation();
		itemTransmitter = new ManagementItemTransmitter(platform, getDeviceRepresentation());
		
		// sets device specific information properties
		updateDefaultDeviceRepresentation(getDeviceRepresentation());

		setOnlineState(true);
	}

	@Override
	public void registerOperationListener(OperationListener handler) {
		logger.debug("Adding handler: "+handler);
		synchronized(operationListeners) {
			operationListeners.add(handler);
		}
		
		if (isOnline) {
			updateSupportedOperations();
		}
	}

	@Override
	public void unregisterOperationListener(OperationListener handler) {
		synchronized(operationListeners) {
			operationListeners.remove(handler);
		}
		
		if (isOnline) {
			updateSupportedOperations();
		}
	}

	/**
	 * Retrieves the inventory id of the device representation by using
	 * the external device id. 
	 * @param externalDeviceId - String: external device id of gateway
	 * @return null if external device id does not exist
	 */
	private GId getInventoryId(String hardwareId, String externalDeviceId) throws Exception {
		IdentityApi identityApi = null; 
		
		identityApi = platform.getIdentityApi();

		ID id = new ID();
		id.setType(hardwareId);
		id.setValue(externalDeviceId);

		ExternalIDRepresentation externalIDGid = identityApi.getExternalId(id);
	
		return externalIDGid.getManagedObject().getId();
	}
	
	/**
	 * Sets a new state of the gateway. Synchronizes with cumulocity in case the gateway
	 * has some cached messages.
	 * 
	 * @param isOnline - boolean: new state of the gateway
	 */
	private void setOnlineState(boolean isOnline) {
		this.isOnline = isOnline;
		
		// sync if necessary
		if (isOnline) {
			logger.debug("Checking for messages to synch");
			
			// sync buffered events and alarms
			synchronized(queue) {
				// create a copy of the current queue
				ArrayList<ManagementItem> itemsToSynch = new ArrayList<ManagementItem>();
				itemsToSynch.addAll(queue);
				
				// clear the queue - if the gw goes offline during synch process we are pushing the messages back to the queue
				queue.clear();
				
				for (ManagementItem currItem : itemsToSynch) {
					if (currItem instanceof Event) sendEvent((Event)currItem);
					else if (currItem instanceof Alarm) sendAlarm((Alarm)currItem);
					else if (currItem instanceof Measurement) sendMeasurement((Measurement)currItem);
					else if (currItem instanceof DeviceProperty) sendDeviceProperty((DeviceProperty)currItem);
					else {
						logger.debug("Unable to synch "+currItem+" since it is unknown");
					}
				}
			}
			
			// sync device representation
			updateSupportedOperations();
		}
	}
	
	/**
	 * Registers this platform to receive operation requests.
	 */
	public void setupOperationListener() {
		logger.debug("Registering operations notificator");
		
		try {
			DeviceControlApi deviceControl = platform.getDeviceControlApi();
			operationSubscriber = deviceControl.getNotificationsSubscriber();
		} catch(Exception e) {
			logger.error("Failed to setup operation listeners. Reason: {}",e.toString(),e);
			return;
		}
		
		final RemoteGatewayManagementAccessor remoteManagementAccessor = this;

		Subscription<GId> subscription = operationSubscriber.subscribe(getDeviceRepresentation().getId(), new SubscriptionListener<GId, OperationRepresentation>() {
		    public void onError(Subscription<GId> sub, Throwable e) {
		        logger.debug("OperationDispatcher error!");
		    }

		    public void onNotification(Subscription<GId> sub, OperationRepresentation operation) {
		    	DeviceControlApi deviceControl;
		    	
		    	try {
		    		deviceControl = platform.getDeviceControlApi();
		    	} catch(SDKException e) {
		    		logger.warn("Failed to handle operation since device control api is not available",e);
		    		return;
		    	}
		    	
		        logger.debug("Operation request received: "+operation);
		    	boolean operationListenerFound = false;
		    	
		    	synchronized(operationListeners) {
		    		for (OperationListener currListener : operationListeners) {
		    			String operationName = currListener.handledOperation().getName();
		    			Object handledOperation = operation.getProperty(operationName);
		    			
		    			if (handledOperation != null) {
		    				// extract parameters - check if all parameters are given
		    				try {
		    					@SuppressWarnings("unchecked")
								HashMap<String, String> recvdParams = (HashMap<String, String>)operation.get(currListener.handledOperation().getName());
		    					HashMap<String, String> params = new HashMap<String, String>();
								List<String> requiredParams = currListener.handledOperation().getParameters();
								
		    					if (requiredParams.size() != recvdParams.size()) {
		    						logger.warn("Skipping listener for operation "+operationName+" since parameter count does not match. Expected params: "+requiredParams+" Received: "+recvdParams);
		    						continue;
		    					}
		    					
		    					boolean allParametersFound = true;
		    					for (String currParam : requiredParams) {
		    						if (recvdParams.get(currParam) != null) {
		    							params.put(currParam, recvdParams.get(currParam));
		    						}
		    						else {
		    							logger.warn("Unable to find parameter "+currParam+" in received parameters.");
		    							allParametersFound = false;
		    						}
		    					}
		    					
		    					if (!allParametersFound) {
		    						logger.warn("Skipping listener for operation "+operationName+" since parameters are missing");
		    						continue;
		    					}
		    					
		    					logger.debug("Found a listener handling the operation "+operationName);
			    				OperationExecutor executor = new OperationExecutor(remoteManagementAccessor, platform, operation,currListener,params);
			    				executor.start();
			    				
			    				operationListenerFound = true;
		    				} catch(Exception e) {
		    					logger.error("Failed to execute operation "+operationName, e);
		    				}
		    				
		    				
		    			}
		    		}
				}
		    	
		    	if (!operationListenerFound) {
		    		logger.debug("No listener was found for handling given operation");
		    		operation.setStatus(OperationStatus.FAILED.toString());
		    		deviceControl.update(operation);
		    	}
		    }
		});
		
		logger.debug("Subscribed: "+subscription);
	}
	
	/**
	 * Creates a thread that periodically updates the online state
	 */
	public void setupOnlineStateThread() {
		if (onlineStateThread != null) return;
		
		onlineStateThread = new Thread() {
			@Override
			public void run() {
				long timeout = CumulocityBundleConstants.ONLINE_STATE_UPDATE_INTERVAL*60*1000/2;
				
				while (true) {
					try {
						Thread.sleep(timeout);
						
						if (stopBinding) return;
							
						if (isOnline) {
							logger.debug("Updating online state.");
							deviceRepresentation.set(new RequiredAvailability(CumulocityBundleConstants.ONLINE_STATE_UPDATE_INTERVAL));
							
							inventory.update(deviceRepresentation);
							logger.debug("Updating online state done.");
						}
					} catch(Exception e) {
						logger.warn("Failed to update state"+e.toString());
						setupConnection();
					}
				}
			}
		};
		
		onlineStateThread.start();
	}
	
	/**
	 * Checks all listeners for their supported operations
	 * 
	 * @param inventoryId
	 */
	public void updateSupportedOperations() {
		try {			
			ManagedObjectRepresentation updatedRepresentation = getDeviceRepresentation();
			
			SupportedOperations ops = new SupportedOperations();
			
			synchronized(operationListeners) {
				for (OperationListener currListener : operationListeners) {
					ops.add(currListener.handledOperation().getName());
				}
			}
			
			logger.debug("Supported operations are: "+ops);
			updatedRepresentation.set(ops);
	
			inventory.update(updatedRepresentation);
		} catch(Exception e) {
			logger.error("Failed to update supported operations: {}",e.toString(),e);
		}
	}
	
	/**
	 * Sets additional parameters for the device representation in inventory
	 */
	public void updateDefaultDeviceRepresentation(ManagedObjectRepresentation representation) {
		try {
			representation.set(new Agent());
			representation.set(new IsDevice());
			
			inventory.update(representation);
		} catch(Exception e) {
			logger.error("Failed to update device representation: {}",e.toString(),e);
		}
	}
	
	@Override
	public void sendDeviceProperty(DeviceProperty property) {
		if (!readyForTransmission(property)) return;
		
		if (!itemTransmitter.sendDeviceProperty(property)) {
			enqueue(property);
			setupConnection();
		}
		else {
			logger.debug("DeviceProeprty sent: "+property);
		}
	}
	
	/**
	 * Creates a minimalistic device representation. It does NOT contain currently stored values
	 * since we dont want to give the device account the necessary rights.
	 * 
	 * @return ManagedObjectRepresentation
	 */
	private ManagedObjectRepresentation getDeviceRepresentation() {
		ManagedObjectRepresentation deviceRepresentation = new ManagedObjectRepresentation();
		deviceRepresentation.setId(inventoryDeviceId);
		return deviceRepresentation;
	}

	@Override
	public void updated(Dictionary<String, ?> config) throws ConfigurationException {
		if (config != null) {
			String host = (String)config.get(CumulocityBundleConstants.CONFIG_PARAM_HOST);
			String port = (String)config.get(CumulocityBundleConstants.CONFIG_PARAM_PORT);
			String externaldeviceid = (String)config.get(CumulocityBundleConstants.CONFIG_PARAM_DEVICE_ID);
			String login = (String)config.get(CumulocityBundleConstants.CONFIG_PARAM_LOGIN);
			String password = (String)config.get(CumulocityBundleConstants.CONFIG_PARAM_PASSWORD);
			String hardwareid = (String)config.get(CumulocityBundleConstants.CONFIG_HARDWARE_ID);
			
			if (host == null || port == null || externaldeviceid == null || login == null || password == null || hardwareid == null) {
				logger.error("Failed to start Cumulocity remote management accessor due to missing login credentials.");
				logger.error("Given: "+CumulocityBundleConstants.CONFIG_PARAM_HOST+"="+host+", "+CumulocityBundleConstants.CONFIG_PARAM_PORT+"="+port+", "+CumulocityBundleConstants.CONFIG_PARAM_DEVICE_ID+"="+externaldeviceid+", "+CumulocityBundleConstants.CONFIG_PARAM_LOGIN+"="+login+", "+CumulocityBundleConstants.CONFIG_PARAM_PASSWORD+".length="+((password == null) ? 0 : password.length())+", "+CumulocityBundleConstants.CONFIG_HARDWARE_ID+"="+hardwareid);
				return;
			}
			
			init(host, Integer.parseInt(port), externaldeviceid, hardwareid, login, password);
		}
	}

	private void enqueue(ManagementItem item) {
		if (stopBinding) {
			logger.debug("Binding was stopped. Ignoring item.");
			return;
		}
		
		synchronized(queue) {
			if (queue.size() >= CumulocityBundleConstants.MAX_OFFLINE_CACHE) {
				queue.remove(0);
			}
			queue.add(item);
		}
	}

	@Override
	public FileDescriptor storeAsTextFile(String content, String storageFilename) {
		if (!isOnline) {
			logger.warn("Unable to upload files while offline");
			return null;
		}
		
		storageFilename = this.externalDeviceId+"-"+storageFilename;
		return fileHandler.storeAsTextFile(content, storageFilename);
	}


	@Override
	public FileDescriptor sendFile(File fileToSend, String storageFilename, boolean zipBeforeSending) {
		if (!isOnline) {
			logger.warn("Unable to upload files while offline");
			return null;
		}
		
		storageFilename = this.externalDeviceId+"-"+storageFilename;
		return fileHandler.sendFile(fileToSend, storageFilename, zipBeforeSending);
	}


	@Override
	public FileDescriptor sendPartOfFile(File fileToSend, int lineCount, boolean startFromBottom, String storageFilename) {
		if (!isOnline) {
			logger.warn("Unable to upload files while offline");
			return null;
		}
		
		storageFilename = this.externalDeviceId+"-"+storageFilename;
		return fileHandler.sendPartOfFile(fileToSend, lineCount, startFromBottom, storageFilename);
	}


	@Override
	public void sendAlarm(Alarm alarm) {
		if (!readyForTransmission(alarm)) return;
		
		if (!itemTransmitter.sendAlarm(alarm)) {
			enqueue(alarm);
			setupConnection();
		}
		else {
			logger.debug("Alarm sent: "+alarm);
		}
	}

	@Override
	public void sendEvent(Event event) {
		if (!readyForTransmission(event)) return;
		
		if (!itemTransmitter.sendEvent(event)) {
			enqueue(event);
			setupConnection();
		}
		else {
			logger.debug("Event sent: "+event);
		}
	}

	@Override
	public void sendMeasurement(Measurement measurement) {
		if (!readyForTransmission(measurement)) return;
		
		if (!itemTransmitter.sendMeasurement(measurement)) {
			enqueue(measurement);
			setupConnection();
		}
		else {
			logger.debug("Measurement sent: "+measurement);
		}
	}

	@Override
	public void sendMeasurements(Measurement[] measurements) {
		if (measurements == null) return;
		
		if (!isOnline) {
			for (Measurement currMeasurement : measurements) {
				enqueue(currMeasurement);
			}
			return;
		}
		
		if (!itemTransmitter.sendMeasurements(measurements)) {
			for (Measurement currMeasurement : measurements) {
				enqueue(currMeasurement);
			}
			setupConnection();
		}
		else {
			logger.debug("Measurements sent: "+measurements);
		}
	}
	
	private boolean readyForTransmission(ManagementItem item) {
		if (item == null) return false;
		
		if (!isOnline) {
			enqueue(item);
			return false;
		}
		
		return true;
	}
}
