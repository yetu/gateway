package com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils;

import java.util.HashMap;

import com.cumulocity.model.operation.OperationStatus;
import com.cumulocity.rest.representation.operation.OperationRepresentation;
import com.cumulocity.sdk.client.Platform;
import com.cumulocity.sdk.client.devicecontrol.DeviceControlApi;
import com.yetu.gateway.home.core.monitoring.RemoteGatewayManagementAccessor;
import com.yetu.gateway.home.core.monitoring.model.OperationListener;
import com.yetu.gateway.home.core.monitoring.model.OperationResult;

/**
 * Simple class that helps to execute operations asynchronously. Automatically updates state
 * depending on return value of called operation.
 * 
 * @author Sebastian Garn
 */
public class OperationExecutor extends Thread {
	private Platform platform;
	private OperationRepresentation operationRepresentation;
	private OperationListener operationListener;
	private HashMap<String, String> parameters;
	private RemoteGatewayManagementAccessor remoteManagementAccessor;
	
	public OperationExecutor(RemoteGatewayManagementAccessor remoteManagementAccessor, Platform platform, OperationRepresentation operationRepresentation, OperationListener operationListener, HashMap<String, String> parameters) {
		this.platform = platform;
		this.operationRepresentation = operationRepresentation;
		this.operationListener = operationListener;
		this.parameters = parameters;
		this.remoteManagementAccessor = remoteManagementAccessor;
	}
	
	@Override
	public void run() {
		DeviceControlApi deviceControl = platform.getDeviceControlApi();
		
		operationRepresentation.setStatus(OperationStatus.EXECUTING.toString());
		deviceControl.update(operationRepresentation);
		 
		OperationResult result = operationListener.execute(parameters, remoteManagementAccessor);
		
		switch (result.getStatus()) {
			case SUCCESS: {
				operationRepresentation.setStatus(OperationStatus.SUCCESSFUL.toString());
				operationRepresentation.set(result.getDescription(), "result");
				break;
			}
			default: {
				operationRepresentation.setStatus(OperationStatus.FAILED.toString());
				operationRepresentation.setFailureReason(result.getDescription());
				break;
			}
		}

		// update the state
		deviceControl.update(operationRepresentation);
	}
}