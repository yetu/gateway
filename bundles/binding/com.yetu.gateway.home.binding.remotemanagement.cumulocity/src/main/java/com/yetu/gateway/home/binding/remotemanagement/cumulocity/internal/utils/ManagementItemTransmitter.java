package com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cumulocity.rest.representation.alarm.AlarmRepresentation;
import com.cumulocity.rest.representation.event.EventRepresentation;
import com.cumulocity.rest.representation.inventory.ManagedObjectRepresentation;
import com.cumulocity.rest.representation.measurement.MeasurementRepresentation;
import com.cumulocity.sdk.client.Platform;
import com.cumulocity.sdk.client.alarm.AlarmApi;
import com.cumulocity.sdk.client.event.EventApi;
import com.cumulocity.sdk.client.inventory.InventoryApi;
import com.cumulocity.sdk.client.measurement.MeasurementApi;
import com.yetu.gateway.home.core.monitoring.model.Alarm;
import com.yetu.gateway.home.core.monitoring.model.DeviceProperty;
import com.yetu.gateway.home.core.monitoring.model.Event;
import com.yetu.gateway.home.core.monitoring.model.Measurement;

public class ManagementItemTransmitter {
	private static Logger logger = LoggerFactory.getLogger(ManagementItemTransmitter.class);

	private Platform platform;
	private ManagedObjectRepresentation deviceRepresentation;
	private MeasurementApi measurementApi;
	
	public ManagementItemTransmitter(Platform platform, ManagedObjectRepresentation deviceRepresentation) {
		this.platform = platform;
		this.deviceRepresentation = deviceRepresentation;
		
		measurementApi = platform.getMeasurementApi();
	}
	
	public boolean sendAlarm(Alarm alarm) {
		try {
			AlarmApi alarmApi = platform.getAlarmApi();
			
			// convert our alarm representation into cumulocities representation
			AlarmRepresentation alarmRepresentation = new AlarmRepresentation();
			alarmRepresentation.setSource(deviceRepresentation);
			alarmRepresentation.setTime(alarm.getTime());
			alarmRepresentation.setText(alarm.getText());
			alarmRepresentation.setType(alarm.getType());
			alarmRepresentation.setStatus(alarm.getStatus().name());
			alarmRepresentation.setSeverity(alarm.getSeverity().name());
			alarmRepresentation.setProperty("Application", alarm.getSource());

			alarmApi.create(alarmRepresentation);
		} catch(Exception e) {
			logger.warn("Failed to send alarm: "+e.toString());
			return false;
		}
		
		return true;
	}

	public boolean sendEvent(Event event) {
		try {
			EventApi eventsApi = platform.getEventApi();
	
			// convert our event representation into cumulocities representation
			EventRepresentation eventRepresentation = new EventRepresentation();
			eventRepresentation.setSource(deviceRepresentation);
			eventRepresentation.setTime(event.getTime());
			eventRepresentation.setText(event.getText());
			eventRepresentation.setType(event.getType());
			eventRepresentation.setProperty("Application", event.getSource());

			eventsApi.create(eventRepresentation);
		} catch(Exception e) {
			logger.warn("Failed to send event: "+e.toString());
			return false;
		}
		
		return true;
	}
	
	public boolean sendDeviceProperty(DeviceProperty property) {
		try {
			InventoryApi inventoryApi = platform.getInventoryApi();
			logger.debug("Setting value: "+property.getKey()+" "+property.getValue());
			deviceRepresentation.setProperty(property.getKey(), property.getValue());
			inventoryApi.update(deviceRepresentation);
		} catch(Exception e) {
			logger.warn("Failed to send device property: "+e.toString());
			return false;
		}

		return true;
	}
	
	public boolean sendMeasurement(Measurement measurement) {
		try {
			MeasurementRepresentation measurementRepresentation = generateMeasurementRepresentation(measurement);
			measurementApi.create(measurementRepresentation);
		} catch(Exception e) {
			logger.warn("Failed to send measurement: "+e.toString());
			return false;
		}
		
		return true;
	}

	public boolean sendMeasurements(Measurement[] measurements) {
		try {
			for (Measurement currMeasurement : measurements) {
				MeasurementRepresentation measurementRepresentation = generateMeasurementRepresentation(currMeasurement);
				measurementApi.create(measurementRepresentation);
			}
		} catch(Exception e) {
			logger.debug("Failed to send measurement: "+e.toString());
			return false;
		}
		
		return true;
	}
	
	/**
	 * Maps yetu Measurement to cumulocity measurement
	 * @param measurement Measurement
	 * @return MeasurementRepresentation
	 */
	private MeasurementRepresentation generateMeasurementRepresentation(Measurement measurement) {
		MeasurementRepresentation measurementRepresentation = new MeasurementRepresentation();
		measurementRepresentation.setSource(deviceRepresentation);
		measurementRepresentation.setTime(measurement.getTime());
		measurementRepresentation.setType("com.type");
		
		HashMap<String, MeasurementEntry> entries = new HashMap<String, MeasurementEntry>();
		entries.put(measurement.getProperty().getName(), new MeasurementEntry(measurement.getValue(),measurement.getProperty().getUnit()));
		
		measurementRepresentation.set(entries, measurement.getName());
		return measurementRepresentation;
	}

	public class MeasurementEntry {
		double value;
		String unit;
		
		public MeasurementEntry(double value, String unit) {
			this.value = value;
			this.unit = unit;
		}
		
		public String getUnit() {
			return unit;
		}

		public void setUnit(String unit) {
			this.unit = unit;
		}
		
		public double getValue() {
			return value;
		}
		public void setValue(double value) {
			this.value = value;
		}
	}
}
