package com.yetu.gateway.home.binding.remotemanagement.cumulocity

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.junit.matchers.JUnitMatchers.*

import java.nio.charset.Charset;
import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.zip.GZIPOutputStream

import org.eclipse.smarthome.test.OSGiTest
import org.junit.Before
import org.junit.Test

import java.io.ByteArrayOutputStream

import com.cumulocity.sdk.client.PlatformImpl
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.CumulocityRemoteGatewayManagementAccessor;
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils.FileHandler;

/**
 * 
 * @author Sebastian Garn
 */
class FileUploadTest extends OSGiTest {
	private int DEFAULT_DUMMY_SOCKET_PORT = 31445;
	CumulocityRemoteGatewayManagementAccessor accessor

	PlatformImpl mockDefaultPlatform;	
	
	@Before
	void setUp() {
		accessor = new CumulocityRemoteGatewayManagementAccessor();
		accessor.fileHandler = new FileHandler("http", "127.0.0.1", DEFAULT_DUMMY_SOCKET_PORT, "");
		accessor.isOnline = true;
	}

	@Test
	void 'Attempts to upload and store invalid files returns null'() {
		assertNull accessor.sendFile(null, null, false);
		assertNull accessor.sendFile(new File("abc"), null, false);
		assertNull accessor.sendFile(new File("abc"), "abc", false);
		assertNull accessor.sendFile(null, null, false);
	}
	
	@Test
	void 'You are not allowed to upload directories'() {
		String directory = this.class.getProtectionDomain().getCodeSource().getLocation().getPath()
		File toUpload = new File(directory);
		
		DummyServerSocket dummyServer = new DummyServerSocket(DEFAULT_DUMMY_SOCKET_PORT);
		dummyServer.start();
		
		// server should be started
		waitFor({dummyServer.isListening()},2000)
		assertTrue dummyServer.isListening()
		assertFalse dummyServer.requestReceived()

		accessor.sendFile(toUpload, "test", false);
		
		// server should not get a request
		waitFor({dummyServer.requestReceived()},2000)
		assertFalse dummyServer.requestReceived()
		
		// server should be stopped
		dummyServer.halt();
	}
	
	@Test
	void 'Uploading an unzipped file and store it unzipped'() {
		String fileContent = "Hello world";

		String directory = this.class.getProtectionDomain().getCodeSource().getLocation().getPath()
		
		// create dummy file
		File toUpload = File.createTempFile("cumulocity-test", ".txt");
		FileWriter writer = new FileWriter(toUpload);
		writer.write(fileContent);
		writer.close();

		DummyServerSocket dummyServer = new DummyServerSocket(DEFAULT_DUMMY_SOCKET_PORT);
		dummyServer.start();
		
		// server should be started
		waitFor({dummyServer.isListening()},2000)
		assertTrue dummyServer.isListening()
		assertFalse dummyServer.requestReceived()

		Thread request = new Thread() {
			public void run() {
				accessor.sendFile(toUpload, "test", false);
			}
		};
		request.start();
		
		// server should now get a request
		waitFor({dummyServer.requestReceived()},2000)
		assertTrue dummyServer.requestReceived()
		
		// search for the request boundary
		validateRequest(dummyServer.getLastRequest(), fileContent, fileContent.length());
		
		// server should be stopped
		dummyServer.halt();
	}
	
	@Test
	void 'Uploading a zipped file and store it'() {
		String fileContent = "Hello world";

		String directory = this.class.getProtectionDomain().getCodeSource().getLocation().getPath()
		
		// create dummy file
		File toUpload = File.createTempFile("cumulocity-test", ".txt.gz");
        
		OutputStream fileOutStream = new GZIPOutputStream(new FileOutputStream(toUpload));
		ByteArrayOutputStream byteArrayOutStream = new ByteArrayOutputStream()
		OutputStream gzippedArrayOutStream = new GZIPOutputStream(byteArrayOutStream);
		
		fileOutStream.write(fileContent.getBytes());
		gzippedArrayOutStream.write(fileContent.getBytes());
		
		fileOutStream.close();
		gzippedArrayOutStream.close();
		byteArrayOutStream.close();
		
		String expectedContent = byteArrayOutStream.toString("UTF-8");
		int expectedSize = byteArrayOutStream.size();

		assertEquals byteArrayOutStream.size(), toUpload.size()
		
		DummyServerSocket dummyServer = new DummyServerSocket(DEFAULT_DUMMY_SOCKET_PORT);
		dummyServer.start();
		
		// server should be started
		waitFor({dummyServer.isListening()},2000)
		assertTrue dummyServer.isListening()
		assertFalse dummyServer.requestReceived()

		Thread request = new Thread() {
			public void run() {
				accessor.sendFile(toUpload, "test", true);
			}
		};
		request.start();
		
		// server should now get a request
		waitFor({dummyServer.requestReceived()},2000)
		assertTrue dummyServer.requestReceived()
		
		// search for the request boundary
		validateRequest(dummyServer.getLastRequest(), expectedContent, expectedSize);
		
		// server should be stopped
		dummyServer.halt();
	}
	
	/**
	 * Checks whether the received requests contains all necessary parts
	 * @param receivedRequest
	 * @return
	 */
	private void validateRequest(String receivedRequest, String expectedContent, long expectedSize) {
		Pattern searchPattern = Pattern.compile("\nContent-Type: multipart/form-data; boundary=([^\n]+)\n");
		String boundary = extractGroupFromRegex(searchPattern, receivedRequest)

		assertNotNull boundary
		
		Pattern boundaryPattern = Pattern.compile("--"+boundary);
		List parts = Arrays.asList(boundaryPattern.split(receivedRequest));
		
		assertThat parts.size(), is(5)

		Pattern namePattern = Pattern.compile("name=\"([^\"]+)\"")		
		Map partMap = parts.groupBy({ extractGroupFromRegex(namePattern, it) });
		
		// part with file contents
		assertTrue partMap.containsKey("file")
		assertTrue partMap.containsKey("filesize")
		assertTrue partMap.containsKey("object")
		
		assertNotNull getLastLine(partMap["file"])
		assertEquals (expectedContent, getLastLine(partMap["file"]))

		assertNotNull getLastLine(partMap["filesize"])
		assertEquals (""+expectedSize, getLastLine(partMap["filesize"]));
	}

	private String extractGroupFromRegex(Pattern pattern, String input) {
		Matcher matcher = pattern.matcher(input);

		String boundary;

		while (matcher.find()) {
			boundary = matcher.group(1);
			break;
		}
		return boundary
	}
	
	private String getLastLine(String multiLineInput) {
		String[] result = multiLineInput.split("\n");

		if (result.length == 0) return null;
		return result[result.size()-1];
	}
	
	/**
	 * Creates a simple socket which closes as soon as it gets a request
	 */
	class DummyServerSocket extends Thread {
		private volatile int port;
		private volatile boolean requestReceived = false;
		private volatile String lastReceivedRequest = "";
		private volatile boolean isListening = true;
		private volatile boolean stop = false;
		private volatile ServerSocket serverSocket;

		public DummyServerSocket(int port) {
			this.port = port;
		}
		
		@Override
		public void run() {
			try {
				// setup socket
				serverSocket = new ServerSocket(port);
				Socket clientSocket = serverSocket.accept();

				BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), Charset.forName("UTF-8")));
				String inputLine = "";
				
				isListening = true;
				while ((inputLine = input.readLine()) != null && !stop) {
					lastReceivedRequest += inputLine+"\n"
					requestReceived = true;
				}
				
				// stop the socket
				input.close();
				clientSocket.close();
				serverSocket.close();
			} catch(Exception e) {
				
			}
			
			isListening = false;
		}
		
		public boolean isListening() {
			return isListening;
		}
		
		public String getLastRequest() {
			return lastReceivedRequest;
		}
		
		public boolean requestReceived() {
			return requestReceived;
		}
		
		public void halt() {
			stop = true;
			serverSocket.close();
		}
	}
}
