package com.yetu.gateway.home.binding.remotemanagement.cumulocity

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.junit.matchers.JUnitMatchers.*

import org.eclipse.smarthome.test.OSGiTest
import org.junit.After
import org.junit.Before
import org.junit.Test

import com.cumulocity.sdk.client.PlatformImpl
import com.cumulocity.sdk.client.alarm.AlarmApi
import com.cumulocity.sdk.client.event.EventApi
import com.cumulocity.sdk.client.inventory.InventoryApi
import com.cumulocity.sdk.client.measurement.MeasurementApi
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils.AdvancedFileReader


/**
 * @author Sebastian Garn
 */
class AdvancedFileReaderTest extends OSGiTest {
	private static int MAX_LINES = 20;
	private static String LINEBREAK = "\n";
	
	private File dummyFile;

	@Before
	void setUp() {
		String fileContent = "";
		for (int i = 0; i < MAX_LINES; i++) {
			if (i != 0) {
				fileContent += LINEBREAK;
			}
			fileContent += (i+1);
		}

		dummyFile = new File("AdvancedFileReaderTest.txt");

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(dummyFile));
			out.write(fileContent);
			out.close();
		} catch ( IOException e ) {
			println e
		}
	}

	@Test
	void 'Read the first 5 lines from a file'() {
		AdvancedFileReader reader = new AdvancedFileReader(dummyFile);
		String result = reader.readFromTop(5);
		assertTrue result.startsWith("1"+LINEBREAK);
		assertTrue result.endsWith(LINEBREAK+"5"+LINEBREAK);
	}
	
	@Test
	void 'Read the last 5 lines from a file'() {
		AdvancedFileReader reader = new AdvancedFileReader(dummyFile);
		String result = reader.readFromBottom(5);
		assertTrue result.startsWith((MAX_LINES-4)+LINEBREAK);
		assertTrue result.endsWith(LINEBREAK+MAX_LINES+LINEBREAK);
	}
	
	@Test
	void 'Reading more from top than is available gives back whole file'() {
		AdvancedFileReader reader = new AdvancedFileReader(dummyFile);
		String result = reader.readFromTop(MAX_LINES+10);
		assertTrue result.startsWith("1"+LINEBREAK);
		assertTrue result.endsWith(LINEBREAK+MAX_LINES+LINEBREAK);
	}
	
	@Test
	void 'Reading more from bottom than is available gives back whole file'() {
		AdvancedFileReader reader = new AdvancedFileReader(dummyFile);
		String result = reader.readFromBottom(MAX_LINES+10);
		assertTrue result.startsWith("1"+LINEBREAK);
		assertTrue result.endsWith(LINEBREAK+MAX_LINES+LINEBREAK);
	}
	
	@Test
	void 'Reading from non existing file causes exception'() {
		try {
			AdvancedFileReader reader = new AdvancedFileReader(new File("abc"));
		} catch(FileNotFoundException fileException) {
			assertTrue true
			return
		} catch(Exception e) {
			assertTrue false
		}
		
		assertTrue false
	} 
	
	@After
	void cleanUp() {
		dummyFile.delete();
	}
}
