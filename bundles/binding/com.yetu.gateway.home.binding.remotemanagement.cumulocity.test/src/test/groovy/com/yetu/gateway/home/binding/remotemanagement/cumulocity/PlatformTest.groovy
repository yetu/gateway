package com.yetu.gateway.home.binding.remotemanagement.cumulocity

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.junit.matchers.JUnitMatchers.*

import org.eclipse.smarthome.test.OSGiTest
import org.junit.Before
import org.junit.Test

import com.cumulocity.model.idtype.GId
import com.cumulocity.rest.representation.alarm.AlarmRepresentation
import com.cumulocity.rest.representation.inventory.ManagedObjectRepresentation
import com.cumulocity.sdk.client.Platform
import com.cumulocity.sdk.client.PlatformImpl
import com.cumulocity.sdk.client.SDKException;
import com.cumulocity.sdk.client.alarm.AlarmApi
import com.cumulocity.sdk.client.devicecontrol.DeviceControlApi;
import com.cumulocity.sdk.client.devicecontrol.DeviceCredentialsApi;
import com.cumulocity.sdk.client.event.EventApi
import com.cumulocity.sdk.client.identity.IdentityApi;
import com.cumulocity.sdk.client.inventory.InventoryApi
import com.cumulocity.sdk.client.measurement.MeasurementApi
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.CumulocityRemoteGatewayManagementAccessor;
import com.yetu.gateway.home.binding.remotemanagement.cumulocity.internal.utils.ManagementItemTransmitter
import com.yetu.gateway.home.core.monitoring.model.Alarm
import com.yetu.gateway.home.core.monitoring.model.Event
import com.yetu.gateway.home.core.monitoring.model.Measurement
import com.yetu.gateway.home.core.monitoring.model.Property

import groovy.mock.interceptor.MockFor

/**
 * 
 * @author Sebastian Garn
 */
class PlatformTest extends OSGiTest {
	CumulocityRemoteGatewayManagementAccessor accessor

	// default values
	Alarm myAlarm
	Event myEvent
	Measurement myMeasurement
	
	Platform mockDefaultPlatform;

	@Before
	void setUp() {
		accessor = new CumulocityRemoteGatewayManagementAccessor();

		def mockAlarmApi = [create:{ AlarmRepresentation alarmRepresentation -> alarmRepresentation }] as AlarmApi
		def mockEventApi = [create:{ eventRepresentation -> eventRepresentation }] as EventApi
		def mockMeasurementApi = [create:{ measurementRepresentation -> measurementRepresentation }] as MeasurementApi
		def mockInventoryApi = [create:{ ManagedObjectRepresentation managedObjectRepresentation -> managedObjectRepresentation }] as InventoryApi

		mockDefaultPlatform = [getAlarmApi:{ mockAlarmApi },
							getEventApi:{ mockEventApi },
							getMeasurementApi: {  mockMeasurementApi },
							getInventoryApi: { mockInventoryApi }] as Platform
		
		InventoryApi api = mockDefaultPlatform.getInventoryApi();
		MeasurementApi mapi = mockDefaultPlatform.getMeasurementApi();
						
		myAlarm = new Alarm("test","testalarm","urgent",new Date(), Alarm.Severity.CRITICAL, Alarm.Status.ACTIVE);
		myEvent = new Event("test","testevent", "info",new Date());
		myMeasurement = new Measurement("test", "cpu", 50.0, new Property("cpu measurement", "%"), new Date());

		accessor.platform = mockDefaultPlatform
		accessor.isOnline = true;
		accessor.activate(null);
	}

	@Test
	void 'Messages get cached if remote management is not online'() {
		assertThat accessor.queue.size(), is(0)
		
		accessor.isOnline = false;
		
		accessor.sendAlarm(myAlarm);
		accessor.sendEvent(myEvent);
		accessor.sendMeasurement(myMeasurement);
		
		assertThat accessor.queue.size(), is(3) 
	}
	
	@Test
	void 'Invalid content types are ignored'() {
		assertThat accessor.queue.size(), is(0)
		accessor.isOnline = false;

		accessor.sendAlarm(null);
		accessor.sendEvent(null);
		accessor.sendMeasurement(null);
		
		assertThat accessor.queue.size(), is(0)
	}
	
	@Test
	void 'Unexpected connection interruption causes reconnect and enqueues messages'() {
		assertThat accessor.queue.size(), is(0)
		accessor.isOnline = true;
		accessor.isFirstConnection = false;

		Platform mockInvalidPlatform = [getAlarmApi:{ null },getMeasurementApi:{ null }] as Platform
		accessor.platform = mockInvalidPlatform;
		accessor.itemTransmitter = new ManagementItemTransmitter(mockInvalidPlatform, null);

		accessor.sendAlarm(myAlarm);
		accessor.sendAlarm(myAlarm);
		
		// both events should have been chached
		assertThat accessor.queue.size(), is(2)

		// platform should try to reconnect
		waitFor({accessor.isConnecting == true}, 5000);
		assertTrue accessor.isConnecting
		
		// switch to proper platform
		accessor.platform = mockDefaultPlatform;
		
		// doing this will simulate a successful connection
		accessor.inventoryDeviceId = new GId("dummy");

		// platform should be connected now
		waitFor({accessor.isConnecting == false}, 10000);
		assertFalse accessor.isConnecting

		// synch should take place
		waitFor({accessor.queue.size() == 0}, 5000);
		assertThat accessor.queue.size(), is(0)
	}
	
	@Test
	void 'assert list directory contents operation restricts which directories can be listed'() {
		println "Accessor is: "+accessor;
		
		
	}
}
