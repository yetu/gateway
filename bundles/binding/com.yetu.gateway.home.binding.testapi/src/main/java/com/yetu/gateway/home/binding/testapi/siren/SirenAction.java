package com.yetu.gateway.home.binding.testapi.siren;

public class SirenAction {
	public String name;
	public String title;
	public String href;
	public String method;
	public String[] fields;

	public SirenAction(String name, String title, String href, String method, String[] fields) {
		this.name = name;
		this.title = title;
		this.href = href;
		this.method = method;
		this.fields = fields;
	}
}