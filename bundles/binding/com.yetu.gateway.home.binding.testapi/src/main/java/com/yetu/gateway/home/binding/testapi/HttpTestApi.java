package com.yetu.gateway.home.binding.testapi;

import java.io.IOException;
import java.util.Dictionary;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.binding.testapi.siren.SirenEntity;
import com.yetu.gateway.home.binding.testapi.utils.EndpointFactory;
import com.yetu.gateway.home.binding.testapi.utils.MethodConverter;
import com.yetu.gateway.home.core.testing.TestEndpoint;
import com.yetu.gateway.home.core.testing.TestEndpoint.Method;
import com.yetu.gateway.home.core.testing.TestEndpointRegistry;

/**
 * Creates a simple api which allows to access so called testendpoints. Testendpoints are
 * offered by bundles by introducing them to the TestEndpointRegistry.
 * 
 * @author Sebastian Garn, Mathias Runge 
 *
 */
public class HttpTestApi implements ManagedService {
	
	private static final Logger logger = LoggerFactory.getLogger(HttpTestApi.class); 

	// defines http://localhost:8080/-endpoint-
	private static String BASE_URI = "testapi";
	
	protected HttpService httpService; 
 
	protected TestEndpointRegistry endpointRegistry;
	
	private EndpointFactory endpointFactory;

	/**
	 * Create a servlet which intercepts all requests starting with BASE_URI
	 */
	public class SimpleServlet extends HttpServlet {
		
		private static final long serialVersionUID = 1L;
		
		// json converter
		private ObjectMapper mapper = new ObjectMapper();
		private ObjectWriter jsonConverter = mapper.defaultPrettyPrintingWriter();

		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				resp.setContentType("text/plain");
				
				if (responsibleForEndpoint(req)) {
					consume(req, resp, HttpMethod.POST);
				}
				else {
					resp.sendError(404);
				}
		}
		
		protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				resp.setContentType("text/plain");
				
				if (responsibleForEndpoint(req)) {
					consume(req, resp, HttpMethod.PUT);
				}
				else {
					resp.sendError(404);
				}
		}
		
		protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				resp.setContentType("text/plain");
				
				if (responsibleForEndpoint(req)) {
					consume(req, resp, HttpMethod.DELETE);
				}
				else {
					resp.sendError(404);
				}
		}

		/**
		 * Prints out a list of all available endpoints if root endpoint gets called
		 */
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				resp.setContentType("text/plain");

				if (responsibleForEndpoint(req)) {
					// endpoint: /testapi
					if (req.getRequestURI().endsWith(BASE_URI)) {
						
						endpointFactory.setup(req.getRequestURL()+"", endpointRegistry);
						SirenEntity testapisEndpoint = endpointFactory.createTestapiEndpoint();
						resp.getWriter().write(jsonConverter.writeValueAsString(testapisEndpoint));
					}
					// endpoint: /testapi/testendpoints
					else if (req.getRequestURI().endsWith(BASE_URI+"/testendpoints")) {
						endpointFactory.setup(req.getRequestURL()+"", endpointRegistry);
						SirenEntity testendpointsEndpoint = endpointFactory.createTestendpointsEndpoint(false);
						resp.getWriter().write(jsonConverter.writeValueAsString(testendpointsEndpoint));
					}
					// endpoint: /testapi/testendpoints/abc
					else {
						boolean found = false;
						for (TestEndpoint currEndpoint : endpointRegistry.getTestEndpoints()) {
							// only /testapi/testendpoints/abc called
							if (req.getRequestURI().endsWith("/"+BASE_URI+"/testendpoints/"+currEndpoint.getEndpointId())) {
								endpointFactory.setup(req.getRequestURL()+"", endpointRegistry);
								SirenEntity endpoint = endpointFactory.createTestendpointEndpoint(currEndpoint.getEndpointId());
								resp.getWriter().write(jsonConverter.writeValueAsString(endpoint));
								resp.setStatus(200);
								found = true;
								break;
							}
						}
						
						// no static endpoint - redirect to endpoints
						if (!found) consume(req, resp, HttpMethod.GET);
					}
				}
				else {
					resp.sendError(404);
				}
		}

		/**
		 * Checks whether called uri is trying to access any subelement of endpoint 
		 * @param req
		 * @return
		 */
		private boolean responsibleForEndpoint(HttpServletRequest req) {
			String uri = req.getRequestURI();
			String[] tokens = uri.split("/");
			if (tokens.length >= 2 && BASE_URI.equals(tokens[1])) return true;
			return false;
		}
		
		/**
		 * Searches for a testendpoint which handles the request.
		 * @param req - HttpServletRequest
		 * @param resp - HttpServletResponse
		 * @return
		 * @throws ServletException
		 * @throws IOException
		 */
		private boolean consume(HttpServletRequest req, HttpServletResponse resp, String method) throws ServletException, IOException {
			for (TestEndpoint currEndpoint : endpointRegistry.getTestEndpoints()) {
				try {
					String endpointIdentifier = currEndpoint.getEndpointId()+"/"+currEndpoint.getOperation();
					String endpointMethod = MethodConverter.toHttpMethod(currEndpoint.getMethod());
					
					if (req.getRequestURI().endsWith(endpointIdentifier) && method.equals(endpointMethod)) {
						logger.debug(currEndpoint.getEndpointId()+", "+currEndpoint.getOperation()+" could handle request. Checking parameters");
						
						// check parameters
						if (!currEndpoint.parametersFulfilled(req.getParameterMap())) {
							String parameters = "";
							for (String currParam : currEndpoint.getExpectedParameters()) {
								parameters += currParam + " ";
							}

							resp.sendError(400, "Endpoint expects parameters: "+parameters);
							return true;
						}
						
						// let endpoint do its job
						String result = "";
						
						try {
							result = currEndpoint.handle(req.getParameterMap());
						} catch(Exception e) {
							resp.sendError(500, "Endpoint was not able to handle request. "+e.toString());
							return false;
						}

						logger.debug("Operation completed");
						
						// its a get request: show output of handle call
						if (currEndpoint.getMethod().equals(Method.GET)) {
							resp.getWriter().write(result);
						}
						else {
							// move one directory up and show bundles testendpoints again
							endpointFactory.setup(endpointFactory.getParent(req.getRequestURL()+""), endpointRegistry);
							SirenEntity endpoint = endpointFactory.createTestendpointEndpoint(currEndpoint.getEndpointId());
							resp.getWriter().write(jsonConverter.writeValueAsString(endpoint));
							
						}
						
						resp.setStatus(200);
						return true;
					}
				} catch (Exception e) {
					logger.error("Failed to process request", e); 
					resp.sendError(500, e.toString());
					return false;
				}
			}
			
			// no service found to handle request
			resp.sendError(404);
			return false;
		}
	}
	
	
	/** service injection **/
	
	public void setHttpService(HttpService httpService) {
		logger.debug("HttpService registry injected");
		
		this.httpService = httpService; 
		
		try {
			this.httpService.registerServlet("/"+BASE_URI, new SimpleServlet(), null, null);
		} catch (ServletException e) {
			logger.error("Servlet reg failed", e); 
		} catch (NamespaceException e) {
			logger.error("Servlet reg failed", e); 
		}
	}

	public void unsetHttpService(HttpService httpService) {
		this.httpService.unregister("/"+BASE_URI);
		this.httpService = null;
	}

	public void addTestEndpointRegistry(TestEndpointRegistry endpointRegistry) {
		logger.debug("Endpoint registry injected");
		
		this.endpointRegistry = endpointRegistry;
		endpointFactory = new EndpointFactory();
	}
	
	public void removeTestEndpointRegistry(TestEndpointRegistry endpointRegistry) {
		this.endpointRegistry = null;
	}

	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
		
	}
}
