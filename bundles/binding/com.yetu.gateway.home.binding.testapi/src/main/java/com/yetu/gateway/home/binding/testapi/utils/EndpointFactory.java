package com.yetu.gateway.home.binding.testapi.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.binding.testapi.siren.SirenAction;
import com.yetu.gateway.home.binding.testapi.siren.SirenEntity;
import com.yetu.gateway.home.binding.testapi.siren.SirenLink;
import com.yetu.gateway.home.core.testing.TestEndpoint;
import com.yetu.gateway.home.core.testing.TestEndpointRegistry;

public class EndpointFactory {
	private static final Logger logger = LoggerFactory.getLogger(EndpointFactory.class);
	
	private String currentUrl = "";
	private TestEndpointRegistry endpointRegistry;
	
	public void setup(String currentUrl, TestEndpointRegistry endpointRegistry) {
		this.currentUrl = currentUrl;
		this.endpointRegistry = endpointRegistry;
	}
	
	/**
	 * Shows content of /testapi
	 * 
	 * @return SirenDescriptor
	 */
	public SirenEntity createTestapiEndpoint() {
		if (endpointRegistry == null) {
			return null;
		}
		
		SirenEntity testendpoints = new SirenEntity(new String[]{"testendpoints","collection"}, currentUrl+"/testendpoints");
		
		// only show different endpoint links
		return new SirenEntity(new String[]{"testapi"}, null, new SirenEntity[]{testendpoints}, null, null);
	}
	
	/**
	 * Shows content of /testapi/testendpoints
	 * 
	 * @return SirenDescriptor
	 */
	public SirenEntity createTestendpointsEndpoint(boolean embed) {
		if (endpointRegistry == null) {
			return null;
		}
		
		ArrayList<String> testendpoints = new ArrayList<String>();
		
		// check how many different endpoints we have
		for (TestEndpoint currEndpoint : endpointRegistry.getTestEndpoints()) {
			if (!testendpoints.contains(currEndpoint.getEndpointId())) {
				testendpoints.add(currEndpoint.getEndpointId());
			}
		}
		
		ArrayList<SirenEntity> entities = new ArrayList<SirenEntity>();
		
		String baseurl = currentUrl;
		for (String currTestendPoint : testendpoints) {
			currentUrl = baseurl +"/"+currTestendPoint;
			if (embed) {
				entities.add(createTestendpointEndpoint(currTestendPoint));
			}
			else {
				entities.add(new SirenEntity(new String[]{"testendpoint"}, currentUrl));
			}
		}
		
		currentUrl = baseurl;
		
		SirenLink parentLink = new SirenLink("parent", getParent(baseurl));
		return new SirenEntity(new String[]{"testendpoints","collection"}, null, entities.toArray(new SirenEntity[entities.size()]), null, new SirenLink[]{parentLink});
	}
	
	/**
	 * Shows content of a specific endpoint like /testapi/testendpoints/endpointABC
	 * 
	 * @return SirenDescriptor
	 */
	public SirenEntity createTestendpointEndpoint(String endpoint) {
		if (endpointRegistry == null) {
			return null;
		}
		
		ArrayList<SirenAction> actions = new ArrayList<SirenAction>();

		for (TestEndpoint currEndpoint : endpointRegistry.getTestEndpoints()) {
			if (currEndpoint.getEndpointId().compareTo(endpoint) == 0) {
				actions.add(
						new SirenAction(	currEndpoint.getOperation(),
											currEndpoint.getDescription(), 
											currentUrl+"/"+currEndpoint.getOperation(),
											MethodConverter.toHttpMethod(currEndpoint.getMethod()),
											currEndpoint.getExpectedParameters()
						));
			}
		}
		
		SirenLink parentLink = new SirenLink("parent", getParent(currentUrl));
		return new SirenEntity(new String[]{"testendpoint"}, null, null, actions.toArray(new SirenAction[actions.size()]), new SirenLink[]{parentLink});
	}
	
	/**
	 * Tries to extract the parent of a given uri.
	 * 
	 * @param url - String
	 * @return parent or null if error occurred
	 */
	public String getParent(String url) {
		try {
			URI uri = new URI(url);
			URI parentUri = uri.getPath().endsWith("/") ? uri.resolve("..") : uri.resolve(".");
			String parent = parentUri.toString();
			
			if (parent.endsWith("/")) {
				return parent.substring(0, parent.length()-1);
			}

			return parent;
		} catch(URISyntaxException ex) {
			logger.error("Failed to get parent.", ex);
		}
		
		return null;
	}
}
