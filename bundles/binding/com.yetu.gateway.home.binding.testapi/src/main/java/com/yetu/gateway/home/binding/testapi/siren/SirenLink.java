package com.yetu.gateway.home.binding.testapi.siren;

public class SirenLink {
	public String rel;
	public String href;

	public SirenLink(String rel, String href) {
		this.rel = rel;
		this.href = href;
	}
}