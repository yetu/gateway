package com.yetu.gateway.home.binding.testapi.siren;

import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SirenEntity {
	@JsonProperty("class")
	public String[] classes;
	public HashMap<String, String> properties;
	public SirenEntity[] entities;
	public SirenAction[] actions;
	public String href;
	public SirenLink[] links;
	
	/**
	 * Creates an embedded entity
	 * @param classes
	 * @param properties
	 * @param entities
	 * @param actions
	 */
	public SirenEntity(String[] classes, HashMap<String, String> properties, SirenEntity[] entities, SirenAction[] actions, SirenLink[] links) {
		this.classes = classes;
		this.properties = properties;
		this.entities = entities;
		this.actions = actions;
		this.links = links;
	}
	
	/**
	 * Creates a non embedded entity
	 * @param classes
	 * @param href
	 */
	public SirenEntity(String[] classes, String href) {
		this.classes = classes;
		this.href = href;
	}
}