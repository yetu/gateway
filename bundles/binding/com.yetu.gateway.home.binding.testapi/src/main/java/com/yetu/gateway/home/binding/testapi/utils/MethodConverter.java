package com.yetu.gateway.home.binding.testapi.utils;

import javax.ws.rs.HttpMethod;

import com.yetu.gateway.home.core.testing.TestEndpoint;

public class MethodConverter {
	public static TestEndpoint.Method toEndpointMethod(String httpMethod) {
		switch (httpMethod) {
			case "POST":
				return TestEndpoint.Method.CREATE;
			case "PUT":
				return TestEndpoint.Method.UPDATE;
			case "DELETE":
				return TestEndpoint.Method.REMOVE;
			case "GET":
				return TestEndpoint.Method.GET;
			default:
				return null;
		}
	}
	
	public static String toHttpMethod(TestEndpoint.Method method) {
		switch (method) {
			case CREATE:
				return HttpMethod.POST;
			case UPDATE:
				return HttpMethod.PUT;
			case REMOVE:
				return HttpMethod.DELETE;
			case GET:
				return HttpMethod.GET;
			default:
				return null;
		}
	}
}
