Gateway Team 
==================================================

# Description #
The main purpose of the gateway software is to communicate with sensors and actors that are paired with the gateway and to propagate collected information or actions to yetus cloud service (household service) or vice versa. It is based on the eclipse smarthome framework and its logic is split up into bundles since eclipse smarthome is based on equinox- a implementation of the OSGI core framework. This document gives an overview over all bundles the gateway software consists of and you can build it.

# Responsible persons #
Mathias Runge	(mathias.runge@domoone.de)
Sebastian Garn	(sebastian.garn@posteo.de)

# Overview bundles #
*com.yetu.gateway.home.core*

* Is a collection of basic classes which are used by other bundles

*com.yetu.gateway.home.core.backend*

* This bundle is responsible for routing incoming and outgoing messages from/to the gateway but in an abstracted way. So, it is technology independent. For the physical connection we have used io.netty and kryo for the serialization and deserialization of messages (see below).

*com.yetu.gateway.home.io.backend.kryo*

* Describes which messages are transferred via lombok annotated classes. Generated classes are serialized and deserialized via kryo framework. Transmission takes place by using io.netty socket implementation

*com.yetu.gateway.home.io.backend.mockconnection*

* A bundle which allows to simulate a backend. The bundle is offering its functionalities to the testapi  (see below). It can be used to simulate e.g. successful handshakes or also connection interrupts

*com.yetu.gateway.home.binding.nest*

* The nest binding is used to connect to the nest api in order to „virtually“ pair nest devices with the gateway.

*com.yetu.gateway.home.binding.zwave*

* Connects with zwave devices in abstracted way. Maps physical devices to appropriate classes. Pyhsical connection takes place in io.zwave.

*com.yetu.gateway.home.io.zwave*

* Driver that physically connectes with zwave devices.

*com.yetu.gateway.home.io.zwave.mock*

* Mocks a physical connection woth zwave devices.

*com.yetu.gateway.home.core.management*

* One of the main-core components. It receives abstracted messages (see bundle below) and controls e.g. all bundles which communicate with devices.

*com.yetu.gateway.home.core.messaging*

* Holds message structures that are shared between the bundles of the gateway software. Additionally offers queues for internal messagin to decouple bundles from each other.

*com.yetu.gateway.home.core.monitoring*

* The monitoring bundle is responsible for gathering data about the gateway itself like free diskcapacity, running processes, cpu and so on. It uses the cumulocity bundle to propagate the collected information to the cloud service of cumulocity.

*com.yetu.gateway.home.binding.remotemanagement.cumulocity*

* Does all the message handling between cumulocity api and gateway.

*com.yetu.gateway.home.core.thingabstraction*

* Holds all necessary classes to abstract things like devices or services.

*com.yetu.gateway.home.binding.mockdriver*

* The mockdriver can be used to pair dummy devices that can be controlled. Offers its functionalities over testapi.

*com.yetu.gateway.home.binding.testapi*

* Is used for end-to-end tests. It collects endpoints which are offered by different bundles and creates a rest api out of them. A siren notation is used to describe the endpoints.

*com.yetu.gateway.home.feature.dependencies*

* Configuration of all dependencies which are needed by the product.runtime

*com.yetu.gateway.home.feature.runtime*

* Describes which bundles of the smarthome stack will be used when product is built

*com.yetu.gateway.home.product.runtime*

* Holds product configuration

*distribution*

* Contains e.g. configuration files and runtime files

*integrationtest*

* Contains tests which are executed whenever the smarthome software is built

*targetplatform*

* Used to resolve dependencies during build

# Installation #

Get source
```
#!java
git clone git@bitbucket.org:yetu/gateway.git
cd gateway
git submodule update --init --recursive
```

Increase memory before building the project:
```
#!java
export MAVEN_OPTS="-Xmx512M -XX:MaxPermSize=2048m"
```

Now build the project:
```
#!java
mvn clean install
```

When build is complete go to target directory and unzip created distribution.zip file
```
#!java
distribution/target/
unzip distribution-1.4.0-SNAPSHOT-runtime.zip
```

Configure the gateway software

Create proper configuration file:
```
#!java
cp configurations/smarthome_dev.cfg configurations/smarthome.cfg
```

Configure your gateway ID and backend
```
#!java
nano configurations/smarthome.cfg
```

And finally you can run the gateway software:
```
#!java
./start.sh
```

# Installation for integration test #
For the integration test some special mock bindings have to be included into the build. Instead of doing a mvn clean install you can do:
```
#!java
mvn clean install -Dintegrationtest
```

This creates next to the normal distribution.zip a distribution-integrationtest.zip file. Extract and configure this zip file as described in the
installation chapter. For more information on how to execute the integration test itself please see here: https://yetu-dev.atlassian.net/wiki/display/TGI/End+to+End+Tests

# Setup Eclipse #
One of our bundles uses lombok - a tool which automatically generates getters, setters etc. by using java annotations. Execute the lombok .jar file by double clicking it.
You should use the lombok version which is used during the build process. It should in:
```
#!java
~/.m2/repository/org/projectlombok/lombok/<USED VERSION>/lombok-<USED VERSION>.jar
```
After executing the jar a dialog will pop up that allows you to select into which IDE you want to integrate lombok. Select the one you are using and
hit "Install/Update" button. Afterwards restart eclipse.