#!/bin/sh

cd `dirname $0`

# set path to eclipse folder. If local folder, use '.'; otherwise, use /path/to/eclipse/
eclipsehome="server";

# set ports for HTTP(S) server
HTTP_PORT=8080
HTTPS_PORT=8443

# get path to equinox jar inside $eclipsehome folder
cp=$(find $eclipsehome -name "org.eclipse.equinox.launcher_*.jar" | sort | tail -1);

echo Launching the yetu smart home runtime...
java -Djavax.net.ssl.trustStorePassword=changeit -Djavax.net.ssl.trustStore=./conf/certificates/bindings.keystore.jks -Dlogback.configurationFile=./conf/logback.xml -Dsmarthome.userdata=./runtime -Dsmarthome.configdir=./runtime/conf -Dsmarthome.servicepid=com.yetu -Dorg.quartz.properties=./conf/quartz.properties -Declipse.ignoreApp=true -Dosgi.noShutdown=true -Dosgi.clean=true -Dorg.osgi.service.http.port=$HTTP_PORT -Dorg.osgi.service.http.port.secure=$HTTPS_PORT -Dequinox.ds.block_timeout=240000 -Dequinox.scr.waitTimeOnBlock=60000 -Dfelix.fileinstall.active.level=4 -Dfelix.fileinstall.dir=addons -Djava.library.path=lib -Djava.awt.headless=true -jar $cp $* -console 
