package com.yetu.gateway.home.integrationtest.backend.types;

import io.netty.channel.ChannelHandlerContext;

import com.yetu.kryo_support.Message;

/**
 * Implementing class tell the dummy backend how to act in specific cases.
 * 
 * @author Sebastian Garn
 *
 */
public interface ConnectionEventHandler {
	/**
	 * Gateway has connected to backend
	 * @param context
	 */
	public void onNewConnectionEstablished(ChannelHandlerContext context, Connection connection);
	
	/**
	 * Gateway has sent message to backend
	 * @param context
	 * @param message
	 */
	public void onMessageReceived(ChannelHandlerContext context, Message message);
	
	/**
	 * Gateway has disconnected for backend
	 * @param context
	 * @param connection
	 */
	public void onConnectionClosed(ChannelHandlerContext context, Connection connection);
}
