package com.yetu.gateway.home.integrationtest.backend.types;

import com.yetu.gatewayprotocol.kryo.GatewayMessage;

public interface MessageResponder {
	public void handleMessage(Connection connection, GatewayMessage message);
}