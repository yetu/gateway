package com.yetu.gateway.home.integrationtest.backend;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yetu.gateway.home.integrationtest.backend.types.Connection;
import com.yetu.gateway.home.integrationtest.backend.types.ConnectionEventHandler;
import com.yetu.kryo_support.KryoSupport;
import com.yetu.kryo_support.Message;

public class DummyBackend {
	protected static Logger logger = LoggerFactory.getLogger(DummyBackend.class);
	private ConnectionEventHandler connectionHandler;
	
	private HashMap<ChannelHandlerContext, Connection> connections;
	private ChannelFuture channelFuture;
	private ServerBootstrap bootstrap;
	
	public DummyBackend() {
		this.connections = new HashMap<ChannelHandlerContext, Connection>();

		EventLoopGroup group = new NioEventLoopGroup();

		// configure server
        bootstrap = new ServerBootstrap();
        bootstrap.group(group);
        bootstrap.channel(NioServerSocketChannel.class);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000);
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
        	@Override
        	public void initChannel(SocketChannel ch) throws Exception {
        		ChannelPipeline pipeline = ch.pipeline();
        		pipeline.addLast("length-decoder" , new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, -4, 4)); 
        		pipeline.addLast("message-decoder", new ChannelInboundHandlerAdapter() {
        			@Override
        			public void channelRegistered(ChannelHandlerContext ctx) {
        				Connection connection = new Connection(ctx);
        				connections.put(ctx, connection);
        				
        				if (connectionHandler != null) {
        					connectionHandler.onNewConnectionEstablished(ctx, connection);
        				}
        			}
        			
        			@Override
        			public void channelRead(ChannelHandlerContext ctx, Object msg) {
        				logger.debug("Reading from dummy backend channel");
        				ByteBuf m = (ByteBuf) msg;
        				
        				try {
        					byte[] bytes = new byte[m.readableBytes()];
        					m.readBytes(bytes);

        					Message gwMsg = KryoSupport.unmarshall(bytes);
        					
        					if (connectionHandler != null) {
            					connectionHandler.onMessageReceived(ctx, gwMsg);
            				}
        				} catch (Exception e) {
        					logger.error("Unable to read from channel",e);
        				} finally {
        					m.release();
        				}
        			}
        			
        			@Override
        			public void channelUnregistered(ChannelHandlerContext ctx) {
        				if (connectionHandler != null) {
        					connectionHandler.onConnectionClosed(ctx, connections.get(ctx));
        				}
        				connections.get(ctx).close();
        				connections.remove(ctx);
        			}
        		});
        		pipeline.addLast("length-encoder" , new LengthFieldPrepender(4, true));
        	}
        	
        });
	}
	
	public void start(int port) {
        try {
	        // Start the server.
	        channelFuture = bootstrap.bind(port).sync();
        } catch (Exception e) {
        	System.out.println("Failed to create ServerSocket: "+e.toString());
        }
	}
	
	public HashMap<ChannelHandlerContext, Connection> getConnections() {
		return connections;
	}
	
	public boolean stop() {
		channelFuture.channel().close();
		
		for (ChannelHandlerContext handler : connections.keySet()) {
			handler.close();
		}
		
		return true;
	}
	
	public void setConnectionEventHandler(ConnectionEventHandler handler) {
		this.connectionHandler = handler;
	}
}
