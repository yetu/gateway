package com.yetu.gateway.home.integrationtest.backend.types;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;

import com.yetu.gatewayprotocol.kryo.ServerMessage;
import com.yetu.kryo_support.KryoSupport;



public class Connection {
	private ChannelHandlerContext context;
	
	public Connection(ChannelHandlerContext context) {
		this.context = context;
	}

	/**
	 * Sends a message over this dummy connection
	 * @param ServerMessage message
	 * @return True if message was sent
	 */
	public boolean sendMessageToGateway(ServerMessage message) {
		byte[] result = KryoSupport.marshall(message);
		ByteBuf msg = Unpooled.copiedBuffer(result);
		
		ChannelFuture future = context.channel().writeAndFlush(msg); 
		try {
			future.sync();
		} catch (InterruptedException e) {
			System.out.println("Failed to send message: "+e.toString());
			return false;
		}
		
		return true;
	}
	
	public void close() {
		context.close();
	}
}
