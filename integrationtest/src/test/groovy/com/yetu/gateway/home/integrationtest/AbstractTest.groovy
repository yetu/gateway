package com.yetu.gateway.home.integrationtest

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.junit.matchers.JUnitMatchers.*
import io.netty.channel.ChannelHandlerContext

import org.eclipse.smarthome.core.events.EventPublisher
import org.eclipse.smarthome.test.OSGiTest
import org.osgi.framework.BundleContext
import org.osgi.framework.ServiceReference
import org.osgi.util.tracker.ServiceTracker

import com.yetu.gateway.home.core.testing.DefaultTestEndpointRegistry
import com.yetu.gateway.home.core.testing.TestEndpoint
import com.yetu.gateway.home.core.testing.TestEndpointRegistry
import com.yetu.gateway.home.integrationtest.backend.types.Connection
import com.yetu.gateway.home.integrationtest.backend.types.ConnectionEventHandler
import com.yetu.kryo_support.Message
import com.yetu.gatewayprotocol.kryo.GatewayMessage.HandshakeRequest

/**
 * Declares some facility methods
 * 
 * @author Sebastian Garn
 */

abstract class AbstractTest extends OSGiTest {
	
	protected EventPublisher eventPublisher;
	protected boolean eventReceived = false;
	
	public static Connection gatewayBackendConnection;
	public static ArrayList<Message> lastGatewayToBackendMessages;

	/**
	 * Waits for a specific service implementation
	 * @param serviceInterfaceClass - Class: Interface of which defines the service
	 * @param implementingServiceClass - Class: The implementing class that implements the service and you are looking for
	 * @param timeout - int: timeout in ms
	 * @return null if timeout occurs
	 */
	public Object waitForService(Class serviceInterfaceClass, Class implementingServiceClass, int timeout) {
		boolean found = false
		long endsAt = Calendar.getInstance().getTimeInMillis() + timeout

		while (Calendar.getInstance().getTimeInMillis() < endsAt) {
			ServiceTracker tracker = new ServiceTracker(bundleContext, serviceInterfaceClass.getName(), null)
			tracker.open()
			tracker.waitForService(1000)

			for (ServiceReference s : tracker.getServiceReferences()) {
				if (bundleContext.getService(s).getClass() == implementingServiceClass) {
					return bundleContext.getService(s)
				}
			}
		}
		
		return null
	}
	
	/**
	 * During the integration test a complete smarthome software stack gets started and all the tests are 
	 * executed within that instance. This method allows to simply print a line within the log so that it
	 * is easier to distinguish the tests from each other
	 *  
	 * @param description - String: usually the test method name
	 */
	public void signalizeNewTest(String description) {
		println "\nNext test: "+description+"\n";
	}
	
	/**
	 * Some bundles expose so called TestEndpoints to the TestEndpointRegistry. They easily allow to access internal functionalities
	 * of bundles without haven access to probably internal packages. This method searches for a specific TestEndpoint.  
	 * @param endpointId
	 * @param operation
	 * @param method
	 * @return TestEndpoint or null if no TestEndpoint with given criteria was found
	 */
	public TestEndpoint getTestEndpoint(String endpointId, String operation, TestEndpoint.Method method) {
		TestEndpointRegistry testEndpointRegistry = waitForService(TestEndpointRegistry.class, DefaultTestEndpointRegistry.class, 5000)
		
		for (TestEndpoint currEndpoint : testEndpointRegistry.getTestEndpoints()) {
			if (currEndpoint.getEndpointId().equals(endpointId) && currEndpoint.getOperation().equals(operation) && currEndpoint.getMethod().compareTo(method) == 0) {
				return currEndpoint
			}
		}
		
		return null;
	}
	
	/**
	 * Waits for a message which should get sent from the gateway to the backend
	 */
	protected boolean isNextOutgoingMessage(Class clazz, int atIndex) {
		return isNextOutgoingMessage(clazz, atIndex, 5000)
	}
	
	protected boolean isNextOutgoingMessage(Class clazz, int atIndex, int timeout) {
		waitFor({lastGatewayToBackendMessages.size() > atIndex}, timeout)
		
		if (lastGatewayToBackendMessages.size() <= atIndex) {
			return false;
		}
		
		if (!clazz.isInstance(lastGatewayToBackendMessages.get(atIndex))) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Resets the list of all messages which were sent to backend
	 */
	protected void resetLastGatewayToBackendMessages() {
		if (lastGatewayToBackendMessages == null) {
			lastGatewayToBackendMessages = new ArrayList<Message>();
		}
		
		lastGatewayToBackendMessages.clear();
	}
	
	/**
	 * Resets the list of all messages which were sent to backend
	 */
	protected void resetGatewayBackendConnection() {
		gatewayBackendConnection = null;
	}
	
	/**
	 * Waits for connection establishment and for auto handshake request by gateway
	 */
	protected void waitForConnectionInitialization() {
		waitFor({gatewayBackendConnection != null}, 10000);
		assert gatewayBackendConnection != null;
		
		waitFor({lastGatewayToBackendMessages.size() > 0}, 5000);
		assert lastGatewayToBackendMessages.get(0) instanceof HandshakeRequest
		
		lastGatewayToBackendMessages.clear();
	}
	
	/**
	 * Necessary helper method since groovy is complaining about the inline string array declaration 
	 * @param args
	 * @return
	 */
	public String[] toStringArray(String... args) {
		return args;
	}
}