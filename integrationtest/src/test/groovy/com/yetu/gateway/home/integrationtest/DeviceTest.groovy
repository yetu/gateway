package com.yetu.gateway.home.integrationtest;

import io.netty.channel.ChannelHandlerContext

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.binding.mockdriver.MockBindingConstants
import com.yetu.gateway.home.core.testing.TestEndpoint
import com.yetu.gateway.home.core.testing.TestEndpoint.Method
import com.yetu.gateway.home.integrationtest.backend.DummyBackend
import com.yetu.gateway.home.integrationtest.backend.types.Connection
import com.yetu.gateway.home.integrationtest.backend.types.ConnectionEventHandler
import com.yetu.gatewayprotocol.kryo.GatewayMessage.DiscoverySessionStarted
import com.yetu.gatewayprotocol.kryo.GatewayMessage.DiscoverySessionStopped
import com.yetu.gatewayprotocol.kryo.GatewayMessage.HandshakeRequest
import com.yetu.gatewayprotocol.kryo.GatewayMessage.PropertyChanged
import com.yetu.gatewayprotocol.kryo.GatewayMessage.StartDiscoverySessionReply
import com.yetu.gatewayprotocol.kryo.GatewayMessage.ThingAdded
import com.yetu.gatewayprotocol.kryo.GatewayMessage.SetPropertyError
import com.yetu.gatewayprotocol.kryo.GatewayMessage.ThingStatusChanged;
import com.yetu.gatewayprotocol.kryo.ServerMessage.HandshakeReply
import com.yetu.gatewayprotocol.kryo.ServerMessage.SetProperty
import com.yetu.gatewayprotocol.kryo.ServerMessage.StartDiscoverySessionRequest
import com.yetu.gatewayprotocol.kryo.thing.ThingDescriptor;
import com.yetu.kryo_support.Message

/**
 * 
 * @author Sebastian Garn
 *
 */
public class DeviceTest extends AbstractTest {
	private TestEndpoint addDeviceEndpoint;
	private TestEndpoint changeThingStatusEndpoint;
	private TestEndpoint getThingUIDsEndpoint;
	private TestEndpoint getYetuThingIDEndpoint;
	
	private int DUMMY_BACKEND_PORT = 55443;
	private DummyBackend dummyBackend;

	@Before
	public void setup() {
		dummyBackend = new DummyBackend();
		
		resetGatewayBackendConnection();
		resetLastGatewayToBackendMessages();
		
		addDeviceEndpoint = getTestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DISCOVERY, MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE, TestEndpoint.Method.CREATE);
		assert addDeviceEndpoint != null
		
		getThingUIDsEndpoint = getTestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DEVICES, MockBindingConstants.TESTENDPOINT_GET_THINGUIDS, Method.GET);
		assert getThingUIDsEndpoint != null
		
		changeThingStatusEndpoint = getTestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DEVICES, MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE, Method.UPDATE);
		assert changeThingStatusEndpoint != null;
	}
	
	/**
	 * Paires a mock device with given device id. Generates necessary session for that.
	 * @param String deviceId: id of the device
	 */
	private String pairDevice(String deviceId) {
		// backend sends start discovery session request
		StartDiscoverySessionRequest requestOne = new StartDiscoverySessionRequest("abc","123");
		gatewayBackendConnection.sendMessageToGateway(requestOne);

		assert isNextOutgoingMessage(StartDiscoverySessionReply, 0);
		assert isNextOutgoingMessage(DiscoverySessionStarted, 1);

		// mock driver detects new device
		HashMap<String, String[]> parameters = new HashMap<String, String[]>();
		parameters.put(MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE_PARAM_DEVICEID, toStringArray(deviceId));

		assert addDeviceEndpoint.parametersFulfilled(parameters);
		addDeviceEndpoint.handle(parameters);
		
		// gw should send thing added message
		assert isNextOutgoingMessage(ThingAdded, 3);
		ThingAdded msg = (ThingAdded)lastGatewayToBackendMessages.get(3);

		// discovery session should stop
		assert isNextOutgoingMessage(DiscoverySessionStopped, 4);
		
		assert isNextOutgoingMessage(ThingStatusChanged, 5);
		
		lastGatewayToBackendMessages.clear();
		
		return msg.getThing().getId();
	}
	
	@Test
	void 'Controlling a device generates property changed event'() {
		signalizeNewTest("Controlling a device generates property changed event")

		String DUMMY_DEVICE_ID = "testdev"+Calendar.getInstance().getTimeInMillis().toString();
		
		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		String yetuThingId = pairDevice(DUMMY_DEVICE_ID);
		
		SetProperty setPropertyRequest = new SetProperty(yetuThingId, "SOCKET", "SWITCHABLE", "on", "ON");
		gatewayBackendConnection.sendMessageToGateway(setPropertyRequest);
		
		assert isNextOutgoingMessage(PropertyChanged, 0);
	}
	
	@Test
	void 'Controlling an invalid property of a device produces error'() {
		signalizeNewTest("Controlling an invalid property of a device produces error")

		String DUMMY_DEVICE_ID = "testdev"+Calendar.getInstance().getTimeInMillis().toString();
		
		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		String yetuThingId = pairDevice(DUMMY_DEVICE_ID);
		
		// invalid capability
		SetProperty setPropertyRequest = new SetProperty(yetuThingId, "SOCKET", "abc", "on", "ON");
		gatewayBackendConnection.sendMessageToGateway(setPropertyRequest);
		
		assert isNextOutgoingMessage(SetPropertyError, 0);
		SetPropertyError error = (SetPropertyError)lastGatewayToBackendMessages.get(0);
		assert SetPropertyError.ErrorCode.ERROR_CAPABILITIY_NOT_FOUND == error.getCode()
		
		// invalid propertyname
		setPropertyRequest = new SetProperty(yetuThingId, "SOCKET", "SWITCHABLE", "bla", "ON");
		gatewayBackendConnection.sendMessageToGateway(setPropertyRequest);
		
		assert isNextOutgoingMessage(SetPropertyError, 1);
		error = (SetPropertyError)lastGatewayToBackendMessages.get(1);
		assert SetPropertyError.ErrorCode.ERROR_PROPERTY_NOT_FOUND == error.getCode()
	}
	
	@Test
	void 'A changing thing status generates a thing status changed event'() {
		signalizeNewTest("A changing thing status generates a thing status changed event")

		String DUMMY_DEVICE_ID = "testdev"+Calendar.getInstance().getTimeInMillis().toString();
		
		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		String yetuThingId = pairDevice(DUMMY_DEVICE_ID);
		
		String result = getThingUIDsEndpoint.handle(new HashMap<String, String[]>());
		String deviceId = extractDeviceById(DUMMY_DEVICE_ID, result);
		
		assert deviceId.length() != 0;
		
		// set new state of the thing
		HashMap<String, String[]> parameters = new HashMap<String, String[]>();
		parameters.put(MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_THINGUID, toStringArray(deviceId));
		parameters.put(MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_NEW_STATUS, toStringArray("OFFLINE"));

		assert changeThingStatusEndpoint.parametersFulfilled(parameters);
		changeThingStatusEndpoint.handle(parameters);
		
		assert isNextOutgoingMessage(ThingStatusChanged, 0);
		ThingStatusChanged statusChangedMessage = (ThingStatusChanged)lastGatewayToBackendMessages.get(0);
		
		assert statusChangedMessage.getThingId() == yetuThingId;
		assert statusChangedMessage.getStatus() == ThingDescriptor.Status.UNAVAILABLE;
		
		// set state of thing to ONLINE
		parameters.put(MockBindingConstants.TESTENDPOINT_TRIGGER_THING_STATUS_CHANGE_PARAM_NEW_STATUS, toStringArray("ONLINE"));
		changeThingStatusEndpoint.handle(parameters);
		
		assert isNextOutgoingMessage(ThingStatusChanged, 1);
		statusChangedMessage = (ThingStatusChanged)lastGatewayToBackendMessages.get(1);
		
		assert statusChangedMessage.getThingId() == yetuThingId;
		assert statusChangedMessage.getStatus() == ThingDescriptor.Status.AVAILABLE;
	}
	
	@After
	public void stop() {
		dummyBackend.stop();
	}
	
	private String extractDeviceById(String deviceId, String resultSet) {
		String[] devices = resultSet.split("\n");
		for (String currDevice : devices) {
			if (currDevice.endsWith(deviceId)) {
				return currDevice;
			}
		}
		
		return "";
	}
	
	protected class DefaultBackendConnectionListener implements ConnectionEventHandler {

		@Override
		public void onNewConnectionEstablished(ChannelHandlerContext context, Connection connection) {
			gatewayBackendConnection = connection
		}

		@Override
		public void onMessageReceived(ChannelHandlerContext context, Message message) {
			lastGatewayToBackendMessages.add(message);

			if (message instanceof HandshakeRequest) {
				gatewayBackendConnection.sendMessageToGateway(new HandshakeReply(((HandshakeRequest)message).getRequestId()));
			}
		}

		@Override
		public void onConnectionClosed(ChannelHandlerContext context, Connection connection) {
				if (gatewayBackendConnection == connection) {
					gatewayBackendConnection = null
				}
		}
	}
}
