package com.yetu.gateway.home.integrationtest;

import io.netty.channel.ChannelHandlerContext

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.binding.mockdriver.MockBindingConstants
import com.yetu.gateway.home.core.testing.TestEndpoint
import com.yetu.gateway.home.integrationtest.backend.DummyBackend
import com.yetu.gateway.home.integrationtest.backend.types.Connection
import com.yetu.gateway.home.integrationtest.backend.types.ConnectionEventHandler
import com.yetu.gatewayprotocol.kryo.GatewayMessage.DiscoverySessionFailed;
import com.yetu.gatewayprotocol.kryo.GatewayMessage.DiscoverySessionStarted
import com.yetu.gatewayprotocol.kryo.GatewayMessage.DiscoverySessionStopped
import com.yetu.gatewayprotocol.kryo.GatewayMessage.HandshakeRequest
import com.yetu.gatewayprotocol.kryo.GatewayMessage.RemoveThingReply;
import com.yetu.gatewayprotocol.kryo.GatewayMessage.StartDiscoverySessionReply
import com.yetu.gatewayprotocol.kryo.GatewayMessage.ThingAdded
import com.yetu.gatewayprotocol.kryo.GatewayMessage.ThingAlreadyAddedError
import com.yetu.gatewayprotocol.kryo.GatewayMessage.ThingDiscovered
import com.yetu.gatewayprotocol.kryo.GatewayMessage.ThingStatusChanged;
import com.yetu.gatewayprotocol.kryo.ServerMessage.HandshakeReply
import com.yetu.gatewayprotocol.kryo.ServerMessage.RemoveThingRequest;
import com.yetu.gatewayprotocol.kryo.ServerMessage.StartDiscoverySessionRequest
import com.yetu.gatewayprotocol.kryo.ServerMessage.StopDiscoverySessionRequest
import com.yetu.gatewayprotocol.kryo.GatewayMessage.StopDiscoverySessionReply
import com.yetu.kryo_support.Message

/**
 * 
 * @author Sebastian Garn
 *
 */
public class DiscoverySessionTest extends AbstractTest {
	private TestEndpoint addDeviceEndpoint;
	private TestEndpoint abortScanningEndpoint;
	
	private int DUMMY_BACKEND_PORT = 55443;
	private DummyBackend dummyBackend;

	@Before
	public void setup() {
		dummyBackend = new DummyBackend();
		
		resetGatewayBackendConnection();
		resetLastGatewayToBackendMessages();
		
		addDeviceEndpoint = getTestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DISCOVERY, MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE, TestEndpoint.Method.CREATE);
		assert addDeviceEndpoint != null
		
		abortScanningEndpoint = getTestEndpoint(MockBindingConstants.ENDPOINT_ID_MOCK_DISCOVERY, MockBindingConstants.TESTENDPOINT_ABORT_SCANNING, TestEndpoint.Method.UPDATE);
		assert abortScanningEndpoint != null
	}
	
	@Test
	void 'Starting discovery session twice ends up with error'() {
		signalizeNewTest("Starting discovery session twice ends up with error")

		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		// backend sends start discovery session request
		StartDiscoverySessionRequest requestOne = new StartDiscoverySessionRequest("abc","123");
		gatewayBackendConnection.sendMessageToGateway(requestOne);

		assert isNextOutgoingMessage(StartDiscoverySessionReply, 0);
		assert isNextOutgoingMessage(DiscoverySessionStarted, 1);
		
		StartDiscoverySessionRequest requestTwo = new StartDiscoverySessionRequest("def","456");
		gatewayBackendConnection.sendMessageToGateway(requestTwo);
		
		assert isNextOutgoingMessage(StartDiscoverySessionReply, 2);

		assert isNextOutgoingMessage(DiscoverySessionFailed, 3);
		
		// stop the initially started session
		abortScanningEndpoint.handle(null);
		assert isNextOutgoingMessage(DiscoverySessionStopped, 4);
		
	}
	
	@Test
	void 'Adding a device via discovery session'() {
		signalizeNewTest("Adding a device via discovery session")
		
		String DUMMY_DEVICE_ID = "testdev"+Calendar.getInstance().getTimeInMillis().toString();
		
		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		// backend sends start discovery session request
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest("abc","123");
		gatewayBackendConnection.sendMessageToGateway(request);

		// gw responds with discovery session reply
		assert isNextOutgoingMessage(StartDiscoverySessionReply, 0);
		
		// gw responds with discovery session started
		assert isNextOutgoingMessage(DiscoverySessionStarted, 1);
		
		// mock driver detects new device
		HashMap<String, String[]> parameters = new HashMap<String, String[]>();
		parameters.put(MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE_PARAM_DEVICEID, toStringArray(DUMMY_DEVICE_ID));

		assert addDeviceEndpoint.parametersFulfilled(parameters);
		addDeviceEndpoint.handle(parameters);
		
		assert isNextOutgoingMessage(ThingDiscovered, 2);
		
		// gw should send thing added message
		assert isNextOutgoingMessage(ThingAdded, 3);
		ThingAdded thingAdded = (ThingAdded)lastGatewayToBackendMessages.get(3);
		
		
		// discovery session should stop
		assert isNextOutgoingMessage(DiscoverySessionStopped, 4);

		assert isNextOutgoingMessage(ThingStatusChanged, 5);
	}
	
	@Test
	void 'Adding a device twice ends up in thing already added error'() {
		signalizeNewTest("Adding a device twice ends up in thing already added error")
		
		String DUMMY_DEVICE_ID = "testdev"+Calendar.getInstance().getTimeInMillis().toString();
		
		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		// Start discovery session
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest("abc","123");
		gatewayBackendConnection.sendMessageToGateway(request);

		assert isNextOutgoingMessage(StartDiscoverySessionReply, 0);
		assert isNextOutgoingMessage(DiscoverySessionStarted, 1);

		HashMap<String, String[]> parameters = new HashMap<String, String[]>();
		parameters.put(MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE_PARAM_DEVICEID, toStringArray(DUMMY_DEVICE_ID));

		addDeviceEndpoint.handle(parameters);
		
		assert isNextOutgoingMessage(ThingDiscovered, 2);
		assert isNextOutgoingMessage(ThingAdded, 3);
		assert isNextOutgoingMessage(DiscoverySessionStopped, 4);
		
		assert isNextOutgoingMessage(ThingStatusChanged, 5);
		
		// Start discovery session
		request = new StartDiscoverySessionRequest("def","456");
		gatewayBackendConnection.sendMessageToGateway(request);

		assert isNextOutgoingMessage(StartDiscoverySessionReply, 6);
		assert isNextOutgoingMessage(DiscoverySessionStarted, 7);

		parameters = new HashMap<String, String[]>();
		parameters.put(MockBindingConstants.TESTENDPOINT_DISCOVER_DEVICE_PARAM_DEVICEID, toStringArray(DUMMY_DEVICE_ID));

		addDeviceEndpoint.handle(parameters);
		
		assert isNextOutgoingMessage(ThingAlreadyAddedError, 8);
		
		// manually stop scan since we dont want to wait
		abortScanningEndpoint.handle(null);
		assert isNextOutgoingMessage(DiscoverySessionStopped, 9);
	}
	
	@Test
	void 'Discovery session gets stopped whenever scanning binding stops'() {
		signalizeNewTest("Discovery session gets aborted")

		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		// Start discovery session
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest("abc","123");
		gatewayBackendConnection.sendMessageToGateway(request);

		assert isNextOutgoingMessage(StartDiscoverySessionReply, 0);
		assert isNextOutgoingMessage(DiscoverySessionStarted, 1);

		// manually stop scan since we dont want to wait
		abortScanningEndpoint.handle(null);
		assert isNextOutgoingMessage(DiscoverySessionStopped, 2);
	}
	
	@Test
	void 'Discovery session gets stopped on StopDiscoverySessionRequest'() {
		signalizeNewTest("Discovery session gets stopped on StopDiscoverySessionRequest")

		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		// Start discovery session
		StartDiscoverySessionRequest request = new StartDiscoverySessionRequest("abc","123");
		gatewayBackendConnection.sendMessageToGateway(request);

		assert isNextOutgoingMessage(StartDiscoverySessionReply, 0);
		assert isNextOutgoingMessage(DiscoverySessionStarted, 1);
		
		StopDiscoverySessionRequest stopRequest = new StopDiscoverySessionRequest("xyz","123");
		gatewayBackendConnection.sendMessageToGateway(stopRequest);
		
		assert isNextOutgoingMessage(StopDiscoverySessionReply, 2);
		assert isNextOutgoingMessage(DiscoverySessionStopped, 3);
		
	}
	
	@Test
	void 'DiscoverySessionStopped on StopDiscoverySessionRequest even without running session'() {
		signalizeNewTest("DiscoverySessionStopped on StopDiscoverySessionRequest even without running session")

		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		
		waitForConnectionInitialization();
		
		StopDiscoverySessionRequest stopRequest = new StopDiscoverySessionRequest("xyz","123");
		gatewayBackendConnection.sendMessageToGateway(stopRequest);
		
		assert isNextOutgoingMessage(StopDiscoverySessionReply, 0);
		assert isNextOutgoingMessage(DiscoverySessionStopped, 1);
		
	}
	
	@After
	public void stop() {
		dummyBackend.stop();
	}
	
	protected class DefaultBackendConnectionListener implements ConnectionEventHandler {

		@Override
		public void onNewConnectionEstablished(ChannelHandlerContext context, Connection connection) {
			gatewayBackendConnection = connection
		}

		@Override
		public void onMessageReceived(ChannelHandlerContext context, Message message) {
			lastGatewayToBackendMessages.add(message);

			if (message instanceof HandshakeRequest) {
				gatewayBackendConnection.sendMessageToGateway(new HandshakeReply(((HandshakeRequest)message).getRequestId()));
			}
		}

		@Override
		public void onConnectionClosed(ChannelHandlerContext context, Connection connection) {
				if (gatewayBackendConnection == connection) {
					gatewayBackendConnection = null
				}
		}
	}
}
