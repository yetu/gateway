package com.yetu.gateway.home.integrationtest;

import io.netty.channel.ChannelHandlerContext

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.yetu.gateway.home.integrationtest.backend.DummyBackend
import com.yetu.gateway.home.integrationtest.backend.types.Connection
import com.yetu.gateway.home.integrationtest.backend.types.ConnectionEventHandler
import com.yetu.gatewayprotocol.kryo.GatewayMessage.DiscoverySessionStopped
import com.yetu.gatewayprotocol.kryo.GatewayMessage.GatewayConfigurationReply
import com.yetu.gatewayprotocol.kryo.GatewayMessage.HandshakeRequest
import com.yetu.gatewayprotocol.kryo.GatewayMessage.RemoveThingReply
import com.yetu.gatewayprotocol.kryo.GatewayMessage.SetPropertyError
import com.yetu.gatewayprotocol.kryo.GatewayMessage.StartDiscoverySessionReply
import com.yetu.gatewayprotocol.kryo.GatewayMessage.StopDiscoverySessionReply
import com.yetu.gatewayprotocol.kryo.ServerMessage.GatewayConfigurationRequest
import com.yetu.gatewayprotocol.kryo.ServerMessage.RemoveThingRequest
import com.yetu.gatewayprotocol.kryo.ServerMessage.SetProperty
import com.yetu.gatewayprotocol.kryo.ServerMessage.StartDiscoverySessionRequest
import com.yetu.gatewayprotocol.kryo.ServerMessage.StopDiscoverySessionRequest
import com.yetu.kryo_support.Message

/**
 * These tests are using a dummy backend connection to check whether messages are correctly passed
 * from/to the gateway manager. 
 * 
 * @author Sebastian Garn
 *
 */
public class BackendGatewayMessageFlowTest extends AbstractTest {
	/** Entries in smarthome-integrationtest cfg **/ 
	private String CFG_GATEWAY_ID = "dummy-gw-id";

	private int DUMMY_BACKEND_PORT = 55443;
	private DummyBackend dummyBackend;

	@Before
	public void setup() {
		dummyBackend = new DummyBackend();
		
		resetGatewayBackendConnection();
		resetLastGatewayToBackendMessages();
	}
	
	@Test
	void 'Gateway sends handshake request after connection setup'() {
		signalizeNewTest("Gateway sends handshake request after connection setup")

		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		
		assert gatewayBackendConnection == null
		assert lastGatewayToBackendMessages.size() == 0
		
		// start dummy backend - gateway should connect
		dummyBackend.start(DUMMY_BACKEND_PORT)

		waitFor({gatewayBackendConnection != null}, 5000);
		assert gatewayBackendConnection != null;
		
		assert isNextOutgoingMessage(HandshakeRequest, 0);

		HandshakeRequest handshakeRequest = (HandshakeRequest)lastGatewayToBackendMessages.get(0)
		assert handshakeRequest.gatewayId == CFG_GATEWAY_ID 
	}
	
	@Test
	void 'Gateway sends gateway configuration reply after configuration request'() {
		signalizeNewTest("Gateway sends gateway configuration reply after configuration request")

		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		waitForConnectionInitialization();

		// let backend send gateway conf request
		GatewayConfigurationRequest request = new GatewayConfigurationRequest();
		gatewayBackendConnection.sendMessageToGateway(request);
		
		// gateway should reply
		assert isNextOutgoingMessage(GatewayConfigurationReply, 0);
	}
	
	@Test
	void 'Gateway sends back StopDiscoverySessionReply and DiscoverySessionStopped on StopDiscoverySessionRequest'() {
		signalizeNewTest("Gateway sends back StopDiscoverySessionReply and DiscoverySessionStopped on StopDiscoverySessionRequest")
		
		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		waitForConnectionInitialization();
		
		// let backend stop discovery session
		StopDiscoverySessionRequest request = new StopDiscoverySessionRequest("dummyreq","dummysess");
		gatewayBackendConnection.sendMessageToGateway(request);
		
		// gateway immediately responds
		assert isNextOutgoingMessage(StopDiscoverySessionReply, 0);
		assert isNextOutgoingMessage(DiscoverySessionStopped, 1);
	}
	
	@Test
	void 'Gateway responds with SetPropertyError when backend sends SetProperty message for unknown thing'() {
		signalizeNewTest("Gateway responds with SetPropertyError when backend sends SetProperty message for unknown thing")
		
		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		waitForConnectionInitialization();
		
		// backend tries to set property for invalid thing
		SetProperty request = new SetProperty("","","","","");
		gatewayBackendConnection.sendMessageToGateway(request);
		
		// backend should respond with error
		assert isNextOutgoingMessage(SetPropertyError, 0);
	}

	@Test
	void 'Gateway sends RemoveThingReply and RemoveThingRequest'() {
		signalizeNewTest("Gateway sends RemoveThingReply ond RemoveThingRequest")

		dummyBackend.setConnectionEventHandler(new DefaultBackendConnectionListener())
		dummyBackend.start(DUMMY_BACKEND_PORT)
		waitForConnectionInitialization();
		
		// backend tries to set property for invalid thing
		RemoveThingRequest request = new RemoveThingRequest("dummysession","dummything");
		gatewayBackendConnection.sendMessageToGateway(request);
		
		// backend should respond with error
		assert isNextOutgoingMessage(RemoveThingReply, 0);
	}
	
	@After
	public void stop() {
		dummyBackend.stop();
	}
	
	protected class DefaultBackendConnectionListener implements ConnectionEventHandler {
		@Override
		public void onNewConnectionEstablished(ChannelHandlerContext context, Connection connection) {
			gatewayBackendConnection = connection
		}

		@Override
		public void onMessageReceived(ChannelHandlerContext context, Message message) {
			lastGatewayToBackendMessages.add(message);
		}

		@Override
		public void onConnectionClosed(ChannelHandlerContext context, Connection connection) {
				if (gatewayBackendConnection == connection) {
					gatewayBackendConnection = null
				}
		}
	}
}
